/* eslint-disable @typescript-eslint/no-unused-vars */

import { InMemoryCache } from '@apollo/client';

const cache: InMemoryCache = new InMemoryCache();

export default cache;
