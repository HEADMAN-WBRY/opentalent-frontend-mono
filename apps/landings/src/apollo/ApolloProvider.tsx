/* eslint-disable no-console */
import {
  ApolloClient,
  ApolloProvider as BaseApolloProvider,
} from '@apollo/client';
import React from 'react';
import { environment } from '../environments/environment';
import cache from './cache';

const useApolloClient = () => {
  return new ApolloClient({
    uri: environment.apiUrl,
    cache,
    connectToDevTools: !environment.production,
  });
};

const ApolloProvider = ({
  children,
}: React.PropsWithChildren<Record<string, unknown>>) => {
  const client = useApolloClient();
  return <BaseApolloProvider client={client}>{children}</BaseApolloProvider>;
};

export default ApolloProvider;
