import ReactDOM from 'react-dom';

import AppWrapper from '../AppWrapper';
import ApolloProvider from '../apollo/ApolloProvider';
import Landing from '../landings/abnamro';

ReactDOM.render(
  <AppWrapper>
    <ApolloProvider>
      <Landing />
    </ApolloProvider>
  </AppWrapper>,
  document.getElementById('root'),
);
