import ReactGA from 'react-ga';
import TagManager from 'react-gtm-module';

import { environment } from '../environments/environment';

export const setupGoogleStuff = () => {
  if (environment.production) {
    ReactGA.initialize('G-XRR2066RVS');
    TagManager.initialize({
      gtmId: 'GTM-WB8QVZJ',
      dataLayer: { 'gtm.start': new Date().getTime(), event: 'gtm.js' },
    });
  }
};
