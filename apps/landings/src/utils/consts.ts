const NX_MAIN_APP_URL = 'https://app.opentalent.co';
const FINAL_NX_MAIN_APP_URL =
  NX_MAIN_APP_URL && NX_MAIN_APP_URL.endsWith('/')
    ? NX_MAIN_APP_URL.slice(0, -1)
    : NX_MAIN_APP_URL;
const MAIN_APP_URL = FINAL_NX_MAIN_APP_URL || 'https://app.opentalent.co';

export const paths = {
  mainAppRoute: MAIN_APP_URL,
  companyLanding: '/hub',
  talentLanding: '/network',
  communitySearch: '/search',
  talentOnboarding: `${MAIN_APP_URL}/talent/onboarding`,
  companyOnboarding: `${MAIN_APP_URL}/company/onboarding-v2`,
  companyOldOnboarding: `${MAIN_APP_URL}/company/onboarding`,
  companyOldOnboardingForm: `${MAIN_APP_URL}/company/onboarding/form`,
};

export const LANDINGS_EXTERNAL_LINKS = {
  newLandingsTypeForm:
    'https://form.typeform.com/to/LajDL40m#hubspot_utk=xxxxx&hubspot_page_name=xxxxx&hubspot_page_url=xxxxx',
  statistics:
    'https://docs.google.com/spreadsheets/d/10LvhvKPRceMDNwC_6jhImU9ezmMqM93ZbyZ7jJ1QDXk/edit#gid=1057004131',
  sangibLink:
    'https://www.meeting.opentalent.co/meetings/sanjib-deka/onboarding',
};

export const INFINITY_SIGN = '∞';

export const UNBREAKABLE_WHITESPACE = String.fromCharCode(160);
