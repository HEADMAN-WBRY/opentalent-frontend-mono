import React from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';

import { configureYup } from '@libs/helpers/yup';

import ApolloProvider from './apollo/ApolloProvider';
import AbnamroLanding from './landings/abnamro';
import CommunitySearch from './landings/community-search';
import Main from './landings/main';
// import CompanyLanding from './landings/companies-landing';
import TalentCompanyLanding from './landings/talent-company-landing';
import TalentPools from './landings/talent-pools';
import TalentsLanding from './landings/talents-landing';
import { setupGoogleStuff } from './utils/google';

configureYup();
setupGoogleStuff();

interface AppProps {}

const App = (props: AppProps) => {
  return (
    <ApolloProvider>
      <BrowserRouter>
        <Switch>
          <Route exact path="/main" component={Main} />
          <Route exact path="/" component={CommunitySearch} />
          <Route exact path="/search" component={CommunitySearch} />
          <Route exact path="/network" component={TalentsLanding} />
          <Route exact path="/abnamro" component={AbnamroLanding} />
          <Route exact path="/hub" component={TalentPools} />
          <Route
            exact
            path="/landing/:companyName"
            component={TalentCompanyLanding}
          />

          <Redirect to="/" />
        </Switch>
      </BrowserRouter>
    </ApolloProvider>
  );
};

export default App;
