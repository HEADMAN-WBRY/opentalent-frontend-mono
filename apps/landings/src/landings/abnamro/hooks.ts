import { useMutation } from '@apollo/client';
import { FormikHelpers } from 'formik';
import { useSnackbar } from 'notistack';
import qs from 'querystring';
import { useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import * as yup from 'yup';

import {
  Mutation,
  MutationCompanyInviteFormSubmitArgs,
} from '@libs/graphql-types';

import { INVITE_FORM_SUBMIT } from './queries';
import { InviteFormModel } from './types';

export const useInviteFormSubmit = () => {
  const history = useHistory();
  const { enqueueSnackbar } = useSnackbar();
  const [submit, { loading }] = useMutation<
    Mutation,
    MutationCompanyInviteFormSubmitArgs
  >(INVITE_FORM_SUBMIT, {
    onCompleted: () => {
      enqueueSnackbar('Success!', { variant: 'success' });
      history.push({ search: qs.stringify({ success: true }) });
    },
    onError: (err) => {
      history.push({ search: qs.stringify({ error: err.message }) });
    },
  });
  const onSubmit = useCallback(
    async (
      variables: InviteFormModel,
      helpers: FormikHelpers<InviteFormModel>
    ) => {
      const { errors } = await submit({ variables });
      if (!errors) {
        helpers.resetForm();
      }
    },
    [submit]
  );

  return { onSubmit, loading };
};

export const useValidator = () => {
  return yup.object().shape({
    first_name: yup.string().required(),
    last_name: yup.string().required(),
    email: yup.string().email().required(),
    // .test(
    //   'email-exist-check',
    //   'A user account with this email already exists',
    //   async function test(email) {
    //     console.log('🚀 ~ file: hooks.ts ~ line 62 ~ test ~ email', email);
    //     console.time('start');
    //     const isValid = await yup.string().email().required().validate(email);
    //     console.timeEnd('start');

    //     if (isValid) {
    //       const res = await client.query({
    //         query: CHECK_TALENT_EMAIL,
    //         variables: { talent_email: email },
    //       });
    //       return !res.data.checkTalentExistsByEmail;
    //     }

    //     return this.schema.validate(email);
    //   },
    // ),
  });
};
