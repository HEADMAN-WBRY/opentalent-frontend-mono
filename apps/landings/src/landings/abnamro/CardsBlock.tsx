import cn from 'classnames';
import React from 'react';

import { Box, Container, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import { ReactComponent as CommissionIcon } from './assets/cards-commission.svg';
import { ReactComponent as FlagIcon } from './assets/cards-flag.svg';
import { ReactComponent as GlassIcon } from './assets/cards-glass.svg';
import { ReactComponent as RocketIcon } from './assets/cards-rocket.svg';
import { ReactComponent as TripIcon } from './assets/cards-trip.svg';
import { ReactComponent as TrophyIcon } from './assets/cards-trophy.svg';

interface CardsBlockProps {}

const DATA = [
  {
    Icon: FlagIcon,
    text: <>Find awesome freelance gigs at forward-looking companies.</>,
  },
  {
    Icon: RocketIcon,
    text: (
      <>
        Get to work on dream projects; <br /> 10,000&nbsp;miles away
      </>
    ),
  },
  {
    Icon: GlassIcon,
    text: <>Invite top talent from your personal network and get paid</>,
  },
  {
    Icon: CommissionIcon,
    text: <>Pay no commission: earn 100% of what you make</>,
  },
  {
    Icon: TripIcon,
    text: <>Work wherever and whenever&nbsp;you&nbsp;want.</>,
  },
  {
    Icon: TrophyIcon,
    text: <>Join a premium network: for&nbsp;talent by&nbsp;talent</>,
  },
];

const useStyles = makeStyles((theme) => ({
  root: {
    padding: '96px 0',

    [theme.breakpoints.down('md')]: {
      'body &': {
        padding: `${theme.spacing(14)} 0`,
      },
    },
  },

  coloredText: {
    background: theme.palette.primary.main,
    textTransform: 'uppercase',
    fontWeight: 600,
    fontSize: 32,
    fontStyle: 'italic',

    [theme.breakpoints.down('md')]: {
      'body &': {
        display: 'block',
      },
    },
  },

  card: {
    padding: 32,
    background: 'rgba(30, 30, 28, 0.04)',
    height: '100%',
  },
  cardWrapper: {
    [theme.breakpoints.up('md')]: {
      '&:nth-child(even) > div': {
        background: 'rgba(250, 250, 1, 0.1)',
      },
    },
    [theme.breakpoints.down('md')]: {
      '&:nth-child(2) > div': {
        background: 'rgba(250, 250, 1, 0.1)',
      },
      '&:nth-child(3) > div': {
        background: 'rgba(250, 250, 1, 0.1)',
      },
      '&:nth-child(6) > div': {
        background: 'rgba(250, 250, 1, 0.1)',
      },
    },
  },
  cardEven: {},
}));

const CardsBlock = (props: CardsBlockProps) => {
  const classes = useStyles();

  return (
    <Box textAlign="center" className={classes.root}>
      <Container>
        <Box>
          <Typography
            component="span"
            className={classes.coloredText}
            variant="h5"
          >
            Europe’s #1 talent-centric FreelanceR Network{' '}
          </Typography>
        </Box>
        <Box margin="0 auto" pt={8}>
          <Typography variant="body1">
            OpenTalent is Europe’s invite-only network for top freelance talent.
          </Typography>
          <Typography variant="body1">
            Get invited by companies like ABN AMRO, or by freelancers already
            part of the network.
          </Typography>
          <Typography variant="body1">
            Designed for you - independent talent - to thrive.
          </Typography>
        </Box>
        <Grid spacing={6} component={Box} container pt={12}>
          {DATA.map(({ text, Icon }, index) => (
            <Grid
              // eslint-disable-next-line react/no-array-index-key
              key={index}
              component={Box}
              md={4}
              xs={6}
              className={cn(classes.cardWrapper)}
              item
            >
              <Box className={cn(classes.card)}>
                <Icon />
                <Box pt={2}>
                  <Typography variant="body2">{text}</Typography>
                </Box>
              </Box>
            </Grid>
          ))}
        </Grid>
      </Container>
    </Box>
  );
};

export default CardsBlock;
