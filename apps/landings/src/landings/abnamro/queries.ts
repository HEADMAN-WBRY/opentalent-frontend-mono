import { gql } from '@apollo/client';

export const INVITE_FORM_SUBMIT = gql`
  mutation InviteFormSubmit(
    $first_name: String!
    $last_name: String!
    $email: Email!
  ) {
    companyInviteFormSubmit(
      first_name: $first_name
      last_name: $last_name
      email: $email
    )
  }
`;
