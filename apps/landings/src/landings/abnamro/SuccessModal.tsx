import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Box,
} from '@mui/material';
import { makeStyles } from '@mui/styles';
import cn from 'classnames';
import qs from 'querystring';
import React from 'react';
import { useHistory } from 'react-router-dom';

import Button from '@libs/ui/components/button';

import { ReactComponent as MailIcon } from '../../assets/mail.svg';

const useStyles = makeStyles((theme) => ({
  modal: {
    padding: theme.spacing(10),
    textAlign: 'center',
  },
  title: {
    padding: `${theme.spacing(4)} 0 0 0`,
  },
  actions: {
    paddingTop: theme.spacing(6),
  },
  noHorizontalPadding: {
    paddingLeft: 0,
    paddingRight: 0,
    paddingTop: theme.spacing(6),
    overflowY: 'visible',
  },
}));

const SuccessModal = () => {
  const classes = useStyles();
  const history = useHistory();
  const { success } = qs.parse(history.location.search.slice(1));
  const handleClose = () => history.push({ search: '' });

  return (
    <Dialog
      classes={{ paper: classes.modal }}
      open={!!success}
      onClose={handleClose}
    >
      <Box textAlign="center">
        <MailIcon />
      </Box>

      <DialogTitle className={classes.title}>Almost done!</DialogTitle>
      <DialogContent className={classes.noHorizontalPadding}>
        We just sent you an email. Check your inbox to accept the invite.
      </DialogContent>
      <DialogActions
        className={cn(classes.actions, classes.noHorizontalPadding)}
      >
        <Button
          fullWidth
          variant="contained"
          color="primary"
          autoFocus
          size="large"
          onClick={handleClose}
        >
          ok
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default SuccessModal;
