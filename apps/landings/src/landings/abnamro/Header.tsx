import React from 'react';

import { AppBar, Grid, Box, Toolbar } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { ReactComponent as OpentalentLogo } from '../../assets/logo.svg';
import amroLogo from './assets/amro@2x.png';
import { ReactComponent as CrossIcon } from './assets/cross.svg';

const useStyles = makeStyles((theme) => ({
  root: {
    height: 84,
    color: theme.palette.primary.main,
  },
  cross: {
    padding: `0 ${theme.spacing(10)}`,
    [theme.breakpoints.down('sm')]: {
      padding: `0 ${theme.spacing(4)}`,
    },
  },
}));

const Header = () => {
  const classes = useStyles();

  return (
    <AppBar color="inherit">
      <Toolbar classes={{ root: classes.root }}>
        <Grid
          wrap="nowrap"
          alignItems="center"
          justifyContent="center"
          container
        >
          <Grid item>
            <img srcSet={`${amroLogo} 2x`} alt="ambo" />
          </Grid>
          <Grid className={classes.cross} component={Box} item>
            <CrossIcon />
          </Grid>
          <Grid item>
            <OpentalentLogo />
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
