import { makeStyles } from '@mui/styles';
import React from 'react';
import { RouteComponentProps, Redirect } from 'react-router-dom';
import { useTitle } from 'react-use';

import Footer from '../../shared/common-footer';
import { paths } from '../../utils/consts';
import { COMPANIES_LIST } from './companiesList';
import Main from './main';

const useStyles = makeStyles(() => ({
  wrapper: {
    overflow: 'hidden',
  },
  '@global': {
    body: {
      width: '100%',
    },
  },
}));

const TalentCompanyLanding: React.FC<
  RouteComponentProps<{ companyName: string }>
> = ({ match }) => {
  const classes = useStyles();
  const { companyName: companyUrl } = match.params;

  const company = COMPANIES_LIST.find((item) => item.pageUrl === companyUrl);

  useTitle(company?.name || window.document.title);

  if (!company) {
    return <Redirect to={paths.talentLanding} />;
  }

  return (
    <div className={classes.wrapper}>
      <Main company={company} />
      <Footer />
    </div>
  );
};

export default TalentCompanyLanding;
