import ABNARMO from './assets/logoABNAMRO.png';

export interface ILandingCompany {
  companyId: string;
  name: string;
  logoPath: string;
  pageUrl: string;
}

export const COMPANIES_LIST: ILandingCompany[] = [
  {
    companyId: '14',
    name: 'ABN AMRO',
    logoPath: ABNARMO,
    pageUrl: 'abn-amro',
  },
];
