import { Box, Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Form, Formik } from 'formik';
import React from 'react';
import * as yup from 'yup';

import Button from '@libs/ui/components/button';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import Typography from '@libs/ui/components/typography';

import { useSubmitAction } from '../../talents-landing/intro/hooks';
import BG from '../assets/bg.svg';
import { ILandingCompany } from '../companiesList';

const useStyles = makeStyles<Theme, { logoPath: string }>((theme) => ({
  wrapper: {
    backgroundColor: theme.palette.secondary.dark,
    color: theme.palette.secondary.contrastText,
    background: `url(${BG}) no-repeat bottom center fixed`,
    height: '100%',
    overflow: 'hidden',
    textAlign: 'center',
    padding: '0 16px',
  },
  logo: {
    background: (props) => `url(${props.logoPath}) no-repeat`,
    backgroundSize: 'cover',
    width: '100px',
    height: '100px',
    overflow: 'hidden',
    margin: '100px auto 40px auto',
  },
  joinText: {
    marginBottom: theme.spacing(7),
    fontStyle: 'italic',

    [theme.breakpoints.down('sm')]: {
      fontSize: 28,
      lineHeight: '32px',
    },
  },
  description: {
    fontSize: '20px',
    lineHeight: '32px',
    marginBottom: theme.spacing(12),

    [theme.breakpoints.down('sm')]: {
      fontSize: 14,
      lineHeight: '24px',
      marginBottom: theme.spacing(6),
    },
  },
  inputWrapper: {
    display: 'flex',
    maxWidth: '678px',
    margin: '0 auto 346px auto',

    [theme.breakpoints.down('sm')]: {
      marginBottom: '120px',
      flexDirection: 'column',
    },
  },
  input: {
    marginBottom: '24px',
    '& input': {
      height: '48px',
      boxSizing: 'border-box',
    },
    '& .MuiOutlinedInput-root': {
      borderRadius: '8px',
      borderTopRightRadius: 0,
      borderBottomRightRadius: 0,

      [theme.breakpoints.down('sm')]: {
        borderTopRightRadius: '8px',
        borderBottomRightRadius: '8px',
      },
    },
  },
  button: {
    maxWidth: '230px',
    height: '48px',
    borderTopRightRadius: '8px',
    borderBottomRightRadius: '8px',

    [theme.breakpoints.down('sm')]: {
      borderTopRightRadius: 0,
      borderBottomRightRadius: 0,
      maxWidth: 'unset',
    },
  },
}));

const validator = yup.object({
  email: yup.string().trim().required('Please add your email').email(),
});

interface IProps {
  company: ILandingCompany;
}

const Main: React.FC<IProps> = ({ company }) => {
  const classes = useStyles({ logoPath: company.logoPath });
  const { onSubmit, loading } = useSubmitAction(company.companyId);

  return (
    <Box className={classes.wrapper}>
      <Formik
        onSubmit={onSubmit}
        validationSchema={validator}
        initialValues={{ email: '' }}
      >
        <Form>
          <Box className={classes.logo} />
          <Typography variant="h2" className={classes.joinText}>
            Join <b>{company.name}</b> on OpenTalent
          </Typography>
          <Typography variant="subtitle1" className={classes.description}>
            Europe’s most talent-centric freelancer network, where everyone
            comes recommended{' '}
            <span aria-label="" role="img">
              👍
            </span>
          </Typography>
          <Box className={classes.inputWrapper}>
            <ConnectedTextField
              placeholder="Enter your email"
              name="email"
              fullWidth
              size="small"
              variant="outlined"
              className={classes.input}
            />
            <Button
              variant="contained"
              color="primary"
              fullWidth
              type="submit"
              size="large"
              disabled={loading}
              className={classes.button}
            >
              Request Access
            </Button>
          </Box>
        </Form>
      </Formik>
    </Box>
  );
};

export default Main;
