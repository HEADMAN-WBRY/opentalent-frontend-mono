import React from 'react';

import { Container } from '@mui/material';
import { makeStyles } from '@mui/styles';

import BottomBlock from '../../../shared/common-footer/BottomBlock';

interface FooterProps {}

const useStyles = makeStyles((theme) => ({
  botSection: {
    padding: `${theme.spacing(6)} 0`,
    overflow: 'hidden',
    background: theme.palette.secondary.dark,
    color: 'white',
  },
}));

const Footer = (props: FooterProps) => {
  const classes = useStyles();

  return (
    <div className={classes.botSection}>
      <Container>
        <BottomBlock />
      </Container>
    </div>
  );
};

export default Footer;
