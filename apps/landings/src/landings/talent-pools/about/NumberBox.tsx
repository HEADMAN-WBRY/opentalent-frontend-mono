import React from 'react';

import { makeStyles } from '@mui/styles';

type NumberBoxProps = React.PropsWithChildren<unknown>;

const useStyles = makeStyles((theme) => ({
  container: {
    background: 'white',
    fontSize: 48,
    lineHeight: '46px',
    fontWeight: 600,
    padding: '14px 8px',
    borderRadius: 10,
    display: 'inline-block',
    margin: '0 auto 6px',
  },
}));

const NumberBox = ({ children }: NumberBoxProps) => {
  const classes = useStyles();
  return <div className={classes.container}>{children}</div>;
};

export default NumberBox;
