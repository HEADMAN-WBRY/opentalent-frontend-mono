export const ACTIVE_ITEMS = [
  { title: 'Invite-only access', subtitle: 'To join you need an invite.' },
  { title: 'EU VAT # check', subtitle: 'Automated in-platform check' },
  { title: 'Country locator', subtitle: 'In-platform + team check' },
  { title: 'LinkedIn review', subtitle: 'In-platform + team check' },
];

export const DISABLED_ITEMS = [
  { title: 'Skills verification', subtitle: 'Coming soon' },
  { title: 'ID checks', subtitle: 'Coming soon' },
  { title: 'Employer verification', subtitle: 'Coming soon' },
  { title: 'Insurance coverage', subtitle: 'Coming soon' },
];
