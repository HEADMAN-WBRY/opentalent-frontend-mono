import cn from 'classnames';
import React from 'react';

import { Card, CardContent } from '@mui/material';
import { green } from '@mui/material/colors';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

interface ToggleItemProps {
  title: string;
  subtitle: string;
  active?: boolean;
  onClick: VoidFunction;
}

const useStyles = makeStyles((theme) => ({
  root: {
    border: 'none',
    cursor: 'pointer',
    background: 'transparent',
    transition: 'all 0.5s ease-in-out',

    '&:hover': {
      background: green[50],
      boxShadow: '0px 6px 16px rgba(0, 0, 0, 0.12)',
    },
  },
  active: {
    background: green[50],
    boxShadow: '0px 6px 16px rgba(0, 0, 0, 0.12)',
  },
}));

const ToggleItem = ({
  title,
  subtitle,
  active = false,
  onClick,
}: ToggleItemProps) => {
  const classes = useStyles();

  return (
    <Card
      onClick={onClick}
      className={cn(classes.root, { [classes.active]: active })}
      variant="outlined"
    >
      <CardContent>
        <Typography variant="h6">{title}</Typography>
        <Typography variant="body1" color="text.secondary">
          {subtitle}
        </Typography>
      </CardContent>
      <span />
    </Card>
  );
};

export default ToggleItem;
