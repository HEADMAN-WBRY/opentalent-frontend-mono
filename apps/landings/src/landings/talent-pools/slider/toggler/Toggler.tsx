import React from 'react';

import { Box } from '@mui/material';

import ToggleItem from './ToggleItem';

interface TogglerProps {
  activeIndex: number;
  items: { title: string; subtitle: string }[];
  onChange: (value: number) => void;
}

const Toggler = ({ activeIndex, items, onChange }: TogglerProps) => {
  return (
    <Box maxWidth={400}>
      {items.map((i, index) => (
        <ToggleItem
          {...i}
          onClick={() => onChange(index)}
          key={i.title}
          active={activeIndex === index}
        />
      ))}
    </Box>
  );
};

export default Toggler;
