import slide1 from '../assets/slides/slide-1@2x.png';
import slide2 from '../assets/slides/slide-2@2x.png';
import slide3 from '../assets/slides/slide-3@2x.png';
import slide4 from '../assets/slides/slide-4@2x.png';
import slide5 from '../assets/slides/slide-5@2x.png';
import slide6 from '../assets/slides/slide-6@2x.png';

export const ITEMS = [
  {
    img: slide1,
    title: 'Secure verification flows',
    subtitle: 'Build a hub of trusted flexible talent',
  },
  {
    img: slide2,
    title: 'Skilled-based matching',
    subtitle: 'Find candidates with the right skills',
  },
  {
    img: slide3,
    title: 'Smart Application Tracking',
    subtitle: 'Easily track all candidates and jobs',
  },
  {
    img: slide4,
    title: 'Direct communication',
    subtitle: 'Reach out to your pool, hassle-free',
  },
  {
    img: slide5,
    title: 'Referral-based growth',
    subtitle: 'Issue Finder Fees to attract new talent',
  },
  {
    img: slide6,
    title: 'Customised reporting',
    subtitle: 'Download relevant data on your hub',
  },
];
