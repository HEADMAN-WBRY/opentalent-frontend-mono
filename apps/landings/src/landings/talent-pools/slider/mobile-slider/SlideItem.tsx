import cn from 'classnames';
import React from 'react';

import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

interface SlideItemProps {
  title: string;
  subtitle: string;
  img: string;
  className?: string;
}

const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: theme.spacing(6),
    width: '100%',
  },

  imageWrap: {
    height: 340,
    textAlign: 'center',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

    '& img': {
      maxWidth: '100%',
      maxHeight: '100%',
    },
  },

  title: {
    textAlign: 'center',
    fontWeight: 500,
    marginBottom: theme.spacing(4),

    '& > span': {
      display: 'inline-block',
      borderBottom: `1px solid ${theme.palette.secondary.main}`,
    },
  },
  subtitle: {
    textAlign: 'center',
    marginBottom: theme.spacing(4),

    [theme.breakpoints.down('md')]: {
      whiteSpace: 'inherit',
    },
  },
}));

const SlideItem = ({ title, subtitle, img, className }: SlideItemProps) => {
  const classes = useStyles();

  return (
    <Box className={cn(className, classes.root)}>
      <Typography className={classes.title} variant="h6" fontWeight={500}>
        <span>{title}</span>
      </Typography>
      <Typography className={classes.subtitle}>{subtitle}</Typography>

      <Box className={classes.imageWrap}>
        <img srcSet={`${img} 2x`} alt="slider" />
      </Box>
    </Box>
  );
};

export default SlideItem;
