import React from 'react';

import { Box, Grid, Grow } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Toggler from '../toggler';

interface DesktopSliderProps {
  activeIndex: number;
  items: any[];
  setActiveIndex: (index: number) => void;
}

const useStyles = makeStyles((theme) => ({
  imageBlock: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexGrow: 1,
  },

  image: {},
}));

const DesktopSlider = ({
  activeIndex,
  setActiveIndex,
  items,
}: DesktopSliderProps) => {
  const classes = useStyles();

  return (
    <Box pt={14}>
      <Grid wrap="nowrap" spacing={8} container>
        <Grid item>
          <Toggler
            items={items}
            activeIndex={activeIndex}
            onChange={(index: number) => setActiveIndex(index)}
          />
        </Grid>
        <Grid key={activeIndex} className={classes.imageBlock} item>
          <Grow timeout={1000} appear in>
            <div>
              <img
                className={classes.image}
                srcSet={`${items[activeIndex]?.img} 2x`}
                alt="Illustration"
              />
            </div>
          </Grow>
        </Grid>
      </Grid>
    </Box>
  );
};

export default DesktopSlider;
