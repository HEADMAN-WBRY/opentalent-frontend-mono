import React from 'react';

import { Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import abn from '../../../assets/intro-logos/abn.png';
import code from '../../../assets/intro-logos/code_azure.svg';
import hc from '../../../assets/intro-logos/hc_health.svg';
import pc from '../../../assets/intro-logos/productpine.svg';
import eu from '../../../assets/logo-line/european-union.svg';

interface LogosLineProps {}

const ICONS = [abn, code, hc, pc];

const useStyles = makeStyles((theme) => ({
  wrapper: {
    [theme.breakpoints.down('md')]: {
      justifyContent: 'space-between',
    },
    '& > div': {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
  },
  text: {
    maxWidth: 260,
    marginRight: '5%',
    display: 'flex',
    alignItems: 'center',

    [theme.breakpoints.down('md')]: {
      margin: '0 auto',
      paddingBottom: theme.spacing(6),
    },

    '& > img': {
      marginRight: theme.spacing(3),
    },
  },
  logos: {
    flexGrow: 1,

    '& > div': {
      display: 'flex',
      alignItems: 'center',
    },
  },
  logo: {
    '&:not(:last-child)': {
      marginRight: '7%',
    },

    [theme.breakpoints.down('md')]: {
      flexGrow: 1,
    },

    '& img': {
      maxWidth: '100%',
    },
  },
}));

const LogosLine = (props: LogosLineProps) => {
  const classes = useStyles();

  return (
    <Grid className={classes.wrapper} container>
      <Grid className={classes.text} item>
        <img src={eu} alt="European Union" />

        <Typography color="secondary.contrastText" variant="caption">
          {`Trusted by leading hiring
teams across Europe`}
        </Typography>
      </Grid>
      <Grid item className={classes.logos}>
        <Grid wrap="nowrap" container>
          {ICONS.map((icon) => (
            <Grid className={classes.logo} key={icon} item>
              <img src={icon} alt="Company" />
            </Grid>
          ))}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default LogosLine;
