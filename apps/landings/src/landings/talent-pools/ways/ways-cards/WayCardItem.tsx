import React from 'react';

import { Box, Card, CardContent, Step, StepLabel } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

interface WayCardItemProps extends React.PropsWithChildren<unknown> {
  index: number;
  title: string;
}

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 400,
    height: 386,
    textAlign: 'center',
    border: '1px solid #000000',
    borderRadius: 8,

    [theme.breakpoints.down('md')]: {
      maxWidth: '100%',
      height: 'auto',
    },
  },
  content: {
    flexDirection: 'column',
    padding: theme.spacing(10, 6),
    display: 'flex',
    height: '100%',
    width: '100%',

    [theme.breakpoints.down('md')]: {
      padding: theme.spacing(4, 4),
    },
  },
  title: {
    height: 64,
    marginBottom: theme.spacing(6),

    [theme.breakpoints.down('md')]: {
      marginBottom: theme.spacing(2),
      height: 'auto',
    },
  },
  step: {
    '& .MuiStepLabel-iconContainer': {
      padding: 0,
    },

    '& svg': {
      width: 32,
      height: 32,
      color: theme.palette.secondary.main,
    },

    '& text': {
      fill: theme.palette.primary.main,
    },
  },
}));

const WayCardItem = ({ index, children, title }: WayCardItemProps) => {
  const classes = useStyles();

  return (
    <Card className={classes.root} variant="outlined">
      <CardContent className={classes.content}>
        <Box display="flex" justifyContent="center" pb={4}>
          <Step
            index={index}
            completed={false}
            className={classes.step}
            color="secondary.main"
            expanded
          >
            <StepLabel />
          </Step>
        </Box>

        <Typography
          textTransform="uppercase"
          fontWeight={600}
          fontStyle="italic"
          variant="h6"
          whiteSpace="break-spaces"
          className={classes.title}
        >
          {title.toUpperCase()}
        </Typography>

        {children}
      </CardContent>
      <span />
    </Card>
  );
};

export default WayCardItem;
