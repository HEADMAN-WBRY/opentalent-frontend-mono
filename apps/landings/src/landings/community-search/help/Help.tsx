import React from 'react';

import { Box } from '@mui/material';

import Section from '../../../shared/new-landings/section';
import SectionTitle from '../../../shared/new-landings/section-title';
import WaysCards from './ways-cards';

interface HelpProps {
  countriesCount?: number;
}

const Help = ({ countriesCount }: HelpProps) => {
  return (
    <Section color="white">
      <SectionTitle>ready to help companies with</SectionTitle>

      <Box pt={10}>
        <WaysCards countriesCount={countriesCount} />
      </Box>
    </Section>
  );
};

export default Help;
