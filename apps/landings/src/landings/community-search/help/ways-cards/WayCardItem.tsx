import React from 'react';

import { Card, CardContent } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

interface WayCardItemProps extends React.PropsWithChildren<unknown> {
  index: number;
  title: string;
}

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 400,
    height: 330,
    textAlign: 'center',
    borderColor: theme.palette.secondary.main,
    transition: 'all 0.3s ease-in-out',

    '&:hover': {
      background: theme.palette.secondary.main,
      color: 'white',

      '& .MuiButton-root': {
        transition: 'all 0.3s ease-in-out',
        background: theme.palette.green.main,
        color: theme.palette.secondary.main,

        '&:hover': {
          background: theme.palette.green.dark,
        },
      },
    },

    [theme.breakpoints.down('md')]: {
      maxWidth: '100%',
      height: 'auto',
    },
  },
  content: {
    flexDirection: 'column',
    padding: theme.spacing(10, 5),
    display: 'flex',
    height: '100%',
    width: '100%',

    [theme.breakpoints.down('md')]: {
      padding: theme.spacing(4, 4),
    },
  },
  title: {
    [theme.breakpoints.down('md')]: {
      marginBottom: theme.spacing(0),
      height: 'auto',
      fontSize: 18,
      lineHeight: '24px',
    },
  },
  step: {
    '& .MuiStepLabel-iconContainer': {
      padding: 0,
    },

    '& svg': {
      width: 32,
      height: 32,
      color: theme.palette.secondary.main,
    },

    '& text': {
      fill: theme.palette.primary.main,
    },
  },
}));

const WayCardItem = ({ index, children, title }: WayCardItemProps) => {
  const classes = useStyles();

  return (
    <Card className={classes.root} variant="outlined">
      <CardContent className={classes.content}>
        <Typography
          textTransform="uppercase"
          fontWeight={600}
          fontStyle="italic"
          variant="h5"
          whiteSpace="break-spaces"
          className={classes.title}
        >
          {title.toUpperCase()}
        </Typography>

        {children}
      </CardContent>
      <span />
    </Card>
  );
};

export default WayCardItem;
