import React from 'react';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { formatNumber } from '@libs/helpers/format';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import { INFINITY_SIGN, paths } from '../../../../utils/consts';
import WayCardItem from './WayCardItem';

interface WaysCardsProps {
  countriesCount?: number;
}

const getData = (countriesCount?: number | string) => [
  {
    title: 'FREELANCER',
    subtitle: 'sourcing',
    text: `Let our community fulfil your immediate ‘high-skilled’ freelancer needs.`,
  },
  {
    title: `Permanent`,
    text: `Let our community find your next ‘high-skilled’ permanent hire.`,
    subtitle: 'recruitment',
  },
];

const useStyles = makeStyles((theme) => ({
  root: {
    justifyContent: 'center',

    [theme.breakpoints.down('md')]: {
      flexDirection: 'column',
    },
  },
  btn: {
    width: 230,
    fontWeight: 'bold',

    [theme.breakpoints.down('md')]: {
      width: '100%',
    },
  },
  title: {
    marginBottom: theme.spacing(8),

    [theme.breakpoints.down('md')]: {
      marginBottom: theme.spacing(4),
      fontSize: 18,
    },
  },
}));

const WaysCards = ({ countriesCount }: WaysCardsProps) => {
  const classes = useStyles();
  const countries = countriesCount
    ? formatNumber(countriesCount)
    : INFINITY_SIGN;

  return (
    <Grid className={classes.root} spacing={4} wrap="nowrap" container>
      {getData(countries).map((i, index) => (
        <Grid key={i.title} item>
          <WayCardItem index={index} title={i.title}>
            <Box display="flex" flexDirection="column" flexGrow={1}>
              <Typography
                fontStyle="italic"
                textTransform="uppercase"
                variant="h5"
                paragraph
                className={classes.title}
              >
                {i.subtitle.toUpperCase()}
              </Typography>

              <Typography flexGrow={1} paragraph variant="body1">
                {i.text}
              </Typography>

              <Box>
                <Button
                  className={classes.btn}
                  variant="contained"
                  color="secondary"
                  size="large"
                  href={paths.companyOnboarding}
                  fullWidth
                >
                  Post a Job
                </Button>
              </Box>
            </Box>
          </WayCardItem>
        </Grid>
      ))}
    </Grid>
  );
};

export default WaysCards;
