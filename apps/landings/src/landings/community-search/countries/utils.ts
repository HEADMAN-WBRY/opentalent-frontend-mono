import { TalentsCountItem } from '@libs/graphql-types';

const CATEGORIES_ORDER = [
  'it_digital',
  'marketin',
  'legal',
  'product_design',
  'project_management',
  'commerce_sales',
  'human_resources',
  'finance',
  'content',
  'admin_customer_support',
];

export const mapCategoriesToFront = (categories: TalentsCountItem[]) => {
  const map = Object.fromEntries(categories.map((i) => [i.slug, i]));
  return CATEGORIES_ORDER.map((i) => map[i]).filter(Boolean);
};
