import { green } from '@mui/material/colors';

import Typography from '@libs/ui/components/typography';

import aleksandar from '../../assets/matchers/aleksandar@2x.png';
import amrin from '../../assets/matchers/amrin@2x.png';
import brian from '../../assets/matchers/brian@2x.png';
import jane from '../../assets/matchers/jane@2x.png';
import nicoleta from '../../assets/matchers/nicoleta@2x.png';

export const CARDS_DATA = [
  {
    name: 'Nicoleta',
    specialization: (
      <Typography>
        +10 years.{' '}
        <span style={{ color: green[300] }}>
          in <b>Tech</b>
        </span>
      </Typography>
    ),
    country: 'Romania',
    image: nicoleta,
  },
  {
    name: 'Aleksandar',
    specialization: (
      <Typography>
        +8 years.{' '}
        <span style={{ color: green[300] }}>
          in <b>Marketing</b>
        </span>
      </Typography>
    ),
    country: 'Spain',
    image: aleksandar,
  },
  {
    name: 'Brian',
    specialization: (
      <Typography>
        +7 years.{' '}
        <span style={{ color: green[300] }}>
          in <b>Crypto</b>
        </span>
      </Typography>
    ),
    country: 'Czech Republic',
    image: brian,
  },
  {
    name: 'Amrin',
    specialization: (
      <Typography>
        +8 years.{' '}
        <span style={{ color: green[300] }}>
          in <b>Data</b>
        </span>
      </Typography>
    ),
    country: 'Netherlands',
    image: amrin,
  },
  {
    name: 'Jane',
    specialization: (
      <Typography>
        +7 years.{' '}
        <span style={{ color: green[300] }}>
          in <b>Finance</b>
        </span>
      </Typography>
    ),
    country: 'Germany',
    image: jane,
  },
];
