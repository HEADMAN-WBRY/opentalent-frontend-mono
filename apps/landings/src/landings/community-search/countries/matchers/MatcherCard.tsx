import React from 'react';

import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

interface MatcherCardProps {
  image: string;
  name: string;
  specialization: React.ReactNode;
  country: string;
}

const useStyles = makeStyles((theme) => ({
  root: {
    color: 'white',
    textAlign: 'center',
  },
  country: {
    opacity: 0.8,
  },
}));

const MatcherCard = ({
  image,
  name,
  specialization,
  country,
}: MatcherCardProps) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Box>
        <img srcSet={`${image} 2x`} alt={name} />
      </Box>
      <Typography variant="h6">{name}</Typography>
      <Typography variant="subtitle1">{specialization}</Typography>
      <Typography className={classes.country} variant="subtitle1">
        {country}
      </Typography>
    </Box>
  );
};

export default MatcherCard;
