export const COUNTRIES = Array.from({ length: 60 }, () => [
  'Austria',
  'Belgium',
  'Bulgaria',
  'Croatia',
  'Denmark',
  'Estonia',
  'Finland',
  'France',
  'Germany',
  'Greece',
  'Hungary',
  'Ireland',
  'Italy',
  'Lithuania',
  'Malta',
  'Netherlands',
  'Poland',
  'Portugal',
  'Romania',
  'Slovakia',
  'Spain',
  'Sweden',
  'Ukraine',
  'United Kingdom',
]).flat();
