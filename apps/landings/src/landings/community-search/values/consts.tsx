import { ReactComponent as ListIcon } from '../../../assets/values/list.svg';
import { ReactComponent as MacIcon } from '../../../assets/values/mac.svg';
import { ReactComponent as PayIcon } from '../../../assets/values/pay.svg';
import { ReactComponent as RefIcon } from '../../../assets/values/ref.svg';
import { ReactComponent as SearchIcon } from '../../../assets/values/search.svg';
import { ReactComponent as SecureIcon } from '../../../assets/values/secure.svg';

export const getValues = () => [
  {
    title: 'Faster results',
    text: `Find top talent faster,
usually within days.`,
    Icon: ListIcon,
  },
  {
    title: 'Competitive rates',
    text: `Hire at fixed competitive
rates, as low as 12%.`,
    Icon: PayIcon,
  },
  {
    title: 'Expert sourcing',
    text: `Get experts in their field to
find your next best hire.`,
    Icon: RefIcon,
  },
  {
    title: 'Technology driven',
    text: `One streamlined process
through OpenTalent’s platform.`,
    Icon: MacIcon,
  },
  {
    title: 'Local sourcing',
    text: `Get recruiting power in
different 29 countries.`,
    Icon: SearchIcon,
  },
  {
    title: 'Secure & safe hiring',
    text: `Let us take care of contracting
& pay-rolling across Europe.`,
    Icon: SecureIcon,
  },
];
