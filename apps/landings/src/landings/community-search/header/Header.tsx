import React, { useState } from 'react';

import MenuIcon from '@mui/icons-material/Menu';
import {
  AppBar,
  Box,
  Button,
  Container,
  Grid,
  Hidden,
  IconButton,
  Toolbar,
} from '@mui/material';
import { makeStyles } from '@mui/styles';

import { EXTERNAL_LINKS } from '@libs/helpers/consts';
import { useMediaQueries } from '@libs/helpers/hooks/media-queries';

import { ReactComponent as LogoIcon } from '../../../assets/logo.svg';
import NewLandingsPageDrawer from '../../../shared/NewLandingsPageDrawer';
import SolutionsMenu from '../../../shared/SolutionsMenu';
import { paths } from '../../../utils/consts';

interface HeaderProps {}

const useStyles = makeStyles((theme) => ({
  logoWrapper: {
    display: 'flex',
    alignItems: 'center',
    color: theme.palette.green.main,
  },
  drawer: {
    '& svg': {
      color: theme.palette.green.main,
    },
  },
  appBar: {
    '& .MuiButton-root': {
      color: theme.palette.secondary.main,
    },
  },
}));

const Header: React.FC<HeaderProps> = (props: HeaderProps) => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen((s) => !s);
  const { isXS } = useMediaQueries();
  const classes = useStyles();

  return (
    <>
      <NewLandingsPageDrawer
        className={classes.drawer}
        toggle={toggle}
        isOpen={isOpen}
      />
      <AppBar className={classes.appBar} color="inherit" position="static">
        <Box>
          <Container>
            <Toolbar disableGutters>
              <Grid
                justifyContent="space-between"
                alignItems="center"
                container
              >
                <Grid item>
                  <Grid container>
                    <Grid display="flex" style={{ alignItems: 'center' }} item>
                      <Hidden mdUp>
                        <IconButton size="small" onClick={toggle}>
                          <MenuIcon />
                        </IconButton>
                      </Hidden>
                    </Grid>
                    <Grid
                      className={classes.logoWrapper}
                      item
                      component={Box}
                      mb={-2}
                    >
                      <a href="/">
                        <LogoIcon height={isXS ? 35 : 54} />
                      </a>
                    </Grid>
                  </Grid>
                </Grid>
                <Hidden mdDown>
                  <Grid item>
                    <Grid spacing={4} container>
                      <Grid item>
                        <Button color="inherit" href="/network">
                          JOIN OUR TALENT COMMUNITY
                        </Button>
                      </Grid>
                      <Grid item>
                        <SolutionsMenu />
                      </Grid>
                      <Grid item>
                        <Button color="inherit" href={EXTERNAL_LINKS.pricing}>
                          PRICING
                        </Button>
                      </Grid>
                      <Grid item>
                        <Button
                          href={paths.mainAppRoute}
                          variant="outlined"
                          color="inherit"
                        >
                          Sign in
                        </Button>
                      </Grid>
                    </Grid>
                  </Grid>
                </Hidden>
              </Grid>
            </Toolbar>
          </Container>
        </Box>
      </AppBar>
    </>
  );
};

export default Header;
