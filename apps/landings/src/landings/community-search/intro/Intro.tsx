/* eslint-disable jsx-a11y/accessible-emoji */
import React from 'react';

import { Box, Button, Container, Grid, Hidden } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { CommonAppInfo } from '@libs/graphql-types';
import { formatNumberSafe } from '@libs/helpers/format';
import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import Typography from '@libs/ui/components/typography';

import { INFINITY_SIGN, paths } from '../../../utils/consts';
import LogosLine from './LogosLine';
import image from './assets/illustration@2x.png';

interface IntroProps {
  appInfo?: Partial<CommonAppInfo>;
}

const useStyles = makeStyles((theme) => ({
  wrapper: {},
  smallTitle: {
    fontSize: 10,
    textTransform: 'uppercase',
    marginBottom: theme.spacing(4),
    color: theme.palette.text.secondary,
    fontWeight: 'bolder',
  },
  underText: {
    marginBottom: theme.spacing(8),
  },
  title: {
    fontSize: 140,
    lineHeight: '110px',
    fontFamily: "'MissRhinetta', sans-serif",
    fontDisplay: 'swap',
    letterSpacing: -1.5,

    [theme.breakpoints.down('md')]: {
      fontSize: 80,
      lineHeight: '80px',
      maxWidth: 380,
      marginBottom: -25,
      letterSpacing: -0.7,
    },

    '& > b': {
      top: -28,

      [theme.breakpoints.down('md')]: {
        top: -33,
      },
    },

    '& *': {
      position: 'relative',
      fontFamily: 'Poppins',
      fontSize: 46,
      letterSpacing: 0,
      fontStyle: 'italic',

      [theme.breakpoints.down('md')]: {
        fontSize: 34,
        lineHeight: '42px',
        maxWidth: 340,
        top: -33,
      },
    },
  },
  container: {
    height: 550,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: `${theme.spacing(20)} ${theme.spacing(5)} 0 ${theme.spacing(5)}`,

    [theme.breakpoints.down('md')]: {
      paddingTop: theme.spacing(8),
      height: 'auto',
    },
  },
  content: {
    background: `url(${image}) no-repeat right center`,
    backgroundSize: '38% !important',
    minHeight: 400,

    [theme.breakpoints.down('md')]: {
      background: 'none',
      minHeight: 'auto',
    },
  },
  info: {
    maxWidth: 540,
    paddingBottom: theme.spacing(8),
    lineHeight: '28px',

    [theme.breakpoints.down('md')]: {
      paddingBottom: theme.spacing(2),
    },
  },
  button: {
    height: 48,
    fontWeight: 'bold',
  },
  input: {
    '& > .MuiInputBase-root': {
      height: 48,
    },
  },
  success: {
    border: `1px solid ${theme.palette.primary.main}`,
    maxWidth: 310,
    padding: theme.spacing(6),
    marginTop: theme.spacing(6),
  },

  firstTitle: {
    paddingBottom: theme.spacing(10),
    whiteSpace: 'break-spaces',

    [theme.breakpoints.down('md')]: {
      fontSize: 32,
      lineHeight: '40px',
      paddingBottom: theme.spacing(6),
    },
  },
  secondTitle: {
    paddingBottom: theme.spacing(6),

    [theme.breakpoints.down('md')]: {
      fontSize: 32,
      lineHeight: '40px',
      paddingBottom: 0,
    },
  },

  logosContainer: {
    marginTop: theme.spacing(20),

    [theme.breakpoints.down('md')]: {
      marginTop: theme.spacing(4),
      marginBottom: theme.spacing(0),
    },
  },

  limitedText: {
    [theme.breakpoints.down('md')]: {
      textAlign: 'center',
    },
  },
  textSection: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },

  mobileImage: {
    paddingTop: theme.spacing(10),
    marginBottom: theme.spacing(5),
    textAlign: 'center',

    '& > img': {
      maxWidth: '100%',

      [theme.breakpoints.down('md')]: {
        maxWidth: '360px',
      },

      [theme.breakpoints.down('sm')]: {
        maxWidth: '80%',
      },
    },
  },
  buttonsSection: {
    maxWidth: 500,

    [theme.breakpoints.down('md')]: {
      maxWidth: '100%',
    },
  },
}));

const Intro = ({ appInfo }: IntroProps) => {
  const { isSM } = useMediaQueries();
  const classes = useStyles();
  const talentsCount = formatNumberSafe(
    appInfo?.total_ot_approved_freelancers_count,
    {
      fallback: INFINITY_SIGN,
    },
  );
  const recruitersCount = formatNumberSafe(appInfo?.total_ot_recruiters_count, {
    fallback: INFINITY_SIGN,
  });
  const countriesCount = formatNumberSafe(
    appInfo?.total_ot_freelancers_countries_count,
    {
      fallback: INFINITY_SIGN,
    },
  );

  return (
    <div className={classes.wrapper}>
      <Container className={classes.container}>
        <Grid className={classes.content} container>
          <Grid className={classes.textSection} sm={isSM ? 12 : 7} item>
            <Hidden mdDown>
              <Typography className={classes.smallTitle}>
                Community-powered search 👋
              </Typography>
            </Hidden>

            <Typography
              fontWeight={700}
              fontStyle="italic"
              variant="h3"
              className={classes.firstTitle}
            >
              {`Find top talent
in record time 🚀`}
            </Typography>

            <Typography variant="body1" className={classes.info}>
              OpenTalent’s community-powered recruitment platform leverages the
              network of <b>{talentsCount} vetted professionals</b> and{' '}
              <b>{recruitersCount}&nbsp;expert recruiters</b> to find you the
              perfect hire in days.
            </Typography>
            <Box className={classes.buttonsSection} pt={4}>
              <Grid direction={isSM ? 'column' : 'row'} spacing={4} container>
                <Grid xs={6} item>
                  <Button
                    variant="contained"
                    color="secondary"
                    fullWidth
                    type="submit"
                    size="large"
                    className={classes.button}
                    href={paths.companyOnboarding}
                  >
                    <b>Post a Job</b>&nbsp;-&nbsp;
                    <span style={{ fontWeight: 500 }}>It’s FREE</span>
                  </Button>
                </Grid>
                <Grid xs={6} item>
                  <Button
                    variant="outlined"
                    fullWidth
                    type="submit"
                    size="large"
                    className={classes.button}
                    href="https://www.meeting.opentalent.co/meetings/sanjib-deka/intake"
                    style={{ color: 'black' }}
                  >
                    Book a Demo
                  </Button>
                </Grid>
              </Grid>
              <Box pt={6} className={classes.limitedText}>
                <Typography
                  variant="subtitle2"
                  color="secondary.main"
                  component="span"
                  fontWeight={700}
                >
                  “No Cure No Pay”
                </Typography>
                {' - '}
                <Typography
                  component="span"
                  variant="subtitle2"
                  color="secondary.main"
                >
                  only&nbsp;pay&nbsp;for&nbsp;success.
                </Typography>
              </Box>
            </Box>
          </Grid>
        </Grid>
      </Container>

      <Hidden mdUp>
        <Box className={classes.mobileImage}>
          <img srcSet={image} alt="Cards" />
        </Box>
      </Hidden>

      <Box className={classes.logosContainer}>
        <LogosLine />
      </Box>
    </div>
  );
};

export default Intro;
