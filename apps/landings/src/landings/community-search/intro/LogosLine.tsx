import IconsRow from 'apps/landings/src/shared/icons-row';
import React from 'react';

import { Container, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import eu from '../../../assets/logo-line/european-union.svg';
import abn from './assets/logos-line/abn.svg';
import aizon from './assets/logos-line/aizon.svg';
import asml from './assets/logos-line/asml.svg';
import atos from './assets/logos-line/atos.svg';
import code from './assets/logos-line/code.svg';
import fedex from './assets/logos-line/fedex.svg';
import hc from './assets/logos-line/hc.svg';
import oyas from './assets/logos-line/oyas.png';
import pon from './assets/logos-line/pon.svg';
import productpine from './assets/logos-line/productpine.svg';
import rabobank from './assets/logos-line/rabobank.svg';
import thales from './assets/logos-line/thales.svg';
import visa from './assets/logos-line/visa.svg';

interface LogosLineProps {}

const ICONS = [
  abn,
  fedex,
  pon,
  asml,
  aizon,
  code,
  hc,
  oyas,
  productpine,
  thales,
  rabobank,
  visa,
  atos,
];

const useStyles = makeStyles((theme) => ({
  root: {
    background: theme.palette.green.light,
  },
  wrapper: {
    padding: theme.spacing(12, 0),
    flexWrap: 'nowrap',

    [theme.breakpoints.down('md')]: {
      justifyContent: 'space-between',
      padding: theme.spacing(6, 0),
      flexWrap: 'wrap',
    },
    '& > div': {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
  },
  text: {
    minWidth: 260,
    maxWidth: 260,
    marginRight: '3%',
    display: 'flex',
    alignItems: 'center',

    [theme.breakpoints.down('md')]: {
      margin: '0 auto',
      paddingBottom: theme.spacing(6),
    },

    '& > img': {
      marginRight: theme.spacing(3),
    },
  },
  logos: {
    flexGrow: 1,

    [theme.breakpoints.down('md')]: {
      maxWidth: '100%',
    },

    '& > div': {
      display: 'flex',
      alignItems: 'center',
    },
  },
  logo: {
    display: 'flex',
    alignItems: 'center',

    '&:not(:last-child)': {
      marginRight: '7%',
    },

    [theme.breakpoints.down('md')]: {
      flexGrow: 1,
    },

    '& img': {
      maxWidth: '100%',
    },
  },

  logosLine: {
    maxWidth: '100%',
  },
}));

const LogosLine = (props: LogosLineProps) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Container>
        <Grid className={classes.wrapper} container>
          <Grid className={classes.text} item>
            <img src={eu} alt="European Union" />

            <Typography whiteSpace="break-spaces" variant="caption">
              {`Used by leading hiring
teams across Europe.`}
            </Typography>
          </Grid>
          <Grid md={8} lg={9} sm={6} item className={classes.logos}>
            <IconsRow
              className={classes.logosLine}
              icons={ICONS}
              shadowColor="green"
            />
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default LogosLine;
