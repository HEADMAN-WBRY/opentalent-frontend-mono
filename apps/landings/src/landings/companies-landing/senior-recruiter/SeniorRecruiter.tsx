import React from 'react';

import { Container, Grid, Button } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { EXTERNAL_LINKS } from '@libs/helpers/consts';
import Typography from '@libs/ui/components/typography';

import image from '../assets/recruiter@2x.png';

interface SeniorRecruiterProps {}

const useStyles = makeStyles((theme) => ({
  title: {
    display: 'block',
    marginBottom: theme.spacing(12),

    [theme.breakpoints.down('md')]: {
      fontSize: 20,
      lineHeight: '26px',
      marginBottom: theme.spacing(8),
      textAlign: 'center',
    },
  },
  wrapper: {
    padding: `${theme.spacing(8)} 0 ${theme.spacing(18)}`,
    overflow: 'hidden',
    background: '#0F0F0D',
    color: 'white',

    [theme.breakpoints.down('md')]: {
      padding: `${theme.spacing(10)} 0`,
    },

    [theme.breakpoints.down('sm')]: {
      padding: `0 0 47px`,
    },

    '& > div': {
      maxWidth: 1000,
      margin: '0 auto',
      justifyContent: 'space-between',
      display: 'flex',

      [theme.breakpoints.down('md')]: {
        padding: `0 ${theme.spacing(4)}`,
      },
    },
  },
  content: {
    flexGrow: 1,
    maxWidth: '50%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',

    [theme.breakpoints.down('md')]: {
      maxWidth: '100%',
    },
  },
  text: {
    maxWidth: 350,
    marginBottom: theme.spacing(12),

    [theme.breakpoints.down('md')]: {
      maxWidth: '100%',
      textAlign: 'center',
      fontSize: 14,
      lineHeight: '20px',
      marginBottom: 16,
    },
  },
  pictureSection: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    textAlign: 'right',
    alignItems: 'center',

    [theme.breakpoints.down('md')]: {
      flexGrow: 1,
      paddingTop: 28,
      textAlign: 'center',
    },

    '& img': {
      maxWidth: 330,
      marginBottom: theme.spacing(5),

      [theme.breakpoints.down('sm')]: {
        maxWidth: 264,
        marginBottom: theme.spacing(3),
      },
    },
  },
  pictureLabel: {
    maxWidth: 250,
  },

  buttonsWrapper: {
    [theme.breakpoints.down('md')]: {
      flexDirection: 'column',
      width: '100%',
      margin: 0,

      '& > div': {
        width: '100%',
        textAlign: 'center',
        paddingLeft: '0 !important',
      },
    },
  },
}));

const SeniorRecruiter = (props: SeniorRecruiterProps) => {
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <Container component={Grid} container>
        <Grid className={classes.content}>
          <Typography
            className={classes.title}
            variant="h4"
            component="i"
            transform="uppercase"
            fontWeight={600}
          >
            A ‘TALENT MATCHER’ For&nbsp;EVERY&nbsp;CLIENT
          </Typography>

          <Typography className={classes.text} variant="body1">
            With OpenTalent, every client gets a Talent Matcher; vetted senior
            recruiters, paired with OpenTalent’s best of breed software, to help
            clients find the right candidate faster at fixed competitive rates.
          </Typography>

          <Grid
            className={classes.buttonsWrapper}
            spacing={4}
            container
            alignItems="center"
          >
            <Grid item>
              <Button
                size="large"
                variant="outlined"
                color="primary"
                fullWidth
                target="_blank"
                href={EXTERNAL_LINKS.bookMeeting}
              >
                SCHEDULE A CALL
              </Button>
            </Grid>
          </Grid>
        </Grid>
        <Grid className={classes.pictureSection}>
          <img srcSet={`${image} 2x`} alt="Recruiter" />
          <Typography
            align="center"
            className={classes.pictureLabel}
            variant="body1"
          >
            Every TMS brings <b>+5 years </b>of experience in recruitment.
          </Typography>
        </Grid>
      </Container>
    </div>
  );
};

export default SeniorRecruiter;
