import avatar1 from './assets/avatars/avatar-1@2x.png';
import avatar2 from './assets/avatars/avatar-2@2x.png';

export const FORM_ID = 'FORM';

export const COMMENTS = [
  {
    id: 1,
    avatarSrc: avatar1,
    name: 'Evi van Splunder',
    position: 'Executive People Director',
    company: `code d’azur`,
    text: `We've been searching for a rockstar Digital Designer for a long time, and found one through OpenTalent in a blink of an eye. OpenTalent is easy to use and connects us to a whole new network of talent.`,
  },
  {
    id: 2,
    avatarSrc: avatar2,
    name: 'Rogier Van Hoorn',
    position: 'Founder',
    company: 'HC Health',
    text: `We know everyone in the marketplace is vetted. We know that candidates earn 100% of their rate, and we know when someone's available, or not. OpenTalent is a game changer when it comes to transparency.`,
  },
];
