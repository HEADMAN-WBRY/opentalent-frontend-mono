import React from 'react';

import { Box, Button, Container, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import Typography from '@libs/ui/components/typography';

import image from '../assets/intro_cards.png';
import image2 from '../assets/singletalentcard.png';
import DynamicTitle from './DynamicTitle';
import LogosLine from './LogosLine';
import { useSubmitAction } from './hooks';

interface IntroProps {}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    background: theme.palette.secondary.main,
    color: 'white',

    [theme.breakpoints.down('md')]: {
      paddingBottom: 6,
    },
  },
  underText: {
    marginBottom: theme.spacing(8),
  },
  title: {
    fontSize: 140,
    lineHeight: '110px',
    fontFamily: "'MissRhinetta', sans-serif",
    fontDisplay: 'swap',
    letterSpacing: -1.5,

    [theme.breakpoints.down('md')]: {
      fontSize: 80,
      lineHeight: '80px',
      maxWidth: 380,
      marginBottom: -25,
      letterSpacing: -0.7,
    },

    '& > b': {
      top: -28,

      [theme.breakpoints.down('md')]: {
        top: -33,
      },
    },

    '& *': {
      position: 'relative',
      fontFamily: 'Poppins',
      fontSize: 46,
      letterSpacing: 0,
      fontStyle: 'italic',

      [theme.breakpoints.down('md')]: {
        fontSize: 34,
        lineHeight: '42px',
        maxWidth: 340,
        top: -33,
      },
    },
  },
  container: {
    height: 750,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: `0 ${theme.spacing(5)}`,

    [theme.breakpoints.down('md')]: {
      height: 'auto',
    },
  },
  content: {
    background: `url(${image}) no-repeat right center`,
    backgroundSize: '33%',

    [theme.breakpoints.down('md')]: {
      paddingTop: 30,
      paddingBottom: '156px',
      background: `url(${image2}) no-repeat right center`,
      backgroundSize: 320,
      backgroundPosition: 'bottom 24px right 50%',
    },

    [theme.breakpoints.down('sm')]: {
      backgroundSize: 280,
      backgroundPosition: 'bottom 24px right 55%',
    },
  },
  info: {
    maxWidth: 610,
    paddingBottom: theme.spacing(8),
    lineHeight: '28px',

    [theme.breakpoints.down('md')]: {
      paddingBottom: theme.spacing(2),
    },
  },
  button: {
    height: 48,
  },
  input: {
    '& > .MuiInputBase-root': {
      height: 48,
    },
  },
  success: {
    border: `1px solid ${theme.palette.primary.main}`,
    maxWidth: 310,
    padding: theme.spacing(6),
    marginTop: theme.spacing(6),
  },

  logosContainer: {
    marginTop: theme.spacing(20),

    [theme.breakpoints.down('md')]: {
      marginTop: theme.spacing(4),
      marginBottom: theme.spacing(10),
    },
  },
}));

const Intro = (props: IntroProps) => {
  const { isSM, isMD } = useMediaQueries();
  const classes = useStyles();
  const { onSubmit } = useSubmitAction();

  return (
    <div className={classes.wrapper}>
      <Container className={classes.container}>
        <Grid className={classes.content} container>
          <Grid sm={isSM ? 12 : 7} item>
            {/* <Typography className={classes.title} color="inherit" variant="h1">
              Ready to hire?
              <br />
              <b>Salesforce Developers</b>
            </Typography> */}

            <DynamicTitle />

            <Typography variant="body1" className={classes.info}>
              <b>Meet them on OpenTalent:</b> Europe’s first talent matching
              platform powered by ‘community’ for leading companies to fill
              urgent demand for hard-to-fill roles in Tech, Data, Product &
              Marketing.
              {/* For hiring managers, who need to close high-skilled roles
              yesterday,{' '}
              <Typography component="span" fontWeight={600} color="primary">
                OpenTalent’s ‘community-powered’ marketplace
              </Typography>{' '}
              is Europe’s new recruitment tool to get the job done. */}
            </Typography>
            <Box pt={4}>
              <Grid direction={isSM ? 'column' : 'row'} spacing={4} container>
                <Grid xs={isMD ? 12 : 9} item>
                  <Button
                    variant="contained"
                    color="primary"
                    fullWidth
                    type="submit"
                    size="large"
                    className={classes.button}
                    onClick={() => onSubmit()}
                  >
                    {isSM ? (
                      <>
                        <b>Post your job</b> - it’s FREE
                      </>
                    ) : (
                      <b>
                        Post your Job & see Instant Matches{' '}
                        <span aria-label="fire" role="img">
                          🔥
                        </span>
                      </b>
                    )}
                  </Button>
                </Grid>
              </Grid>
            </Box>
            {/* {isSended && (
                  <Box className={classes.success}>
                    <Typography variant="body1">
                      Please check your inbox.
                    </Typography>
                  </Box>
                )} */}
          </Grid>
        </Grid>
        <Box className={classes.logosContainer}>
          <LogosLine />
        </Box>
      </Container>
    </div>
  );
};

export default Intro;
