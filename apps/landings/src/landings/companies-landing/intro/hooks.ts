import { useCallback } from 'react';

import { paths } from '../../../utils/consts';

export const useSubmitAction = () => {
  const onSubmit = useCallback(async () => {
    window.location.href = paths.companyOnboarding;
  }, []);

  return { onSubmit };
};
