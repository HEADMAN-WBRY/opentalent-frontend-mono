import React from 'react';

import { Box, Container, Grid, Hidden } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { CommonAppInfo } from '@libs/graphql-types';
import { formatNumber } from '@libs/helpers/format';
import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import Typography from '@libs/ui/components/typography';

import amazonIcon from '../../../assets/companies/amazon@2x.png';
import barcaIcon from '../../../assets/companies/barca@2x.png';
import boeingIcon from '../../../assets/companies/boeing@2x.png';
import HSBCIcon from '../../../assets/companies/hsbc@2x.png';
import knabIcon from '../../../assets/companies/knab@2x.png';
import nestleIcon from '../../../assets/companies/nestle@2x.png';
import omegaIcon from '../../../assets/companies/omega@2x.png';
import redBullIcon from '../../../assets/companies/red-bull@2x.png';
import tescoIcon from '../../../assets/companies/tesco@2x.png';
import warChildIcon from '../../../assets/companies/war-child@2x.png';
import wishIcon from '../../../assets/companies/wish@2x.png';
import firstLine from '../../../assets/logo-line/logo_line_1.png';
import secondLine from '../../../assets/logo-line/logo_line_2.png';
import thirdLine from '../../../assets/logo-line/logo_line_3.png';
import fourthLine from '../../../assets/logo-line/logo_line_4.png';
import fifthLine from '../../../assets/logo-line/logo_line_5.png';
import { INFINITY_SIGN } from '../../../utils/consts';
import NumberBox from './NumberBox';

interface AboutProps {
  appInfo?: Partial<CommonAppInfo>;
}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    padding: `${theme.spacing(18)} 0`,
    overflow: 'hidden',

    [theme.breakpoints.down('md')]: {
      padding: `44px 0 38px`,
    },
  },
  title: {
    marginBottom: theme.spacing(18),
    display: 'block',

    [theme.breakpoints.down('md')]: {
      fontSize: 20,
      lineHeight: '21px',
      marginBottom: theme.spacing(9),
    },
  },

  firstLine: {
    paddingBottom: theme.spacing(14),

    [theme.breakpoints.down('md')]: {
      flexDirection: 'column',
      paddingBottom: theme.spacing(4),

      '& > div': {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      },
    },
  },

  middleWords: {
    lineHeight: '27px',
  },

  icons: {
    maxWidth: 860,
    margin: '0 auto',

    [theme.breakpoints.down('md')]: {
      maxWidth: '100%',
      paddingTop: 36,
    },
  },
  iconItem: {
    [theme.breakpoints.down('md')]: {
      marginBottom: 18,
      display: 'flex',
      minHeight: 50,
      alignItems: 'center',
    },
  },
}));

const ICONS = [
  amazonIcon,
  knabIcon,
  wishIcon,
  tescoIcon,
  boeingIcon,
  HSBCIcon,
  warChildIcon,
  redBullIcon,
  barcaIcon,
  nestleIcon,
  omegaIcon,
];

const MOBILE_ICONS = [fifthLine, fourthLine, thirdLine, secondLine, firstLine];

const About = ({ appInfo }: AboutProps) => {
  const classes = useStyles();
  const { isSM } = useMediaQueries();

  return (
    <Box className={classes.wrapper}>
      <Container>
        <Typography
          className={classes.title}
          style={{ display: 'block' }}
          align="center"
          variant="h4"
          component="i"
          transform="uppercase"
          fontWeight={600}
        >
          ABOUT THE Talent Network
        </Typography>

        <Grid
          className={classes.firstLine}
          justifyContent="center"
          container
          spacing={isSM ? 6 : 10}
        >
          <Grid item>
            <NumberBox>
              {appInfo?.total_ot_freelancers_count
                ? formatNumber(appInfo.total_ot_freelancers_count)
                : INFINITY_SIGN}
            </NumberBox>
            <Typography align="center" color="textSecondary">
              vetted candidates
            </Typography>
          </Grid>
          <Grid item>
            <Box pt={isSM ? 0 : 3}>
              <Typography className={classes.middleWords} variant="h5">
                from
              </Typography>
            </Box>
          </Grid>
          <Grid
            style={{ display: 'flex', flexDirection: 'column' }}
            alignItems="center"
            item
          >
            <NumberBox>
              {appInfo?.total_ot_freelancers_countries_count
                ? formatNumber(appInfo.total_ot_freelancers_countries_count)
                : INFINITY_SIGN}
            </NumberBox>
            <Typography align="center" color="textSecondary">
              countries
            </Typography>
          </Grid>
          <Grid item>
            <Box pt={isSM ? 0 : 3}>
              <Typography className={classes.middleWords} variant="h5">
                with
              </Typography>
            </Box>
          </Grid>
          <Grid item>
            <NumberBox>
              {appInfo?.total_unique_skills_count
                ? formatNumber(appInfo.total_unique_skills_count)
                : INFINITY_SIGN}
            </NumberBox>
            <Typography align="center" color="textSecondary">
              unique skills
            </Typography>
          </Grid>
        </Grid>

        <Grid
          className={classes.firstLine}
          justifyContent="center"
          alignItems="center"
          container
          spacing={isSM ? 6 : 10}
        >
          <Grid item>
            <Typography className={classes.middleWords} variant="h5">
              who worked at{' '}
            </Typography>
          </Grid>

          <Grid item>
            <NumberBox>
              {appInfo?.total_unique_companies_count
                ? formatNumber(appInfo.total_unique_companies_count)
                : INFINITY_SIGN}
            </NumberBox>
            <Typography align="center" color="textSecondary">
              previous employers
            </Typography>
          </Grid>
        </Grid>

        <Hidden mdDown>
          <Grid
            className={classes.icons}
            justifyContent="center"
            alignItems="center"
            container
            spacing={10}
          >
            {ICONS.map((icon) => (
              <Grid key={icon} item>
                <img srcSet={`${icon} 2x`} alt="icon" />
              </Grid>
            ))}
          </Grid>
        </Hidden>

        <Hidden mdUp>
          <Grid
            className={classes.icons}
            justifyContent="center"
            alignItems="center"
            container
          >
            {MOBILE_ICONS.map((icon) => (
              <Grid className={classes.iconItem} key={icon} item>
                <div>
                  <img srcSet={`${icon} 2x`} alt="icon" />
                </div>
              </Grid>
            ))}
          </Grid>
        </Hidden>
      </Container>
    </Box>
  );
};

export default About;
