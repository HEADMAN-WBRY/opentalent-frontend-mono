import React from 'react';

import image from '../../../assets/slider/enterprise.svg';
import SliderItem from '../SliderItem';
import { DefaultSlideProps } from './types';

interface EnterpriseSecuritySlideProps extends DefaultSlideProps {}

export const EnterpriseSecuritySlide = (
  props: EnterpriseSecuritySlideProps,
) => {
  return (
    <SliderItem
      {...props}
      img={image}
      title="Enterprise Security"
      text="Compliance & data security with EU GDPR compliance, 99,9% uptime guarantees, data protection and the security of the AWS cloud."
    />
  );
};
