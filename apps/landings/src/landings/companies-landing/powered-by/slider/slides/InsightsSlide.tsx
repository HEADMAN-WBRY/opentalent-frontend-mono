import React from 'react';

import image from '../../../assets/slider/insights.svg';
import SliderItem from '../SliderItem';
import { DefaultSlideProps } from './types';

interface InsightsSlideProps extends DefaultSlideProps {}

export const InsightsSlide = (props: InsightsSlideProps) => {
  return (
    <SliderItem
      {...props}
      img={image}
      title="Insights & Reporting"
      text="Track live jobs, candidate pipelines combined with extensive remote workforce statistics on size, location and overall spend."
    />
  );
};
