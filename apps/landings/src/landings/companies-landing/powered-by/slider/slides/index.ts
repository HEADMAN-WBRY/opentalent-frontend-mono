export * from './InstantMatchesSlide';
export * from './DirectSourcingSlide';
export * from './SkillsBasedSlide';
export * from './ATSSlide';
export * from './EnterpriseSecuritySlide';
export * from './DirectCommunicationSlide';
export * from './InsightsSlide';
