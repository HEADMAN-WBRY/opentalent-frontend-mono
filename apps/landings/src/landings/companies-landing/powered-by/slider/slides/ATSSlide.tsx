import React from 'react';

import image from '../../../assets/slider/ats.svg';
import SliderItem from '../SliderItem';
import { DefaultSlideProps } from './types';

interface ATSSlideProps extends DefaultSlideProps {}

export const ATSSlide = (props: ATSSlideProps) => {
  return (
    <SliderItem
      {...props}
      img={image}
      title="ATS"
      text="Track talent pipelines for every job using the OpenTalent Application Tracking System, or sync with your existing ATS."
    />
  );
};
