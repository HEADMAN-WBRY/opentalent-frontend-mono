import React from 'react';

import image from '../../../assets/slider/direct_sourcing.svg';
import SliderItem from '../SliderItem';
import { DefaultSlideProps } from './types';

interface DirectSourcingSlideProps extends DefaultSlideProps {}

export const DirectSourcingSlide = (props: DirectSourcingSlideProps) => {
  return (
    <SliderItem
      {...props}
      img={image}
      title="Direct Sourcing"
      text="Build private talent pools to hire candidates commission-free - all while building record of your flexible workforce."
    />
  );
};
