import cn from 'classnames';
import React, { useState } from 'react';

import { Box, Button, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import {
  getNextIndex,
  getPreviousIndex,
} from '@libs/helpers/common/moveToIndex';
import { EXTERNAL_LINKS } from '@libs/helpers/consts';
import CircleStepper from '@libs/ui/components/circle-stepper';

import { useAutoChangeSlider } from '../../../../shared/new-landings/slider/hooks';
import { ReactComponent as ArrowIcon } from '../../assets/slider/slider_arrow.svg';
import {
  DirectSourcingSlide,
  InstantMatchesSlide,
  SkillsBasedSlide,
  ATSSlide,
  EnterpriseSecuritySlide,
  DirectCommunicationSlide,
  InsightsSlide,
} from './slides';

interface SliderProps {}

const SLIDES = [
  InstantMatchesSlide,
  DirectSourcingSlide,
  SkillsBasedSlide,
  DirectCommunicationSlide,
  ATSSlide,
  EnterpriseSecuritySlide,
  InsightsSlide,
];

const useStyles = makeStyles((theme) => ({
  content: {
    minHeight: 264,
    display: 'flex',
    alignItems: 'center',
    marginBottom: theme.spacing(20),

    [theme.breakpoints.down('md')]: {
      marginBottom: theme.spacing(8),
    },
  },

  buttonWrapper: {
    display: 'flex',
    justifyContent: 'center',
    width: '100%',
    marginTop: theme.spacing(1),

    '& > a': {
      [theme.breakpoints.down('md')]: {
        width: '100%',
      },
    },
  },

  '@keyframes zoomIn': {
    from: {
      opacity: 0,
      transform: 'scale3d(0.3, 0.3, 0.3)',
    },

    to: {
      opacity: 1,
    },
  },

  slide: {
    position: 'absolute',
    left: -1000000,
    visibility: 'hidden',
  },
  slideActive: {
    position: 'static',
    visibility: 'visible',
    animation: '$zoomIn 0.5s ease-in-out',
  },

  arrow: {
    cursor: 'pointer',
  },
  arrowRight: {
    transform: 'rotate(180deg)',
  },
}));

const Slider = (props: SliderProps) => {
  const classes = useStyles();
  const [activeSlide, setActiveSlide] = useState(0);
  const { wrappedChangeSlide } = useAutoChangeSlider({
    currentSlide: activeSlide,
    setSlide: setActiveSlide,
    slidesCount: SLIDES.length,
  });
  const selectPrev = () =>
    wrappedChangeSlide(getPreviousIndex(activeSlide, SLIDES.length));
  const selectNext = () =>
    wrappedChangeSlide(getNextIndex(activeSlide, SLIDES.length));

  return (
    <>
      <div className={classes.content}>
        {SLIDES.map((Slide, index) => (
          <Slide
            // eslint-disable-next-line react/no-array-index-key
            key={index}
            className={cn(classes.slide, {
              [classes.slideActive]: index === activeSlide,
            })}
          />
        ))}
      </div>

      <Grid
        spacing={6}
        wrap="nowrap"
        justifyContent="center"
        alignItems="center"
        container
      >
        <Grid item>
          <ArrowIcon onClick={selectPrev} className={classes.arrow} />
        </Grid>
        <Grid item>
          <CircleStepper
            steps={SLIDES.length}
            activeStep={activeSlide}
            onStepChange={wrappedChangeSlide}
            color="primary"
          />
        </Grid>
        <Grid item>
          <ArrowIcon
            onClick={selectNext}
            className={cn(classes.arrowRight, classes.arrow)}
          />
        </Grid>
      </Grid>

      <Box className={classes.buttonWrapper}>
        <Button
          href={EXTERNAL_LINKS.pieterLink}
          target="_blank"
          size="large"
          variant="outlined"
          color="primary"
        >
          SCHEDULE A DEMO&nbsp;&nbsp;
        </Button>
      </Box>
    </>
  );
};

export default Slider;
