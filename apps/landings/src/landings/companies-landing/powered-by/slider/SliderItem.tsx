import { Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';
import cn from 'classnames';
import React from 'react';

import Typography from '@libs/ui/components/typography';

interface SliderItemProps {
  img: string;
  text: string;
  title: string;
  className?: string;
}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    alignItems: 'center',

    [theme.breakpoints.down('md')]: {
      flexDirection: 'column',
    },

    '& img': {
      maxWidth: '100%',
    },
  },
  textContainer: {
    maxWidth: '330px',
    marginRight: theme.spacing(8),

    [theme.breakpoints.down('md')]: {
      marginBottom: theme.spacing(8),
      maxWidth: '100%',
    },

    [theme.breakpoints.down('sm')]: {
      marginBottom: theme.spacing(8),
      maxWidth: '100%',
      margin: 0,
    },

    '& h5': {
      fontStyle: 'italic',

      [theme.breakpoints.down('md')]: {
        fontSize: '16px',
      },
    },

    '& p': {
      [theme.breakpoints.down('md')]: {
        fontSize: '14px',
      },
    },
  },
}));

const SliderItem = ({ img, title, text, className }: SliderItemProps) => {
  const classes = useStyles();

  return (
    <Grid
      className={cn(classes.wrapper, className)}
      justifyContent="center"
      wrap="nowrap"
      container
    >
      <Grid className={classes.textContainer} item>
        <Typography component="h5" fontWeight={600} variant="h5" paragraph>
          {title}
        </Typography>
        <Typography variant="body1">{text}</Typography>
      </Grid>
      <Grid item>
        <img src={img} alt="" />
      </Grid>
    </Grid>
  );
};

export default SliderItem;
