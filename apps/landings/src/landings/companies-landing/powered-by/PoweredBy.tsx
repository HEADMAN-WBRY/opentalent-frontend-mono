import { Box, Container } from '@mui/material';
import { makeStyles } from '@mui/styles';
import React from 'react';

import Typography from '@libs/ui/components/typography';

import Slider from './slider';

interface PoweredByProps {}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    background: theme.palette.secondary.main,
    color: 'white',
    paddingTop: 100,
    paddingBottom: 90,
    overflow: 'hidden',

    [theme.breakpoints.down('md')]: {
      paddingTop: 50,
      paddingBottom: 50,
    },
  },
  container: {
    maxWidth: 1024,
    overflow: 'hidden',
    padding: `0 ${theme.spacing(5)}`,
  },
  title: {
    maxWidth: 690,
    textAlign: 'center',
    margin: '0 auto',
    paddingBottom: theme.spacing(8),

    '&  i': {
      [theme.breakpoints.down('md')]: {
        fontSize: 20,
        lineHeight: '25px',
      },
    },
  },
}));

const PoweredBy = (props: PoweredByProps) => {
  const classes = useStyles();

  return (
    <Box className={classes.wrapper}>
      <Container className={classes.container}>
        <Box className={classes.title}>
          <Typography
            component="i"
            fontWeight={600}
            transform="uppercase"
            variant="h4"
          >
            POWERED&nbsp;by OpenTalent’s&nbsp;BEST&nbsp;OF&nbsp;BREED{' '}
            <Typography component="i" color="primary" variant="h4">
              FLEXIBLE&nbsp;WORKFORCE&nbsp;CLOUD
            </Typography>
          </Typography>
        </Box>
        <Slider />
      </Container>
    </Box>
  );
};

export default PoweredBy;
