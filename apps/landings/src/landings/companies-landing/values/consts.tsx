import { formatNumber } from '@libs/helpers/format';

import { ReactComponent as DataIcon } from '../../../assets/values/data.svg';
import { ReactComponent as ListIcon } from '../../../assets/values/list.svg';
import { ReactComponent as MacIcon } from '../../../assets/values/mac.svg';
import { ReactComponent as PayIcon } from '../../../assets/values/pay.svg';
import { ReactComponent as RefIcon } from '../../../assets/values/ref.svg';
import { ReactComponent as SearchIcon } from '../../../assets/values/search.svg';
import { ReactComponent as SecureIcon } from '../../../assets/values/secure.svg';
import { ReactComponent as SupportIcon } from '../../../assets/values/support.svg';
import { UNBREAKABLE_WHITESPACE } from '../../../utils/consts';

export const getValues = ({ talentsCount }: { talentsCount: number }) => [
  {
    title: 'Community-powered',
    text: `Global sourcing with our community of${UNBREAKABLE_WHITESPACE}${formatNumber(
      talentsCount,
    )} professionals strong`,
    Icon: SearchIcon,
  },
  {
    title: 'Referral only',
    text: 'OpenTalent is ‘invite-only’; everyone comes recommended.',
    Icon: RefIcon,
  },
  {
    title: 'Faster matching',
    text: `+80% of new job posts get a qualified match within 5${UNBREAKABLE_WHITESPACE}minutes.`,
    Icon: ListIcon,
  },
  {
    title: 'No Cure No Pay',
    text: 'Only pay if we find your dream candidate - it’s that simple!',
    Icon: PayIcon,
  },
  {
    title: 'Secure hiring',
    text: 'Contracting and pay-rolling to securely hire anyone in Europe.',
    // text: (
    //   <>
    //     Only when we found your ideal match you’ll pay a Finder’s Fee.{' '}
    //     <OuterLink
    //       href="https://www.notion.so/opentalent/Finder-s-Fees-explained-0b24c6d75e9f4e938d07c48b7714217a"
    //       variant="body2"
    //       color="primary"
    //       pointer
    //       target="_blank"
    //     >
    //       Learn&nbsp;more.
    //     </OuterLink>
    //   </>
    // ),
    Icon: SecureIcon,
  },

  {
    title: 'New insights',
    text: 'Gain new insights from OpenTalent’s data-driving platform.',
    Icon: DataIcon,
  },
  {
    title: 'Built for Enterprise',
    text: 'OpenTalent’s robust & secure platform was built for the enterprise.',
    Icon: MacIcon,
  },
  {
    title: 'Support & Services',
    text: 'Benefit from expert support and premium services for all clients.',
    // text: (
    //   <>
    //     Benefit from a full-service offering and expert support for all clients.{' '}
    //     <OuterLink
    //       href={EXTERNAL_LINKS.pricing}
    //       variant="body2"
    //       color="primary"
    //       pointer
    //       target="_blank"
    //     >
    //       Learn&nbsp;more.
    //     </OuterLink>
    //   </>
    // ),
    Icon: SupportIcon,
  },
];
