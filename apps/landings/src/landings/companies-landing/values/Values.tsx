import React from 'react';

import { Box, Container, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import Typography from '@libs/ui/components/typography';

import { getValues } from './consts';

interface ValuesProps {
  talentsCount?: number;
}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    background: '#0F0F0D',
    color: 'white',
    paddingTop: 100,
    paddingBottom: 90,
    overflow: 'hidden',

    [theme.breakpoints.down('md')]: {
      paddingTop: 50,
      paddingBottom: 48,
    },
  },
  container: {
    maxWidth: 1024,
    overflow: 'hidden',
  },
  title: {
    maxWidth: 660,
    textAlign: 'center',
    margin: '0 auto',
    paddingBottom: theme.spacing(20),

    [theme.breakpoints.down('md')]: {
      paddingBottom: theme.spacing(8),
    },

    '& > i': {
      [theme.breakpoints.down('md')]: {
        fontSize: 20,
        lineHeight: '25px',
      },
    },
  },

  valueContent: {
    maxWidth: 308,

    [theme.breakpoints.down('md')]: {
      maxWidth: '100%',
    },
  },

  values: {
    maxWidth: 900,
    margin: '-40px auto 0',
  },
  valueItem: {
    '& p': {
      lineHeight: '28px',

      [theme.breakpoints.down('sm')]: {
        lineHeight: '22px',
      },
    },

    '& svg': {
      [theme.breakpoints.down('sm')]: {
        width: 42,
      },
    },
  },
  valueTitle: {
    display: 'block',
    fontSize: 20,
    lineHeight: '28px',

    [theme.breakpoints.down('sm')]: {
      display: 'inline',
      fontSize: 18,
      lineHeight: '22px',
    },
  },
  iconWrap: {
    color: theme.palette.primary.main,
  },
}));

const Values = ({ talentsCount = 0 }: ValuesProps) => {
  const classes = useStyles();
  const { isSM } = useMediaQueries();

  return (
    <Box className={classes.wrapper}>
      <Container className={classes.container}>
        <Box className={classes.title}>
          <Typography
            component="i"
            fontWeight={600}
            transform="uppercase"
            variant="h4"
          >
            What Value Companies Get Out&nbsp;Of&nbsp;USING&nbsp;OpenTalent?
          </Typography>
        </Box>

        <Grid
          className={classes.values}
          container
          justifyContent="center"
          spacing={isSM ? 6 : 10}
        >
          {getValues({ talentsCount }).map(({ title, text, Icon }) => (
            <Grid xs={isSM ? 11 : 6} key={title} item>
              <Grid
                className={classes.valueItem}
                spacing={isSM ? 4 : 6}
                wrap="nowrap"
                container
              >
                <Grid className={classes.iconWrap} item>
                  <Icon />
                </Grid>
                <Grid item>
                  <Typography className={classes.valueContent} variant="body2">
                    <Typography
                      className={classes.valueTitle}
                      component="span"
                      variant="body1"
                      fontWeight={600}
                    >
                      {title}
                    </Typography>{' '}
                    {text}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          ))}
        </Grid>
      </Container>
    </Box>
  );
};

export default Values;
