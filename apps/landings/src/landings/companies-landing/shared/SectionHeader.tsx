import { makeStyles } from '@mui/styles';
import cn from 'classnames';
import React from 'react';

import Typography from '@libs/ui/components/typography';

interface SectionHeaderProps
  extends React.PropsWithChildren<unknown>,
    React.HTMLAttributes<HTMLDivElement> {}

const useStyles = makeStyles((theme) => ({
  title: {
    display: 'block',
    padding: `0 ${theme.spacing(4)}`,
    textAlign: 'center',
    textTransform: 'uppercase',

    [theme.breakpoints.down('md')]: {
      fontSize: 20,
      lineHeight: '32px',
    },
  },
}));

const SectionHeader = ({ children, className }: SectionHeaderProps) => {
  const classes = useStyles();

  return (
    <Typography
      className={cn(classes.title, className)}
      paragraph
      component="i"
      fontWeight={600}
      variant="h4"
    >
      {children}
    </Typography>
  );
};

export default SectionHeader;
