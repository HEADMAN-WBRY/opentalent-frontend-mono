import React from 'react';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { CommonAppInfo, TalentsCountItem } from '@libs/graphql-types';
import { formatNumber } from '@libs/helpers/format';
import Typography from '@libs/ui/components/typography';

import { INFINITY_SIGN } from '../../../utils/consts';
import SectionHeader from '../shared/SectionHeader';
import Categories from './Categories';
import { COUNTRIES } from './consts';

interface CountriesProps {
  appInfo?: Partial<CommonAppInfo>;
  counts: TalentsCountItem[];
}

const useStyles = makeStyles((theme) => ({
  container: {
    padding: `${theme.spacing(20)} 0`,
    width: '100vw',
    overflow: 'hidden',

    [theme.breakpoints.down('md')]: {
      padding: `48px 0 32px`,
    },
  },
  title: {
    [theme.breakpoints.down('md')]: {
      margin: 0,
    },
  },
  '@keyframes slideRight': {
    from: { transform: 'translateX(0px)' },
    to: { transform: 'translateX(-9000px)' },
  },
  countries: {
    width: 1000000,
    animationName: '$slideRight',
    animationDuration: '240s',
    animationIterationCount: 'infinite',
    animationTimingFunction: 'linear',
  },
  country: {
    fontSize: 72,
    lineHeight: '80px',

    [theme.breakpoints.down('md')]: {
      fontSize: 36,
      lineHeight: '48px',
    },
  },
}));

const Countries = ({ appInfo, counts }: CountriesProps) => {
  const classes = useStyles();
  const freelancersCount = appInfo?.total_ot_freelancers_count
    ? formatNumber(appInfo.total_ot_freelancers_count)
    : INFINITY_SIGN;

  return (
    <Box className={classes.container}>
      <Box pb={6}>
        <Grid justifyContent="center" container>
          <Grid item>
            <SectionHeader className={classes.title}>
              POWERED BY {freelancersCount} PROFESSIONALS&nbsp;IN
            </SectionHeader>
          </Grid>
        </Grid>
      </Box>

      <Grid className={classes.countries} wrap="nowrap" spacing={8} container>
        {COUNTRIES.map((i, index) => (
          // eslint-disable-next-line react/no-array-index-key
          <Grid key={index} item>
            <Typography
              className={classes.country}
              paragraph
              transform="uppercase"
              component="i"
              variant="h2"
              fontWeight="bold"
            >
              {i}
            </Typography>
          </Grid>
        ))}
      </Grid>

      <Box pt={11}>
        <Categories counts={counts} />
      </Box>
    </Box>
  );
};

export default Countries;
