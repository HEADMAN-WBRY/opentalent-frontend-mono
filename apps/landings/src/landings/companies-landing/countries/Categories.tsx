import React from 'react';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { TalentsCountItem } from '@libs/graphql-types';
import { formatNumber } from '@libs/helpers/format';

import TextBox3d from '../../../shared/text-box-3d';
import SectionHeader from '../shared/SectionHeader';

interface CategoriesProps {
  counts: TalentsCountItem[];
}

const useStyles = makeStyles((theme) => ({
  title: {
    marginBottom: theme.spacing(8),

    [theme.breakpoints.down('md')]: {
      fontSize: 18,
      lineHeight: '24px',
    },

    '& i': {
      width: '100%',
      textAlign: 'center',
      display: 'block',
      padding: `0 ${theme.spacing(2)}px`,

      [theme.breakpoints.down('md')]: {
        fontSize: 20,
        lineHeight: '26px',
      },
    },
  },
  categoriesWrap: {
    margin: '0 auto',
    maxWidth: 900,
    width: `calc(100% - ${theme.spacing(8)})`,
  },
  categories: {
    padding: `${theme.spacing(4)} 0 0`,
  },
  categoriesItem: {
    marginRight: theme.spacing(4),
    marginBottom: theme.spacing(4),

    '&:first-child': {
      marginRight: theme.spacing(8),
      marginLeft: theme.spacing(8),
    },
  },
}));

const Categories = ({ counts }: CategoriesProps) => {
  const classes = useStyles();

  return (
    <div>
      <Grid className={classes.title} justifyContent="center" container>
        <Grid item>
          <SectionHeader className={classes.title}>
            ACROSS {counts.length} ‘high-skilled’ remote&nbsp;categories
          </SectionHeader>
        </Grid>
      </Grid>

      <Box className={classes.categoriesWrap}>
        <Grid className={classes.categories} justifyContent="center" container>
          {counts.map((i) => (
            <Grid className={classes.categoriesItem} key={i.name} item>
              <TextBox3d
                text={i.name}
                value={
                  <>
                    <b>{formatNumber(i.talents_count)}</b> profiles
                  </>
                }
              />
            </Grid>
          ))}
        </Grid>
      </Box>
    </div>
  );
};

export default Categories;
