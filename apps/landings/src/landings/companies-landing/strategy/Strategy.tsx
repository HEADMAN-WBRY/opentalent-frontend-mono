import React from 'react';

import { Box, Button, Container, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { EXTERNAL_LINKS } from '@libs/helpers/consts';
import Typography from '@libs/ui/components/typography';

import image from '../../../assets/strategy/strategy@2x.png';
import abnLogo from '../assets/abn_logo.png';

interface StrategyProps {}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    background: '#0F0F0D',
    color: 'white',
  },
  title: {
    display: 'block',
    marginBottom: theme.spacing(12),
    maxWidth: 500,
    fontStyle: 'italic',

    [theme.breakpoints.down('md')]: {
      fontSize: 20,
      lineHeight: '21px',
      marginBottom: theme.spacing(8),
      textAlign: 'center',
      maxWidth: '100%',
    },
  },
  container: {
    height: 700,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: `0 ${theme.spacing(5)}`,

    [theme.breakpoints.down('md')]: {
      height: 'auto',
    },
  },
  content: {
    justifyContent: 'space-between',
    alignItems: 'center',

    [theme.breakpoints.down('md')]: {
      paddingTop: '48px',
      paddingBottom: 54,
      flexDirection: 'column',
    },

    [theme.breakpoints.down('sm')]: {
      backgroundSize: '70%',
    },
  },
  info: {
    maxWidth: 440,
    paddingBottom: theme.spacing(14),

    [theme.breakpoints.down('md')]: {
      paddingBottom: theme.spacing(8),
      maxWidth: '100%',
    },
  },
  abnLogo: {
    [theme.breakpoints.down('md')]: {
      textAlign: 'center',
    },
  },
  buttonWrapper: {
    [theme.breakpoints.down('md')]: {
      width: '100%',
      paddingBottom: 48,

      '& > a': {
        width: '100%',
      },
    },
  },
  imageSection: {
    [theme.breakpoints.down('md')]: {
      textAlign: 'center',
    },

    '& > img': {
      maxWidth: '100%',
    },
  },
}));

const Strategy = (props: StrategyProps) => {
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <Container className={classes.container}>
        <Grid wrap="nowrap" className={classes.content} container>
          <Grid item>
            <Box className={classes.abnLogo} pb={8}>
              <img srcSet={`${abnLogo} 2x`} alt="ABN" />
            </Box>
            <Typography
              className={classes.title}
              fontWeight={600}
              color="inherit"
              variant="h4"
            >
              OPTIMIZING THE ‘FREELANCER’ SOURCING STRATEGY.
            </Typography>

            <Typography variant="subtitle1" className={classes.info}>
              Since 2021, OpenTalent and ABN AMRO have pioneered a new way of
              freelancer sourcing by building an Internal Talent Marketplace and
              adding ‘remote’ to the hiring mix. Download the case for details.
            </Typography>

            <Box className={classes.buttonWrapper}>
              <Button
                href={EXTERNAL_LINKS.ABNCase}
                variant="outlined"
                color="primary"
                size="large"
                target="_blank"
              >
                DOWNLOAD CASE
              </Button>
            </Box>
          </Grid>
          <Grid className={classes.imageSection} item>
            <img srcSet={`${image} 2x`} alt="Cases" />
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default Strategy;
