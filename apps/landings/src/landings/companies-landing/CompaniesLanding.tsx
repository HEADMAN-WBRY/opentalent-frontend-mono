import React from 'react';
import { useTitle } from 'react-use';

import { makeStyles } from '@mui/styles';

import { TalentsCountItem } from '@libs/graphql-types';
import { useMediaQueries } from '@libs/helpers/hooks/media-queries';

import Comments from '../../shared/Comments';
import Join from '../../shared/Join';
import Footer from '../../shared/common-footer';
import About from './about';
import { COMMENTS } from './consts';
import Countries from './countries';
import Header from './header';
import { useAppInfo } from './hooks';
import HowItWorks from './how-it-works';
import Intro from './intro';
import PoweredBy from './powered-by';
import SeniorRecruiter from './senior-recruiter';
import Strategy from './strategy';
import Values from './values';

interface CompaniesLandingProps {}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    width: '100vw',
    overflow: 'hidden',

    '& .MuiButton-root': {
      borderRadius: 4,
      textTransform: 'none',
    },
  },
  '@global': {
    body: {
      width: '100%',
    },
  },
  '@font-face': {
    fontFamily: 'MissRhinetta',
    src: 'url(assets/fonts/Miss-Rhinetta.otf)',
  },

  join: {
    background: 'white',
    color: theme.palette.text.primary,

    '& button': {
      boxShadow: '0px 8px 16px rgba(0, 0, 0, 0.14) !important',
    },
  },
}));

const CompaniesLanding = (props: CompaniesLandingProps) => {
  const classes = useStyles();
  const { isMD } = useMediaQueries();
  const { data = {} } = useAppInfo();
  const { commonAppInfo, talentsCountByCategories } = data;
  const counts = (talentsCountByCategories || []) as TalentsCountItem[];

  useTitle('Next-Generation Talent Acquisition');

  return (
    <div className={classes.wrapper}>
      <Header />
      <Intro />
      <Countries appInfo={commonAppInfo} counts={counts} />
      <Values talentsCount={commonAppInfo?.total_ot_freelancers_count} />
      <PoweredBy />
      <About appInfo={commonAppInfo} />
      <HowItWorks />
      <SeniorRecruiter />
      <Comments title="What do our clients SAY?" comments={COMMENTS} />
      <Strategy />
      <Join
        title={
          isMD
            ? `On a mission to find your next best hire, always! join us.`.toUpperCase()
            : `On a mission to find your next
best hire, always! join us.`.toUpperCase()
        }
        btnText={
          <>
            <b>Post your job</b> - it’s FREE
          </>
        }
        className={classes.join}
      />
      <Footer />
    </div>
  );
};

export default CompaniesLanding;
