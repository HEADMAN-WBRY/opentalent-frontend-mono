import { makeStyles } from '@mui/styles';
import React from 'react';

import Comments from '../../shared/Comments';
import Footer from '../../shared/common-footer';
import CommonHeader from './CommonHeader';
import { COMMENTS } from './consts';
import Intro from './intro';
import Join from './join';
import WhatToExpect from './whatToExpect';

const useStyles = makeStyles(() => ({
  wrapper: {
    overflow: 'hidden',

    '& .MuiButtonBase-root': {
      height: 48,
      textTransform: 'none',
      fontWeight: 'bold',
      borderRadius: '4px',
    },
  },
  '@global': {
    body: {
      width: '100%',
    },
  },
}));

const TalentsLanding: React.FC = () => {
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <CommonHeader isForTalent />
      <Intro />
      <WhatToExpect />
      <Comments title="What do our members say?" comments={COMMENTS} />
      <Join
        title={`Join thousands of FREELANCERS ACROSS
        EUROPE THRIVING WITH OPENTALENT.`}
        btnText="Join OpenTalent"
      />
      <Footer />
    </div>
  );
};

export default TalentsLanding;
