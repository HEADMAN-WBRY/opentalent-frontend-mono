import React from 'react';
import { useToggle } from 'react-use';

import MenuIcon from '@mui/icons-material/Menu';
import {
  AppBar,
  Box,
  Button,
  Container,
  Grid,
  Hidden,
  IconButton,
  Toolbar,
} from '@mui/material';
import { makeStyles } from '@mui/styles';

import { useMediaQueries } from '@libs/helpers/hooks/media-queries';

import { ReactComponent as LogoIcon } from '../../assets/opentalent_dark.svg';
import PageDrawer from '../../shared/PageDrawer';
import { paths } from '../../utils/consts';

interface HeaderProps {
  isForTalent?: boolean;
}

const useStyles = makeStyles((theme) => ({
  replaceBtn: {
    transition: `color ${theme.transitions.duration.short}s ${theme.transitions.easing.easeInOut}`,

    '&:hover': {
      color: theme.palette.primary.main,
    },
  },

  container: {
    '& .MuiButtonBase-root': {
      fontWeight: '400 !important' as any,
      height: '36px !important',
    },
  },

  logoWrapper: {
    display: 'flex',
    alignItems: 'center',
    color: theme.palette.primary.main,
    paddingLeft: theme.spacing(2),

    [theme.breakpoints.down('sm')]: {
      marginLeft: -12,
      paddingLeft: 0,
    },
  },
}));

const CommonHeader: React.FC<HeaderProps> = ({ isForTalent }) => {
  const [isOpen, toggle] = useToggle(false);
  const { isXS, isMD } = useMediaQueries();
  const classes = useStyles();

  return (
    <>
      <PageDrawer toggle={toggle} isOpen={isOpen} isForTalent={isForTalent} />
      <AppBar color="secondary" position="static">
        <Box pt={isMD ? 2 : 6}>
          <Container className={classes.container}>
            <Toolbar disableGutters>
              <Grid
                justifyContent="space-between"
                alignItems="center"
                container
              >
                <Grid item>
                  <Grid container>
                    {isXS && (
                      <Grid item>
                        <IconButton
                          size="small"
                          onClick={toggle}
                          style={{ color: 'white' }}
                        >
                          <MenuIcon />
                        </IconButton>
                      </Grid>
                    )}
                    <Grid
                      className={classes.logoWrapper}
                      item
                      component={Box}
                      mb={-2}
                    >
                      <a href="/">
                        <LogoIcon height={isXS ? 35 : 54} />
                      </a>
                    </Grid>
                  </Grid>
                </Grid>
                <Hidden smDown>
                  <Grid item>
                    <Grid spacing={4} container>
                      <Grid item>
                        <Button
                          color="inherit"
                          className={classes.replaceBtn}
                          href="/"
                        >
                          FOR COMPANIES
                        </Button>
                      </Grid>
                      <Grid item>
                        <Button
                          href={paths.mainAppRoute}
                          variant="outlined"
                          color="inherit"
                        >
                          Sign in
                        </Button>
                      </Grid>
                    </Grid>
                  </Grid>
                </Hidden>
              </Grid>
            </Toolbar>
          </Container>
        </Box>
      </AppBar>
    </>
  );
};

export default CommonHeader;
