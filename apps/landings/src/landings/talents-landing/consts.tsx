import React from 'react';

import avatar1 from './assets/avatars/avatar-1@2x.png';
import avatar2 from './assets/avatars/avatar-2@2x.png';
import { ReactComponent as HundredPercent } from './assets/hundred-percent.svg';
import { ReactComponent as Invitations } from './assets/invitations.svg';
import { ReactComponent as Matching } from './assets/matching.svg';
import { ReactComponent as Notifications } from './assets/notifications.svg';

export const FORM_ID = 'FORM';

export interface ItemType {
  id: number;
  title: string;
  subtitle: string;
  points: Array<string>;
  imgComponent: JSX.Element;
  revert: boolean;
}

export const WHAT_TO_EXPECT_ITEMS: Array<ItemType> = [
  {
    id: 1,
    title: 'Where Talent is King.',
    subtitle: `At OpenTalent we're building Europe's №1 talent-centric freelancer community; a place that supports people in building a business-of-one and doing what they love. Welcome to a better alternative!`,
    points: [
      '0% commissions on your earnings',
      'Receive payments within 1 day',
      'Earn money referring new talent',
    ],
    imgComponent: <HundredPercent />,
    revert: false,
  },
  {
    id: 2,
    title: `Relevant roles only,
     based on your skills.`,
    subtitle: `Get matched with relevant roles only. At fast-growing, talent-centric companies. Creating your profile, tell us what you’re looking for and what you’re good at, and we’ll take care of the rest!`,
    points: [
      'Get matched with relevant roles only',
      'Unique skills-based matching algorithm',
      'At fast-growing, innovative companies',
    ],
    imgComponent: <Matching />,
    revert: true,
  },
  {
    id: 3,
    title: `Top job matches,
    delivered instantly.`,
    subtitle: `When new jobs match your profile we’ll send you an email. No need to constantly login to OpenTalent - we’ll let you know when things get interesting!`,
    points: ['Top job match alerts only', 'Delivered straight to your inbox'],
    imgComponent: <Notifications />,
    revert: false,
  },
  {
    id: 4,
    title: `Extra earnings by
    spreading the word.`,
    subtitle: `We believe that talented individuals like you are best equipped to find new talent. So we productized the referral fee: invite new talent to our community and we’ll split all commissions with you!`,
    points: [
      'Get paid up to €4.320 per referral',
      'Build an additional revenue stream',
    ],
    imgComponent: <Invitations />,
    revert: true,
  },
];

export const COMMENTS = [
  {
    id: 1,
    avatarSrc: avatar1,
    name: 'Mariusz',
    position: 'Security Cloud Architect',
    text: `OpenTalent helped me to find an incredible new remote contract at a leading company in The Netherlands. 
    Everything about the experience rocks.`,
  },
  {
    id: 2,
    avatarSrc: avatar2,
    name: 'Kevin',
    position: 'Online Marketeer',
    text: `OpenTalent brings access to a new group of companies; firms open to working differently, which unlocks new opportunities for me.`,
  },
];
