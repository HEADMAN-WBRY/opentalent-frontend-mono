import cn from 'classnames';
import React from 'react';

import { Grid, Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

import abn from './assets/icons/abn.png';
import aizon from './assets/icons/aizon.svg';
import asml from './assets/icons/asml.svg';
import cde from './assets/icons/cde.svg';
import fedex from './assets/icons/fedex.svg';
import hc from './assets/icons/hc.svg';
import oyas from './assets/icons/oyas.png';
import pon from './assets/icons/pon.svg';
import productpine from './assets/icons/productpine.svg';
import thales from './assets/icons/thales.svg';

interface IconsProps {
  shadowColor?: 'black' | 'green';
  className?: string;
  icons?: string[];
}

export const ICONS = [
  abn,
  cde,
  hc,
  productpine,
  fedex,
  pon,
  asml,
  aizon,
  thales,
  oyas,
];

const getColor = (color: IconsProps['shadowColor'], theme: Theme) => {
  switch (color) {
    case 'green':
      return theme.palette.green.light;
    default:
      return theme.palette.secondary.main;
  }
};

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    overflow: 'hidden',
    maxWidth: 1000,
    margin: '0 auto',

    '&::before': {
      content: '""',
      position: 'absolute',
      left: 0,
      top: 0,
      height: '100%',
      width: 30,
      zIndex: 2,
      background: ({ shadowColor }: IconsProps) =>
        `linear-gradient(to right, ${getColor(
          shadowColor,
          theme,
        )}, transparent)`,
    },

    '&::after': {
      zIndex: 2,
      content: '""',
      position: 'absolute',
      right: 0,
      top: 0,
      height: '100%',
      width: 30,
      background: ({ shadowColor }: IconsProps) =>
        `linear-gradient(to left, ${getColor(
          shadowColor,
          theme,
        )}, transparent)`,
    },
  },
  '@keyframes slideRight': {
    from: { transform: 'translateX(0px)' },
    to: { transform: 'translateX(-9000px)' },
  },
  grid: {
    flexWrap: 'nowrap',
    animationName: '$slideRight',
    animationDuration: '240s',
    animationIterationCount: 'infinite',
    animationTimingFunction: 'linear',

    '&::-webkit-scrollbar': {
      display: 'none',
    },
    '-ms-overflow-style': 'none',
    scrollbarWidth: 'none',
  },
}));

const IconsRow = (props: IconsProps) => {
  const { className, icons = ICONS } = props;
  const classes = useStyles(props);

  return (
    <div className={cn(classes.root, className)}>
      <Grid className={classes.grid} alignItems="center" spacing={20} container>
        {Array.from({ length: 20 }, () => icons)
          .flat()
          .map((icon, index) => (
            <Grid key={index} item>
              <img src={icon} alt="icon" />
            </Grid>
          ))}
      </Grid>
    </div>
  );
};

export default IconsRow;
