import cn from 'classnames';
import React from 'react';

import { makeStyles } from '@mui/styles';

import Typography, { TypographyProps } from '@libs/ui/components/typography';

interface SectionTitleProps
  extends React.PropsWithChildren<unknown>,
    TypographyProps {}

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontStyle: 'italic',
    whiteSpace: 'break-spaces',

    [theme.breakpoints.down('md')]: {
      fontSize: 20,
      lineHeight: '32px',
      whiteSpace: 'initial',
    },
  },
}));

const SectionTitle = ({ children, className, ...rest }: SectionTitleProps) => {
  const classes = useStyles();

  return (
    <Typography
      className={cn(className, classes.root)}
      align="center"
      variant="h4"
      transform="uppercase"
      fontWeight={600}
      whiteSpace="break-spaces"
      paragraph
      {...rest}
    >
      {children}
    </Typography>
  );
};

export default SectionTitle;
