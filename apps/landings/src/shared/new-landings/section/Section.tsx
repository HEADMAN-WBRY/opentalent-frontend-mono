import cn from 'classnames';
import React from 'react';

import { Container } from '@mui/material';
import { makeStyles } from '@mui/styles';

interface SectionProps extends React.PropsWithChildren<unknown> {
  color?: 'black' | 'white' | 'grey';
  classes?: {
    root?: string;
    container?: string;
  };
  overline?: React.ReactNode;
}

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(20, 0),
    background: ({ color }: SectionProps) => {
      switch (color) {
        case 'white':
          return 'white';
        case 'grey':
          return theme.palette.secondary.main;
        default:
          return theme.palette.secondary.dark;
      }
    },

    [theme.breakpoints.down('md')]: {
      padding: theme.spacing(12, 0),
    },
  },
  container: {
    [theme.breakpoints.down('md')]: {
      padding: theme.spacing(0, 4),
    },
  },
}));

const Section = (props: SectionProps) => {
  const { children, classes, overline } = props;
  const styleClasses = useStyles(props);

  return (
    <div className={cn(styleClasses.root, classes?.root)}>
      {overline}
      <Container className={cn(styleClasses.container, classes?.container)}>
        {children}
      </Container>
    </div>
  );
};

export default Section;
