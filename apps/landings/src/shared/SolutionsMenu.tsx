import { paths } from 'apps/landings/src/utils/consts';
import React from 'react';

import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Menu, MenuItem } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Button from '@libs/ui/components/button';

interface SolutionsMenuProps {}

const useStyles = makeStyles((theme) => ({
  list: {
    background: theme.palette.secondary.main,
    color: 'white',
  },
  listItem: {
    '&:hover': {
      background: ' rgba(242, 255, 136, 0.08)',
    },
  },
}));

const SolutionsMenu = (props: SolutionsMenuProps) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <Button
        id="basic-button"
        color="inherit"
        aria-controls={open ? 'basic-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick as any}
        endIcon={open ? <ExpandLessIcon /> : <ExpandMoreIcon />}
      >
        {`solutions`.toUpperCase()}
      </Button>
      <Menu
        classes={{ paper: classes.list }}
        id="basic-menu"
        color="primary"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
      >
        <MenuItem
          component="a"
          href={paths.communitySearch}
          className={classes.listItem}
        >
          Community-powered Search
        </MenuItem>
        <MenuItem
          component="a"
          href={paths.companyLanding}
          className={classes.listItem}
        >
          Direct Sourcing Hub
        </MenuItem>
      </Menu>
    </div>
  );
};

export default SolutionsMenu;
