import { StrictMode } from 'react';
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import { CssBaseline, StyledEngineProvider } from '@mui/material';
import { ThemeProvider } from '@mui/material/styles';

import { SnackbarProvider } from '@libs/ui/components/snackbar';
import defaultTheme, { Theme } from '@libs/ui/themes/v5-theme';

interface AppWrapperProps extends React.PropsWithChildren<unknown> {
  theme?: Theme;
}

const AppWrapper = ({ children = defaultTheme }: AppWrapperProps) => {
  return (
    <StrictMode>
      <BrowserRouter>
        <StyledEngineProvider injectFirst>
          <ThemeProvider theme={defaultTheme}>
            <SnackbarProvider>
              <CssBaseline />
              {children}
            </SnackbarProvider>
          </ThemeProvider>
        </StyledEngineProvider>
      </BrowserRouter>
    </StrictMode>
  );
};

export default AppWrapper;
