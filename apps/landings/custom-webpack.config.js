const path = require('path');
const { readdirSync } = require('fs');

const { merge } = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const nrwlConfig = require('@nrwl/react/plugins/webpack.js');

const getFilenameWithoutExtension = (filenameWithExtension) =>
  path.parse(filenameWithExtension).name;
const landingsDir = path.resolve(__dirname, './src/individual-pages');
const landingFileNames = readdirSync(landingsDir);
const landingEntries = landingFileNames.reduce((acc, fileName) => {
  return {
    ...acc,
    [getFilenameWithoutExtension(fileName)]: path.resolve(
      landingsDir,
      fileName,
    ),
  };
}, {});
const landingsHtmlPlugins = landingFileNames.map((fileNameWithExtension) => {
  const filename = getFilenameWithoutExtension(fileNameWithExtension);
  return new HtmlWebpackPlugin({
    filename: `${filename}.html`,
    template: path.resolve(__dirname, './src/index.html'),
    chunks: [filename],
  });
});

module.exports = (config) => {
  const defaultConfig = nrwlConfig(config);
  const final = merge(defaultConfig, {
    output: {
      publicPath: '/',
    },
    entry: landingEntries,
  });
  final.plugins.push(...landingsHtmlPlugins);

  return final;
};
