import { useCurrentUser } from 'hooks/auth';
import { formatName } from 'utils/talent';

import { Talent } from '@libs/graphql-types';

export const useHaveAccessToTalent = (talent?: Talent | null): boolean => {
  const { data: userData } = useCurrentUser();
  const needShowFullName =
    userData?.currentCompanyUser?.company?.id === talent?.source?.id ||
    !!userData?.currentCompanyUser?.company?.contract?.read_contacts_permission;

  return needShowFullName;
};

export const useTalentName = (talent?: Talent) => {
  const needShowFullName = useHaveAccessToTalent(talent);

  const name = formatName({
    firstName: talent?.first_name,
    lastName: talent?.last_name,
    hideFullName: !needShowFullName,
  });

  return name;
};
