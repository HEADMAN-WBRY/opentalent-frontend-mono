/* eslint-disable no-console */
import { getIn, useFormikContext } from 'formik';
import { useCallback } from 'react';
import { isNil } from 'utils/common';

interface UseFormikListActionsArgs<T> {
  getDefaultItem?: () => T;
  pathToList: string;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const useFormikListActions = <T extends any, I>({
  getDefaultItem,
  pathToList,
}: UseFormikListActionsArgs<I>) => {
  const { values, setFieldValue, errors } = useFormikContext<T>();
  const list: I[] = getIn(values, pathToList);
  const error = getIn(errors, pathToList);

  const onAddBunch = useCallback(
    (items: I[]) => {
      setFieldValue(pathToList, list.concat(...items));
    },
    [setFieldValue, pathToList, list],
  );

  const onAddItem = useCallback(
    (item?: I) => {
      const itemToAdd = item !== undefined ? item : getDefaultItem?.();

      if (!isNil(itemToAdd)) {
        setFieldValue(pathToList, list.concat(itemToAdd));
      } else if (!getDefaultItem) {
        console.error('No "getDefaultItem" method');
      } else {
        console.error('No item to add');
      }
    },
    [setFieldValue, pathToList, list, getDefaultItem],
  );

  const onDeleteItem = useCallback(
    (indexToDelete: number) => {
      const filteredItems = list.filter((_, index) => index !== indexToDelete);
      setFieldValue(pathToList, filteredItems);
    },
    [list, pathToList, setFieldValue],
  );

  const onToggleItem = useCallback(
    (itemToToggle: I) => {
      const filteredItems = list.filter((item) => itemToToggle !== item);

      if (filteredItems.length === list.length) {
        onAddItem(itemToToggle);
        return;
      }

      setFieldValue(pathToList, filteredItems);
    },
    [list, onAddItem, pathToList, setFieldValue],
  );

  const replaceList = useCallback(
    (newList: I[]) => {
      setFieldValue(pathToList, newList);
    },
    [pathToList, setFieldValue],
  );

  return {
    onAddBunch,
    onAddItem,
    onDeleteItem,
    list,
    onToggleItem,
    replaceList,
    error,
  };
};
