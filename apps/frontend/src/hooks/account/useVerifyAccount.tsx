import { useMutation } from '@apollo/client';
import { Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { VERIFY_TALENT_ACCOUNT } from 'graphql/talents';
import { useConfirm } from 'material-ui-confirm';
import { useSnackbar } from 'notistack';
import React from 'react';

import {
  Mutation,
  MutationVerifyTalentAccountArgs,
  Talent,
} from '@libs/graphql-types';

const useStyles = makeStyles(() => ({
  dialogClass: {
    padding: '40px',

    '& > .MuiDialogTitle-root, .MuiDialogContent-root': {
      padding: 0,
    },

    '& > .MuiDialogActions-root': {
      justifyContent: 'center',
      flexDirection: 'row-reverse',

      '& > button': {
        width: '220px',

        '&:not(:first-child)': {
          marginLeft: '0',
        },
        '&:first-child': {
          marginLeft: '24px',
        },
      },
    },
  },
  dialogTitle: {
    padding: 0,
    maxWidth: '325px',
    width: '100%',
    margin: '0 auto 24px auto',
    textAlign: 'center',
    lineHeight: '24px',
  },
  dialogContent: {
    padding: 0,
    maxWidth: '325px',
    width: '100%',
    margin: '0 auto 16px auto',
    textAlign: 'center',
    lineHeight: '20px',
  },
}));

export const useVerifyAccount = (talent: Talent, refetch?: VoidFunction) => {
  const confirm = useConfirm();
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();

  const [verify, { loading }] = useMutation<
    Mutation,
    MutationVerifyTalentAccountArgs
  >(VERIFY_TALENT_ACCOUNT, {
    onCompleted: () => {
      enqueueSnackbar('Talent account has been verified', {
        variant: 'success',
      });
      if (refetch) {
        refetch();
      }
    },
  });

  const verifyAccount = async () => {
    await confirm({
      confirmationText: 'verify account',
      content: (
        <Typography variant="subtitle2" className={classes.dialogContent}>
          {talent.first_name} ({talent.email}) is waiting for profile
          verification.
        </Typography>
      ),
      title: (
        <Typography variant="h6" className={classes.dialogTitle}>
          Verify user profile
        </Typography>
      ),
      dialogProps: {
        classes: {
          paper: classes.dialogClass,
        },
      },
      cancellationButtonProps: {
        variant: 'outlined',
        color: 'secondary',
      },
    });

    await verify({ variables: { talent_id: talent.id } });
  };

  return { verifyAccount, loading };
};
