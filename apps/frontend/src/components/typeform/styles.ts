import { makeStyles } from '@mui/styles';

import { Theme } from '@libs/ui/themes/default';

const useStyles = makeStyles((theme: Theme) => ({
  bth: {
    letterSpacing: 0,
    color: theme.palette.text.primary,
  },
}));

export default useStyles;
