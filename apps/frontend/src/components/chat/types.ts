import { UserToTalentChatMeta } from './client-to-talent';
import { JobChatMeta } from './job-chat';

export enum ChatTypes {
  UserToTalent = 'userToTalent',
  JobChannel = 'jobChannel',
}

export type ChatMetaInfo = UserToTalentChatMeta | JobChatMeta;
