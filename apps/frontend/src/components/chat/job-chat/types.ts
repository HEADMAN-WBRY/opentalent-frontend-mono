import { ChatTypes } from '../types';

export interface JobChatMeta {
  jobId: string;
  jobName?: string;
  type: ChatTypes.JobChannel;
}
