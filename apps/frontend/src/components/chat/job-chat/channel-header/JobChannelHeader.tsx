import cn from 'classnames';
import { DefaultHeaderProps } from 'components/chat/common/channel-header';
import { DEFAULT_AVATAR } from 'consts/common';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { pathManager } from 'routes';
import { useChannelStateContext } from 'stream-chat-react';
import { stopEvent } from 'utils/common';

import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import {
  Avatar,
  IconButton,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from '@mui/material';
import { makeStyles } from '@mui/styles';

import { JobChatMeta } from '../types';

interface CustomChannelHeaderProps extends DefaultHeaderProps {}

const useStyles = makeStyles((theme) => ({
  root: {
    background: 'white',
  },
  clickable: {
    cursor: 'pointer',
  },
  btn: {
    marginRight: theme.spacing(2),
    display: 'none',
    marginLeft: -10,

    [theme.breakpoints.down('md')]: {
      display: 'block',
    },
  },
}));

const DEFAULT_CHAT_VALUES = {
  title: 'Chat',
  subtitle: '',
  image: DEFAULT_AVATAR,
  link: '',
};

const useChatInfo = (): typeof DEFAULT_CHAT_VALUES => {
  const { channel } = useChannelStateContext();
  const { jobId, jobName } =
    (channel.data?.channelDetails as JobChatMeta) || {};

  if (!channel) {
    return DEFAULT_CHAT_VALUES;
  }

  const link = pathManager.company.job.generatePath({
    id: jobId,
  });
  const title =
    jobName || channel.data?.name || `Chat for job with id ${jobId}`;

  return {
    image: DEFAULT_AVATAR,
    title,
    subtitle: `Comments / chat about this job`,
    link,
  };
};

export const JobChannelHeader = ({
  toggleMobileView,
}: CustomChannelHeaderProps) => {
  const classes = useStyles();
  const history = useHistory();
  const { title, subtitle, image, link } = useChatInfo();

  const onUserClick = () => {
    if (link) {
      history.push(link);
    }
  };

  return (
    <ListItem
      onClick={onUserClick}
      classes={{ root: cn(classes.root, { [classes.clickable]: !!link }) }}
    >
      {toggleMobileView && (
        <IconButton
          className={classes.btn}
          onClick={(e) => {
            stopEvent(e);
            toggleMobileView();
          }}
          size="small"
        >
          <ChevronLeftIcon />
        </IconButton>
      )}
      <ListItemAvatar>
        <Avatar alt="Avatar" src={image} />
      </ListItemAvatar>
      <ListItemText primary={title} secondary={subtitle} />
    </ListItem>
  );
};
