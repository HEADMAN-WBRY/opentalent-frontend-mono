import React from 'react';
import { MessageList } from 'stream-chat-react';

import { CustomMessage } from './CustomMessage';
import { MASSAGE_ACTIONS } from './consts';

interface CustomMessageListProps {}

export const CustomMessageList = (props: CustomMessageListProps) => {
  return (
    <MessageList
      onMentionsClick={console.log}
      Message={CustomMessage}
      messageActions={MASSAGE_ACTIONS}
    />
  );
};
