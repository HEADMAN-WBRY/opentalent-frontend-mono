import { ChannelPreviewUIComponentProps } from 'stream-chat-react';
import { DefaultStreamChatGenerics } from 'stream-chat-react/dist/types/types';

export interface ChatSidebarItemProps
  extends ChannelPreviewUIComponentProps<DefaultStreamChatGenerics> {
  onChannelSelect?: VoidFunction;
}
