import { CustomStyles, Streami18n } from 'stream-chat-react';

export const CHAT_STYLE_VARS: CustomStyles = {
  '--grey-gainsboro': 'white',
};

export const streamI18nInstance = new Streami18n({
  language: 'en',
});
