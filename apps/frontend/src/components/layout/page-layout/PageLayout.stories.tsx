import React from 'react';

import Typography from '@libs/ui/components/typography';

import { FAKE_ITEMS } from '../drawer/consts';
import { FAKE_ACCOUNT_PROPS } from '../header/consts';
import PageLayout from './PageLayout';

export default {
  title: 'Components/PageLayout',
  component: PageLayout,
};

const PROPS = {
  headerProps: {
    accountProps: FAKE_ACCOUNT_PROPS,
    notificationsCount: 7,
    dontConnectAccount: true,
  },
  drawerProps: {
    items: FAKE_ITEMS,
  },
};

export const Default = () => (
  <div style={{ width: '100vw', height: '100vh', margin: '-1rem 0 0 -1rem' }}>
    <PageLayout {...PROPS}>
      <Typography paragraph>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Rhoncus dolor purus
        non enim praesent elementum facilisis leo vel. Risus at ultrices mi
        tempus imperdiet. Semper risus in hendrerit gravida rutrum quisque non
        tellus. Convallis convallis tellus id interdum velit laoreet id donec
        ultrices. Odio morbi quis commodo odio aenean sed adipiscing. Amet nisl
        suscipit adipiscing bibendum est ultricies integer quis. Cursus euismod
        quis viverra nibh cras. Metus vulputate eu scelerisque felis imperdiet
        proin fermentum leo. Mauris commodo quis imperdiet massa tincidunt. Cras
        tincidunt lobortis feugiat vivamus at augue. At augue eget arcu dictum
        varius duis at consectetur lorem. Velit sed ullamcorper morbi tincidunt.
        Lorem donec massa sapien faucibus et molestie ac.
      </Typography>
    </PageLayout>
  </div>
);

export const LoadingLayout = () => (
  <div style={{ width: '100vw', height: '100vh', margin: '-1rem 0 0 -1rem' }}>
    <PageLayout
      isLoading
      headerProps={{ isLoading: true }}
      drawerProps={{ isLoading: true }}
    />
  </div>
);
