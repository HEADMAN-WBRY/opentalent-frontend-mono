import { useDynamicChatDataContext } from 'components/chat/common/provider';
import { DrawerProps } from 'components/layout/drawer';
import { DrawerListItemProps } from 'components/layout/drawer/drawer-list-item';
import { HeaderProps } from 'components/layout/header';
import { useCurrentUser } from 'hooks/auth';
import { useAuth0 } from 'hooks/auth/useAuth0';
import { useMemo } from 'react';
import { formatName } from 'utils/talent';

import { Talent, User } from '@libs/graphql-types';

import {
  DEFAULT_TALENT_ITEMS,
  DEFAULT_COMPANY_ITEMS,
  NOT_ACCEPTED_TALENT_ITEMS,
  MESSENGER_ITEM,
} from './consts';

const getDrawerItems = ({
  isTalent,
  isAccepted,
}: {
  isTalent: boolean;
  isAccepted: boolean;
}): DrawerListItemProps[] => {
  if (isTalent && !isAccepted) {
    return NOT_ACCEPTED_TALENT_ITEMS;
  }
  const mainItems = isTalent ? DEFAULT_TALENT_ITEMS : DEFAULT_COMPANY_ITEMS;

  return mainItems;
};

const useConnectedDrawerProps = (): DrawerListItemProps[] => {
  const { isTalent, user } = useCurrentUser();
  const { unreadMessageCount } = useDynamicChatDataContext();
  const items = useMemo(() => {
    return getDrawerItems({
      isTalent,
      isAccepted: (user as Talent)?.is_invitation_accepted ?? false,
    });
  }, [isTalent, user]);

  return items.concat({ ...MESSENGER_ITEM, badge: unreadMessageCount });
};

export const useLayoutProps = (): {
  connectedDrawerProps: DrawerProps;
  connectedHeaderProps: HeaderProps;
} => {
  const { user: auth0User } = useAuth0();
  const { data, isTalent } = useCurrentUser();
  const drawerItems = useConnectedDrawerProps();
  const user = data?.currentTalent ?? data?.currentCompanyUser;

  const firstName = user?.first_name ?? auth0User?.given_name;
  const lastName = user?.last_name ?? auth0User?.family_name;
  const accountProps = {
    name: formatName({ firstName, lastName }),
    avatar: (user as Talent)?.avatar?.avatar ?? auth0User?.picture,
    position: isTalent
      ? (user as Talent)?.source?.name
      : (user as User)?.company?.name || 'Company user',
  };
  const connectedDrawerProps = {
    items: drawerItems,
  };

  const connectedHeaderProps: HeaderProps = { accountProps, inventionCount: 1 };

  return { connectedHeaderProps, connectedDrawerProps };
};
