import { ReactComponent as SlackIcon } from 'assets/icons/slack.svg';
import { DrawerTypeFormItem } from 'components/typeform';
import { useCurrentUser } from 'hooks/auth';
import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';

import CampaignIcon from '@mui/icons-material/Campaign';
import { Skeleton } from '@mui/lab';
import { Box } from '@mui/material';
import MuiDrawer, { DrawerProps as MuiDrawerProps } from '@mui/material/Drawer';
import Toolbar from '@mui/material/Toolbar';

import Button from '@libs/ui/components/button';

import DrawerList from './drawer-list';
import { DrawerListItemProps } from './drawer-list-item';
import useStyles from './styles';

export interface DrawerProps extends MuiDrawerProps {
  items?: DrawerListItemProps[];
  isLoading?: boolean;
}

const Drawer = (props: DrawerProps) => {
  const { items, isLoading, ...rest } = props;
  const classes = useStyles(props);
  const { isSM } = useMediaQueries();
  const { isTalent } = useCurrentUser();

  return (
    <MuiDrawer
      {...rest}
      className={classes.drawer}
      variant={isSM ? 'temporary' : 'permanent'}
      classes={{
        paper: classes.paper,
      }}
    >
      <Toolbar />
      {!isLoading && <DrawerList items={items} />}
      {isLoading &&
        Array(4)
          .fill('')
          .map((_, key) => (
            <Skeleton
              key={key as any}
              height="60px"
              className={classes.skeleton}
            />
          ))}
      <Box display="flex" flexGrow={1} alignItems="flex-end" pb={3} pl={3}>
        <Box flexGrow={1} display="flex" flexDirection="column">
          {isTalent && (
            <>
              <a
                href="https://form.typeform.com/to/IsXp4ljT"
                target="_blank"
                rel="noreferrer"
              >
                <Button
                  className={classes.contactBtn}
                  startIcon={<CampaignIcon />}
                >
                  refer a client 💰
                </Button>
              </a>
              <a
                href="https://join.slack.com/t/opentalentcommunity/shared_invite/zt-wgcocyxg-63qpP96wR5biBAF5GGmlOw"
                target="_blank"
                rel="noreferrer"
              >
                <Button
                  className={classes.contactBtn}
                  startIcon={<SlackIcon />}
                >
                  we&apos;re on Slack
                </Button>
              </a>
            </>
          )}
          <DrawerTypeFormItem />
        </Box>
      </Box>
    </MuiDrawer>
  );
};

export default Drawer;
