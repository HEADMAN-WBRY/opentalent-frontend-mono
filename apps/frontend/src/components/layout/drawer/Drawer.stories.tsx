import React from 'react';

import Drawer from './Drawer';
import { FAKE_ITEMS } from './consts';

export default {
  title: 'Components/Drawer',
  component: Drawer,
};

export const DefaultDrawer = () => <Drawer items={FAKE_ITEMS} />;
