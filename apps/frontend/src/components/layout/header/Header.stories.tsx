import React from 'react';

import Header from './Header';
import { FAKE_ACCOUNT_PROPS } from './consts';

export default {
  title: 'Components/Header',
  component: Header,
};

export const Default = () => <Header />;

export const WithAccountDefault = () => (
  <Header accountProps={FAKE_ACCOUNT_PROPS} />
);
