import { makeStyles } from '@mui/styles';
import React from 'react';

interface FixedFooterProps extends React.PropsWithChildren<unknown> {}

export const useStyles = makeStyles((theme) => ({
  bottom: {
    height: 74,
    backgroundColor: theme.palette.other.black,
    position: 'fixed',
    bottom: 0,
    left: 0,
    right: 0,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    zIndex: 2,
    padding: `0 ${theme.spacing(4)}`,
  },
  wrapper: {
    width: '100%',
    maxWidth: 600,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    '& > *': {
      flexGrow: 1,
    },
  },
}));

const FixedFooter = ({ children }: FixedFooterProps) => {
  const classes = useStyles();

  return (
    <div className={classes.bottom}>
      <div className={classes.wrapper}>{children}</div>
    </div>
  );
};

export default FixedFooter;
