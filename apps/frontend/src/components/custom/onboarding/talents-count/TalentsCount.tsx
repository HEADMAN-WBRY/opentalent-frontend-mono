import cn from 'classnames';
import { INFINITY_SIGN } from 'consts/common';
import React from 'react';

import { Card, CardContent } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { formatNumber } from '@libs/helpers/format';
import Typography from '@libs/ui/components/typography';

interface TalentsCountProps {
  count?: number | string;
  className?: string;
  isLoading?: boolean;
}

const useStyles = makeStyles(() => ({
  card: {
    maxWidth: 300,
    boxShadow: 'none',
  },
}));

const TalentsCount = ({ count, className, isLoading }: TalentsCountProps) => {
  const classes = useStyles();
  const parsedValue = typeof count === 'number' ? Number(count) : 0;
  const formattedValue = formatNumber(parsedValue);
  const isSmallCount = parsedValue < 50;
  const finalCount = isSmallCount ? 50 : formattedValue;

  return (
    <Card classes={{ root: cn(classes.card, className) }}>
      <CardContent>
        <Typography fontWeight={500} variant="h6" gutterBottom>
          {isLoading ? 'Calculating results...' : 'Results'}
        </Typography>
        {isSmallCount && (
          <Typography color="info.main" variant="subtitle1">
            Less than
          </Typography>
        )}
        <Typography color="info.main" variant="h3">
          {isLoading ? INFINITY_SIGN : finalCount}
        </Typography>
        <Typography variant="subtitle1">Candidate matches</Typography>
        <Typography variant="body2" color="text.secondary">
          Available on OpenTalent
        </Typography>
      </CardContent>
    </Card>
  );
};

export default TalentsCount;
