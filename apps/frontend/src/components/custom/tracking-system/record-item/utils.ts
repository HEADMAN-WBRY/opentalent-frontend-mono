import { formatName } from 'utils/talent';

import { AtsRecordJobMatchAction } from '@libs/graphql-types';

export const getActorNameFromRecord = (record: AtsRecordJobMatchAction) =>
  formatName({
    firstName: record.actor?.first_name,
    lastName: record.actor?.last_name,
  }) || '[no name]';

export const getTalentNameFromRecord = (record: AtsRecordJobMatchAction) => {
  const talent = record.job_match_action?.job_match?.talent;
  const name =
    formatName({
      firstName: talent?.first_name,
      lastName: talent?.last_name,
    }) || '[no name]';

  return name;
};

export const getJobFromRecord = (record: AtsRecordJobMatchAction) =>
  record.job_match_action?.job_match?.job;
