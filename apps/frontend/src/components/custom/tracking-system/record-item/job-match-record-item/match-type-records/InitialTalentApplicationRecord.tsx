import { Grid } from '@mui/material';
import React from 'react';
import { pathManager } from 'routes';

import Typography, { RouterLink } from '@libs/ui/components/typography';

import DefaultRecordItem from '../../DefaultRecordItem';
import { getActorNameFromRecord, getJobFromRecord } from '../../utils';
import ChipsComponent from '../ChipsComponent';
import { JobMatchRecordItemProps } from '../types';

interface InitialTalentApplicationRecordProps extends JobMatchRecordItemProps {}

export const InitialTalentApplicationRecord = ({
  record,
}: InitialTalentApplicationRecordProps) => {
  const talentName = getActorNameFromRecord(record);
  const job = getJobFromRecord(record);

  return (
    <DefaultRecordItem
      record={record}
      status={
        <Typography variant="body2" color="textSecondary">
          {talentName} applied for a job{' '}
          <RouterLink
            to={pathManager.company.job.generatePath({ id: job?.id || '' })}
          >
            {job?.name || '[no_name_job]'}
          </RouterLink>
        </Typography>
      }
    >
      <Grid alignItems="center" spacing={2} container>
        <ChipsComponent record={record} />
      </Grid>
    </DefaultRecordItem>
  );
};
