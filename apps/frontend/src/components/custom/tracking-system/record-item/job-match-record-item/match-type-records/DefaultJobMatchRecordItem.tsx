import React from 'react';
import { pathManager } from 'routes';

import { Box, Grid } from '@mui/material';

import Typography, { RouterLink } from '@libs/ui/components/typography';

import DefaultRecordItem from '../../DefaultRecordItem';
import { getActorNameFromRecord, getJobFromRecord } from '../../utils';
import ChipsComponent from '../ChipsComponent';
import { JobMatchRecordItemProps } from '../types';

interface DefaultJobMatchRecordItemProps extends JobMatchRecordItemProps {}

export const DefaultJobMatchRecordItem = ({
  record,
}: DefaultJobMatchRecordItemProps) => {
  const actorName = getActorNameFromRecord(record);
  const job = getJobFromRecord(record);
  const link = job?.id
    ? pathManager.company.job.generatePath({ id: job?.id })
    : '#';

  return (
    <DefaultRecordItem
      record={record}
      status={
        <Typography variant="body2" color="textSecondary">
          {actorName} changed status
        </Typography>
      }
    >
      <Grid alignItems="center" spacing={2} container>
        <ChipsComponent record={record} />
        <Grid item>
          <Box pt={1}>
            <Typography component="span" color="textSecondary">
              for
            </Typography>{' '}
            <RouterLink to={link}>{job?.name || '[no_name_job]'}</RouterLink>
          </Box>
        </Grid>
      </Grid>
    </DefaultRecordItem>
  );
};
