import { Grid } from '@mui/material';
import React from 'react';
import { pathManager } from 'routes';

import Typography, { RouterLink } from '@libs/ui/components/typography';

import DefaultRecordItem from '../../DefaultRecordItem';
import { getJobFromRecord, getActorNameFromRecord } from '../../utils';
import ChipsComponent from '../ChipsComponent';
import { JobMatchRecordItemProps } from '../types';

interface ApplicationRecordProps extends JobMatchRecordItemProps {}

export const ApplicationRecord = ({ record }: ApplicationRecordProps) => {
  const talentName = getActorNameFromRecord(record);
  const job = getJobFromRecord(record);

  return (
    <DefaultRecordItem
      record={record}
      status={
        <Typography variant="body2" color="textSecondary">
          {talentName} applied for a{' '}
          <RouterLink
            to={pathManager.company.job.generatePath({ id: job?.id || '' })}
          >
            {job?.name || '[no_name_job]'}
          </RouterLink>
        </Typography>
      }
    >
      <Grid alignItems="center" spacing={2} container>
        <ChipsComponent record={record} />
      </Grid>
    </DefaultRecordItem>
  );
};
