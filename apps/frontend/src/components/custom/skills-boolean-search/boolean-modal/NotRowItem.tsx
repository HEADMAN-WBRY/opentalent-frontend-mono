import { renderTags } from 'components/custom/skill-select/SkillSelect';
import React from 'react';

import { Box, Grid, Typography } from '@mui/material';

import { SkillTypeEnum } from '@libs/graphql-types';
import { modelPath } from '@libs/helpers/form';
import { ConnectedMultipleSelect } from '@libs/ui/components/form/multiple-select';

import { BooleanModalState } from '../types';
import { useSkillsSuggest } from './hooks';

interface NotRowItemProps {}

const NotRowItem = (props: NotRowItemProps) => {
  const { loading, createOnChangeHandler, skillsOptions } = useSkillsSuggest();

  return (
    <>
      <Box pb={4}>
        <Typography textAlign="center" textTransform="uppercase">
          NOT
        </Typography>
      </Box>
      <Box pr={14} pl={6}>
        <Grid flexWrap="nowrap" spacing={4} container>
          <Grid
            display="flex"
            alignItems="center"
            style={{ minWidth: 140 }}
            item
          >
            <Typography variant="body2">Not these skills</Typography>
          </Grid>
          <Grid flexGrow={1} item>
            <ConnectedMultipleSelect
              options={skillsOptions}
              name={modelPath<BooleanModalState>((m) => m.not)}
              loading={loading}
              chipProps={{
                size: 'small',
              }}
              renderTags={renderTags}
              autoCompleteProps={{
                loading,
                filterSelectedOptions: true,
                popupIcon: null,
              }}
              inputProps={{
                onChange: createOnChangeHandler([
                  SkillTypeEnum.HardSkills,
                  SkillTypeEnum.SoftSkills,
                  SkillTypeEnum.Solutions,
                ]),
                variant: 'filled',
                label: 'Enter Technologies, Hard Skills & Soft Skills',
                name: 'skills',
              }}
            />
          </Grid>
        </Grid>
      </Box>
    </>
  );
};

export default NotRowItem;
