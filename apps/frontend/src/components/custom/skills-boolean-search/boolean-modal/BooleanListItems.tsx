import { useFormikListActions } from 'hooks/form';
import React from 'react';

import { Box } from '@mui/material';

import { BooleanOperatorsEnum } from '@libs/graphql-types';
import { modelPath } from '@libs/helpers/form';

import { BooleanModalState, DefaultSkillsRowItem } from '../types';
import BooleanRowItem from './BooleanRowItem';

interface BooleanListItemsProps {
  items: DefaultSkillsRowItem[];
}

const getDefaultListItem = (): DefaultSkillsRowItem => ({
  or: [],
  and: [],
  relationWithNext: BooleanOperatorsEnum.And,
});

const BooleanListItems = ({ items }: BooleanListItemsProps) => {
  const { onAddItem, onDeleteItem } = useFormikListActions({
    getDefaultItem: getDefaultListItem,
    pathToList: modelPath<BooleanModalState>((m) => m.items),
  });

  return (
    <>
      {items.map((item, index, items) => (
        <Box pb={4} key={index}>
          <BooleanRowItem
            onDelete={items.length > 1 ? onDeleteItem : undefined}
            onAdd={index === items.length - 1 ? onAddItem : undefined}
            index={index}
            hideRelation={items.length - 1 === index}
          />
        </Box>
      ))}
    </>
  );
};

export default BooleanListItems;
