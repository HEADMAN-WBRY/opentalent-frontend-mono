import { useFormikContext } from 'formik';
import { useMemo, useState } from 'react';

import { debounce } from '@mui/material';

import { SkillTypeEnum, useSearchSkillsLazyQuery } from '@libs/graphql-types';
import { OptionType } from '@libs/ui/components/form/select';

export const useSkillsSuggest = () => {
  const [skillsOptions, setOptions] = useState<OptionType[]>([]);
  const [suggest, { loading }] = useSearchSkillsLazyQuery({
    onCompleted: ({ skillSearch }) => {
      const options =
        skillSearch?.map((i) => ({
          ...i,
          text: i.name,
          value: i.id,
        })) || [];

      setOptions(options);
    },
  });
  const debouncedSuggest = useMemo(() => debounce(suggest, 1000), [suggest]);
  const createOnChangeHandler =
    (types: SkillTypeEnum[] = []): React.ChangeEventHandler<HTMLInputElement> =>
    async (e) => {
      const search = e.target.value;

      if (search.length < 2) {
        return;
      }

      setOptions([]);

      await debouncedSuggest({
        variables: {
          search,
          skill_type: types,
        },
      });
    };

  const onFocus = () => {
    setOptions([]);
  };
  return { skillsOptions, loading, createOnChangeHandler, onFocus };
};

export const useSelectSkill = ({
  onChange,
}: {
  onChange: (val: any) => void;
}) => {
  const { setFieldValue } = useFormikContext();

  const onAutocompleteChange = (name: string) => (options: OptionType[]) => {
    setFieldValue(name, options);
  };

  return { onAutocompleteChange };
};
