import { getIn, useFormikContext } from 'formik';
import React from 'react';
import { CreateJobForm } from 'screens/company-user/job-form/types';

import { makeStyles } from '@mui/styles';

import { modelPath } from '@libs/helpers/form';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import { useOpenBooleanModal } from '../boolean-modal';
import BooleanSearchFormula from '../boolean-modal/BooleanSearchFormula';
import { isBooleanSearchEmpty } from '../utils';

interface BooleanControlProps {
  disabled?: boolean;
}

const useStyles = makeStyles((theme) => ({
  root: {
    textAlign: 'left',
    paddingBottom: theme.spacing(2),

    '& > div': {
      textAlign: 'left',
    },
  },
}));

export const BooleanControl = ({ disabled }: BooleanControlProps) => {
  const openBooleanModal = useOpenBooleanModal();
  const { values, errors } = useFormikContext<CreateJobForm>();
  const classes = useStyles();
  const path = modelPath<CreateJobForm>((m) => m.skills_boolean_v2);
  const hasError = getIn(errors, path);
  const currentState = getIn(values, path);
  const isEmpty = isBooleanSearchEmpty(currentState);

  return (
    <>
      {isEmpty ? (
        <Typography variant="body2" paragraph>
          Your search query is empty.
        </Typography>
      ) : (
        <BooleanSearchFormula className={classes.root} state={currentState} />
      )}

      <Button
        disabled={disabled}
        onClick={() => openBooleanModal({ initialValues: currentState })}
        variant="outlined"
        color={hasError ? 'error' : 'info'}
      >
        {isEmpty ? 'Create boolean query' : 'Edit boolean query'}
      </Button>
    </>
  );
};
