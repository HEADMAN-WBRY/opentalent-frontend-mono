export enum TalentModals {
  InviteByCompany = 'invite',
  CreateCompany = 'createCompany',
  LimitedAccess = 'access',

  Chat = 'chat',
  StartChatting = 'startChat',
}
