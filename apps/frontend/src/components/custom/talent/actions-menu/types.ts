import { CheckCircle } from '@mui/icons-material';

export interface ActionItem {
  text: string;
  Icon: typeof CheckCircle;
  onClick: VoidFunction;
  disabled?: boolean;
}

export interface ActionsMenuProps {
  open: boolean;
  anchorEl: any;
  handleClose: VoidFunction;
  actions: ActionItem[];
  hideDisabled?: boolean;
}
