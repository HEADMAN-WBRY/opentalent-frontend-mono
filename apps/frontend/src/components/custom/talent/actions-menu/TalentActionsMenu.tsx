import React from 'react';
import { noop } from 'utils/common';

import { Talent } from '@libs/graphql-types';

import ActionsMenu from './ActionsMenu';
import { useTalentActions } from './hooks/hooks';
import { ActionsMenuProps } from './types';

interface TalentActionsMenuProps extends Omit<ActionsMenuProps, 'actions'> {
  talent: Talent;
  onSuccess?: VoidFunction;
}

export const TalentActionsMenu = ({
  talent,
  onSuccess = noop,
  ...rest
}: TalentActionsMenuProps) => {
  const action = useTalentActions({ talent, onSuccess });

  return <ActionsMenu {...rest} actions={action} />;
};
