import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Formik } from 'formik';
import React from 'react';
import { mapJobsToOptions } from 'screens/profile/modals/utils';
import { DefaultModalProps, withLocationStateModal } from 'utils/modals';
import * as yup from 'yup';

import Button from '@libs/ui/components/button';
import { ConnectedGraphSelect } from '@libs/ui/components/form/select';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import { DefaultModal } from '@libs/ui/components/modals';
import Typography from '@libs/ui/components/typography';

import { GET_JOBS } from '../../queries';
import { TalentModals } from '../../types';
import { useSubmitHandler } from './hooks';
import { FormModel, InviteByCompanyModalData } from './types';

const useStyles = makeStyles(() => ({
  paper: {
    width: 620,
  },
  content: {
    textAlign: 'left',
  },
}));

const validation = yup.object().shape({
  message: yup.string().nullable().trim().required(),
  job_id: yup.string().nullable().trim().required(),
});

const InviteByCompanyModalComponent = ({
  modalData,
  isOpen,
  close,
}: DefaultModalProps<InviteByCompanyModalData>) => {
  const classes = useStyles();
  const talentId = modalData?.talentId || '';
  const talentName = modalData?.talentName || '';
  const title = talentName ? `Invite ${talentName} to Job` : 'Invite to Apply';
  const { onSubmit, loading } = useSubmitHandler({
    talentId,
    handleClose: close,
  });

  return (
    <Formik<FormModel>
      initialValues={{ job_id: '', message: '' }}
      onSubmit={onSubmit as any}
      validationSchema={validation}
      key={talentId}
    >
      {({ handleSubmit }) => (
        <DefaultModal
          actions={
            <Button
              fullWidth
              color="primary"
              variant="contained"
              onClick={() => handleSubmit()}
              disabled={loading}
            >
              invite
            </Button>
          }
          handleClose={close}
          open={isOpen}
          title={title}
          className={classes.paper}
        >
          <Box pb={6}>
            <Typography>
              Select a job and invite this person to apply.
            </Typography>
          </Box>

          <Grid
            className={classes.content}
            container
            spacing={4}
            direction="column"
          >
            <Grid item>
              <ConnectedGraphSelect
                query={GET_JOBS}
                dataPath="availableJobsForTalent"
                dataMap={mapJobsToOptions}
                formControlProps={{ size: 'small' }}
                fullWidth
                name="job_id"
                variant="filled"
                label="Job name"
                queryOptions={{
                  variables: {
                    talent_id: talentId,
                  },
                }}
              />
            </Grid>
            <Grid item>
              <ConnectedTextField
                fullWidth
                size="small"
                variant="filled"
                multiline
                label="Write a message"
                name="message"
                rows={6}
              />
            </Grid>
          </Grid>
        </DefaultModal>
      )}
    </Formik>
  );
};

export const InviteByCompanyModal =
  withLocationStateModal<InviteByCompanyModalData>({
    id: TalentModals.InviteByCompany,
  })(InviteByCompanyModalComponent);
