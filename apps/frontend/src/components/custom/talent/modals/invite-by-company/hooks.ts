import { useMutation } from '@apollo/client';
import { useSnackbar } from 'notistack';

import { Mutation, MutationInviteToJobArgs } from '@libs/graphql-types';

import { INVITE_TO_JOB } from '../../queries';
import { FormModel } from './types';

export const useSubmitHandler = ({
  talentId,
  handleClose,
}: {
  talentId: string;
  handleClose: VoidFunction;
}) => {
  const { enqueueSnackbar } = useSnackbar();
  const [invite, { loading }] = useMutation<Mutation, MutationInviteToJobArgs>(
    INVITE_TO_JOB,
    {
      onCompleted: () => {
        enqueueSnackbar('Talent successfully invited!', { variant: 'success' });
        handleClose();
      },
    },
  );
  const onSubmit = (values: FormModel) => {
    invite({ variables: { ...values, talent_id: talentId } });
  };
  return { loading, onSubmit };
};
