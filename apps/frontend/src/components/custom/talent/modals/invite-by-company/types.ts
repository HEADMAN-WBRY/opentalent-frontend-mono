export interface FormModel {
  job_id: string;
  message: string;
}

export interface InviteByCompanyModalData {
  talentId: string;
  talentName: string;
}
