import WarningIcon from '@mui/icons-material/Warning';
import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';
import React from 'react';

import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

interface SuggestSkillActionProps {
  onCreate: VoidFunction;
}

const useStyles = makeStyles((theme) => ({
  noOption: {
    display: 'flex',
    alignItems: 'center',
  },
  noOptionText: {
    marginLeft: '12px',
    marginRight: '20px',
  },
  noOptionIcon: {
    color: theme.palette.warning.main,
  },
}));

export const SuggestSkillAction = ({ onCreate }: SuggestSkillActionProps) => {
  const classes = useStyles();

  return (
    <Box className={classes.noOption}>
      <WarningIcon className={classes.noOptionIcon} />
      <Typography className={classes.noOptionText}>Skill not found</Typography>
      <Button variant="text" color="tertiary" onMouseDown={onCreate}>
        Suggest a skill
      </Button>
    </Box>
  );
};
