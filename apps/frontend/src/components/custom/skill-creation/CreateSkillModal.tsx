import ErrorIcon from '@mui/icons-material/Error';
import {
  Box,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Tooltip,
} from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Formik } from 'formik';
import React from 'react';
import * as yup from 'yup';

import Button from '@libs/ui/components/button';
import { ConnectedSelect } from '@libs/ui/components/form/select';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import Typography from '@libs/ui/components/typography';

import { CreateSkillFields } from './types';

export const SKILL_TYPE_OPTIONS = [
  {
    text: 'Technologies',
    value: 'SOLUTIONS',
  },
  {
    text: 'Hard Skill',
    value: 'HARD_SKILLS',
  },
  {
    text: 'Soft Skill',
    value: 'SOFT_SKILLS',
  },
];

interface CreateSkillModalProps {
  close: VoidFunction;
  initialValues: CreateSkillFields;
  isOpen: boolean;
  onSubmit: any;
}

const validator = yup.object({
  skill_type: yup.string().required(),
  name: yup.string().required(),
  reason: yup.string().max(300),
});

const useStyles = makeStyles((theme) => ({
  dialogClass: {
    padding: '40px 24px',
    width: '500px',

    '& > .MuiDialogTitle-root, .MuiDialogContent-root': {
      padding: 0,
      overflow: 'hidden',
    },

    '& > .MuiDialogActions-root': {
      padding: 0,
    },

    '& .MuiFormControl-root': {
      marginBottom: theme.spacing(4),
    },
  },
  dialogTitle: {
    padding: 0,
    width: '100%',
    marginBottom: theme.spacing(4),
    fontSize: 20,
    lineHeight: '32px',
    fontWeight: 500,
  },
  dialogContent: {
    padding: 0,
    width: '100%',
  },
  confirmBtn: {},
  dialogDesc: {
    marginBottom: theme.spacing(4),
  },
  tooltipIcon: {
    fontSize: 16,
    height: '24px',
    verticalAlign: 'middle',
    marginLeft: '6px',
    color: theme.palette.text.secondary,
  },
}));

export const CreateSkillModal = ({
  close,
  initialValues,
  isOpen,
  onSubmit,
}: CreateSkillModalProps) => {
  const classes = useStyles();

  return (
    <Formik
      onSubmit={onSubmit}
      initialValues={initialValues}
      validationSchema={validator}
      enableReinitialize
      validateOnMount
    >
      {({ isValid, handleSubmit }) => {
        return (
          <Dialog
            open={isOpen}
            onClose={close}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            classes={{
              paper: classes.dialogClass,
            }}
          >
            <DialogTitle>
              <Typography className={classes.dialogTitle}>
                Suggest a new skill
                <Tooltip
                  title={`You can suggest a new technology experience or hard skill missed in our database if you think it’s relevant to your expertise.
                  Notice that Technology Knowledge are technical knowledge of a particular software (e.g. Figma, Jira, etc.) or technology you use in your work (e.g. HTML5, React, Vue.js, etc.)
                  Hard Skills are technical knowledge or training that you have gained through any life experience, including in your career or education (e.g. UX design, Web Development, etc.).`}
                >
                  <ErrorIcon fontSize="small" className={classes.tooltipIcon} />
                </Tooltip>
              </Typography>
            </DialogTitle>
            <DialogContent>
              <Box className={classes.dialogContent}>
                <Typography className={classes.dialogDesc}>
                  If we found the skill suitable we will add it to your Skills
                  Passport within 48 hours.
                </Typography>
                <ConnectedSelect
                  name="skill_type"
                  hideNoneValue
                  options={SKILL_TYPE_OPTIONS}
                  fullWidth
                  variant="filled"
                  label="Choose skill type"
                  helperText="Is it a Technology or Hard skill?"
                />
                <ConnectedTextField
                  name="name"
                  label="Skill name"
                  variant="filled"
                  fullWidth
                  size="small"
                />
                <ConnectedTextField
                  name="reason"
                  label="Why are you suggesting this skill?"
                  variant="filled"
                  multiline
                  rows={5}
                  fullWidth
                  size="small"
                  helperText="You can include links, e.g. https://reactjs.org/"
                />
              </Box>
            </DialogContent>
            <DialogActions>
              <Button
                className={classes.confirmBtn}
                fullWidth
                color="primary"
                variant="contained"
                disabled={!isValid}
                onClick={handleSubmit}
              >
                Suggest a skill
              </Button>
              <Button
                className={classes.confirmBtn}
                fullWidth
                onClick={close}
                variant="outlined"
              >
                Cancel
              </Button>
            </DialogActions>
          </Dialog>
        );
      }}
    </Formik>
  );
};
