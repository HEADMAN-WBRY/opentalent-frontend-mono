export interface SelectImageComponentProps {
  label?: string;
  Icon?: React.ElementType;

  onChange?: (file: File | null) => void;
  error?: boolean;
  name: string;

  uploaded?: boolean;
  loading?: boolean;
  imgSrc?: string;
  dark?: boolean;
}
