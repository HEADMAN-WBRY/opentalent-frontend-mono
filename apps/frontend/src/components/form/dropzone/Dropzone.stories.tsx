import { Box, Grid } from '@mui/material';
import { Formik } from 'formik';
import React from 'react';
import { noop } from 'utils/common';
import * as yup from 'yup';

import Button from '@libs/ui/components/button';

import ConnectedDropzone from './ConnectedDropzone';
import Dropzone from './Dropzone';

export default {
  title: 'Form/Dropzone',
  component: Dropzone,
};

export const Default = () => {
  return (
    <>
      <Box mb={2}>
        <Dropzone label="Upload some document. Drop it here" />
      </Box>
    </>
  );
};

const validation = yup.object().shape({
  dropzone: yup.string().required(),
});

export const BasicConnectedDropzone = () => {
  return (
    <Formik
      onSubmit={noop}
      validationSchema={validation}
      initialValues={{ dropzone: null, alo: null }}
    >
      {({ handleSubmit }) => {
        return (
          <Grid direction="column" container>
            <Grid item>
              <ConnectedDropzone name="dropzone" />
            </Grid>
            <br />
            <Grid item>
              <Button variant="contained" onClick={handleSubmit}>
                Submit
              </Button>
            </Grid>
          </Grid>
        );
      }}
    </Formik>
  );
};
