import { useScrollToFirstError } from 'hooks/form';

const ScrollToFirstError = (params: { delay?: number }) => {
  useScrollToFirstError(params);
  return null;
};

export default ScrollToFirstError;
