import { gql } from '@apollo/client';

export default gql`
  query ChatMessages($talent_id: ID!, $first: Int = 10, $page: Int) {
    chatMessages(talent_id: $talent_id, first: $first, page: $page) {
      paginatorInfo {
        count
        currentPage
        firstItem
        hasMorePages
        lastItem
        lastPage
        perPage
        total
      }
      data {
        id
        parent_id
        user {
          id
          first_name
          last_name
        }
        company_id
        talent_id
        message
        created_at
        updated_at
      }
    }
  }
`;
