import { gql } from '@apollo/client';

export default gql`
  mutation CreateChatMessage(
    $talent_id: ID!
    $message: String!
    $parent_id: ID = 0
  ) {
    createChatMessage(
      talent_id: $talent_id
      message: $message
      parent_id: $parent_id
    ) {
      id
      parent_id
      user {
        id
        first_name
        last_name
      }
      company_id
      talent_id
      message
      created_at
      updated_at
    }
  }
`;
