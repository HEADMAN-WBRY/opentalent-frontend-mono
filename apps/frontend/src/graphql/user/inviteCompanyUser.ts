import { gql } from '@apollo/client';

export default gql`
  mutation InviteCompanyUser($email: Email!) {
    inviteCompanyUser(email: $email)
  }
`;
