import { gql } from '@apollo/client';

export default gql`
  mutation CreateUntrustedTalentProfile(
    $email: Email!
    $general_info: TalentGeneralInfoInput!
    $source_company_id: ID
    $apply_to_job_id: ID
  ) {
    createUntrustedTalentProfile(
      email: $email
      general_info: $general_info
      source_company_id: $source_company_id
      apply_to_job_id: $apply_to_job_id
    )
  }
`;
