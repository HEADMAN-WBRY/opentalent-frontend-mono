import { gql } from '@apollo/client';

export const SET_TMP_DATA = gql`
  mutation SetTmpData($email: Email, $device_id: String!, $data: String) {
    setTempDataItem(email: $email, device_id: $device_id, data: $data)
  }
`;

export const GET_TMP_DATA = gql`
  query GetTmpData($email: Email, $device_id: String!) {
    getTempDataItem(email: $email, device_id: $device_id) {
      success
      result {
        email
        device_id
        data
      }
    }
  }
`;
