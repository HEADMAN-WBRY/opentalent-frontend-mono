import { gql } from '@apollo/client';

export default gql`
  fragment FullJobMatch on JobMatch {
    job_invitation {
      is_declined
    }
    job_application {
      pitch
    }
    cv_skills_keywords {
      id
      skill_type
      name
    }
    id
    talent {
      first_name
      last_name
      id
      email
      address
      recent_position_title
      location
      rate_max
      rate_min
      is_invitation_accepted
      available_now
      avatar {
        avatar
      }
      category {
        id
        name
      }
      subcategories {
        id
        name
      }
    }
    matched_skills {
      id
      name
      skill_type
    }
    match_percent
    match_type
    is_instant_match
    is_applied
    is_invited
    is_shortlist
    is_new
  }
`;
