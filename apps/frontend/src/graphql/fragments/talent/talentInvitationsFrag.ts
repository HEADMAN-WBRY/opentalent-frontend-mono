import { gql } from '@apollo/client';

export const GET_INVITATION_INFO_FRAG = gql`
  fragment FullInviteInfo on InviteInfo {
    invitations_left
    invited_talents {
      id
      email
      first_name
      last_name
      is_invitation_accepted
      avatar {
        avatar
      }
    }
  }
`;
