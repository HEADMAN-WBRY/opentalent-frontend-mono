import { gql } from '@apollo/client';

export default gql`
  mutation DeclineTalentAccount($talent_id: ID!) {
    declineTalentAccount(talent_id: $talent_id)
  }
`;
