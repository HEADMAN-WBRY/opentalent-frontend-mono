import { gql } from '@apollo/client';

export default gql`
  mutation VerifyTalentAccount($talent_id: ID!) {
    verifyTalentAccount(talent_id: $talent_id)
  }
`;
