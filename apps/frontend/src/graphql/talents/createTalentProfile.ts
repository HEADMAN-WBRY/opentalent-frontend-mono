import { gql } from '@apollo/client';

export default gql`
  mutation CreateTalentProfile(
    $user_id: ID!
    $general_info: TalentGeneralInfoInput!
    $email: Email!
    $is_ot_pool: Boolean
    $tags_ids: [ID!]
  ) {
    createTalentProfile(
      user_id: $user_id
      tags_ids: $tags_ids
      general_info: $general_info
      email: $email
      is_ot_pool: $is_ot_pool
    )
  }
`;
