import { gql } from '@apollo/client';

export default gql`
  query TalentSources {
    talentSources {
      id
      name
      created_at
      updated_at
    }
  }
`;
