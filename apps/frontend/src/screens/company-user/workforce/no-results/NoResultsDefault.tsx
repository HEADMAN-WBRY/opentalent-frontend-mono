import { Box } from '@mui/material';
import { ReactComponent as NoData } from 'assets/icons/nodata.svg';
import React from 'react';

import Typography from '@libs/ui/components/typography';

interface NoResultsDefaultProps {}

export const NoResultsDefault = (props: NoResultsDefaultProps) => {
  return (
    <Box>
      <Typography paragraph variant="h6">
        No results found
      </Typography>
      <Typography paragraph variant="body1">
        Try to apply other filters or broader the search with OpenTalent network
      </Typography>
      <Box pt={4}>
        <NoData />
      </Box>
    </Box>
  );
};
