import { useFormikContext } from 'formik';
import React, { useCallback } from 'react';
import { Link } from 'react-router-dom';
import { pathManager } from 'routes';

import { Box } from '@mui/material';

import Typography from '@libs/ui/components/typography';

import { OPENTALENT_CIRCLE_OPTION } from '../search-filter/consts';

interface NoWorkforceResultsProps {}

const useChangeFilter = () => {
  const { setFieldValue } = useFormikContext();
  return useCallback(() => {
    setFieldValue('source_type', OPENTALENT_CIRCLE_OPTION);
  }, [setFieldValue]);
};

export const NoWorkforceResults = (props: NoWorkforceResultsProps) => {
  const onClick = useChangeFilter();
  return (
    <Box maxWidth="570px">
      <Typography paragraph variant="h6">
        There' no one here yet!
      </Typography>
      <Typography paragraph variant="body2">
        You can{' '}
        <Typography component="span" color="tertiary" variant="body2">
          <Link to={pathManager.company.createProfile.generatePath()}>
            invite
          </Link>
        </Typography>{' '}
        someone to join your workforce, or you can search for people within the{' '}
        <Typography
          style={{ cursor: 'pointer' }}
          onClick={onClick}
          component="span"
          color="tertiary"
          variant="body2"
        >
          OpenTalent network
        </Typography>
        .
      </Typography>
    </Box>
  );
};
