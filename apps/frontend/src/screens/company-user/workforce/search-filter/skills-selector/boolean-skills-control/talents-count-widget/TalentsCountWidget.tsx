import TalentsCount from 'components/custom/onboarding/talents-count';
import { INFINITY_SIGN } from 'consts/common';
import React from 'react';
import { isNil } from 'utils/common';

import { makeStyles } from '@mui/styles';

interface TalentsCountWidgetProps {
  talentsCount?: number;
}

const useStyles = makeStyles((theme) => ({
  card: {
    position: 'fixed',
    top: 94,
    right: 24,
    zIndex: 1400,
  },
}));

const TalentsCountWidget = ({ talentsCount }: TalentsCountWidgetProps) => {
  const classes = useStyles();
  const finalCount = isNil(talentsCount) ? INFINITY_SIGN : talentsCount || 0;

  return <TalentsCount className={classes.card} count={finalCount} />;
};

export default TalentsCountWidget;
