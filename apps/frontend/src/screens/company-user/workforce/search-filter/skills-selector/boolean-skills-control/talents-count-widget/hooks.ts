import { BooleanModalState } from 'components/custom/skills-boolean-search';
import { useFormikContext } from 'formik';
import { useCallback } from 'react';

import { useSearchTalentsLazyQuery } from '@libs/graphql-types';

import { FilterValues } from '../../../consts';
import { mapFormValuesToQueryVariables } from '../../../utils';

export const useTalentCountRequest = () => {
  const { values } = useFormikContext<FilterValues>();
  const [request, { data }] = useSearchTalentsLazyQuery({
    fetchPolicy: 'network-only',
  });

  const getTalents = useCallback(
    async (booleanSearch: BooleanModalState) => {
      const variables = mapFormValuesToQueryVariables({
        ...values,
        booleanSearch,
        isBooleanSkills: true,
      });
      await request({ variables });
    },
    [request, values],
  );

  const talentsCount = data?.talentsSearch?.custom_paginator_info?.total;

  return { getTalents, talentsCount };
};
