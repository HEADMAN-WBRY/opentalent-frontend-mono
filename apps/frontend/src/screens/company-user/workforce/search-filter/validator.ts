import * as yup from 'yup';

export default yup.object().shape({
  search: yup.string().nullable().min(3, 'Must be at least 3 characters'),
  category_ids: yup.array().of(yup.object()),
});
