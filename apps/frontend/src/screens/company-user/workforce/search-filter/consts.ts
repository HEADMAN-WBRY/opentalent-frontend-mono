import { BooleanModalState } from 'components/custom/skills-boolean-search';

import { SourceTypeEnum } from '@libs/graphql-types';
import { COUNTRY_OPTIONS } from '@libs/helpers/consts/countries';
import { OptionType } from '@libs/ui/components/form/select';

export const OPENTALENT_CIRCLE_OPTION = {
  text: 'OpenTalent',
  value: SourceTypeEnum.Opentalent,
};
export const CIRCLE_OPTIONS = [
  { text: 'My Workforce', value: SourceTypeEnum.Own },
  OPENTALENT_CIRCLE_OPTION,
];

export const getCircleOptions = (name: string) => [
  { text: name, value: SourceTypeEnum.Own },
  OPENTALENT_CIRCLE_OPTION,
];

export const DEFAULT_SKILLS: BooleanModalState = {
  items: [{ or: [], and: [], relationWithNext: 'AND' }],
  not: [],
};

export interface FilterValues {
  search?: string;
  category_ids?: OptionType[];
  skills_ids?: OptionType[];
  is_active?: boolean;
  is_inactive?: boolean;
  is_recruiter?: boolean;
  min_rate?: number;
  max_rate?: number;
  source_type: OptionType;
  available_now?: boolean;
  is_verification_required?: boolean;
  page?: number;
  country?: string;
  tags_ids?: OptionType[];

  isBooleanSkills?: boolean;
  booleanSearch?: BooleanModalState;
}

export const INITIAL_VALUES: FilterValues = {
  search: '',
  category_ids: [],
  skills_ids: [],
  is_active: true,
  is_inactive: false,
  source_type: CIRCLE_OPTIONS[0],
  available_now: false,
  is_verification_required: false,
};

export const EUROPE_COUNTRY = 'Europe';

export const COUNTRY_OPTIONS_CUSTOM = [
  { text: 'Europe', value: EUROPE_COUNTRY },
  ...COUNTRY_OPTIONS,
];
