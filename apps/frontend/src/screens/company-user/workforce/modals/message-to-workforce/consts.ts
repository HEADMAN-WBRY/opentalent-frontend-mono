import { makeOptionsFromObject } from '@libs/ui/components/form/select/utils';

export const MESSAGE_TYPES = {
  NO_REPLY: 'Announcement (no reply needed)',
  WITH_REPLY: 'Input request',
};

export const MESSAGE_TYPES_OPTIONS = makeOptionsFromObject(MESSAGE_TYPES);
