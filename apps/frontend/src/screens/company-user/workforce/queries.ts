import { gql } from '@apollo/client';

export const SEARCH_TALENTS = gql`
  query GetTalents(
    $search: String
    $country: String
    $category_ids: [ID]
    $skills_ids: [ID]
    $max_rate: Float
    $min_rate: Float
    $source_type: SourceTypeEnum
    $is_active: Boolean
    $is_recruiter: Boolean
    $is_verification_required: Boolean
    $first: Int = 10
    $page: Int
    $available_now: Boolean
    $tags_ids: [ID]
    $skills_boolean_filter: TalentsBooleanSkillsFilterInput
  ) {
    talents(source_type: OWN, is_verification_required: true) {
      paginatorInfo {
        total
      }
    }
    talentsSearch(
      country: $country
      search_string: $search
      category_ids: $category_ids
      skills_ids: $skills_ids
      max_rate: $max_rate
      min_rate: $min_rate
      source_type: $source_type
      is_active: $is_active
      first: $first
      available_now: $available_now
      page: $page
      is_verification_required: $is_verification_required
      skills_boolean_filter: $skills_boolean_filter
      is_recruiter: $is_recruiter
      tags_ids: $tags_ids
    ) {
      custom_paginator_info {
        current_page
        last_page
        per_page
        total
      }
      data {
        highlights {
          source
          text
        }
        talent {
          id
          reminded_at
          created_at
          is_invitation_accepted
          is_verification_required
          first_name
          last_name
          address
          rate
          recent_position_title
          email
          available_now
          location
          is_ot_pool
          category {
            id
            name
          }
          subcategories {
            id
            name
          }
          is_recruiter
          source {
            id
          }
          avatar {
            avatar
          }
          talent_data {
            phone
          }
        }
      }
    }
  }
`;
