import { gql } from '@apollo/client';
import FULL_JOB_FRAGMENT from 'graphql/fragments/companyUser/jobFragment';
import FULL_JOB_MATCH_FRAGMENT from 'graphql/fragments/companyUser/jobMatchFragment';

export const GET_JOB_INFO = gql`
  ${FULL_JOB_FRAGMENT}
  ${FULL_JOB_MATCH_FRAGMENT}
  query GetJob($id: ID!) {
    job(id: $id) {
      ...FullJob
    }
  }
`;

export const SAVE_JOB_MATCH = gql`
  mutation SaveJobMatch($id: ID!) {
    saveMatchToShortList(match_id: $id)
  }
`;

export const DELETE_JOB_MATCH = gql`
  mutation DeleteJobMatch($id: ID!) {
    deleteMatchFromShortList(match_id: $id)
  }
`;

export const ARCHIVE_JOB = gql`
  mutation ArchiveJob($id: ID!) {
    updateJob(id: $id, is_archived: true) {
      id
    }
  }
`;

export const HIRE_TO_JOB = gql`
  mutation HireToJob($job_match_id: ID!) {
    hireJobMatch(job_match_id: $job_match_id)
  }
`;

export const INTAKE_TO_JOB = gql`
  mutation intakeToJob($job_match_id: ID!) {
    intakeJobMatch(job_match_id: $job_match_id)
  }
`;

export const INVITE_TO_JOB = gql`
  mutation InviteToJob($job_id: ID!, $talent_id: ID!, $message: String) {
    inviteToJob(talent_id: $talent_id, job_id: $job_id, message: $message)
  }
`;

export const DECLINE_JOB_MATCH = gql`
  mutation DeclineJobMatch(
    $job_match_id: ID!
    $reason: JobMatchDeclinationReasonsEnum
    $message: String
  ) {
    declineJobMatch(
      job_match_id: $job_match_id
      reason: $reason
      message: $message
    )
  }
`;
