import HowToRegIcon from '@mui/icons-material/HowToReg';
import PeopleIcon from '@mui/icons-material/People';
import { Box, Grid, Paper } from '@mui/material';
import { makeStyles } from '@mui/styles';
import useMediaQueries from 'hooks/common/useMediaQueries';
import React, { useMemo } from 'react';
import { CampaignStatus } from 'utils/job';
import { formatName } from 'utils/talent';

import { Job } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import Timer from './Timer';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    minWidth: 240,

    [theme.breakpoints.down('md')]: {
      'body &': {
        width: '100%',
      },
    },
  },
  icon: {
    color: theme.palette.grey[500],
  },
}));

interface OverallProps {
  job?: Job;
  campaignStatus?: CampaignStatus;
  currentTime: Date;
}

const Overall = ({ job, campaignStatus, currentTime }: OverallProps) => {
  const classes = useStyles();
  const { isSM } = useMediaQueries();
  const DATA = useMemo(
    () => [
      {
        Icon: PeopleIcon,
        value: job?.matches_count,
        label: 'Total leads',
      },
      {
        Icon: HowToRegIcon,
        value: job?.matches_with_application_count,
        label: 'Applications',
      },
    ],
    [job],
  );

  return (
    <Paper elevation={0}>
      <Box p={isSM ? 4 : 6} className={classes.root}>
        <Typography variant="h6">Campaign overall</Typography>
        <Box pt={isSM ? 4 : 6}>
          <Typography variant="subtitle2">Time remained</Typography>
          <Box pt={2}>
            <Timer
              campaignStatus={campaignStatus || CampaignStatus.NotStarted}
              startDate={job?.campaign_start_date}
              endDate={job?.campaign_end_date}
              currentTime={currentTime}
            />
          </Box>
          {/* {campaignStatus === CampaignStatus.Started && (
            
          )} */}
        </Box>
        <Box pt={isSM ? 4 : 6}>
          <Typography variant="subtitle2">Results</Typography>
          <Box pt={2}>
            {DATA.map(({ Icon, value, label }) => (
              <Grid
                key={label}
                wrap="nowrap"
                spacing={2}
                alignItems="center"
                container
              >
                <Grid display="flex" component={Box} item>
                  <Icon
                    className={classes.icon}
                    fontSize={isSM ? 'small' : 'inherit'}
                  />
                </Grid>
                <Grid item>
                  <Typography
                    component="span"
                    variant={isSM ? 'body2' : 'h6'}
                    color="tertiary"
                  >
                    {value}
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography
                    component="span"
                    variant={isSM ? 'caption' : 'body2'}
                  >
                    {label}
                  </Typography>
                </Grid>
              </Grid>
            ))}
          </Box>
        </Box>
        <Box pt={isSM ? 4 : 6}>
          <Typography variant="subtitle2">Campaign owner</Typography>
          <Box pt={isSM ? 1 : 2}>
            <Typography component="span" color="tertiary" variant="body2">
              {formatName({
                firstName: job?.campaign_owner?.first_name,
                lastName: job?.campaign_owner?.last_name,
              })}
            </Typography>
            {/* &nbsp;
            <Typography component="span" variant="body2">
              (you)
            </Typography> */}
          </Box>
        </Box>
      </Box>
    </Paper>
  );
};

export default Overall;
