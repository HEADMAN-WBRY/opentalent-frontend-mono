import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';
import { pathManager } from 'routes';

import ShareIcon from '@mui/icons-material/Share';
import { Button, Fade, Menu, MenuItem } from '@mui/material';

import { Job } from '@libs/graphql-types';

import { useCopyAction } from './hooks';

interface ShareButtonProps {
  job?: Job;
}

const ShareButton = ({ job }: ShareButtonProps) => {
  const onCopy = useCopyAction(job);
  const { isSM } = useMediaQueries();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <Button
        id="share-button"
        aria-controls={open ? 'fade-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
        endIcon={<ShareIcon />}
        variant="outlined"
        color="secondary"
        disabled={job?.is_archived}
      >
        {!isSM && 'Share'}
      </Button>
      <Menu
        id="fade-menu"
        MenuListProps={{
          'aria-labelledby': 'share-button',
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        <MenuItem
          component="a"
          target="_blank"
          href={
            job
              ? pathManager.public.job.generatePath({ id: job?.id || '' })
              : ''
          }
        >
          Visit
        </MenuItem>
        <MenuItem
          onClick={() => {
            onCopy();
            handleClose();
          }}
        >
          Copy url
        </MenuItem>
      </Menu>
    </div>
  );
};

export default ShareButton;
