import { useSnackbar } from 'notistack';
import { useCallback } from 'react';
import { useCopyToClipboard } from 'react-use';
import { pathManager } from 'routes';

import { Job } from '@libs/graphql-types';

export const useCopyAction = (job?: Job) => {
  const { enqueueSnackbar } = useSnackbar();
  const [, copy] = useCopyToClipboard();
  const jobId = job?.id;
  const onCopy = useCallback(() => {
    if (!jobId) {
      enqueueSnackbar(`No job data provided`, { variant: 'error' });
      return;
    }
    const link = new URL(
      pathManager.public.job.generatePath({ id: jobId }),
      window.location.origin,
    );

    copy(link.toString());
    enqueueSnackbar(`Link copied to clipboard`, { variant: 'success' });
  }, [copy, enqueueSnackbar, jobId]);

  return onCopy;
};
