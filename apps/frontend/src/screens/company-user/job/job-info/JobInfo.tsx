import BooleanSkills from 'components/job/boolean-skills';
import Description from 'components/job/description';
import GeneralInfo from 'components/job/general-info';
import RequiredSkills from 'components/job/required-skills';
import React from 'react';
import useToggle from 'react-use/lib/useToggle';
import { CampaignStatus } from 'utils/job';

import { Box, Collapse } from '@mui/material';

import { Job } from '@libs/graphql-types';

interface JobInfoProps {
  job: Job;
  campaignStatus: CampaignStatus;
}

const JobInfo = ({ job, campaignStatus }: JobInfoProps) => {
  const [withDetails, setWithDetails] = useToggle(false);

  return (
    <>
      <GeneralInfo
        setWithDetails={setWithDetails}
        campaignStatus={campaignStatus}
        withDetails={withDetails}
        job={job}
      />
      <Collapse in={withDetails}>
        <Box style={{ marginTop: '3px' }}>
          {job.skills_boolean_filter ? (
            <BooleanSkills job={job} />
          ) : (
            <RequiredSkills job={job} />
          )}
        </Box>
        <Box style={{ marginTop: '3px' }}>
          <Description job={job} />
        </Box>
      </Collapse>
    </>
  );
};

export default JobInfo;
