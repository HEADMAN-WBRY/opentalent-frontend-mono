import React from 'react';

const Job = React.lazy(() => import('./Job'));

export default Job;
