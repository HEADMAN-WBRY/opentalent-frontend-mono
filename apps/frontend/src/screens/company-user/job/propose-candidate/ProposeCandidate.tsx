import React from 'react';

import AddIcon from '@mui/icons-material/Add';

import { Job } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';

import { ProposeModal, useOpenProposeModal } from './propose-modal';

interface ProposeCandidateProps {
  job: Job;
}

const ProposeCandidate = ({ job }: ProposeCandidateProps) => {
  const open = useOpenProposeModal();

  return (
    <>
      <Button
        onClick={() => open()}
        startIcon={<AddIcon />}
        color="secondary"
        variant="outlined"
      >
        Add a candidate
      </Button>
      <ProposeModal job={job} />
    </>
  );
};

export default ProposeCandidate;
