import * as yup from 'yup';

import { useCheckTalentExistsByEmailLazyQuery } from '@libs/graphql-types';

const emailSchema = yup.string().email().nullable().trim().required();

export const useModalValidationSchema = () => {
  const [check] = useCheckTalentExistsByEmailLazyQuery();
  const schema = yup.object().shape({
    email: emailSchema.test({
      name: 'checkDuplicateEmail1',
      message: 'This talent already exists on the platform',
      test: async (email) => {
        const isValidEmail = await emailSchema.isValidSync(email);

        if (!isValidEmail) {
          return true;
        }
        const res = await check({ variables: { talent_email: email } });

        return !res.data?.checkTalentExistsByEmail;
      },
    }),
    firstName: yup.string().nullable().trim().required(),
  });

  return { schema };
};
