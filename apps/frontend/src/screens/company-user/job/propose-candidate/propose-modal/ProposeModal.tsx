import { Form, Formik } from 'formik';
import React from 'react';
import {
  DefaultModalProps,
  useOpenModal,
  withLocationStateModal,
} from 'utils/modals';

import { Box, Button, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { Job } from '@libs/graphql-types';
import { modelPath } from '@libs/helpers/form';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import { DefaultModal } from '@libs/ui/components/modals';
import Typography from '@libs/ui/components/typography';

import { useProposeHandler } from '../hooks';
import { useModalValidationSchema } from './hooks';
import { IFormState } from './types';

interface ProposeModalProps extends DefaultModalProps<boolean> {
  job: Job;
}

const MODAL_ID = 'proposeCandidate';

const useStyles = makeStyles((theme) => ({
  paper: {
    width: 540,
  },
}));

const INITIAL_STATE = {
  email: '',
  firstName: '',
};

const ProposeModalComponent = ({ isOpen, close, job }: ProposeModalProps) => {
  const classes = useStyles();
  const { schema } = useModalValidationSchema();
  const { onSubmit, isLoading } = useProposeHandler({
    onSuccess: close,
    jobId: job.id,
  });

  return (
    <Formik<IFormState>
      initialValues={INITIAL_STATE}
      onSubmit={onSubmit}
      validationSchema={schema}
      validateOnChange={false}
    >
      {({ submitForm }) => {
        return (
          <Form>
            <DefaultModal
              actions={
                <Grid container spacing={4}>
                  <Grid xs={6} item>
                    <Button
                      fullWidth
                      color="primary"
                      variant="contained"
                      size="large"
                      onClick={submitForm}
                      type="submit"
                      disabled={isLoading}
                    >
                      Add a candidate
                    </Button>
                  </Grid>
                  <Grid xs={6} item>
                    <Button
                      size="large"
                      fullWidth
                      variant="outlined"
                      onClick={close}
                      disabled={isLoading}
                    >
                      Cancel
                    </Button>
                  </Grid>
                </Grid>
              }
              handleClose={close}
              open={isOpen}
              title="Add a candidate"
              className={classes.paper}
            >
              <Box pb={4} pt={6}>
                <Typography>
                  This person will be invited to OpenTalent and added into the
                  tracker of this job (in the ‘Invited’ tab)
                </Typography>
              </Box>
              <Grid container spacing={4} direction="column">
                <Grid item>
                  <ConnectedTextField
                    fullWidth
                    variant="filled"
                    label="First name"
                    name={modelPath<IFormState>((m) => m.firstName)}
                  />
                </Grid>
                <Grid item>
                  <ConnectedTextField
                    fullWidth
                    variant="filled"
                    label="Email"
                    name={modelPath<IFormState>((m) => m.email)}
                  />
                </Grid>
              </Grid>
            </DefaultModal>
          </Form>
        );
      }}
    </Formik>
  );
};

export const ProposeModal = withLocationStateModal({ id: MODAL_ID })(
  ProposeModalComponent,
);

export const useOpenProposeModal = () => useOpenModal<boolean>(MODAL_ID);
