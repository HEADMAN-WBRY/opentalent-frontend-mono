import React from 'react';
import { Link } from 'react-router-dom';
import { pathManager } from 'routes';

import { JobMatchTypeEnum } from '@libs/graphql-types';
import Typography, { RouterLink } from '@libs/ui/components/typography';

import { useEuropeFilterSearch } from '../hooks';

interface NoResponsesProps {
  matchType: JobMatchTypeEnum | string;
}

const NoResponses = ({ matchType }: NoResponsesProps) => {
  const europeSearch = useEuropeFilterSearch();

  if (matchType === JobMatchTypeEnum.InstantMatch) {
    return (
      <Typography variant="body1" color="textSecondary">
        No candidates found! Filter by{' '}
        <RouterLink color="info.main" to={{ search: europeSearch }}>
          Europe
        </RouterLink>{' '}
        for more results.
      </Typography>
    );
  }
  if (matchType === JobMatchTypeEnum.TalentApplication) {
    return (
      <Typography variant="body1" color="textSecondary">
        There are no applications from Talents here yet.
        <br />
        You can try to look for Talents in{' '}
        <Link to={pathManager.company.workforce.generatePath()}>
          <Typography component="span" variant="body1" color="tertiary">
            the Workforce
          </Typography>
        </Link>{' '}
        on your own.
      </Typography>
    );
  }

  if (matchType === 'SHORTLIST') {
    return (
      <Typography variant="body1" color="textSecondary">
        Here you can add Talents who do you think matches your job best.
        <br />
        You can add them from Instant matches and Direct applications tabs.
      </Typography>
    );
  }

  return (
    <Typography variant="body1" color="textSecondary">
      There are no talents here yet.
    </Typography>
  );
};

export default NoResponses;
