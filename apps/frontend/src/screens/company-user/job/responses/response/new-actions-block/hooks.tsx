import { useMutation } from '@apollo/client';
import { useConfirm } from 'material-ui-confirm';
import { useSnackbar } from 'notistack';
import React, { useCallback } from 'react';

import IntakeIcon from '@mui/icons-material/AssignmentOutlined';
import BlockIcon from '@mui/icons-material/BlockOutlined';
import RemoveIcon from '@mui/icons-material/HighlightOffOutlined';
import PersonAddIcon from '@mui/icons-material/PersonAddOutlined';
import HireIcon from '@mui/icons-material/WorkOutlineOutlined';

import {
  JobMatchTypeEnum,
  Mutation,
  MutationHireJobMatchArgs,
} from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import { GET_JOB_INFO, HIRE_TO_JOB, INTAKE_TO_JOB } from '../../../queries';
import { useOpenDeclineModal } from '../../decline-modal';
import { useOpenInviteModal } from '../../invite-modal';
import { useOnRemoveModal } from '../../remove-response-modal';
import { useStyles } from './styles';
import { ActionOptionType, NewActionsBlockProps } from './types';

export const useChildMenuAction = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const open = Boolean(anchorEl);

  const onClick = (event: any) => {
    if (!anchorEl) {
      setAnchorEl(event.currentTarget);
    } else {
      setAnchorEl(null);
    }
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return {
    open,
    handleClose,
    anchorEl,
    onClick,
  };
};

export const useOnInvite = (talentId: string, firstName: string) => {
  const openInviteModal = useOpenInviteModal();
  const onInvite = useCallback(
    () => openInviteModal({ talentId, firstName }),
    [openInviteModal, talentId, firstName],
  );

  return onInvite;
};

export const useOnDecline = (matchId: string, firstName: string) => {
  const openDeclineModal = useOpenDeclineModal();
  const onDecline = useCallback(
    () => openDeclineModal({ matchId, firstName }),
    [matchId, openDeclineModal, firstName],
  );

  return onDecline;
};

export const useOnRemove = (matchId: string) => {
  const openRemoveModal = useOnRemoveModal();
  const onRemove = useCallback(
    () => openRemoveModal({ matchId }),
    [matchId, openRemoveModal],
  );

  return onRemove;
};

const useOnHire = (matchId: string, jobId: string) => {
  const { enqueueSnackbar } = useSnackbar();
  const confirm = useConfirm();
  const classes = useStyles();
  const [hire] = useMutation<Mutation, MutationHireJobMatchArgs>(HIRE_TO_JOB, {
    onCompleted: () => {
      enqueueSnackbar('Success!', { variant: 'success' });
    },
    refetchQueries: [{ query: GET_JOB_INFO, variables: { id: jobId } }],
  });

  const hireWithoutContact = useCallback(
    () => hire({ variables: { job_match_id: matchId } }),
    [hire, matchId],
  );

  const hireWithContact = useCallback(async () => {
    await confirm({
      confirmationText: 'Sign contract',
      content: (
        <Typography variant="subtitle2" className={classes.dialogContent}>
          Thank you for choosing OpenTalent as your contracting partner.
          <br />
          Someone from OpenTalent will contact you within 24 hours to complete
          the contracting process.
        </Typography>
      ),
      title: (
        <Typography className={classes.dialogTitle}>Sign a contract</Typography>
      ),
      cancellationButtonProps: {
        variant: 'outlined',
        color: 'secondary',
      },
      dialogProps: {
        classes: {
          paper: classes.dialogClass,
        },
      },
    });

    hire({ variables: { job_match_id: matchId, with_contract: true } });
  }, [hire, confirm, matchId, classes]);

  return [hireWithoutContact, hireWithContact];
};

const useOnIntake = (matchId: string, jobId: string) => {
  const { enqueueSnackbar } = useSnackbar();
  const [mutate] = useMutation<Mutation, MutationHireJobMatchArgs>(
    INTAKE_TO_JOB,
    {
      onCompleted: () => {
        enqueueSnackbar('Success!', { variant: 'success' });
      },
      refetchQueries: [{ query: GET_JOB_INFO, variables: { id: jobId } }],
    },
  );

  return useCallback(
    () => mutate({ variables: { job_match_id: matchId } }),
    [mutate, matchId],
  );
};

export const useMenuActions = ({
  jobId,
  jobMatch,
  isSaved,
}: NewActionsBlockProps): ActionOptionType[] => {
  const jobMatchId = jobMatch.id;
  const [onHire, onHireWithContact] = useOnHire(jobMatchId, jobId);
  const onIntake = useOnIntake(jobMatchId, jobId);
  const firstName = jobMatch.talent?.first_name || '';
  const onInvite = useOnInvite(jobMatch.talent?.id || '', firstName);
  // const { action: saveAction, isLoading } = useSaveAction({ id: jobId });
  // const saveOption = {
  //   text: !isSaved ? 'Add to shortlist' : 'Remove from shortlist',
  //   Icon: !isSaved ? StarBorderIcon : StarIcon,
  //   onClick: () => saveAction({ matchId: jobMatch.id, isSaved }),
  //   isLoading,
  // };
  const inviteOption = {
    text: 'Invite to Apply',
    Icon: PersonAddIcon,
    onClick: onInvite,
  };

  const onRemove = useOnRemove(jobMatch.id);
  const removeOption = {
    text: 'Remove',
    Icon: RemoveIcon,
    onClick: onRemove,
  };

  const onDecline = useOnDecline(jobMatch.id, firstName);
  const declineOption = {
    text: 'Decline',
    Icon: BlockIcon,
    onClick: onDecline,
  };

  const hireWithContact = {
    text: 'Sign a contract',
    onClick: onHireWithContact,
  };

  const hireWithoutContact = {
    text: 'Hire without contract',
    onClick: onHire,
  };

  const hireOption = {
    text: 'Hire',
    Icon: HireIcon,
    childActions: [hireWithContact, hireWithoutContact],
  };

  const intakeOption = {
    text: 'Add to intake',
    Icon: IntakeIcon,
    onClick: onIntake,
  };

  if (jobMatch.match_type === JobMatchTypeEnum.InstantMatch) {
    return [inviteOption, removeOption, intakeOption, hireOption];
  }

  if (jobMatch.match_type === JobMatchTypeEnum.TalentApplication) {
    return [intakeOption, hireOption, declineOption];
  }

  if (jobMatch.match_type === JobMatchTypeEnum.Invited) {
    return [intakeOption, hireOption, declineOption];
  }

  if (jobMatch.match_type === JobMatchTypeEnum.Intake) {
    return [hireOption, declineOption];
  }

  return [];
};
