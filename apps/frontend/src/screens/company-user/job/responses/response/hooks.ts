import React, { useEffect, useState } from 'react';
import { useIntersection } from 'react-use';

import { useMarkNewJobMatchesAsReadMutation } from '@libs/graphql-types';

const INTERSECTION_THRESHOLD = 0.8;

export const useOnViewAction = ({
  isNewMatch,
  matchId,
}: {
  isNewMatch: boolean;
  matchId: string;
}) => {
  const intersectionRef = React.useRef(null);
  const [mark] = useMarkNewJobMatchesAsReadMutation();
  const intersection = useIntersection(intersectionRef, {
    root: null,
    rootMargin: '0px',
    threshold: 1,
  });
  const [isNewMatchFinal, setIsNewMatch] = useState(isNewMatch);
  const isIntercepted =
    (intersection?.intersectionRatio || 0) > INTERSECTION_THRESHOLD;

  useEffect(() => {
    const needAction = isNewMatch && isIntercepted;

    if (needAction) {
      mark({ variables: { job_matches_ids: [matchId] } });
      setIsNewMatch(false);
    }
  }, [isIntercepted, isNewMatch, isNewMatchFinal, mark, matchId]);

  return { ref: intersectionRef };
};
