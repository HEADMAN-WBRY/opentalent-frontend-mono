import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(() => ({
  dialogClass: {
    padding: '40px',
    maxWidth: '450px',

    '& > .MuiDialogTitle-root, .MuiDialogContent-root': {
      padding: 0,
      margin: '0 auto',
    },

    '& > .MuiDialogActions-root': {
      justifyContent: 'center',
      flexDirection: 'row-reverse',

      '& > button': {
        width: '175px',

        '&:not(:first-child)': {
          marginLeft: '0',
        },
        '&:first-child': {
          marginLeft: '24px',
        },
      },
    },
  },
  dialogTitle: {
    padding: 0,
    width: '100%',
    marginBottom: '24px',
    textAlign: 'center',
    lineHeight: '24px',
    fontSize: '20px',
    fontWeight: 500,
  },
  dialogContent: {
    padding: 0,
    width: '100%',
    marginBottom: '16px',
    textAlign: 'center',
    lineHeight: '20px',
  },
  arrow: {
    transition: 'transform .2s linear',
  },
  downArrow: {
    transform: 'rotate(90deg)',
  },
}));
