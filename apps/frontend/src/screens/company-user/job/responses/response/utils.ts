import { useMemo } from 'react';

import { JobMatch, JobMatchTypeEnum } from '@libs/graphql-types';

export const useCountryOptionsFromMatches = (matches: JobMatch[]) => {
  return useMemo(() => {
    const instantMatches = matches.filter(
      (match) => match.match_type === JobMatchTypeEnum.InstantMatch,
    );
    const counts = instantMatches.reduce<Record<string, number>>(
      (acc, match) => {
        const country = match?.talent?.location;

        if (country) {
          acc[country] = (acc[country] || 0) + 1;
        }

        return acc;
      },
      {},
    );
    const options = Object.entries(counts)
      .sort((a, b) => (a[1] >= b[1] ? -1 : 1))
      .map(([country, count]) => ({
        text: `${country} (${count})`,
        value: country,
      }));

    return { options, allMatchesCount: instantMatches.length };
  }, [matches]);
};
