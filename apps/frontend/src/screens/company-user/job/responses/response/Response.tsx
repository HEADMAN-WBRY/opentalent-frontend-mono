import React from 'react';
import { Link } from 'react-router-dom';
import { useToggle } from 'react-use';
import { pathManager } from 'routes';
import { stopEvent } from 'utils/common';

import { Box, Grid, Paper } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { Job, JobMatch } from '@libs/graphql-types';

import MainInfo from './MainInfo';
import UserBlock from './UserBlock';
import { useOnViewAction } from './hooks';
import NewActionsBlock from './new-actions-block';

interface ResponseProps {
  jobMatch: JobMatch;
  isSaved: boolean;
  job: Job;
}

const useStyles = makeStyles((theme) => ({
  mainInfo: {
    paddingLeft: '112px',

    [theme.breakpoints.down('md')]: {
      'body &': {
        paddingLeft: 0,
      },
    },
  },
  userInfo: {
    [theme.breakpoints.down('md')]: {
      'body &': {
        flexDirection: 'column',
      },
    },
  },
  paper: {
    position: 'relative',
    cursor: 'pointer',
  },
  actionsWrapper: {
    position: 'absolute',
    top: 16,
    right: 16,
  },
}));

const Response = ({ jobMatch, isSaved, job }: ResponseProps) => {
  const classes = useStyles();
  const [isPitchShown, togglePitch] = useToggle(false);
  const { talent } = jobMatch;
  const isNewMatch = jobMatch.is_new;
  const { ref } = useOnViewAction({ isNewMatch, matchId: jobMatch.id });

  return (
    <Link
      to={pathManager.company.talentProfile.generatePath({
        id: talent?.id || '',
      })}
    >
      <Paper
        data-test-id="response-card"
        className={classes.paper}
        elevation={0}
        ref={ref}
      >
        <Box p={4}>
          <Grid
            className={classes.userInfo}
            wrap="nowrap"
            justifyContent="space-between"
            container
          >
            <Grid item>
              <UserBlock talent={talent} isNewMatch={isNewMatch} />
            </Grid>
            <Grid onClick={stopEvent} className={classes.actionsWrapper} item>
              <NewActionsBlock
                jobId={job.id}
                jobMatch={jobMatch}
                isSaved={isSaved}
                togglePitch={togglePitch}
              />
            </Grid>
          </Grid>
          <Box className={classes.mainInfo} mt={2} pl={10}>
            <MainInfo isPitchShown={isPitchShown} jobMatch={jobMatch} />
          </Box>
        </Box>
      </Paper>
    </Link>
  );
};

export default Response;
