import BlockIcon from '@mui/icons-material/Block';
import DoneIcon from '@mui/icons-material/Done';
import React from 'react';

import { JobMatch } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';

interface InviteButtonProps {
  jobMatch: JobMatch;
  onInvite: VoidFunction;
}

const InviteButton = ({ jobMatch, onInvite }: InviteButtonProps) => {
  const {
    job_invitation,
    is_applied = false,
    is_invited = false,
  } = jobMatch || {};
  const isDeclined = !!job_invitation?.is_declined;
  if (isDeclined) {
    return (
      <Button
        disabled
        variant="contained"
        color="error"
        endIcon={<BlockIcon />}
        data-test-id="declined"
      >
        declined
      </Button>
    );
  }
  if (is_applied) {
    return (
      <Button disabled variant="contained">
        Applied
      </Button>
    );
  }
  if (is_invited) {
    return (
      <Button disabled variant="contained" endIcon={<DoneIcon />}>
        Invited
      </Button>
    );
  }

  return (
    <Button onClick={onInvite} color="secondary" variant="contained">
      Invite to job
    </Button>
  );
};

export default InviteButton;
