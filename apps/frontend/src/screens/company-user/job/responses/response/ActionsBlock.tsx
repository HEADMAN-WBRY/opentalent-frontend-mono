/* eslint-disable no-nested-ternary */
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import HowToRegIcon from '@mui/icons-material/HowToReg';
import StarIcon from '@mui/icons-material/Star';
import StarBorderIcon from '@mui/icons-material/StarBorder';
import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';
import cn from 'classnames';
import React from 'react';
import { Link } from 'react-router-dom';
import { pathManager } from 'routes/consts';
import { formatName } from 'utils/talent';

import { JobMatch, JobMatchTypeEnum } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import { useSaveAction } from '../../hooks';
import InviteButton from './InviteButton';
import { useOnInvite } from './new-actions-block/hooks';

interface ActionsBlockProps {
  jobMatch: JobMatch;
  isSaved: boolean;
  jobId: string;
  isInvited: boolean;
}

const useStyles = makeStyles((theme) => ({
  root: {
    [theme.breakpoints.down('md')]: {
      'body &': {
        paddingLeft: 72,
      },
    },
  },
  iconButton: {
    minWidth: 'auto',
    padding: 5,
  },
  iconButtonActive: {
    background: 'rgba(30, 30, 28, 0.08)',
  },
  matchIcon: {
    color: theme.palette.success.main,
  },
  progress: {
    background: theme.palette.success.main,
  },
  progressRoot: {
    background: '#eee',
  },
  talentPageBtn: {
    [theme.breakpoints.down('md')]: {
      'body &': {
        width: '100%',
      },
    },
  },
}));

const ActionsBlock = ({
  isSaved,
  jobId,
  isInvited,
  jobMatch,
}: ActionsBlockProps) => {
  const classes = useStyles();
  const isInstant = jobMatch.match_type === JobMatchTypeEnum.InstantMatch;
  const { id: jobMatchId, talent } = jobMatch || {};
  const { action: saveAction, isLoading } = useSaveAction({ id: jobId });
  const onInvite = useOnInvite(
    talent?.id || '',
    formatName({ firstName: talent?.first_name, lastName: talent?.last_name }),
  );

  return (
    <Box className={classes.root}>
      <Grid container wrap="nowrap">
        <Grid item component={Box} pr={6}>
          <Button
            disabled={isLoading}
            onClick={() => saveAction({ matchId: jobMatchId, isSaved })}
            className={cn(classes.iconButton, {
              [classes.iconButtonActive]: isSaved,
            })}
            variant="outlined"
          >
            {isSaved ? <StarIcon /> : <StarBorderIcon />}
          </Button>
        </Grid>
        <Grid component={Box} flexGrow={1} pr={6} item>
          <Link
            to={pathManager.company.talentProfile.generatePath({
              id: talent?.id || '',
            })}
          >
            <Button
              className={classes.talentPageBtn}
              color="primary"
              variant="contained"
              endIcon={<AccountBoxIcon />}
            >
              talent page
            </Button>
          </Link>
        </Grid>
        <Grid component={Box} flexGrow={1} item>
          <InviteButton jobMatch={jobMatch} onInvite={onInvite} />
        </Grid>
      </Grid>

      {isInstant && (
        <Grid
          spacing={2}
          justifyContent="flex-end"
          component={Box}
          pt={5}
          container
        >
          <Grid item>
            <HowToRegIcon className={classes.matchIcon} />
          </Grid>
          <Grid item>
            <Typography variant="caption">Instant Match</Typography>
          </Grid>
        </Grid>
      )}
    </Box>
  );
};

export default ActionsBlock;
