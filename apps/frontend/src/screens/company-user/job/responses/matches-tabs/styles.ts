import { makeStyles } from '@mui/styles';

const useStyles = makeStyles(
  (theme) =>
    ({
      root: {
        minWidth: '100%',
      },
      flexContainer: {
        minWidth: '100%',
      },
      tab: {
        flexGrow: 1,
        textAlign: 'center',
      },
      scroller: {
        overflowX: 'scroll !important',
        scrollbarWidth: 'none',

        '&::-webkit-scrollbar': {
          display: 'block',
        },
      },
    } as any),
);

export default useStyles;
