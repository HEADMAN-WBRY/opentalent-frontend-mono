import { Tabs, Tab } from '@mui/material';
import { usePushWithQuery } from 'hooks/routing/usePushWithQuery';
import React, { useCallback } from 'react';

import { JobMatchTypeEnum } from '@libs/graphql-types';

import { MATCHES_TABS } from '../../consts';
import useStyles from './styles';

interface MatchesTabsProps {
  value: JobMatchTypeEnum;
  count: Partial<Record<JobMatchTypeEnum | string, number>>;
}

const MatchesTabs = ({ value, count }: MatchesTabsProps) => {
  const classes: any = useStyles();
  const push = usePushWithQuery();
  const onChange = useCallback(
    (_, match: string) => push({ query: { match } }),
    [push],
  );

  return (
    <Tabs
      data-test-id="tabs"
      value={value}
      onChange={onChange}
      indicatorColor="secondary"
      textColor="secondary"
      classes={{
        root: classes.root,
        flexContainer: classes.flexContainer,
        scroller: classes.scroller,
      }}
      scrollButtons
      allowScrollButtonsMobile
    >
      <Tab
        value={MATCHES_TABS.instant}
        classes={{ root: classes.tab }}
        label={`Instant matches (${count[MATCHES_TABS.instant] || 0})`}
      />
      <Tab
        value={MATCHES_TABS.talent}
        classes={{ root: classes.tab }}
        label={`applied (${count[MATCHES_TABS.talent] || 0})`}
      />
      <Tab
        value={MATCHES_TABS.invited}
        classes={{ root: classes.tab }}
        label={`Invited (${count[MATCHES_TABS.invited] || 0})`}
      />
      <Tab
        value={MATCHES_TABS.saved}
        classes={{ root: classes.tab }}
        label={`SHORTLISted (${count[MATCHES_TABS.saved] || 0})`}
      />
    </Tabs>
  );
};

export default MatchesTabs;
