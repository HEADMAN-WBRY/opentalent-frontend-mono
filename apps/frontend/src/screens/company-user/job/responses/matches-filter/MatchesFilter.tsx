import FormikAutoSaving from 'components/form/formik/FormikAutoSave';
import { Formik } from 'formik';
import { usePushWithQuery } from 'hooks/routing';
import React from 'react';

import { makeStyles } from '@mui/styles';

import { ConnectedSelect, OptionType } from '@libs/ui/components/form/select';

interface MatchesFilterProps {
  location?: string;
  countryOptions?: OptionType[];
  allMatchesCount?: number;
}

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 300,

    '& .MuiInputBase-root': {
      background: 'transparent',
    },
  },
}));

const MatchesFilter = ({
  location = '',
  countryOptions = [],
  allMatchesCount = 0,
}: MatchesFilterProps) => {
  const classes = useStyles();
  const push = usePushWithQuery();

  return (
    <Formik
      enableReinitialize
      initialValues={{ location }}
      onSubmit={(query) => push({ query })}
    >
      {({ submitForm }) => (
        <div className={classes.root}>
          <FormikAutoSaving debounceMs={1000} onValuesChange={submitForm} />
          <ConnectedSelect
            name="location"
            options={countryOptions}
            fullWidth
            variant="standard"
            label="Location"
            displayEmpty
            noneItemText={`Europe (${allMatchesCount})`}
            labelShrink
          />
        </div>
      )}
    </Formik>
  );
};

export default MatchesFilter;
