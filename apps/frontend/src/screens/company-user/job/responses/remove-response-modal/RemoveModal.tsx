import { Box, Button, Grid } from '@mui/material';
import React from 'react';
import {
  DefaultModalProps,
  useOpenModal,
  withLocationStateModal,
} from 'utils/modals';

import { DefaultModal } from '@libs/ui/components/modals';
import Typography from '@libs/ui/components/typography';

import { MODAL_TYPES } from '../../consts';
import { useInviteHandler } from './hooks';

interface ModalData {
  matchId: string;
}

interface RemoveModalProps extends DefaultModalProps<ModalData> {
  jobId: string;
}

const RemoveModalComponent = ({
  jobId,
  isOpen,
  close,
  modalData,
}: RemoveModalProps) => {
  const jobMatchId = modalData?.matchId || '';
  const { onSubmit, loading } = useInviteHandler({
    jobMatchId,
    jobId,
    onClose: close,
  });

  return (
    <DefaultModal
      actions={
        <Grid spacing={4} container>
          <Grid xs={6} item>
            <Button
              fullWidth
              color="primary"
              variant="contained"
              onClick={onSubmit}
              disabled={loading}
            >
              Decline
            </Button>
          </Grid>
          <Grid xs={6} item>
            <Button
              fullWidth
              variant="outlined"
              color="secondary"
              onClick={close}
              disabled={loading}
            >
              Cancel
            </Button>
          </Grid>
        </Grid>
      }
      handleClose={close}
      open={isOpen}
      title="You are going to remove a candidate?"
    >
      <Box>
        <Typography>Please confirm</Typography>
      </Box>
    </DefaultModal>
  );
};

export const RemoveModal = withLocationStateModal({ id: MODAL_TYPES.REMOVE })(
  RemoveModalComponent,
);

export const useOnRemoveModal = () =>
  useOpenModal<ModalData>(MODAL_TYPES.REMOVE);
