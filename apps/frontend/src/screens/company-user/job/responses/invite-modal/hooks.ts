import { useMutation } from '@apollo/client';
import { useSnackbar } from 'notistack';
import { useCallback } from 'react';

import { Mutation, MutationInviteToJobArgs } from '@libs/graphql-types';

import { GET_JOB_INFO, INVITE_TO_JOB } from '../../queries';

export const useInviteHandler = ({
  onClose,
  jobId,
  talentId,
}: {
  jobId: string;
  talentId: string;
  onClose: VoidFunction;
}) => {
  const { enqueueSnackbar } = useSnackbar();
  const [invite, { loading }] = useMutation<Mutation, MutationInviteToJobArgs>(
    INVITE_TO_JOB,
    {
      onCompleted: () => {
        enqueueSnackbar('Talent successfully Invited!', {
          variant: 'success',
        });
        onClose();
      },
      refetchQueries: [{ query: GET_JOB_INFO, variables: { id: jobId } }],
    },
  );

  const onSubmit = useCallback(
    async (values, helpers) => {
      await invite({
        variables: {
          job_id: jobId,
          talent_id: talentId,
          message: values.message,
        },
      });
      helpers.resetForm({
        values: { jobId: values.jobId, talentId: values.talentId },
      });
    },
    [invite, jobId, talentId],
  );

  return { onSubmit, loading };
};
