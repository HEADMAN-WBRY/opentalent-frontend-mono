import { useMutation } from '@apollo/client';
import { FormikConfig } from 'formik';
import { useSnackbar } from 'notistack';
import { useCallback } from 'react';

import { Mutation, MutationDeclineJobMatchArgs } from '@libs/graphql-types';

import { GET_JOB_INFO, DECLINE_JOB_MATCH } from '../../queries';
import { FormState } from './types';

export const useDeclineAction = ({
  jobId,
  matchId,
  onClose,
}: {
  jobId: string;
  matchId: string;
  onClose: VoidFunction;
}) => {
  const { enqueueSnackbar } = useSnackbar();
  const [decline, { loading }] = useMutation<
    Mutation,
    MutationDeclineJobMatchArgs
  >(DECLINE_JOB_MATCH, {
    onCompleted: () => {
      enqueueSnackbar('Talent successfully declined!', {
        variant: 'success',
      });
      onClose();
    },
    refetchQueries: [{ query: GET_JOB_INFO, variables: { id: jobId } }],
  });

  const onSubmit: FormikConfig<FormState>['onSubmit'] = useCallback(
    async (values, helpers) => {
      await decline({
        variables: {
          job_match_id: matchId,
          reason: values.reason,
          message: values.message,
        },
      });
      helpers.resetForm();
    },
    [decline, matchId],
  );

  return { onSubmit, loading };
};
