import { JobMatchDeclinationReasonsEnum } from '@libs/graphql-types';

export interface FormState {
  reason?: JobMatchDeclinationReasonsEnum;
  message?: string;
}

export interface DeclineModalData {
  matchId: string;
  firstName: string;
}
