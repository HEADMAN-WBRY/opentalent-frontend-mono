import { ConnectedPageLayout } from 'components/layout/page-layout';
import { useCurrentTime } from 'hooks/common/useCurrentTime';
import useMediaQueries from 'hooks/common/useMediaQueries';
import { useSearchParams } from 'hooks/routing';
import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { isNil } from 'utils/common';
import { getCampaignStatus } from 'utils/job';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import { ArchiveModal } from './archive-modal';
import { useJobScreenData } from './hooks';
import JobInfo from './job-info';
import Overall from './overall';
import ProposeCandidate from './propose-candidate';
import Responses from './responses';
import { DeclineModal } from './responses/decline-modal';
import { InviteModal } from './responses/invite-modal';
import { RemoveModal } from './responses/remove-response-modal';
import TimelineAndComments from './timeline-and-comments';
import TitleRow from './title-row';

interface JobProps extends RouteComponentProps<{ id: string }> {}

const useStyles = makeStyles((theme) => ({
  contentWrapper: {
    overflow: 'hidden',
  },
  contentGrid: {
    [theme.breakpoints.down('md')]: {
      'body &': {
        flexDirection: 'column',
        paddingBottom: theme.spacing(14),
      },
    },
  },
  cardsContainer: {
    width: ({ is_draft }: { is_draft: boolean }) => (is_draft ? '100%' : 768),

    [theme.breakpoints.down('md')]: {
      'body &': {
        order: 1,
        width: 'auto',
      },
    },
  },
  overallContainer: {
    flexGrow: 1,

    [theme.breakpoints.down('md')]: {
      'body &': {
        order: 0,
      },
    },
  },
}));

const JobScreen = ({ match }: JobProps) => {
  const currentTime = useCurrentTime();
  const { isSM } = useMediaQueries();
  const { id } = match.params;
  const { location: searchLocation } = useSearchParams();
  const { data, loading } = useJobScreenData({ id });
  const job = data?.job;
  const classes = useStyles({ is_draft: job?.is_draft || false });
  const campaignStatus = getCampaignStatus(job);
  const location = !isNil(searchLocation)
    ? (searchLocation as string)
    : job?.country || '';

  return (
    <ConnectedPageLayout
      documentTitle="Job board"
      headerProps={{ accountProps: {} }}
      drawerProps={{}}
      contentSpacing={isSM ? 4 : 6}
      classes={{ contentWrapper: classes.contentWrapper }}
      isLoading={loading}
    >
      {!job && !loading && <Typography variant="h5">Job not found</Typography>}
      {job && (
        <>
          <TitleRow campaignStatus={campaignStatus} job={job} />
          <Grid
            className={classes.contentGrid}
            spacing={isSM ? 4 : 6}
            container
            wrap="nowrap"
          >
            <Grid
              md={8}
              className={classes.cardsContainer}
              component={Box}
              item
            >
              <Box mb={6}>
                <JobInfo job={job} campaignStatus={campaignStatus} />
              </Box>

              <Box mb={6}>
                <ProposeCandidate job={job} />
              </Box>

              <Responses job={job} location={location as string} />
            </Grid>

            {!job.is_draft && (
              <Grid md={4} className={classes.overallContainer} item>
                <Overall
                  currentTime={currentTime}
                  campaignStatus={campaignStatus}
                  job={job}
                />

                <Box mt={4}>
                  <TimelineAndComments job={job} />
                </Box>
              </Grid>
            )}
          </Grid>
        </>
      )}
      <ArchiveModal jobId={id} />
      <InviteModal jobId={id} />
      <RemoveModal jobId={id} />
      <DeclineModal jobId={id} />
    </ConnectedPageLayout>
  );
};

export default JobScreen;
