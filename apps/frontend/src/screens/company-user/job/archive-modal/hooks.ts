import { useMutation } from '@apollo/client';
import { useSnackbar } from 'notistack';
import { useCallback } from 'react';

import { Mutation, MutationUpdateJobArgs } from '@libs/graphql-types';

import { ARCHIVE_JOB, GET_JOB_INFO } from '../queries';

interface HookParams {
  jobId: string;
  handleClose: VoidFunction;
}

export const useSubmitHandler = ({ jobId, handleClose }: HookParams) => {
  const { enqueueSnackbar } = useSnackbar();
  const [action, { loading }] = useMutation<Mutation, MutationUpdateJobArgs>(
    ARCHIVE_JOB,
    {
      onCompleted: () => {
        enqueueSnackbar(`Job successfully archived`, {
          variant: 'success',
        });
        handleClose();
      },
      refetchQueries: [{ query: GET_JOB_INFO, variables: { id: jobId } }],
    },
  );

  const onSubmit = useCallback(() => {
    action({
      variables: { id: jobId, is_archived: true },
    });
  }, [action, jobId]);

  return { onSubmit, loading };
};
