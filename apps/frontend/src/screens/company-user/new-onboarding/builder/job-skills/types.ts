import { OptionType } from '@libs/ui/components/form/select';

export interface JobSkillsInfo {
  mandatory: OptionType[];
  common: OptionType[];
}
