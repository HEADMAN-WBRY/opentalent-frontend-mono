export interface DefaultStepProps {
  index: number;
  nextStepRoute: string;
  prevStepRoute: string;
}
