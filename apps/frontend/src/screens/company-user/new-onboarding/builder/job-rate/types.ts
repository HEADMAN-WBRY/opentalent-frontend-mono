export interface RateInfo {
  minRate: number;
  maxRate: number;
  minSalary: number;
  maxSalary: number;
}
