import { JobTypeEnum } from "@libs/graphql-types";

export interface JobTypeState {
  type: JobTypeEnum;
}
