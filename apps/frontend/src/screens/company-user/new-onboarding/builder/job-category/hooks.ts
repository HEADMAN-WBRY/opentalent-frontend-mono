import { useSnackbar } from 'notistack';
import { useCallback, useMemo } from 'react';
import { useHistory } from 'react-router-dom';
import { pathManager } from 'routes';

import {
  useCreateCompanyUserMutation,
  useTalentCategoriesQuery,
} from '@libs/graphql-types';
import { FormikSubmit } from '@libs/helpers/form';
import { OptionsGroup } from '@libs/ui/components/form/select';

import { JobCategoryInfo } from './types';

export const useSubmitHandler = () => {
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();
  const [createUser, { loading }] = useCreateCompanyUserMutation({
    onCompleted() {
      enqueueSnackbar('User account successfully created', {
        variant: 'success',
      });

      history.push(
        pathManager.company.newOnboarding.builder.step.generatePath({
          step: 2,
        }),
      );
    },
  });
  const onSubmit: FormikSubmit<JobCategoryInfo> = useCallback(
    async (values) => {
      await createUser({});
    },
    [createUser],
  );

  return { onSubmit, loading };
};

export const useLoadCategories = () => {
  const { data, loading } = useTalentCategoriesQuery();
  const subcategoriesOptions = useMemo(() => {
    return data?.talentCategories?.reduce<OptionsGroup[]>((acc, category) => {
      const subcategories = category?.subcategories || [];
      const mapped = subcategories.map((subcategory) => ({
        text: subcategory.name || '',
        value: `${category?.id}:${subcategory.id}`,
      }));

      return mapped?.length
        ? acc.concat({ label: category?.name || '', options: mapped })
        : acc;
    }, []);
  }, [data?.talentCategories]);

  return { subcategoriesOptions, isSubcategoriesLoading: loading };
};
