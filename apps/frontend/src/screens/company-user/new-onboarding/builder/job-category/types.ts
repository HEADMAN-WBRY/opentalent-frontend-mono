import { OptionType } from '@libs/ui/components/form/select';

export interface JobCategoryInfo {
  title: string;
  category: string;
  subcategory: OptionType;
}
