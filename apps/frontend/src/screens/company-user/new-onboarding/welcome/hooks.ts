import { gql, useQuery } from '@apollo/client';

import { Query } from '@libs/graphql-types';

export const GET_APP_INFO = gql`
  query GetAppInfo {
    commonAppInfo {
      total_ot_recruiters_count
      total_ot_freelancers_count
    }
  }
`;

export const useAppInfo = () => {
  const { loading, data } = useQuery<Query>(GET_APP_INFO);

  return {
    recruitersCount: data?.commonAppInfo?.total_ot_recruiters_count,
    talentsCount: data?.commonAppInfo?.total_ot_freelancers_count,
    loading,
  };
};
