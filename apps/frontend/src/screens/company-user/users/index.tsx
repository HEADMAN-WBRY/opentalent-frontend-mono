import React from 'react';

const Users = React.lazy(() => import('./Users'));

export default Users;
