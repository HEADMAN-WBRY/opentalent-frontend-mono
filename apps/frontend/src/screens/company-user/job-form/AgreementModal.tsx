import { Box, Grid } from '@mui/material';
import React from 'react';
import {
  DefaultModalProps,
  useOpenModal,
  withLocationStateModal,
} from 'utils/modals';

import Button from '@libs/ui/components/button';
import { DefaultModal } from '@libs/ui/components/modals';
import Typography from '@libs/ui/components/typography';

import { JobFormModals } from './types';

interface AgreementModalProps extends DefaultModalProps<true> {
  onSubmit: VoidFunction;
}

const AgreementModalComponent = ({
  onSubmit,
  isOpen,
  close,
}: AgreementModalProps) => {
  return (
    <DefaultModal
      actions={
        <Grid spacing={4} container>
          <Grid xs={8} item>
            <Button
              size="large"
              fullWidth
              color="primary"
              variant="contained"
              onClick={onSubmit}
            >
              save as draft
            </Button>
          </Grid>
          <Grid xs={4} item>
            <Button
              size="large"
              fullWidth
              variant="outlined"
              color="secondary"
              onClick={close}
            >
              Cancel
            </Button>
          </Grid>
        </Grid>
      }
      handleClose={close}
      open={isOpen}
      title="Sign an agreement"
    >
      <Box pt={4}>
        <Typography paragraph>
          Before posting a job you need to sign the Client’s agreement in the
          ‘Legal’ section of your account settings.
        </Typography>

        <Typography>
          After that we’ll give you full access to the app and you’ll be ready
          to hire people. You can save your job as draft for now.
        </Typography>
      </Box>
    </DefaultModal>
  );
};

export const AgreementModal = withLocationStateModal({
  id: JobFormModals.Agreement,
})(AgreementModalComponent);

export const useOpenAgreementModal = () =>
  useOpenModal(JobFormModals.Agreement);
