/* eslint-disable func-names, object-shorthand  */
import { DefaultSkillsRowItem } from 'components/custom/skills-boolean-search';
import { MAX_JOB_RATE } from 'consts/jobs';
import validationErrors from 'consts/validationErrors';
import ERRORS from 'consts/validationErrors';
import { isValid } from 'date-fns';
import { isNumber } from 'utils/common';
import * as yup from 'yup';

import { JobTypeEnum, SourceTypeEnum } from '@libs/graphql-types';
import { formatCurrency } from '@libs/helpers/format';
import { maxStringValidator } from '@libs/helpers/yup';
import { OptionType } from '@libs/ui/components/form/select';

// import { FINDERS_FEE_VALUES } from './consts';

const IGNORE_VALUES = ['', null, undefined];
const checkNumber = (value: any) =>
  IGNORE_VALUES.includes(value) ? true : isNumber(value);

// const findersFeeValidator = yup.number().nullable();
// .when(['campaign_talent_pool'], {
//   is: (pool: SourceTypeEnum) => {
//     return pool === SourceTypeEnum.Own;
//   },
//   then: (scheme) => scheme,
//   otherwise: (scheme) =>
//     scheme.min(FINDERS_FEE_VALUES.min).max(FINDERS_FEE_VALUES.max),
// });

const numberValidator = yup
  .string()
  .nullable()
  .transform(function (value) {
    const replaced = `${value}`.replace(',', '.');
    return isNumber(replaced) ? replaced : value;
  })
  .test({
    name: 'numberTest',
    test: checkNumber,
    message: validationErrors.invalidNumber,
  });

const getRateValidator = (
  type: 'rate_min' | 'rate_max' | 'min_salary' | 'max_salary',
) =>
  numberValidator.test({
    name: 'minNumberTest',
    test: function (value) {
      const firstValue: string = this.parent.rate_min; // this.parent[type === 'rate_min' ? 'rate_max' : 'rate_min'];
      if (isNumber(firstValue) && isNumber(value)) {
        return +firstValue <= value;
      }
      return true;
    },
    message: `Min rate is bigger than max rate`,
  });

const getSalaryValidator = () =>
  numberValidator.test({
    name: 'salaryRangeTest',
    test: function (value) {
      const minSalary: string = this.parent.salary_min;
      const maxSalary: string = this.parent.salary_max;
      if (
        !isNumber(minSalary) ||
        !isNumber(maxSalary) ||
        +minSalary === 0 ||
        +maxSalary === 0
      ) {
        return true;
      }
      return +minSalary <= +maxSalary;
    },
    message: `Min salary is bigger than max salary`,
  });

const getPeriodDateValidator = ({
  compareWith,
}: {
  compareWith: 'start_date' | 'end_date';
}) =>
  yup
    .date()
    .min(new Date(), validationErrors.presentDate)
    .when(compareWith, (startDate: number, schema) => {
      const compareWithStart = compareWith === 'start_date';
      return isValid(startDate)
        ? schema[compareWithStart ? 'min' : 'max'](
            startDate,
            validationErrors.invalidIntervalDate,
          )
        : schema;
    })
    .required();

const hasOpenTalentPool = (opt?: string[]) =>
  opt?.includes(SourceTypeEnum.Opentalent);

const locationTypeValidator = yup.string().when(['campaign_talent_pool'], {
  is: (pools: string[]) => {
    return hasOpenTalentPool(pools);
  },
  then: yup.string(),
  otherwise: yup.string().required(),
});

const requiredSkillsValidator = yup.array().when('is_boolean_search', {
  is: true,
  then: (schema) => schema,
  otherwise: yup
    .array()
    .min(2, validationErrors.mandatoryJobSkillsNumber())
    .max(6, validationErrors.mandatoryJobSkillsNumber()),
});

const skillsBooleanValidator = yup.object().when('is_boolean_search', {
  is: true,
  then: (schema) =>
    schema.shape(
      {
        not: yup
          .array()
          .of(yup.object())
          .when('items', {
            is: (items: DefaultSkillsRowItem[]) => {
              return items.every((i) => !i?.and?.length && !i?.or?.length);
            },
            then: (schema) => schema.min(1),
            otherwise: (schema) => schema,
          }),
        items: yup
          .array()
          .of(
            yup.object().shape(
              {
                or: yup
                  .array()
                  .of(yup.object())
                  .when('and', {
                    is: (items: OptionType[]) => !items?.length,
                    then: (schema) => schema.min(1),
                    otherwise: (schema) => schema,
                  }),
                and: yup
                  .array()
                  .of(yup.object())
                  .when('or', {
                    is: (items: OptionType[]) => !items?.length,
                    then: (schema) => schema.min(1),
                    otherwise: (schema) => schema,
                  }),
                relationWithNext: yup.string().required(),
              },
              ['or', 'and'] as any,
            ),
          )
          .when('not', {
            is: (not: OptionType[]) => !!not?.length,
            then: (schema) => schema,
            otherwise: (schema) => schema.min(1),
          }),
      },
      ['items', 'not'] as any,
    ),
  otherwise: (schema) => schema,
});

const freelanceValidator = yup.object().shape(
  {
    is_boolean_search: yup.boolean(),
    category_id: yup.string().required(),
    subcategory_id: yup.object().nullable().required(),
    name: maxStringValidator.required(),
    client: maxStringValidator,
    description: yup.string().required(),
    end_date: getPeriodDateValidator({ compareWith: 'start_date' }),
    start_date: getPeriodDateValidator({ compareWith: 'end_date' }),
    office_hours_per_month: yup
      .number()
      .integer(ERRORS.invalidNumber)
      .typeError(ERRORS.invalidNumber)
      .nullable()
      .min(0)
      .max(7),
    rate_min: getRateValidator('rate_min').test({
      name: 'maxRate',
      test: (val: any) => val === undefined || val <= MAX_JOB_RATE,
      message: `Maximum: ${formatCurrency(MAX_JOB_RATE)}`,
    }),
    rate_max: getRateValidator('rate_max').test({
      name: 'maxRate',
      test: (val) => val === undefined || (val || 0) <= MAX_JOB_RATE,
      message: `Maximum: ${formatCurrency(MAX_JOB_RATE)}`,
    }),
    location_type: locationTypeValidator,
    required_skills: requiredSkillsValidator,

    skills_boolean_v2: skillsBooleanValidator,

    campaign_start_date: yup
      .date()
      .nullable()
      .when(
        'is_instant_campaign_start',
        (is_instant_campaign_start: boolean, schema: any) => {
          return is_instant_campaign_start
            ? schema
            : schema.required(validationErrors.required);
        },
      ),
    campaign_duration: yup.number().required(),
    campaign_owner_id: yup.string().required(),
    // campaign_talent_pool: yup.string().required(),
    // finders_fee: findersFeeValidator,
  },
  [
    ['rate_max', 'rate_min'],
    ['start_date', 'end_date'],
  ] as any,
);

const permanentValidator = yup.object().shape(
  {
    is_boolean_search: yup.boolean(),
    category_id: yup.string().required(),
    subcategory_id: yup.object().nullable().required(),
    name: maxStringValidator.required(),
    client: maxStringValidator,
    description: yup.string().required(),
    office_hours_per_month: yup
      .number()
      .integer(ERRORS.invalidNumber)
      .typeError(ERRORS.invalidNumber)
      .nullable()
      .min(0)
      .max(7),
    start_date: getPeriodDateValidator({ compareWith: 'end_date' }),
    salary_min: getSalaryValidator(),
    salary_max: getSalaryValidator(),
    location_type: yup.string().required(),

    required_skills: requiredSkillsValidator,
    skills_boolean_v2: skillsBooleanValidator,

    campaign_duration: yup.number().required(),
    campaign_start_date: yup
      .date()
      .nullable()
      .when(
        'is_instant_campaign_start',
        (is_instant_campaign_start: boolean, schema: any) => {
          return is_instant_campaign_start
            ? schema
            : schema.required(validationErrors.required);
        },
      ),
    campaign_owner_id: yup.string().required(),

    // finders_fee: findersFeeValidator.required(),
  },
  [
    ['min_salary', 'max_salary'],
    ['start_date', 'end_date'],
  ] as any,
);

const projectValidator = yup.object().shape(
  {
    name: maxStringValidator.required(),
    description: yup.string().required(),
    end_date: getPeriodDateValidator({ compareWith: 'start_date' }),
    start_date: getPeriodDateValidator({ compareWith: 'end_date' }),
    max_project_budget: numberValidator,

    skills_required: yup.array().min(1),

    campaign_start_date: yup
      .date()
      .nullable()
      .when(
        'is_instant_campaign_start',
        (is_instant_campaign_start: boolean, schema: any) => {
          return is_instant_campaign_start
            ? schema
            : schema.required(validationErrors.required);
        },
      ),
    campaign_duration: yup.number().required(),
    campaign_owner_id: yup.string().required(),
  },
  [['start_date', 'end_date']] as any,
);

const validator = {
  [JobTypeEnum.Freelance]: freelanceValidator,
  [JobTypeEnum.Permanent]: permanentValidator,
  [JobTypeEnum.Project]: projectValidator,
};

export default validator;
