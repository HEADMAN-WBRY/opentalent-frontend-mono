/* eslint-disable no-nested-ternary */
import { BooleanModal } from 'components/custom/skills-boolean-search';
import ScrollToFirstError from 'components/form/scroll-to-first-error';
import { ConnectedPageLayout } from 'components/layout/page-layout';
import { Formik } from 'formik';
import { useCompanyContract } from 'hooks/auth/useCompanyContract';
import { useCurrentTime } from 'hooks/common/useCurrentTime';
import useMediaQueries from 'hooks/common/useMediaQueries';
import { usePushWithQuery } from 'hooks/routing';
import React from 'react';
import { pathManager } from 'routes/consts';
import { CampaignStatus, getCampaignStatus } from 'utils/job';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { JobServiceTypeEnum, JobTypeEnum, User } from '@libs/graphql-types';
import { modelPath } from '@libs/helpers/form';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import { STEPS_ITEMS } from '../choose-new-job-type/consts';
import TopSteps from '../choose-new-job-type/shared/TopSteps';
import { AgreementModal, useOpenAgreementModal } from './AgreementModal';
import ContractAlert from './ContractAlert';
import InfoBlock from './InfoBlock';
import { EDIT_JOB_TEST_ATTRS } from './consts';
import Details from './details';
import FindersFee from './finders-fee';
import General from './general';
import { useInitialValues, useScreenData, useSubmitHandler } from './hooks';
import Skills from './skills';
import ConnectedTalentsCount from './talents-count';
import { CreateJobForm, PageProps } from './types';
import { getPageTexts } from './utils';
import validators from './validator';

const useStyles = makeStyles((theme) => ({
  form: {
    width: 624,

    [theme.breakpoints.down('md')]: {
      width: '100%',
    },
  },
  contentWrapper: {
    position: 'relative',

    [theme.breakpoints.down('md')]: {
      padding: theme.spacing(4),
    },
  },
  contentRoot: {
    paddingTop: theme.spacing(4),

    [theme.breakpoints.down('md')]: {
      paddingTop: 0,
    },
  },

  '@global': {
    '.MuiPopover-root': {
      zIndex: 1500,
    },
  },
}));

const JobForm = ({ match }: PageProps) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const currentTime = useCurrentTime();
  const push = usePushWithQuery();
  const { isMD } = useMediaQueries();
  const openAgreementModal = useOpenAgreementModal();
  const { isContractSigned } = useCompanyContract();
  const jobId = match.params?.id;
  const isDuplicate =
    !!jobId &&
    match.url === pathManager.company.duplicateJob.generatePath({ id: jobId });
  const { data, loading: dataLoading } = useScreenData({
    jobId,
    isDuplicate,
  });
  const {
    loading: submitLoading,
    onSubmit,
    setRedirectToAgreement,
  } = useSubmitHandler({
    jobId,
    isDuplicate,
  });
  const classes = useStyles();
  const isItDraft = !!data?.job?.is_draft;
  const isCreate = !jobId;
  const campaignStatus =
    isDuplicate || isItDraft
      ? CampaignStatus.NotStarted
      : getCampaignStatus(data?.job);

  const initialValues = useInitialValues({
    job: data?.job,
    isCreate,
    isDuplicate,
    jobType: data?.job?.type || JobTypeEnum.Freelance,
    isContractSigned,
  });

  const jobNotFound = !!jobId && !data?.job;
  const texts = getPageTexts({
    name: initialValues.name,
    isCreate,
    isDuplicate,
    jobType: initialValues.type,
    serviceType: initialValues.service_type,
    isItDraft,
  });

  const jobType = initialValues.type;

  const steps = STEPS_ITEMS[initialValues.service_type!];

  return (
    <ConnectedPageLayout
      documentTitle="Job board"
      headerProps={{ accountProps: {} }}
      drawerProps={{}}
      contentSpacing={6}
      isLoading={dataLoading}
      classes={{ contentWrapper: classes.contentWrapper }}
    >
      <Box className={classes.contentRoot}>
        <Typography variant="h5" paragraph>
          {texts.title}
        </Typography>
        {!jobId && (
          <>
            <TopSteps onlyActiveLabel={isMD} steps={steps} activeIndex={2} />
            <Box mt={4}>
              <InfoBlock jobType={jobType} />
            </Box>
          </>
        )}
        {jobNotFound && (
          <Typography paragraph variant="body1">
            Job not found
          </Typography>
        )}
        {!jobNotFound && (
          <Formik
            validationSchema={validators[jobType]}
            initialValues={initialValues}
            onSubmit={onSubmit}
            validateOnChange={false}
          >
            {({ handleSubmit, values, setFieldValue }) => (
              <Box className={classes.form}>
                <BooleanModal
                  onSave={(state) =>
                    setFieldValue(
                      modelPath<CreateJobForm>((m) => m.skills_boolean_v2),
                      state,
                    )
                  }
                />
                <ScrollToFirstError />
                {values.service_type === JobServiceTypeEnum.SelfService && (
                  <ConnectedTalentsCount jobType={jobType} />
                )}
                <General
                  status={campaignStatus}
                  jobType={jobType}
                  remoteIsOption={!!values.is_remote_an_option}
                  locationType={values.location_type}
                  talentPool={values.campaign_talent_pool}
                />
                <Skills
                  status={campaignStatus}
                  isBooleanSearch={!!values.is_boolean_search}
                />
                <Details
                  isInstant={!!values?.is_instant_campaign_start}
                  isEdit={!isDuplicate && !!jobId}
                  status={campaignStatus}
                  owners={data?.currentCompanyUserColleagues as User[]}
                  isItDraft={isItDraft}
                />
                {values.service_type !== JobServiceTypeEnum.DedicatedSearch && (
                  <FindersFee
                    currentValue={Number(initialValues.finders_fee) || 0}
                    status={campaignStatus}
                    talentPool={values.campaign_talent_pool}
                  />
                )}
                <Grid spacing={4} container>
                  <Grid item>
                    <Button
                      size="large"
                      data-test-id={EDIT_JOB_TEST_ATTRS.submitButton}
                      onClick={() => {
                        if (!isContractSigned) {
                          openAgreementModal();
                        } else {
                          setFieldValue('is_draft', false);
                          handleSubmit();
                        }
                      }}
                      variant="contained"
                      color="primary"
                      disabled={
                        submitLoading ||
                        campaignStatus === CampaignStatus.Finished
                      }
                    >
                      {texts.submit}
                    </Button>
                  </Grid>
                  {(isItDraft || isCreate) && (
                    <Grid item>
                      <Button
                        size="large"
                        data-test-id={EDIT_JOB_TEST_ATTRS.saveButton}
                        onClick={() => {
                          setFieldValue('is_draft', true);
                          handleSubmit();
                        }}
                        variant="outlined"
                        color="secondary"
                        disabled={
                          submitLoading ||
                          campaignStatus === CampaignStatus.Finished
                        }
                      >
                        {texts.save}
                      </Button>
                    </Grid>
                  )}
                </Grid>
                <AgreementModal
                  onSubmit={() => {
                    push({ query: { modal: '' } });
                    setFieldValue('is_draft', true);
                    setRedirectToAgreement(true);
                    handleSubmit();
                  }}
                />

                {!isContractSigned && (
                  <Box style={{ maxWidth: 560 }} pt={4}>
                    <ContractAlert />
                  </Box>
                )}
              </Box>
            )}
          </Formik>
        )}
      </Box>
    </ConnectedPageLayout>
  );
};

export default JobForm;
