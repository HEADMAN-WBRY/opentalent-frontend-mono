import { useMutation, useQuery } from '@apollo/client';
import { useCurrentUser } from 'hooks/auth';
import { useSearchParams } from 'hooks/routing';
import { useSnackbar } from 'notistack';
import { useCallback, useMemo, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { pathManager } from 'routes';
import { stringify } from 'utils/querystring';

import {
  Job,
  JobServiceTypeEnum,
  JobTypeEnum,
  MutationCreateJobArgs,
  MutationUpdateJobArgs,
  Query,
  QueryJobArgs,
  SourceTypeEnum,
} from '@libs/graphql-types';

import { SettingsTabs } from '../settings/types';
import {
  CREATE_NEW_JOB,
  GET_NEW_JOB_DATA,
  GET_EDIT_JOB_DATA,
  UPDATE_JOB,
} from './queries';
import { CreateJobForm } from './types';
import { getInitialValues, mapJobToServer } from './utils';

interface HookParams {
  jobId?: string;
  isDuplicate: boolean;
}

export const useSubmitHandler = ({ jobId, isDuplicate }: HookParams) => {
  const { enqueueSnackbar } = useSnackbar();
  const [isDraft, setIsDraft] = useState(false);
  const [needRedirectToAgreement, setRedirectToAgreement] = useState(false);
  const history = useHistory();
  const [action, { loading }] = useMutation<
    any,
    MutationCreateJobArgs | MutationUpdateJobArgs
  >(jobId && !isDuplicate ? UPDATE_JOB : CREATE_NEW_JOB, {
    onCompleted: ({ createJob }) => {
      enqueueSnackbar(
        `Job successfully ${
          // eslint-disable-next-line no-nested-ternary
          isDuplicate ? 'duplicated' : jobId ? 'updated' : 'created'
        }`,
        {
          variant: 'success',
        },
      );

      if (isDraft) {
        if (needRedirectToAgreement) {
          history.push({
            pathname: pathManager.company.settings.main.generatePath(),
            search: stringify({ tab: SettingsTabs.Legal }),
          });
          return;
        }

        history.push({
          pathname: pathManager.company.jobBoard.generatePath(),
          search: stringify({ jobsType: 'draft' }),
        });
      } else {
        history.push(
          pathManager.company.job.generatePath({
            id: createJob || jobId,
          }),
        );
      }
    },
  });

  const mapForm = useCallback(
    (jobForm: CreateJobForm) => ({
      variables: {
        ...mapJobToServer(jobForm),
        ...(jobId ? { id: jobId } : {}),
      } as any,
      ...(jobId
        ? {
            refetchQueries: [
              { query: GET_EDIT_JOB_DATA, variables: { id: jobId } },
            ],
          }
        : {}),
    }),
    [jobId],
  );

  const onSubmit = useCallback(
    (jobForm: CreateJobForm) => {
      setIsDraft(jobForm.is_draft || false);
      action(mapForm(jobForm));
    },
    [action, mapForm],
  );

  return { onSubmit, loading, setRedirectToAgreement };
};

export const useScreenData = ({ jobId }: HookParams) => {
  const query = jobId ? GET_EDIT_JOB_DATA : GET_NEW_JOB_DATA;
  const { data, loading } = useQuery<Query, QueryJobArgs>(query, {
    variables: { id: jobId || '' },
  });

  return { data, loading };
};

const useSearchQueryValues = () => {
  const { otCommunity, selfCommunity, serviceType, jobType } =
    useSearchParams();
  const pools = Object.entries({
    [SourceTypeEnum.Own]: selfCommunity,
    [SourceTypeEnum.Opentalent]: otCommunity,
  })
    .filter(([_, value]) => !!value)
    .map(([key]) => key);

  return {
    service_type: serviceType as JobServiceTypeEnum,
    campaign_talent_pool: pools as unknown as SourceTypeEnum[],
    ...(jobType && { type: jobType as JobTypeEnum }),
  };
};

export const useInitialValues = ({
  job,
  isCreate,
  isDuplicate,
  jobType,
  isContractSigned,
}: {
  job?: Job;
  isCreate: boolean;
  isDuplicate: boolean;
  jobType: JobTypeEnum;
  isContractSigned: boolean;
}) => {
  const talentPoolOptions = useTalentPoolOptions();
  const { data } = useCurrentUser();
  const searchData = useSearchQueryValues();
  const initials = getInitialValues({ job, talentPoolOptions, isDuplicate });

  return {
    is_draft: !isContractSigned,
    ...initials,
    type: jobType,
    ...(isCreate && searchData),
    ...(isCreate &&
      data?.currentCompanyUser && {
        client: data?.currentCompanyUser?.company?.name,
        campaign_owner_id:
          data?.currentCompanyUser?.id || initials?.campaign_owner_id,
      }),
  };
};

export const useTalentPoolOptions = () => {
  const { data } = useCurrentUser();
  const companyName = data?.currentCompanyUser?.company?.name;

  return useMemo(
    () => [
      { text: `My Workforce (${companyName})`, value: SourceTypeEnum.Own },
      { text: 'OpenTalent network', value: SourceTypeEnum.Opentalent },
    ],
    [companyName],
  );
};
