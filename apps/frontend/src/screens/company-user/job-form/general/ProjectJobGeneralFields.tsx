import { Grid } from '@mui/material';
import { ConnectedTextEditor } from 'components/form/text-editor';
import { addDays } from 'date-fns';
import React from 'react';
import { CampaignStatus } from 'utils/job';

import { ConnectedDatePicker } from '@libs/ui/components/form/datepicker';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import StepSection from '@libs/ui/components/step-section';

import { getFieldActivity } from '../utils';
import { GeneralProps } from './types';

const ProjectJobGeneralFields = ({ status }: GeneralProps) => {
  return (
    <StepSection index={1} title="General information">
      <Grid spacing={4} direction="column" container>
        <Grid item>
          <ConnectedTextField
            name="name"
            fullWidth
            variant="filled"
            label="Title (e.g. UX designer, PHP developer)"
            disabled={getFieldActivity({
              status,
              blockStatuses: [CampaignStatus.Finished],
            })}
          />
        </Grid>
        <Grid item>
          <ConnectedTextEditor
            disabled={getFieldActivity({
              status,
              blockStatuses: [CampaignStatus.Finished],
            })}
            placeholder="Description"
            name="description"
          />
        </Grid>
        <Grid item>
          <Grid spacing={4} container>
            <Grid xs={12} sm={6} item>
              <ConnectedDatePicker
                inputFormat="dd/MM/yyyy"
                name="start_date"
                minDate={addDays(new Date(), 1)}
                TextFieldProps={{
                  variant: 'filled',
                  fullWidth: true,
                  label: 'Starting date',
                }}
                disabled={getFieldActivity({
                  status,
                  blockStatuses: [
                    CampaignStatus.Finished,
                    CampaignStatus.Started,
                  ],
                })}
              />
            </Grid>
            <Grid xs={12} sm={6} item>
              <ConnectedDatePicker
                inputFormat="dd/MM/yyyy"
                name="end_date"
                minDate={addDays(new Date(), 1)}
                TextFieldProps={{
                  variant: 'filled',
                  fullWidth: true,
                  label: 'Ending date',
                }}
                disabled={getFieldActivity({
                  status,
                  blockStatuses: [
                    CampaignStatus.Finished,
                    CampaignStatus.Started,
                  ],
                })}
              />
            </Grid>
          </Grid>
        </Grid>

        <Grid item>
          <ConnectedTextField
            name="max_project_budget"
            fullWidth
            variant="filled"
            label="max_project_budget"
            InputProps={{ endAdornment: '€' }}
            disabled={getFieldActivity({
              status,
              blockStatuses: [CampaignStatus.Finished, CampaignStatus.Started],
            })}
          />
        </Grid>
      </Grid>
    </StepSection>
  );
};

export default ProjectJobGeneralFields;
