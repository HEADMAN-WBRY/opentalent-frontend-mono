import React from 'react';

import { ConnectedSelect, OptionType } from '@libs/ui/components/form/select';

interface MinDaysPerWeekSelectorProps {
  disabled?: boolean;
  name: string;
}

const DAYS_OPTIONS: OptionType[] = Array.from({ length: 5 }, (_, index) => ({
  text: `${index + 1} day${!!index ? 's' : ''}`,
  value: index + 1,
}));

const MinDaysPerWeekSelector = ({
  disabled,
  name,
}: MinDaysPerWeekSelectorProps) => {
  return (
    <ConnectedSelect
      name={name}
      fullWidth
      variant="filled"
      label="Min. days per week at the office"
      hideNoneValue
      options={DAYS_OPTIONS}
      disabled={disabled}
    />
  );
};

export default MinDaysPerWeekSelector;
