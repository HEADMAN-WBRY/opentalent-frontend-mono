import { useLazyQuery } from '@apollo/client';
import { debounce } from '@mui/material';
import { SEARCH_SKILL } from 'graphql/skills';
import { useCallback, useMemo, useState } from 'react';

import {
  Query,
  QuerySkillSearchArgs,
  SkillTypeEnum,
} from '@libs/graphql-types';
import { OptionType } from '@libs/ui/components/form/select';

const useSkillsSuggestion = ({
  activeSkillType,
}: {
  activeSkillType?: SkillTypeEnum[];
}) => {
  const [getSkills, { data, variables: suggestVariables, loading }] =
    useLazyQuery<Query, QuerySkillSearchArgs>(SEARCH_SKILL, {
      fetchPolicy: 'network-only',
    });
  const getSkillsSuggest = useMemo(() => debounce(getSkills, 300), [getSkills]);
  const skillsSuggest: OptionType[] = useMemo(() => {
    const currentSuggest = (data?.skillSearch ?? [])?.map(
      ({ id, name, ...rest }) => ({
        ...rest,
        value: id,
        text: name,
      }),
    );
    return activeSkillType &&
      suggestVariables?.skill_type?.every((i) =>
        activeSkillType.includes(i as SkillTypeEnum),
      )
      ? currentSuggest
      : [];
  }, [data, suggestVariables, activeSkillType]);

  return { getSkillsSuggest, skillsSuggest, loading };
};

export const useAddSkill = () => {
  const [inputValue, setInputValue] = useState<string>();
  const [activeSkillType, setActiveTypeSkill] = useState<
    { type: SkillTypeEnum[]; name: string } | null | undefined
  >();

  const { getSkillsSuggest, skillsSuggest, loading } = useSkillsSuggestion({
    activeSkillType: activeSkillType?.type,
  });

  const onInputBlur = useCallback(() => {
    setActiveTypeSkill(null);
    setInputValue('');
  }, []);
  const onInputFocus = (type: SkillTypeEnum[]) => (e: any) => {
    e.persist();
    setActiveTypeSkill({ type, name: e.target.name });
  };
  const onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const inputValue = e?.target?.value || '';
    if (inputValue.length > 1) {
      setInputValue(e?.target?.value);
      getSkillsSuggest({
        variables: {
          search: e?.target?.value || '',
          skill_type: activeSkillType?.type,
        },
      });
    }
  };

  const onSelectSkill = () => {
    setInputValue('');
  };

  return {
    onInputBlur,
    onInputFocus,
    onInputChange,
    skillsSuggest,
    onSelectSkill,
    inputValue,
    loading,
    activeSkillType: activeSkillType?.type,
  };
};
