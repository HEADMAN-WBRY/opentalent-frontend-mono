import { Box, Grid } from '@mui/material';
import { ReactComponent as WarningIcon } from 'assets/icons/warning.svg';
import React from 'react';

import Typography from '@libs/ui/components/typography';

import { useOpenAgreementModal } from './AgreementModal';

interface ContractAlertProps {}

const ContractAlert = (props: ContractAlertProps) => {
  const openAgreementModal = useOpenAgreementModal();

  return (
    <Box pt={6}>
      <Grid style={{ maxWidth: 500 }} wrap="nowrap" spacing={4} container>
        <Grid item>
          <WarningIcon style={{ marginTop: 8 }} />
        </Grid>
        <Grid item>
          <Typography style={{ paddingBottom: 8 }}>
            You are now in demo mode of OpenTalent.
          </Typography>
          <Typography style={{ paddingBottom: 8 }}>
            To get full access to OpenTalent, and begin to hire people, please
            go to{' '}
            <Typography
              onClick={() => openAgreementModal()}
              component="span"
              color="tertiary"
              pointer
            >
              Settings {'>'} Legal
            </Typography>{' '}
            and request to view our “Hiring Agreement”.
          </Typography>
        </Grid>
      </Grid>
    </Box>
  );
};

export default ContractAlert;
