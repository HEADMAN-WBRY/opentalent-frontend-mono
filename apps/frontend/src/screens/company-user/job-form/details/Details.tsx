import { addDays } from 'date-fns';
import React, { useMemo } from 'react';
import { CampaignStatus } from 'utils/job';

import { Grid } from '@mui/material';

import { User } from '@libs/graphql-types';
import { ConnectedCheckbox } from '@libs/ui/components/form/checkbox';
import { ConnectedDatePicker } from '@libs/ui/components/form/datepicker';
import { ConnectedSelect } from '@libs/ui/components/form/select';
import StepSection from '@libs/ui/components/step-section';
import Typography from '@libs/ui/components/typography';

import { getFieldActivity, getOptionFromUser } from '../utils';

interface DetailsProps {
  owners?: User[];
  status: CampaignStatus;
  isEdit: boolean;
  isInstant: boolean;
  isItDraft: boolean;
}

const DURATION_OPTIONS = [
  { text: '2 days', value: 2 },
  { text: '5 days', value: 5 },
  { text: '10 days', value: 10 },
  { text: '30 days', value: 30 },
];

const Details = ({
  owners = [],
  status,
  isEdit,
  isInstant,
  isItDraft,
}: DetailsProps) => {
  const ownersOptions = useMemo(() => owners.map(getOptionFromUser), [owners]);

  return (
    <StepSection index={3} title="Campaign details">
      <Typography paragraph>
        Tell us when you want to run this campaign.
      </Typography>
      <Grid spacing={4} direction="column" container>
        <Grid item>
          <ConnectedDatePicker
            inputFormat="dd/MM/yyyy"
            name="campaign_start_date"
            label="Campaign start date"
            minDate={addDays(new Date(), 1)}
            TextFieldProps={{
              variant: 'filled',
              fullWidth: true,
              label: 'Campaign starting date',
            }}
            disabled={
              getFieldActivity({
                status,
                blockStatuses: [
                  CampaignStatus.Finished,
                  CampaignStatus.Started,
                ],
              }) || isInstant
            }
          />
        </Grid>
        <Grid item>
          <ConnectedCheckbox
            name="is_instant_campaign_start"
            label="Start campaign now"
            disabled={
              !isItDraft &&
              (getFieldActivity({
                status,
                blockStatuses: [
                  CampaignStatus.Finished,
                  CampaignStatus.Started,
                ],
              }) ||
                isEdit)
            }
          />
        </Grid>
        <Grid item>
          <ConnectedSelect
            name="campaign_duration"
            fullWidth
            options={DURATION_OPTIONS}
            variant="filled"
            label="Campaign period"
            formControlProps={{ size: 'small' }}
            disabled={getFieldActivity({
              status,
              blockStatuses: [CampaignStatus.Finished],
            })}
          />
        </Grid>
        <Grid item>
          <ConnectedSelect
            name="campaign_owner_id"
            options={ownersOptions}
            fullWidth
            variant="filled"
            label="Campaign owner (hiring manager)"
            formControlProps={{ size: 'small' }}
            disabled={getFieldActivity({
              status,
              blockStatuses: [CampaignStatus.Finished],
            })}
          />
        </Grid>
      </Grid>
    </StepSection>
  );
};

export default Details;
