import { BooleanModalState } from 'components/custom/skills-boolean-search';
import { RouteComponentProps } from 'react-router-dom';

import { MutationCreateJobArgs, SourceTypeEnum } from '@libs/graphql-types';
import { OptionType } from '@libs/ui/components/form/select';

export type PageProps = RouteComponentProps<{ id: string }>;

export interface CreateJobForm
  extends Omit<
    MutationCreateJobArgs,
    | 'campaign_end_date'
    | 'skills'
    | 'solutions_required'
    | 'hard_skills_required'
    | 'campaign_talent_pool'
    | 'subcategory_id'
  > {
  campaign_duration: number;
  skills: OptionType[];
  skills_boolean_v2: BooleanModalState;
  required_skills: OptionType[];
  campaign_talent_pool: SourceTypeEnum[];
  subcategory_id: OptionType;
  is_boolean_search: boolean;
}

export enum JobFormModals {
  Agreement = 'agreement',
}
