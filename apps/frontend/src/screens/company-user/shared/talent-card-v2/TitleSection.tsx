import { ReactComponent as CheckIcon } from 'assets/icons/check.svg';
import { TalentActions } from 'components/custom/talent/actions-menu';
import React from 'react';

import { Grid, Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { Talent } from '@libs/graphql-types';
import { stopEvent } from '@libs/helpers/common';
import Chip from '@libs/ui/components/chip';
import Typography from '@libs/ui/components/typography';

import RemindText from '../talent-card/RemindText';

interface TitleSectionProps {
  name: string;
  talent: Talent;
  refetch: VoidFunction;
}

const useStyles = makeStyles((theme) => ({
  checkIcon: {
    transform: 'translateY(5px)',
    display: 'inline-block',
  },
  titleChip: {
    transform: 'translateY(-2px)',
  },
  inviteButton: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'flex-end',
  },
  pendingVerification: {
    backgroundColor: theme.palette.warning.dark,
    color: theme.palette.secondary.contrastText,
    borderRadius: '16px',
    padding: '0 6px',
    lineHeight: '20px',
    height: '20px',
    fontSize: '12px',
    marginLeft: theme.spacing(4),
  },
}));

const TitleSection = ({ name, talent, refetch }: TitleSectionProps) => {
  const classes = useStyles();
  const status = talent?.is_invitation_accepted ? (
    <CheckIcon className={classes.checkIcon} />
  ) : (
    <Chip
      className={classes.titleChip}
      label="pending activation"
      color="grey"
      size="small"
    />
  );

  return (
    <Grid container wrap="nowrap">
      <Grid item flexGrow={1}>
        <Box mb={1}>
          <Typography component="span" variant="h6">
            {name}
          </Typography>

          <Box display="inline-block" ml={2}>
            {status}
          </Box>

          {talent?.is_recruiter && (
            <Box display="inline-block" ml={2}>
              <Chip
                size="small"
                label="Recruiter"
                className={classes.titleChip}
              />
            </Box>
          )}

          {talent?.is_verification_required && (
            <Box display="inline-block" ml={2}>
              <Chip
                size="small"
                label="pending verification"
                color="tertiary"
                className={classes.titleChip}
              />
            </Box>
          )}
        </Box>
        <Box mb={1}>
          <Typography variant="body2">
            {talent.recent_position_title}
          </Typography>
        </Box>
      </Grid>

      <Grid item>
        <Box onClick={stopEvent} className={classes.inviteButton}>
          <TalentActions talent={talent} refetch={refetch} />
        </Box>
        {!talent?.is_invitation_accepted && (
          <Box pt={2}>
            <RemindText talent={talent} />
          </Box>
        )}
      </Grid>
    </Grid>
  );
};

export default TitleSection;
