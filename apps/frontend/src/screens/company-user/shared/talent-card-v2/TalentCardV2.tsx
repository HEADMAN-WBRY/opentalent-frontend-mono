import { useTalentName } from 'hooks/talents';
import React from 'react';
import { Link } from 'react-router-dom';
import { pathManager } from 'routes';

import { Box, Grid, Paper } from '@mui/material';

import { Talent, TalentSearchResult } from '@libs/graphql-types';

import CategoriesList from './CategoriesList';
import Highlights from './Highlights';
import LogoBlock from './LogoBlock';
import MainInfo from './MainInfo';
import TitleSection from './TitleSection';

interface TalentCardV2Props {
  talentSearch: TalentSearchResult;
  refetch: VoidFunction;
}

const TalentCardV2 = (props: TalentCardV2Props) => {
  const { talentSearch, refetch } = props;
  const talent = talentSearch?.talent as Talent;
  const highlights = talentSearch?.highlights || [];
  const name = useTalentName(talent);
  const talentLink = pathManager.company.talentProfile.generatePath({
    id: talent.id,
  });

  return (
    <Link to={talentLink}>
      <Paper component={Box} p={6} data-test-id="talent-card" elevation={0}>
        <Grid wrap="nowrap" spacing={6} container>
          <Grid item>
            <LogoBlock name={name} talent={talent} />
          </Grid>
          <Grid flexGrow={1} item>
            <TitleSection refetch={refetch} talent={talent} name={name} />

            <MainInfo talent={talent} />

            <CategoriesList talent={talent} />
          </Grid>
        </Grid>

        {!!highlights.length && (
          <Box mt={3}>
            <Highlights items={highlights} />
          </Box>
        )}
      </Paper>
    </Link>
  );
};

export default TalentCardV2;
