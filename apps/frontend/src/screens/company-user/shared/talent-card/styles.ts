import { DEFAULT_AVATAR } from 'consts/common';

import { makeStyles } from '@mui/styles';

import { TalentCardProps } from './TalentCard';

const useStyles = makeStyles((theme) => ({
  paper: {
    '&:hover': {
      boxShadow: theme.shadows[6],
      cursor: 'pointer',
    },
    maxWidth: 744,
    borderRadius: 4,
    padding: 24,
  },
  recruiterChip: {
    background: theme.palette.tertiary.light,
  },
  avatarWrapper: {
    marginRight: 24,
    [theme.breakpoints.down('md')]: {
      marginRight: 12,
    },
    [theme.breakpoints.down('sm')]: {
      marginRight: 16,
    },
  },
  avatarBadge: {
    background: ({ talentSearch }: TalentCardProps) =>
      talentSearch?.talent?.available_now
        ? theme.palette.primary.light
        : theme.palette.grey[500],
    border: `2px solid ${theme.palette.grey[200]}`,
    width: 22,
    height: 22,
  },
  avatar: {
    background: `url(${DEFAULT_AVATAR}) no-repeat center`,
    border: `2px solid ${theme.palette.grey[200]}`,
    borderRadius: '100%',
    [theme.breakpoints.down('md')]: {
      width: theme.spacing(18),
      height: theme.spacing(18),
    },
    [theme.breakpoints.down('sm')]: {
      width: theme.spacing(14),
      height: theme.spacing(14),
    },
    width: theme.spacing(24),
    height: theme.spacing(24),
  },
  inviteButton: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'flex-end',
  },
  firstRow: {
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  secondRow: {
    [theme.breakpoints.down('sm')]: {
      marginBottom: 0,
    },
    marginBottom: 20,
  },
  hRate: {
    marginRight: 4,
  },
  contentWrapper: {
    width: '100%',
    overflow: 'hidden',
  },
  successCheck: {
    color: theme.palette.info.main,
  },
  nameAndVerification: {
    display: 'flex',
    alignItems: 'center',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
  },
  nameTitle: {
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
  },
  pendingVerification: {
    backgroundColor: theme.palette.warning.dark,
    color: theme.palette.secondary.contrastText,
    borderRadius: '16px',
    padding: '0 6px',
    lineHeight: '20px',
    height: '20px',
    fontSize: '12px',
    marginLeft: theme.spacing(4),
  },

  actionsBlock: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
}));

export default useStyles;
