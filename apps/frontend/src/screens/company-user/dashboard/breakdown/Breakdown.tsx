import React from 'react';

import { DashboardAnalyticsItem, Maybe } from '@libs/graphql-types';

import DataBlock from '../data-block';
import BreakdownChart from './BreakdownChart';

interface BreakdownProps {
  data: Maybe<DashboardAnalyticsItem>[];
}

const Breakdown = ({ data }: BreakdownProps) => {
  return (
    <DataBlock fullHeight title="Breakdown by categories">
      <BreakdownChart data={data} />
    </DataBlock>
  );
};

export default Breakdown;
