import { Box, Paper } from '@mui/material';
import React from 'react';

import Typography from '@libs/ui/components/typography';

interface DataBlockProps extends React.HTMLAttributes<HTMLDivElement> {
  title: string;
  fullHeight?: boolean;
  button?: JSX.Element;
}

const DataBlock = ({ children, title, fullHeight, button }: DataBlockProps) => {
  return (
    <Paper style={{ minHeight: fullHeight ? '100%' : 'auto' }}>
      <Box padding={5}>
        <Box style={{ display: 'flex' }}>
          <Typography component="span" variant="h6" paragraph>
            {title}
          </Typography>
          {button}
        </Box>
        <Box>{children}</Box>
      </Box>
    </Paper>
  );
};

export default DataBlock;
