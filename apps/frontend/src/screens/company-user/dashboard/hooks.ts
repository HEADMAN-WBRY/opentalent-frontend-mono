import { useQuery } from '@apollo/client';
import { useSnackbar } from 'notistack';

import { Query } from '@libs/graphql-types';

import { GET_DASHBOARD_INFO } from './queries';

export const useDashboardData = () => {
  const { enqueueSnackbar } = useSnackbar();
  const { data, loading } = useQuery<Query>(GET_DASHBOARD_INFO, {
    fetchPolicy: 'network-only',
    onError: () => enqueueSnackbar('Filed to load data', { variant: 'error' }),
  });
  return { data, loading };
};
