import { Grid } from '@mui/material';
import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';

import { DashboardAnalyticsItem, Maybe } from '@libs/graphql-types';

import DataBlock from '../data-block';
import ByCountryChart from './ByCountryChart';

interface ByCountryProps {
  data: Maybe<DashboardAnalyticsItem>[];
}

const ByCountry = (props: ByCountryProps) => {
  const { isSM } = useMediaQueries();
  const data = ((props?.data as DashboardAnalyticsItem[]) || []).map(
    ({ key, value }) => [key, value],
  );

  return (
    <DataBlock title="Breakdown by country">
      <Grid spacing={4} direction={isSM ? 'column' : 'row'} container>
        <Grid xs={isSM ? 12 : 6} item container direction="column">
          {data.map(([country, value]) => (
            <Grid justifyContent="space-between" key={country} item container>
              <Grid item>{country}</Grid>
              <Grid item>{value}</Grid>
            </Grid>
          ))}
        </Grid>
        {!!data.length && (
          <Grid xs={isSM ? 12 : 6} item>
            <ByCountryChart data={data} />
          </Grid>
        )}
      </Grid>
    </DataBlock>
  );
};

export default ByCountry;
