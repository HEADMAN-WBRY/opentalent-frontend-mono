import { Grid } from '@mui/material';
import { ConnectedPageLayout } from 'components/layout/page-layout';
import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';

import Typography from '@libs/ui/components/typography';

import Breakdown from './breakdown';
import ByCountry from './by-country';
import { useDashboardData } from './hooks';
import LatestAdditions from './latest-additions';
import YourWorkforce from './your-workforce';

interface DashboardProps {}

const Dashboard = (props: DashboardProps) => {
  const { isXS, isSM } = useMediaQueries();
  const { data, loading } = useDashboardData();

  return (
    <ConnectedPageLayout
      documentTitle="Dashboard"
      contentSpacing={isXS ? 4 : 8}
      isLoading={loading}
      drawerProps={{}}
    >
      <Typography variant="h5" paragraph>
        Dashboard
      </Typography>
      <Grid spacing={4} container direction="column">
        <Grid spacing={4} container item>
          <Grid xs={isSM ? 12 : 6} item>
            <YourWorkforce data={data?.generalAnalytics || []} />
          </Grid>
          <Grid xs={isSM ? 12 : 6} item>
            <Breakdown data={data?.categoryAnalytics || []} />
          </Grid>
        </Grid>
        <Grid spacing={4} container item>
          <Grid xs={isSM ? 12 : 6} item>
            <ByCountry data={data?.countryAnalytics || []} />
          </Grid>
          <Grid xs={isSM ? 12 : 6} item>
            <LatestAdditions lastTalents={data?.lastTalents || []} />
          </Grid>
        </Grid>
      </Grid>
    </ConnectedPageLayout>
  );
};

export default Dashboard;
