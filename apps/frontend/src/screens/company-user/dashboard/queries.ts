import { gql } from '@apollo/client';

export const GET_DASHBOARD_INFO = gql`
  query GetDashboardInfo {
    generalAnalytics {
      key
      value
      slug
    }
    countryAnalytics {
      key
      value
      slug
    }
    categoryAnalytics {
      key
      value
      slug
    }
    lastTalents {
      first_name
      last_name
      id
      location
      avatar {
        avatar
      }
      rate
      is_invitation_accepted
      recent_position_title
    }
  }
`;
