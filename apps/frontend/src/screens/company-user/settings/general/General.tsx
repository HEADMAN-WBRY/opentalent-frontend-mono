import React from 'react';

import CompanyForm from '../company-form';

interface GeneralProps {}

const General = (props: GeneralProps) => {
  return <CompanyForm />;
};

export default General;
