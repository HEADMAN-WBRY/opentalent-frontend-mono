import { useMutation } from '@apollo/client';
import {
  GET_CURRENT_COMPANY_USER,
  UPDATE_CURRENT_COMPANY_USER,
} from 'graphql/user';
import { useCurrentUser } from 'hooks/auth';
import { useSnackbar } from 'notistack';

import {
  Mutation,
  MutationUpdateCompanyUserProfileArgs,
} from '@libs/graphql-types';

export const useInitialValues = () => {
  const { data: userData } = useCurrentUser();
  const companyUser = userData?.currentCompanyUser;
  const initialValues: MutationUpdateCompanyUserProfileArgs = {
    avatar: companyUser?.avatar?.hash,
    first_name: companyUser?.first_name ?? '',
    last_name: companyUser?.last_name ?? '',
    email: companyUser?.email ?? '',
    position: companyUser?.position ?? '',
    company_user_id: companyUser?.id || '',
  };

  return {
    initialValues,
    initialAvatar: companyUser?.avatar?.avatar,
  };
};

export const useUpdateUser = () => {
  const { enqueueSnackbar } = useSnackbar();
  const [request, { loading }] = useMutation<
    Mutation,
    MutationUpdateCompanyUserProfileArgs
  >(UPDATE_CURRENT_COMPANY_USER, {
    onCompleted: () => {
      enqueueSnackbar('Profile updated', { variant: 'success' });
    },
    onError: () => {
      enqueueSnackbar('Error', { variant: 'error' });
    },
    refetchQueries: [{ query: GET_CURRENT_COMPANY_USER }],
  });
  const updateRequest = (variables: MutationUpdateCompanyUserProfileArgs) => {
    request({ variables });
  };

  return { updateRequest, loading };
};
