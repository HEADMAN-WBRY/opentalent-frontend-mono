import errors from 'consts/validationErrors';
import * as yup from 'yup';

import { maxStringValidator } from '@libs/helpers/yup';

export const getValidator = (isCreate: boolean) =>
  yup.object().shape({
    first_name: maxStringValidator.required(errors.required),
    last_name: maxStringValidator.required(errors.required),
    ...(isCreate
      ? {
          email: yup.string().trim().email().required(errors.required),
        }
      : {}),
  });
