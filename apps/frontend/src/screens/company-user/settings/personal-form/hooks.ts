import { useMutation } from '@apollo/client';
import {
  GET_CURRENT_COMPANY_USER,
  UPDATE_CURRENT_COMPANY_USER,
  CREATE_COMPANY_USER,
  GET_COLLEAGUES,
} from 'graphql/user';
import { useCurrentUser } from 'hooks/auth';
import { useSnackbar } from 'notistack';
import { noop } from 'utils/common';

import {
  Mutation,
  MutationUpdateCompanyUserProfileArgs,
} from '@libs/graphql-types';

export const useInitialValues = () => {
  const { data: userData } = useCurrentUser();
  const companyUser = userData?.currentCompanyUser;
  const initialValues: MutationUpdateCompanyUserProfileArgs = {
    avatar: companyUser?.avatar?.hash,
    first_name: companyUser?.first_name || '',
    last_name: companyUser?.last_name || '',
    company_user_id: companyUser?.id || '',
    position: companyUser?.position || '',
    email: companyUser?.email || '',
    ...{ is_primary: true, is_onboarding: true },
  };

  return {
    initialValues,
    initialAvatar: companyUser?.avatar?.avatar,
  };
};

export const useUpdateOrCreateUser = ({
  isCreate,
  onSuccess = noop,
}: {
  isCreate: boolean;
  onSuccess?: VoidFunction;
}) => {
  const { enqueueSnackbar } = useSnackbar();
  const mutation = isCreate ? CREATE_COMPANY_USER : UPDATE_CURRENT_COMPANY_USER;
  const [request, { loading }] = useMutation<
    Mutation,
    MutationUpdateCompanyUserProfileArgs
  >(mutation, {
    onCompleted: () => {
      enqueueSnackbar(`Profile ${isCreate ? 'created' : 'updated'}`, {
        variant: 'success',
      });
      onSuccess();
    },
    onError: () => {
      enqueueSnackbar('Error', { variant: 'error' });
    },
    refetchQueries: [
      { query: GET_CURRENT_COMPANY_USER },
      { query: GET_COLLEAGUES },
    ],
  });
  const updateRequest = (variables: MutationUpdateCompanyUserProfileArgs) => {
    request({ variables });
  };

  return { updateRequest, loading };
};
