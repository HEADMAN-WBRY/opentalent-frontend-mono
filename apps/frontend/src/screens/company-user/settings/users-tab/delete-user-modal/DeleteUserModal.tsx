import { Box, Grid, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import React from 'react';
import { DefaultModalProps, withLocationStateModal } from 'utils/modals';

import Button from '@libs/ui/components/button';
import { DefaultModal } from '@libs/ui/components/modals';

import { Modals } from '../types';
import { useSubmitHandler } from './hooks';
import { DeleteUserModalData } from './types';

interface DeleteUserModalProps extends DefaultModalProps<DeleteUserModalData> {}

const useStyles = makeStyles((theme) => ({
  archiveButton: {
    background: `${theme.palette.error.light} !important`,
  },
}));

const DeleteUserModalComponent = ({
  close,
  isOpen,
  modalData,
}: DeleteUserModalProps) => {
  const userId = modalData?.userId || '';
  const classes = useStyles();
  const { onSubmit, isLoading } = useSubmitHandler({
    id: userId,
    handleClose: close,
  });

  return (
    <DefaultModal
      actions={
        <Grid spacing={4} container>
          <Grid xs={6} item>
            <Button
              className={classes.archiveButton}
              fullWidth
              color="inherit"
              variant="contained"
              onClick={onSubmit}
              disabled={isLoading}
            >
              Delete
            </Button>
          </Grid>
          <Grid xs={6} item>
            <Button
              fullWidth
              variant="outlined"
              color="secondary"
              onClick={close}
              disabled={isLoading}
            >
              Cancel
            </Button>
          </Grid>
        </Grid>
      }
      handleClose={close}
      open={isOpen}
      title="Delete user"
    >
      <Box>
        <Typography>Do you want to permanently delete this user?</Typography>
      </Box>
    </DefaultModal>
  );
};

export const DeleteUserModal = withLocationStateModal<DeleteUserModalData>({
  id: Modals.Delete,
})(DeleteUserModalComponent);
