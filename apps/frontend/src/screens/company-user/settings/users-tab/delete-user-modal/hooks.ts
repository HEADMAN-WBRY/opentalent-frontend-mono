import { useMutation } from '@apollo/client';
import { GET_COLLEAGUES } from 'graphql/user';
import { useSnackbar } from 'notistack';
import { useCallback } from 'react';

import {
  Mutation,
  MutationDeleteCompanyUserProfileArgs,
} from '@libs/graphql-types';

import { DELETE_USER } from '../../queries';

interface HookParams {
  id: string;
  handleClose: VoidFunction;
}

export const useSubmitHandler = ({ id, handleClose }: HookParams) => {
  const { enqueueSnackbar } = useSnackbar();
  const [action, { loading }] = useMutation<
    Mutation,
    MutationDeleteCompanyUserProfileArgs
  >(DELETE_USER, {
    onCompleted: () => {
      enqueueSnackbar(`User deleted`, {
        variant: 'success',
      });
      handleClose();
    },
    refetchQueries: [{ query: GET_COLLEAGUES }],
  });

  const onSubmit = useCallback(() => {
    action({ variables: { company_user_id: id } });
  }, [action, id]);

  return { onSubmit, isLoading: loading };
};
