import AddIcon from '@mui/icons-material/Add';
import { Box, Grid } from '@mui/material';
import React from 'react';
import { Link } from 'react-router-dom';
import { pathManager } from 'routes';

import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import UsersTable from './UsersTable';
import { DeleteUserModal } from './delete-user-modal';
import { useColleagues } from './hooks';

interface UsersTabProps {}

const UsersTab = (props: UsersTabProps) => {
  const { isLoading, colleagues } = useColleagues();

  if (isLoading) {
    <p>Loading...</p>;
  }

  return (
    <Box pt={4}>
      <Grid container>
        <Grid style={{ flexGrow: 1 }} item>
          <Typography variant="h5">Company Users List</Typography>
        </Grid>
        <Grid item>
          <Link to={pathManager.company.users.create.generatePath()}>
            <Button
              color="secondary"
              variant="contained"
              startIcon={<AddIcon />}
            >
              add new user
            </Button>
          </Link>
        </Grid>
      </Grid>
      <Box pt={8} pb={4}>
        <Typography variant="body1" color="textSecondary">
          You can also invite your colleagues to your company account at
          OpenTalent
        </Typography>
      </Box>
      <UsersTable colleagues={colleagues} />
      <DeleteUserModal />
    </Box>
  );
};

export default UsersTab;
