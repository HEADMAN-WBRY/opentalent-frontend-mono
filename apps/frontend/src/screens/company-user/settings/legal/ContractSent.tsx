import { Box } from '@mui/material';
import React from 'react';

import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import { ContentProps } from './types';

interface ContractVerifiedProps extends ContentProps {}

const ContractSent = (props: ContractVerifiedProps) => {
  return (
    <div style={{ maxWidth: 570 }}>
      <Typography variant="body2" paragraph>
        We&apos;ve sent you an agreement. Please check your inbox, review the
        agreement and sign to start hiring with OpenTalent
      </Typography>
      <Box pt={4}>
        <Button variant="contained" disabled color="secondary">
          Request the agreement
        </Button>
      </Box>
    </div>
  );
};

export default ContractSent;
