import { Box, Grid } from '@mui/material';
import React from 'react';

import { OtContract, OtContractStatusEnum } from '@libs/graphql-types';
import Chip from '@libs/ui/components/chip/Chip';
import Typography from '@libs/ui/components/typography';

import { useRequestContract } from '../hooks';
import ContractPending from './ContractPending';
import ContractSent from './ContractSent';
import ContractVerified from './ContractVerified';
import NotRequested from './NotRequested';

interface LegalProps {
  contract?: OtContract;
}

const CONTENT = {
  [OtContractStatusEnum.Unknown]: {
    Content: NotRequested,
    chip: <Chip color="red" size="small" label="Awaiting approval" />,
  },
  [OtContractStatusEnum.Requested]: {
    Content: ContractPending,
    chip: <Chip color="tertiary" size="small" label="Pending" />,
  },
  [OtContractStatusEnum.SentToCompany]: {
    Content: ContractSent,
    chip: <Chip color="tertiary" size="small" label="Sent" />,
  },
  [OtContractStatusEnum.Signed]: {
    Content: ContractVerified,
    chip: <Chip color="green" size="small" label="Verified" />,
  },
};

const Legal = ({ contract }: LegalProps) => {
  const status = contract?.status || OtContractStatusEnum.Unknown;
  const { Content, chip } = CONTENT[status];
  const { isLoading, request } = useRequestContract();

  return (
    <div>
      <Typography variant="h6" paragraph>
        Client agreement
      </Typography>

      <Grid spacing={2} container>
        <Grid item>
          <Typography variant="subtitle1" color="textSecondary">
            Client agreement status:
          </Typography>
        </Grid>
        <Grid item>
          <Typography>{chip}</Typography>
        </Grid>
      </Grid>
      <Box pt={4}>
        <Content isLoading={isLoading} request={request} />
      </Box>
    </div>
  );
};

export default Legal;
