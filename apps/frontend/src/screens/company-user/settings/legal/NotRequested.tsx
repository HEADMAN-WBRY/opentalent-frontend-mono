import { Box } from '@mui/material';
import React from 'react';

import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import { ContentProps } from './types';

interface NotRequestedProps extends ContentProps {}

const EMAIL = 'info@opentalent.co';

const NotRequested = ({ isLoading, request }: NotRequestedProps) => {
  return (
    <div style={{ maxWidth: 570 }}>
      <Typography variant="body2" paragraph>
        To get full access to the platform and start hiring you must sign the
        “Hiring Agreement”. You can start the signing process by clicking on the
        “Request to View” button.
      </Typography>
      <Typography variant="body2" paragraph>
        For any questions, please send an email to{' '}
        <a href={`mailto:${EMAIL}`}>
          <Typography component="span" color="tertiary">
            {EMAIL}
          </Typography>
        </a>
      </Typography>
      <Box pt={4}>
        <Button
          disabled={isLoading}
          onClick={request}
          variant="contained"
          color="secondary"
        >
          REQUEST TO VIEW
        </Button>
      </Box>
    </div>
  );
};

export default NotRequested;
