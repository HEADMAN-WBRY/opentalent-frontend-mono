import { Box } from '@mui/material';
import React from 'react';

import Button from '@libs/ui/components/button';
import Typography, { OuterLink } from '@libs/ui/components/typography';

import { ContentProps } from './types';

interface ContractVerifiedProps extends ContentProps {}

const ContractVerified = (props: ContractVerifiedProps) => {
  return (
    <div style={{ maxWidth: 570 }}>
      <Typography color="success">Congratulations!</Typography>
      <Typography variant="body2" paragraph>
        An active hiring agreement is in place.{' '}
        <OuterLink href="mailto:info@opentalent.co">Contact us</OuterLink> for
        details.
      </Typography>
      <Box pt={4}>
        <Button variant="contained" disabled color="secondary">
          Request the agreement
        </Button>
      </Box>
    </div>
  );
};

export default ContractVerified;
