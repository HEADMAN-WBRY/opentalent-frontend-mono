import React from 'react';
import { Link } from 'react-router-dom';

import { Box } from '@mui/material';

import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import { pathManager } from '../../../../routes';
import { ContentProps } from './types';

interface ContractPendingProps extends ContentProps {}

const ContractPending = (props: ContractPendingProps) => {
  return (
    <div style={{ maxWidth: 570 }}>
      <Typography variant="body2" paragraph>
        Thanks for your request! We&apos;ll get back to you within the next 24
        hours.
      </Typography>
      <Typography variant="body2" paragraph>
        In the mean time you can{' '}
        <Link to={pathManager.company.workforce.generatePath()}>
          <Typography component="span" color="tertiary">
            search for talent
          </Typography>
        </Link>{' '}
        or{' '}
        <Link to={pathManager.company.newJob.form.generatePath()}>
          <Typography component="span" color="tertiary">
            create your first job post
          </Typography>
        </Link>
        .
      </Typography>
      <Box pt={4}>
        <Button disabled variant="contained" color="secondary">
          Request the agreement
        </Button>
      </Box>
    </div>
  );
};

export default ContractPending;
