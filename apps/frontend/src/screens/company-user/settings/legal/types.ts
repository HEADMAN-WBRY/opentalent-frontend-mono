export interface ContentProps {
  request?: VoidFunction;
  isLoading?: boolean;
}
