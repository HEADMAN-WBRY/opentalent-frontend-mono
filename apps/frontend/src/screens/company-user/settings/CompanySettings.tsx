import PriorityHighIcon from '@mui/icons-material/PriorityHigh';
import { TabContext, TabPanel } from '@mui/lab';
import { Box, Grid, Tab, Tabs } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { ConnectedPageLayout } from 'components/layout/page-layout';
import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';

import { OtContractStatusEnum } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import General from './general';
import { useSettingsTabs, useClientContract } from './hooks';
import Legal from './legal';
import { SettingsTabs } from './types';
import UsersTab from './users-tab';

interface CompanySettingsProps {}

const useStyles = makeStyles((theme) => ({
  tabPanel: {
    padding: `${theme.spacing(6)} 0`,
  },
  legalTabIcon: {
    background: theme.palette.primary.main,
    borderRadius: '100%',
  },
}));

const CompanySettings = (props: CompanySettingsProps) => {
  const { isXS } = useMediaQueries();
  const classes = useStyles();
  const { tab, onTabChange } = useSettingsTabs();
  const contract = useClientContract();
  const noContract = contract?.status !== OtContractStatusEnum.Signed;

  const legalContent = noContract ? (
    <Grid spacing={2} alignItems="center" justifyContent="center" container>
      <Grid item>
        <PriorityHighIcon className={classes.legalTabIcon} />
      </Grid>
      <Grid item>Legal</Grid>
    </Grid>
  ) : (
    'Legal'
  );

  return (
    <ConnectedPageLayout
      drawerProps={{}}
      documentTitle="Settings"
      contentSpacing={isXS ? 4 : 8}
    >
      <Box mb={4}>
        <Typography variant="h5" paragraph>
          Company Account Settings
        </Typography>
      </Box>

      <TabContext value={tab as string}>
        <Tabs
          value={tab as string}
          selectionFollowsFocus
          onChange={onTabChange}
        >
          <Tab value={SettingsTabs.General} label="General" />
          <Tab value={SettingsTabs.Users} label="Users" />
          <Tab value={SettingsTabs.Legal} label={legalContent} />
        </Tabs>
        <TabPanel
          classes={{ root: classes.tabPanel }}
          value={SettingsTabs.General}
        >
          <General />
        </TabPanel>
        <TabPanel
          classes={{ root: classes.tabPanel }}
          value={SettingsTabs.Users}
        >
          <UsersTab />
        </TabPanel>
        <TabPanel
          classes={{ root: classes.tabPanel }}
          value={SettingsTabs.Legal}
        >
          <Legal contract={contract} />
        </TabPanel>
      </TabContext>
    </ConnectedPageLayout>
  );
};

export default CompanySettings;
