import { useMutation } from '@apollo/client';
import { useFormikContext } from 'formik';
import UPLOAD_FILE from 'graphql/files/uploadFile';
import { useCurrentUser } from 'hooks/auth';
import { useCallback, useEffect } from 'react';

import {
  FileTypeEnum,
  Mutation,
  MutationUploadFileArgs,
} from '@libs/graphql-types';

export const useAvatarChange = ({
  name,
  fileType,
}: {
  name: string;
  fileType: FileTypeEnum;
}) => {
  const { data: userData } = useCurrentUser();
  const userId = userData?.currentCompanyUser?.id as string;
  const [upload, { data, called }] = useMutation<
    Mutation,
    MutationUploadFileArgs
  >(UPLOAD_FILE);
  const { setFieldValue } = useFormikContext();

  useEffect(() => {
    if (called) {
      setFieldValue(name, data?.uploadFile?.hash);
    }
  }, [data, name, setFieldValue, called]);

  return useCallback(
    async (files: File[]) => {
      upload({
        variables: { file: files[0], owner_id: userId, file_type: fileType },
      });
    },
    [upload, userId, fileType],
  );
};
