import React from 'react';

const CompanySettings = React.lazy(() => import('./CompanySettings'));

export default CompanySettings;
