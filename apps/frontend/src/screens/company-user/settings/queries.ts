import { gql } from '@apollo/client/core';

export const REQUEST_CONTRACT = gql`
  mutation RequestContract {
    requestOTContract
  }
`;

export const DELETE_USER = gql`
  mutation DeleteUser($company_user_id: ID!) {
    deleteCompanyUserProfile(company_user_id: $company_user_id)
  }
`;
