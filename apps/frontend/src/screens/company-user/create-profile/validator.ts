import errors from 'consts/validationErrors';
import * as yup from 'yup';

import { varCharStringValidator } from '@libs/helpers/yup';

export default yup.object().shape({
  first_name: varCharStringValidator.required(errors.required),
  last_name: varCharStringValidator.required(errors.required),
  // recent_position_title: maxStringValidator.required(),
  // talent_category_id: yup.string().required(errors.required),
  email: varCharStringValidator
    .email(errors.invalidEmail)
    .required(errors.required),
});
