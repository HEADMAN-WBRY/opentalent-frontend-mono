/* eslint-disable react/display-name */
import { useLazyQuery } from '@apollo/client';
import { Box } from '@mui/material';
import CircularProgress from '@mui/material/CircularProgress';
import validationErrors from 'consts/validationErrors';
import { useFormikContext } from 'formik';
import { CHECK_TALENT_EXIST_BY_EMAIL } from 'graphql/talents';
import { useSnackbar } from 'notistack';
import React, { useCallback, useEffect } from 'react';

import { Query, QueryCheckTalentExistsByEmailArgs } from '@libs/graphql-types';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import { ConnectedTextFieldProps } from '@libs/ui/components/form/text-field/ConnectedTextField';

import { INITIAL_VALUES } from '../const';

type EmailControlProps = ConnectedTextFieldProps & {
  name: 'email';
  withLoadingIndicator?: boolean;
};

function EmailControl({
  name,
  InputProps,
  withLoadingIndicator = false,
  ...props
}: EmailControlProps) {
  const { enqueueSnackbar } = useSnackbar();
  const [checkEmail, { loading, data }] = useLazyQuery<
    Query,
    QueryCheckTalentExistsByEmailArgs
  >(CHECK_TALENT_EXIST_BY_EMAIL);
  const { errors, values, setFieldValue } =
    useFormikContext<typeof INITIAL_VALUES>();
  const onBlur = useCallback(async () => {
    const needCheck = !!values[name] && !errors[name];

    if (!needCheck) {
      return;
    }
    await checkEmail({ variables: { talent_email: values[name] } });
  }, [values, name, errors, checkEmail]);

  const onChangeHandler = useCallback(async () => {
    setFieldValue('is_email_valid', false);
  }, [setFieldValue]);

  useEffect(() => {
    if (data?.checkTalentExistsByEmail) {
      enqueueSnackbar(validationErrors.emailIsTaken, { variant: 'error' });
      return;
    }
    setFieldValue('is_email_valid', true);
  }, [data, setFieldValue, name, enqueueSnackbar]);

  const errorProps = data?.checkTalentExistsByEmail
    ? { error: true, helperText: validationErrors.emailIsTaken }
    : {
        ...(withLoadingIndicator && {
          renderSuffix: () => {
            return (
              loading && (
                <Box pr={2}>
                  <CircularProgress color="secondary" size={32} />
                </Box>
              )
            );
          },
        }),
      };

  return (
    <ConnectedTextField
      InputProps={{
        ...InputProps,
        onBlur,
      }}
      name={name}
      onChange={onChangeHandler}
      {...errorProps}
      {...props}
    />
  );
}

export default EmailControl;
