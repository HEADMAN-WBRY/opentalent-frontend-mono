import { useMutation } from '@apollo/client';
import { CREATE_TALENT_PROFILE } from 'graphql/talents';
import { useSnackbar } from 'notistack';
import React, { useCallback } from 'react';

import { Mutation, MutationCreateTalentProfileArgs } from '@libs/graphql-types';

import { INITIAL_VALUES } from './const';

export const useSubmitAction = () => {
  const { enqueueSnackbar } = useSnackbar();
  const [createTalentProfile, { loading }] = useMutation<
    Mutation,
    MutationCreateTalentProfileArgs
  >(CREATE_TALENT_PROFILE);
  const onSubmit = useCallback(
    async (
      { email, is_email_valid, tags, ...general_info }: typeof INITIAL_VALUES,
      formHelpers,
    ) => {
      try {
        await createTalentProfile({
          variables: {
            user_id: '1',
            general_info,
            email,
            tags_ids: tags.map((t) => t.value as string),
          },
        });
        enqueueSnackbar(
          <span>
            You have successfully created a new user. <br />
            He/she will now receive an invitation to complete their profile.
          </span>,
          { variant: 'success' },
        );
        formHelpers.resetForm();
      } catch (e) {
        // eslint-disable-next-line no-console
        console.error(e);
      }
    },
    [createTalentProfile, enqueueSnackbar],
  );

  return { onSubmit, loading };
};
