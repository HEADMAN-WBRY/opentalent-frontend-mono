import SideCardMark from 'components/side-card-mark';
import { formatDuration } from 'date-fns';
import useMediaQueries from 'hooks/common/useMediaQueries';
import useMixPanel from 'hooks/common/useMixPanel';
import React, { useMemo, useState } from 'react';
import { Link } from 'react-router-dom';
import { pathManager } from 'routes/consts';
import { useIntervalTimer } from 'screens/talent/job-apply/job-info/hooks';
import { getJobFieldsForCompany, JobFieldType } from 'utils/job';

import InfoIcon from '@mui/icons-material/Info';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import {
  Box,
  Collapse,
  Grid,
  Hidden,
  Paper,
  Tooltip,
  Zoom,
} from '@mui/material';
import { makeStyles } from '@mui/styles';

import { Job, SourceTypeEnum } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import JobKeySkills from './JobKeySkills';

interface JobCardProps {
  job: Job;
  currentTime: Date;
}

const TYPE_NAME_MAP = {
  [SourceTypeEnum.Opentalent]: { title: 'OpenTalent', color: 'green' },
  [SourceTypeEnum.Own]: { title: 'My Hub', color: 'primary' },
};

const getBadgeProps = (source: SourceTypeEnum[] = []) => {
  if (source?.length > 1) {
    return { title: 'My Hub + OpenTalent', color: 'info' };
  }
  if (source?.[0]) {
    return TYPE_NAME_MAP[source[0]];
  }
  return { title: 'My Hub + OpenTalent', color: 'info' };
};

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    paddingTop: theme.spacing(2),
  },
  toggler: {
    height: 0,
    position: 'relative',
    top: 10,

    '& button': {
      color: theme.palette.tertiary.main,
    },
  },
  togglerIcon: {
    transform: ({ isOpen }: { isOpen: boolean }) =>
      `rotate(${isOpen ? 0 : 180}deg)`,
  },
  description: {
    '& p': {
      margin: 0,
    },
  },
  titleBadge: {
    background: theme.palette.grey[400],
    fontSize: 12,
    lineHeight: '24px',
    marginLeft: 8,
    position: 'relative',
    top: -3,
    display: 'inline-block',
    padding: '0 8px',
  },
  title: {
    '& h6': {
      paddingRight: 20,
    },

    [theme.breakpoints.down('md')]: {
      'body &': {
        marginRight: 200,
      },
    },
  },
  cardLeftContent: {
    // minWidth: 550,

    [theme.breakpoints.down('md')]: {
      'body &': {
        width: 'auto',
      },
    },
  },
  countItem: {
    whiteSpace: 'nowrap',
    flexWrap: 'nowrap',
  },
  countItemLabel: {
    width: 130,
    display: 'inline-block',
    transform: 'translateY(-2px)',

    [theme.breakpoints.down('md')]: {
      'body &': {
        transform: 'none',
      },
    },
  },

  countWrapper: {
    alignItems: 'flex-end',
    paddingBottom: theme.spacing(6),

    [theme.breakpoints.down('md')]: {
      'body &': {
        alignItems: 'flex-start',
        paddingBottom: 0,
      },
    },
  },

  mobileCount: {
    paddingTop: theme.spacing(4),
    display: 'none',

    [theme.breakpoints.down('md')]: {
      'body &': {
        display: 'block',
      },
    },
  },
  desktopCount: {
    display: 'flex',

    [theme.breakpoints.down('md')]: {
      'body &': {
        display: 'none',
      },
    },
  },
  infoRow: {
    display: 'flex',
    flexWrap: 'wrap',

    '& > div': {
      paddingRight: 20,
      paddingBottom: 4,
      width: '50%',

      [theme.breakpoints.down('md')]: {
        'body &': {
          minWidth: '50% !important',
        },
      },
    },
  },
}));

const JobCard = ({ job, currentTime }: JobCardProps) => {
  const mixPanel = useMixPanel();
  const [isOpen, setIsOpen] = useState(false);
  const classes = useStyles({ isOpen });
  const handleShowMore = () => {
    setIsOpen((s) => !s);
    mixPanel.track("User clicked 'Show more' on a job card");
  };

  const duration = useIntervalTimer({
    start: job?.campaign_start_date,
    end: job?.campaign_end_date,
    currentTime,
  });

  const durationText = formatDuration(duration || { hours: 0 }, {
    format: ['days', 'hours'],
  });
  const { isSM } = useMediaQueries();
  const badgeProps = getBadgeProps(job.campaign_talent_pool as any);
  const infoItems = useMemo(() => getJobFieldsForCompany(job), [job]);

  const count = (
    <Grid
      className={classes.countWrapper}
      alignItems="flex-end"
      direction="column"
      wrap="nowrap"
      container
    >
      {[
        {
          label: 'Total leads',
          value: job.matches_count || 0,
        },
        {
          label: 'Applications',
          value: job.matches_with_application_count || 0,
        },
      ].map(({ label, value }) => (
        <Grid className={classes.countItem} key={label} item>
          <Typography
            component="span"
            variant={isSM ? 'body2' : 'h6'}
            color="tertiary"
          >
            {value}
          </Typography>{' '}
          <Typography
            className={classes.countItemLabel}
            component="span"
            variant={isSM ? 'caption' : 'body2'}
          >
            {label}
          </Typography>
        </Grid>
      ))}
    </Grid>
  );

  return (
    <Paper
      data-test-id="job-card"
      classes={{ root: classes.root }}
      elevation={0}
    >
      <SideCardMark color={badgeProps?.color as any}>
        {badgeProps.title}
      </SideCardMark>
      <Grid
        component={Box}
        padding={isSM ? 4 : 6}
        wrap="nowrap"
        justifyContent="space-between"
        container
      >
        <Grid className={classes.cardLeftContent} component={Box} item>
          <div className={classes.title}>
            <Typography variant="h6">
              {job.name}
              {job.is_draft && (
                <span className={classes.titleBadge}>Draft</span>
              )}
            </Typography>
          </div>
          <Box pb={5}>
            <Typography
              component="span"
              paragraph
              variant="body2"
              color="textSecondary"
            >
              Campaign period {'>'}&nbsp;
            </Typography>
            {!durationText || job.is_archived ? (
              <Typography component="span" paragraph variant="subtitle2">
                Campaign finished
              </Typography>
            ) : (
              <>
                <Typography component="span" paragraph variant="subtitle2">
                  {formatDuration(duration || { hours: 0 }, {
                    format: ['days', 'hours'],
                  })}
                  &nbsp;
                </Typography>
                <Typography component="span" paragraph variant="body2">
                  left
                </Typography>
              </>
            )}
          </Box>
          <div className={classes.infoRow}>
            {infoItems.map(({ label, value, hint, id }) => (
              <div key={`${label}-${value}`}>
                {id === JobFieldType.Fee && (
                  <Typography
                    style={{
                      height: 20,
                      lineHeight: '20px',
                      display: 'inline-block',
                      transform: 'translateY(2px)',
                    }}
                    component="span"
                    variant="h6"
                  >
                    💶
                  </Typography>
                )}{' '}
                <Typography component="span" variant="subtitle2">
                  {label}
                </Typography>{' '}
                <Typography
                  style={{ whiteSpace: 'nowrap' }}
                  component="span"
                  variant="body2"
                >
                  {value}
                </Typography>{' '}
                {hint && (
                  <Typography
                    component="span"
                    style={{ verticalAlign: 'middle' }}
                  >
                    <Tooltip
                      TransitionComponent={Zoom}
                      title={hint}
                      placement="right"
                    >
                      <InfoIcon
                        style={{ margin: '0 auto -4px' }}
                        fontSize="small"
                        color="disabled"
                      />
                    </Tooltip>
                  </Typography>
                )}
              </div>
            ))}
          </div>
          <Box className={classes.mobileCount}>{count}</Box>

          <Box
            className={classes.toggler}
            display="flex"
            justifyContent="flex-end"
          >
            <Button
              onClick={handleShowMore}
              variant="text"
              endIcon={
                <KeyboardArrowUpIcon
                  className={classes.togglerIcon}
                  color="inherit"
                />
              }
            >
              <Typography
                style={{ textTransform: 'none' }}
                variant="body2"
                color="tertiary"
              >
                Show more
              </Typography>
            </Button>
          </Box>
          <JobKeySkills job={job} isOpen={isOpen} />
          <Collapse in={isOpen} collapsedSize={80}>
            <Box
              pt={8}
              className={classes.description}
              dangerouslySetInnerHTML={{ __html: job?.description || '' }}
            />
          </Collapse>
        </Grid>
        <Grid
          className={classes.desktopCount}
          component={Box}
          pt={25}
          pl={2}
          item
          flexDirection="column"
        >
          {count}

          <Link to={pathManager.company.job.generatePath({ id: job.id })}>
            <Button
              data-test-id="open-job"
              color="secondary"
              variant="outlined"
            >
              open job page
            </Button>
          </Link>
        </Grid>
      </Grid>
      <Hidden mdUp>
        <Box padding={4}>
          <Link to={pathManager.company.job.generatePath({ id: job.id })}>
            <Button fullWidth color="secondary" variant="outlined">
              open job page
            </Button>
          </Link>
        </Box>
      </Hidden>
    </Paper>
  );
};

export default JobCard;
