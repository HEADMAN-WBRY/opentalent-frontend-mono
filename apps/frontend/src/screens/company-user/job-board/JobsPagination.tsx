import { Pagination } from '@mui/lab';
import { Box } from '@mui/material';
import React from 'react';

import { usePaginationChange } from './hooks';

interface JobsPaginationProps {
  currentPage?: number;
  lastPage?: number;
}

const JobsPagination = ({
  currentPage = 1,
  lastPage = 1,
}: JobsPaginationProps) => {
  const onChange = usePaginationChange();

  return (
    <Box display="flex" justifyContent="center" mt={4}>
      <Pagination
        page={currentPage}
        showFirstButton
        showLastButton
        count={lastPage}
        variant="outlined"
        shape="rounded"
        onChange={onChange}
      />
    </Box>
  );
};

export default JobsPagination;
