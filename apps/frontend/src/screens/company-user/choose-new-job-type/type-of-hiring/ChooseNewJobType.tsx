import { ConnectedPageLayout } from 'components/layout/page-layout';
import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';

import { Box } from '@mui/material';

import Typography from '@libs/ui/components/typography';

import { DEDICATED_SEARCH_STEPS } from '../consts';
import TopSteps from '../shared/TopSteps';
import OptionsForm from './OptionsForm';
import { PageProps } from './types';

const ChooseNewJobType = (props: PageProps) => {
  const { isMD } = useMediaQueries();

  return (
    <ConnectedPageLayout
      documentTitle="Post a Job > Dedicated Search"
      headerProps={{ accountProps: {} }}
      drawerProps={{}}
      contentSpacing={6}
      // classes={{ contentWrapper: classes.contentWrapper }}
    >
      <Box pt={4}>
        <Typography paragraph variant="h5">
          Post a Job &gt; Dedicated Search
        </Typography>
      </Box>
      <TopSteps
        onlyActiveLabel={isMD}
        activeIndex={1}
        steps={DEDICATED_SEARCH_STEPS}
      />
      <OptionsForm />
    </ConnectedPageLayout>
  );
};

export default ChooseNewJobType;
