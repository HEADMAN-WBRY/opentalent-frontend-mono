import React from 'react';

const ChooseNewJobType = React.lazy(() => import('./ChooseNewJobType'));

export default ChooseNewJobType;
