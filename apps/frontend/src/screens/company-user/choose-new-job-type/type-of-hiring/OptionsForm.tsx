import { usePushWithQuery } from 'hooks/routing';
import React, { useState } from 'react';
import { pathManager } from 'routes';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { JobTypeEnum } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';

import OptionBox from './OptionBox';

interface OptionsFormProps {}

const OPTIONS = {
  [JobTypeEnum.Freelance]: {
    title: 'Freelancer',
    subtitle: 'hourly based',
    info: 'Get matched to available freelancers from OpenTalent’s exclusive European network.',
  },
  [JobTypeEnum.Permanent]: {
    title: 'Permanent Hire',
    subtitle: 'Full-time',
    info: 'Find your next best hire by having people from the OpenTalent network sourced for you.',
    isBeta: true,
    disabled: false,
  },
  // [JobTypeEnum.Project]: {
  //   title: 'Project',
  //   subtitle: 'Fixed budget',
  //   info: 'Get matched to Talent Collectives and Agencies from OpenTalent’s exclusive European network.',
  // },
};

const useStyles = makeStyles((theme) => ({
  root: {
    textAlign: 'center',
    paddingTop: theme.spacing(8),

    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
    },
  },
}));

const OptionsForm = (props: OptionsFormProps) => {
  const classes = useStyles();
  const push = usePushWithQuery();
  const [activeOption, setActiveOption] = useState<JobTypeEnum | null>(null);
  const onSubmit = () => {
    push({
      pathname: pathManager.company.newJob.form.generatePath(),
      query: { jobType: activeOption },
    });
  };

  return (
    <>
      <Grid className={classes.root} wrap="nowrap" container spacing={8}>
        {Object.entries(OPTIONS).map(([type, optionProps]) => (
          <Grid key={type} item>
            <OptionBox
              {...optionProps}
              type={type as JobTypeEnum}
              isActive={type === activeOption}
              onClick={() => setActiveOption(type as JobTypeEnum)}
            />
          </Grid>
        ))}
      </Grid>
      <Box pt={8}>
        <Button
          disabled={!activeOption}
          size="large"
          variant="contained"
          color="primary"
          onClick={onSubmit}
          data-test-id="submit-job-type"
        >
          Continue
        </Button>
      </Box>
    </>
  );
};

export default OptionsForm;
