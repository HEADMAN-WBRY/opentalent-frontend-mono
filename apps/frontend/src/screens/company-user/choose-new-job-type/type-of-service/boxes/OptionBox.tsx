import cn from 'classnames';
import React from 'react';

import { Box, Paper } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { JobServiceTypeEnum } from '@libs/graphql-types';
import Checkbox from '@libs/ui/components/form/checkbox';
import Typography from '@libs/ui/components/typography';

export interface OptionBoxProps extends React.PropsWithChildren<unknown> {
  title: string;
  text: string | React.ReactNode;
  isActive: boolean;
  onClick: VoidFunction;
  type: JobServiceTypeEnum;
  Icon: React.ComponentType;
  disabled?: boolean;
}

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    height: '100%',
    textAlign: 'center',
    transition: 'border .3s',
    border: ({ isActive }: OptionBoxProps) =>
      isActive
        ? `2px solid ${theme.palette.success.main}`
        : `2px solid ${theme.palette.grey[300]}`,
  },
  container: {
    padding: theme.spacing(8, 6),
  },
  checkboxChecked: {
    color: `${theme.palette.success.main} !important`,
  },
  checkboxLabel: {
    margin: 0,
    fontSize: '24px !important',
    fontWeight: '500 !important',

    color: ({ disabled }: OptionBoxProps) =>
      disabled ? theme.palette.text.secondary : theme.palette.text.primary,
  },
  betaBadge: {
    padding: '6px 12px',
    borderRadius: '24px',
    position: 'absolute',
    right: '20px',
    top: '20px',
    color: `${theme.palette.text.secondary}`,
    backgroundColor: `${theme.palette.other.green}`,
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
  rootDisabled: {
    pointerEvents: 'none',

    '& $container': {
      opacity: 0.4,
    },
  },
}));

const OptionBox = (props: OptionBoxProps) => {
  const classes = useStyles(props);
  const { title, text, isActive, type, onClick, Icon, children, disabled } =
    props;

  return (
    <Paper elevation={0} classes={{ root: cn(classes.root) }}>
      <Box className={classes.container} position="relative">
        <Box pb={3}>
          <Checkbox
            checked={isActive}
            formControlLabelProps={{
              classes: { root: classes.checkboxLabel },
              disableTypography: true,
            }}
            classes={{ checked: classes.checkboxChecked }}
            name={type}
            label={title}
            onChange={onClick}
            disabled={disabled}
          />
        </Box>
        <Box pb={3}>
          <Icon />
        </Box>
        <Typography variant="body2" color="textSecondary">
          {text}
        </Typography>

        <Box>{children}</Box>
      </Box>
    </Paper>
  );
};

export default OptionBox;
