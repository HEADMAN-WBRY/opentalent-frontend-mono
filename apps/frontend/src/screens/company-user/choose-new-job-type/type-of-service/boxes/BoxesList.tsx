import { INFINITY_SIGN } from 'consts/common';
import React from 'react';

import { Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { ChooseJobServiceQuery, JobServiceTypeEnum } from '@libs/graphql-types';
import { isNumber, noop } from '@libs/helpers/common';

import { getItems } from '../consts';
import { useBoxClick } from '../hooks';
import DedicatedSearchBox from './DedicatedSearchBox';
import SelfServiceBox from './SelfServiceBox';

interface BoxesListProps {
  data?: ChooseJobServiceQuery;
}

const useStyles = makeStyles((theme) => ({
  root: {
    textAlign: 'center',
    paddingTop: theme.spacing(8),

    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
    },
  },
}));

const BoxesList = ({ data }: BoxesListProps) => {
  const classes = useStyles();
  const { value, onChange } = useBoxClick();
  const recruitersCount = data?.commonAppInfo?.total_ot_recruiters_count;
  const items = getItems({
    recruitersCount: isNumber(recruitersCount)
      ? recruitersCount
      : INFINITY_SIGN,
  });

  return (
    <Grid className={classes.root} wrap="nowrap" container spacing={8}>
      <Grid item>
        <SelfServiceBox
          {...items[JobServiceTypeEnum.SelfService]}
          isActive={JobServiceTypeEnum.SelfService === value}
          onClick={() => onChange(JobServiceTypeEnum.SelfService)}
          data={data}
        />
      </Grid>

      <Grid item>
        <DedicatedSearchBox
          {...items[JobServiceTypeEnum.DedicatedSearch]}
          isActive={false}
          onClick={noop}
          disabled
          // onClick={() => onChange(JobServiceTypeEnum.DedicatedSearch)}
        />
      </Grid>
    </Grid>
  );
};

export default BoxesList;
