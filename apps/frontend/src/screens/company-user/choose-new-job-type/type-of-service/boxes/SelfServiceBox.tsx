import { INFINITY_SIGN } from 'consts/common';
import React from 'react';
import { pathManager } from 'routes';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { ChooseJobServiceQuery } from '@libs/graphql-types';
import { modelPath } from '@libs/helpers/form';
import { formatNumberSafe } from '@libs/helpers/format';
import { ConnectedCheckbox } from '@libs/ui/components/form/checkbox';
import Typography, {
  OuterLink,
  RouterLink,
} from '@libs/ui/components/typography';

import { useHasOTAccess } from '../hooks';
import { FormState } from '../types';
import OptionBox, { OptionBoxProps } from './OptionBox';

interface SelfServiceBoxProps extends OptionBoxProps {
  data?: ChooseJobServiceQuery;
}

const useStyles = makeStyles((theme) => ({
  content: {
    paddingTop: theme.spacing(4),

    [theme.breakpoints.down('md')]: {
      flexDirection: 'column',
    },
  },
  checkbox: {
    padding: '0 8px 0 0',
    alignSelf: 'flex-start',
  },
  section: {
    padding: '8px 0',
    flexGrow: 1,
    textAlign: 'left',

    '&:nth-child(2)': {
      borderLeft: `1px solid ${theme.palette.grey[300]}`,
      paddingLeft: theme.spacing(4),

      [theme.breakpoints.down('md')]: {
        paddingLeft: theme.spacing(0),
        borderLeft: 'none',
      },
    },
  },
  label: {
    marginLeft: 0,
  },
}));

const SelfServiceBox = ({ data, isActive, ...rest }: SelfServiceBoxProps) => {
  const classes = useStyles({ isActive });
  const ownCandidates = data?.talentsSearch?.custom_paginator_info?.total;
  const hasNoOwnCandidates = ownCandidates === 0;
  const otCandidates = data?.commonAppInfo?.total_ot_approved_freelancers_count;
  const hasAccess = useHasOTAccess();

  return (
    <>
      <OptionBox isActive={isActive} {...rest}>
        <Grid className={classes.content} container>
          <Grid md={6} className={classes.section} item>
            <Box pb={2}>
              <ConnectedCheckbox
                formControlLabelProps={{ className: classes.label }}
                className={classes.checkbox}
                label="My talent pool"
                name={modelPath<FormState>((m) => m.selfCommunity)}
                disabled={!isActive}
              />
            </Box>
            <Box pl={8}>
              <Typography variant="caption" component="p">
                <Typography
                  variant="caption"
                  component="span"
                  color={hasNoOwnCandidates ? 'error.main' : 'info.main'}
                >
                  {formatNumberSafe(ownCandidates, { fallback: 0 })}
                </Typography>{' '}
                candidates
              </Typography>
              {hasNoOwnCandidates && (
                <RouterLink
                  to={pathManager.company.createProfile.generatePath()}
                  variant="caption"
                  color="info.main"
                  style={{ textDecoration: 'underline' }}
                >
                  Invite people - it’s FREE
                </RouterLink>
              )}
            </Box>
          </Grid>
          <Grid md={6} className={classes.section} item>
            <Box pb={2}>
              <ConnectedCheckbox
                disabled={!hasAccess || !isActive}
                formControlLabelProps={{ className: classes.label }}
                className={classes.checkbox}
                label="OpenTalent network"
                name={modelPath<FormState>((m) => m.otCommunity)}
              />
            </Box>
            <Box pl={8}>
              <Typography variant="caption">
                <Typography
                  variant="caption"
                  component="span"
                  color="info.main"
                >
                  {formatNumberSafe(otCandidates, { fallback: INFINITY_SIGN })}
                </Typography>{' '}
                candidates
              </Typography>

              {!hasAccess && (
                <>
                  <Typography
                    component="p"
                    variant="caption"
                    color="warning.main"
                  >
                    (Premium feature)
                  </Typography>
                  <OuterLink
                    href="https://www.meeting.opentalent.co/meetings/pieter40/opentalent-recruiter-call"
                    variant="caption"
                    color="info.main"
                    target="_blank"
                    style={{ textDecoration: 'underline' }}
                  >
                    Subscribe to Premium
                  </OuterLink>
                </>
              )}
            </Box>
          </Grid>
        </Grid>
      </OptionBox>
    </>
  );
};

export default SelfServiceBox;
