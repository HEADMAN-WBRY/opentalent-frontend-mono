import { Formik } from 'formik';
import React from 'react';
import * as yup from 'yup';

import { Box } from '@mui/material';

import {
  JobServiceTypeEnum,
  useChooseJobServiceQuery,
} from '@libs/graphql-types';
import Button from '@libs/ui/components/button';

import BoxesList from './boxes/BoxesList';
import { useSubmitAction } from './hooks';
import { FormState } from './types';

interface OptionsFormProps {}

const INITIAL_VALUES = {
  serviceType: '',
  otCommunity: false,
  selfCommunity: false,
} as unknown as FormState;

const validator = yup.object().shape(
  {
    serviceType: yup.string().required().nullable(),
    otCommunity: yup.bool().when(['selfCommunity', 'serviceType'], {
      is: (selfCommunity: boolean, serviceType: JobServiceTypeEnum) =>
        !!selfCommunity ||
        serviceType === JobServiceTypeEnum.DedicatedSearch ||
        !serviceType,
      then: (schema) => schema,
      otherwise: (schema) => schema.isTrue(),
    }),

    selfCommunity: yup.bool().when(['otCommunity'], {
      is: (otCommunity: boolean, serviceType: JobServiceTypeEnum) =>
        !!otCommunity ||
        serviceType === JobServiceTypeEnum.DedicatedSearch ||
        !serviceType,
      then: (schema) => schema,
      otherwise: (schema) => schema.isTrue(),
    }),
  },
  ['otCommunity', 'selfCommunity'] as any,
);

const OptionsForm = (props: OptionsFormProps) => {
  const { data } = useChooseJobServiceQuery();
  const onSubmit = useSubmitAction();

  return (
    <Formik
      onSubmit={onSubmit}
      initialValues={INITIAL_VALUES}
      validationSchema={validator}
      validateOnMount
      validateOnChange
    >
      {({ handleSubmit, errors }) => {
        return (
          <>
            <BoxesList data={data} />
            <Box pt={8}>
              <Button
                size="large"
                variant="contained"
                color="primary"
                disabled={!!Object.keys(errors).length}
                onClick={handleSubmit}
                data-test-id="submit-job-service-type"
              >
                Continue
              </Button>
            </Box>
          </>
        );
      }}
    </Formik>
  );
};

export default OptionsForm;
