import { JobServiceTypeEnum } from '@libs/graphql-types';
import { formatNumberSafe } from '@libs/helpers/format';
import Typography from '@libs/ui/components/typography';

import { ReactComponent as SearchIcon } from './assets/search.svg';
import { ReactComponent as SupportIcon } from './assets/support.svg';

export const getItems = ({
  recruitersCount,
}: {
  recruitersCount: number | string;
}) => ({
  [JobServiceTypeEnum.SelfService]: {
    title: '1. Self-service',
    text: `Post a Job for FREE. Push to people in your Talent Pool,
and the penTalent network. Hire commission-free.`,
    type: JobServiceTypeEnum.SelfService,
    Icon: SearchIcon,
  },
  [JobServiceTypeEnum.DedicatedSearch]: {
    title: '2. Dedicated search',
    text: (
      <>
        Push a job and let our community of{' '}
        <Typography component="span" color="info.main">
          {formatNumberSafe(recruitersCount)}
        </Typography>{' '}
        expert recruiters present you vetted candidates - No Cure No Pay.
      </>
    ),
    type: JobServiceTypeEnum.DedicatedSearch,
    Icon: SupportIcon,
  },
});
