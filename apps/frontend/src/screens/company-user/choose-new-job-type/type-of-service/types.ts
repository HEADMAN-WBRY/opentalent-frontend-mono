import { RouteComponentProps } from 'react-router-dom';

import { JobServiceTypeEnum } from '@libs/graphql-types';

export type PageProps = RouteComponentProps<{ id: string }>;

export interface FormState {
  serviceType: JobServiceTypeEnum;
  otCommunity: boolean;
  selfCommunity: boolean;
}
