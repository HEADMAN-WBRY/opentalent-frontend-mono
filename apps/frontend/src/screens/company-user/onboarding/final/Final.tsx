import React from 'react';

import { Box, Grow } from '@mui/material';
import { makeStyles } from '@mui/styles';

// import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

interface FinalProps {
  link: string;
}

const useStyles = makeStyles(() => ({
  wrapper: {
    maxWidth: 700,
    position: 'relative',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    textAlign: 'center',
  },
}));

const Final = ({ link }: FinalProps) => {
  const classes = useStyles();

  return (
    <Box className={classes.wrapper}>
      <Grow in timeout={600}>
        <div>
          <Typography variant="h3" fontWeight={600} paragraph>
            <span role="img">🥳</span>{' '}
            <Typography
              component="span"
              variant="h3"
              fontStyle="italic"
              fontWeight={600}
            >
              Congratulations!
            </Typography>
          </Typography>

          <Box pb={4}>
            <Typography variant="h6" paragraph>
              You are all set.
            </Typography>
          </Box>

          <Typography margin="0 auto" maxWidth={270} variant="body1" paragraph>
            ✉️ We sent you an email.{' '}
            <Typography fontWeight={600} component="span" variant="body1">
              Check&nbsp;your&nbsp;inbox
            </Typography>{' '}
            to verify&nbsp;your&nbsp;email address and&nbsp;login to OpenTalent.
          </Typography>

          {/* <Box pt={2}>
            <Link to={link}>
              <Button
                type="submit"
                fullWidth
                color="primary"
                variant="contained"
              >
                Let&apos;s get started!
              </Button>
            </Link>
          </Box> */}
        </div>
      </Grow>
    </Box>
  );
};

export default Final;
