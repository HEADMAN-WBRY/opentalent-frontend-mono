import { useSearchParams } from 'hooks/routing';
import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import TopSteps from 'screens/company-user/choose-new-job-type/shared/TopSteps';

import { TabContext, TabPanel } from '@mui/lab';
import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import CompanyForm from './company';
import { Steps } from './types';
import UserForm from './user';

interface FormProps extends RouteComponentProps {}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    minWidth: 500,
    margin: '0 auto',
    paddingTop: theme.spacing(8),
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    height: '100%',

    [theme.breakpoints.down('md')]: {
      minWidth: 'auto',
    },
  },

  tabPanel: {
    width: '100%',
  },

  steps: {
    marginBottom: theme.spacing(6),

    '& .MuiStepLabel-label': {
      color: 'white',
    },
    '& svg': {
      color: `${theme.palette.grey[700]} !important`,

      '&.Mui-active': {
        color: `${theme.palette.primary.main} !important`,
      },

      '& .MuiStepIcon-text': {
        fill: theme.palette.text.primary,
      },
    },
  },
}));

const STEPS = ['Profile setup', 'Company setup'];

const Form = ({ location, history }: FormProps) => {
  const classes = useStyles();
  const { step = 0 } = useSearchParams();
  const currentStep = Number(step) as unknown as number;

  return (
    <Box className={classes.wrapper}>
      <TopSteps
        className={classes.steps}
        activeIndex={currentStep}
        steps={STEPS}
      />
      <TabContext value={`${currentStep}`}>
        <TabPanel className={classes.tabPanel} value={Steps.Company}>
          <CompanyForm />
        </TabPanel>
        <TabPanel className={classes.tabPanel} value={Steps.User}>
          <UserForm />
        </TabPanel>
      </TabContext>
    </Box>
  );
};

export default Form;
