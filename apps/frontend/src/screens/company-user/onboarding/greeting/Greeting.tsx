import React from 'react';
import { Link } from 'react-router-dom';

import { Box, Grow } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

interface GreetingProps {
  link: string;
}

const useStyles = makeStyles(() => ({
  wrapper: {
    maxWidth: 500,
    position: 'relative',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  },
}));

const Greeting = ({ link }: GreetingProps) => {
  const classes = useStyles();

  return (
    <Box className={classes.wrapper}>
      <Grow in timeout={500}>
        <div>
          <Typography
            fontWeight={600}
            textAlign="center"
            variant="h4"
            paragraph
            fontStyle="italic"
          >
            Welcome to OpenTalent!&nbsp;<span role="img">🎉</span>
          </Typography>
          <Box px={4}>
            <Typography variant="body2" paragraph>
              Someone we trust invited you to our platform so we’re excited to
              have you here!
            </Typography>
          </Box>
          <Box textAlign="center" pt={2}>
            <Link to={link}>
              <Button
                type="submit"
                // fullWidth
                color="primary"
                variant="contained"
                style={{ width: 300 }}
              >
                Let&apos;s get started!
              </Button>
            </Link>
          </Box>
        </div>
      </Grow>
    </Box>
  );
};

export default Greeting;
