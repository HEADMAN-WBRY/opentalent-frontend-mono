// import { ReactComponent as WorkforceIcon } from 'assets/icons/workforce.svg';
// import { RouterButton } from '@libs/ui/components/button';
import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { ConnectedPageLayout } from 'components/layout/page-layout';
// import { useSearchParams } from 'hooks/routing';
import React from 'react';

// import { useLocation } from 'react-router-dom';
import Typography from '@libs/ui/components/typography';

import ChatApp from './chat-app';
import { MessageToWorkforceModal } from './modals';

interface ChatProps {}

const PAGE_TITLE = 'Messenger';

const useStyles = makeStyles((theme) => ({
  container: {
    height: 'calc(100vh - 120px)',
    overflow: 'hidden',
  },
  '@global .typeFormBtn': {
    display: 'none',
  },
}));

const Chat = (props: ChatProps) => {
  const classes = useStyles();
  // const location = useLocation<{ activeChannelId?: string }>();

  return (
    <ConnectedPageLayout
      documentTitle={PAGE_TITLE}
      headerProps={{ accountProps: {} }}
      drawerProps={{}}
      classes={{ contentWrapper: classes.container }}
    >
      <Box py={2} pb={6}>
        <Grid justifyContent="space-between" container>
          <Grid item>
            <Typography variant="h5">{PAGE_TITLE}</Typography>
          </Grid>
          {/* <Grid item>
            <RouterButton
              to={{ state: { [ChatModals.MessageToWorkforce]: true } }}
              startIcon={<WorkforceIcon />}
            >
              message MY workforce
            </RouterButton>
          </Grid> */}
        </Grid>
      </Box>
      <ChatApp />
      <MessageToWorkforceModal />
    </ConnectedPageLayout>
  );
};

export default Chat;
