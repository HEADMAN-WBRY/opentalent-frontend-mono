import { gql } from '@apollo/client';

export const SEARCH_TALENTS = gql`
  query GetTalents(
    $category_ids: [ID]
    $source_type: SourceTypeEnum = 'OWN'
    $first: Int = 1000
    $page: Int = 1
  ) {
    talentsSearch(
      category_ids: $category_ids
      source_type: $source_type
      first: $first
      page: $page
    ) {
      data {
        talent {
          id
          stream_chat_id
          first_name
          last_name
          recent_position_title
        }
      }
    }
  }
`;
