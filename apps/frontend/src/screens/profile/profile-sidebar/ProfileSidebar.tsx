import { Grid } from '@mui/material';
import React from 'react';

import { Talent } from '@libs/graphql-types';

import About from './cards/About';
import Availability from './cards/Availability';
import Documents from './cards/Documents';
import Social from './cards/Social';
import WorkHistory from './cards/WorkHistory';

interface ProfileSidebarProps {
  talent: Partial<Talent>;
}

const ProfileSidebar = (props: ProfileSidebarProps) => {
  const { talent } = props;
  return (
    <Grid spacing={2} direction="column" container wrap="nowrap">
      <Grid item>
        <Availability talent={talent} />
      </Grid>
      <Grid item>
        <Social talent={talent} />
      </Grid>
      <Grid item>
        <Documents talent={talent} />
      </Grid>
      <Grid item>
        <About>{talent?.about ?? ''}</About>
      </Grid>
      <Grid item>
        <WorkHistory talent={talent} />
      </Grid>
    </Grid>
  );
};

export default ProfileSidebar;
