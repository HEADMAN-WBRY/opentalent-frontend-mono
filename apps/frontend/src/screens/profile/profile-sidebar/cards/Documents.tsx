/* eslint-disable react/no-array-index-key */
import React from 'react';

import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Grid from '@mui/material/Grid';

import { Talent } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import { useOpenLimitedProfileAccessModal } from '../../LimitedProfileAccessModal';
import { useNeedShowContacts } from '../../profile-header/action-block/hooks';
import useClasses from '../styles';

interface DocumentsProps {
  talent: Partial<Talent>;
}

const Documents = (props: DocumentsProps) => {
  const { talent } = props;
  const classes = useClasses(props);
  const needShowContacts = useNeedShowContacts(talent);
  const openLimitedAccessModal = useOpenLimitedProfileAccessModal();

  return (
    <Card elevation={0} variant="outlined">
      <CardContent>
        <Typography className={classes.title} variant="h6">
          Portfolio
        </Typography>
        <Grid spacing={2} direction="column" container>
          {talent?.documents?.map((doc) => (
            <Grid
              title={doc?.title}
              className={classes.ellipsisContainer}
              key={doc?.hash}
              item
            >
              {needShowContacts ? (
                <a
                  href={doc?.document}
                  target="_blank"
                  rel="noreferrer"
                  download
                >
                  <Typography
                    className={classes.ellipsis}
                    variant="body2"
                    color="tertiary"
                  >
                    {doc?.title}
                  </Typography>
                </a>
              ) : (
                <Typography
                  style={{ cursor: 'pointer' }}
                  className={classes.ellipsis}
                  variant="body2"
                  color="tertiary"
                  onClick={() => openLimitedAccessModal()}
                >
                  {doc?.title}
                </Typography>
              )}
            </Grid>
          ))}
        </Grid>
      </CardContent>
    </Card>
  );
};

export default Documents;
