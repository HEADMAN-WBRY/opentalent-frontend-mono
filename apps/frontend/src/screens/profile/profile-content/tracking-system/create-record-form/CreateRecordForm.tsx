import { Grid } from '@mui/material';
import validationErrors from 'consts/validationErrors';
import { Formik, Form } from 'formik';
import React from 'react';
import * as yup from 'yup';

import { Talent } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import Typography from '@libs/ui/components/typography';

import { useCreateComment } from '../hooks';

interface CreateCommentFormProps {
  talent: Talent;
  onSubmit: VoidFunction;
}

const validator = yup.object().shape({
  message: yup.string().required(validationErrors.required),
});

const CreateCommentForm = ({ talent, onSubmit }: CreateCommentFormProps) => {
  const { createComment, isLoading } = useCreateComment(talent);

  return (
    <Formik
      validateOnBlur={false}
      initialValues={{ message: '' }}
      validationSchema={validator}
      onSubmit={async ({ message }, helpers) => {
        await createComment(message);
        helpers.resetForm();
        onSubmit();
      }}
    >
      {({ handleSubmit }) => (
        <Form>
          <Grid container spacing={4}>
            <Grid style={{ flexGrow: 1 }} item>
              <ConnectedTextField
                name="message"
                size="small"
                fullWidth
                label={
                  <Grid container>
                    <Typography variant="body1" color="textSecondary">
                      Post a comment...
                    </Typography>
                  </Grid>
                }
                variant="filled"
              />
            </Grid>
            <Grid item>
              <Button
                disabled={isLoading}
                onClick={() => handleSubmit()}
                fullWidth
                size="large"
                variant="outlined"
                color="secondary"
                type="submit"
              >
                Add comment
              </Button>
            </Grid>
          </Grid>
        </Form>
      )}
    </Formik>
  );
};

export default CreateCommentForm;
