import { makeStyles } from '@mui/styles';

import { Theme } from '@libs/ui/themes/default';

const useStyles = makeStyles((theme: Theme) => ({
  skillsWrapper: {
    marginTop: theme.spacing(3),
  },
  skillWrapper: {
    marginRight: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
}));

export default useStyles;
