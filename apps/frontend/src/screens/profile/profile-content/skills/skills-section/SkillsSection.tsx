import { Grid } from '@mui/material';
import React from 'react';

import Chip from '@libs/ui/components/chip/Chip';
import Typography from '@libs/ui/components/typography';

import { Skill } from '../../../types';
import useStyles from './styles';

export interface SkillsSectionProps {
  title?: string;
  skillsData?: Skill[];
  chipColor?: 'green' | 'lightBlue';
}

const SkillsSection = (props: SkillsSectionProps) => {
  const classes = useStyles(props);

  const { title, skillsData, chipColor } = props;

  return (
    <>
      <Typography variant="subtitle2">{title}</Typography>
      <Grid
        classes={{
          root: classes.skillsWrapper,
        }}
        container
        wrap="wrap"
      >
        {skillsData &&
          skillsData?.map(({ id, name }) => (
            <Grid
              classes={{
                root: classes.skillWrapper,
              }}
              key={id}
              item
            >
              <Chip
                color={chipColor || 'primary'}
                size="small"
                label={
                  <Typography variant="caption" color="textPrimary">
                    {name}
                  </Typography>
                }
              />
            </Grid>
          ))}
      </Grid>
    </>
  );
};

export default SkillsSection;
