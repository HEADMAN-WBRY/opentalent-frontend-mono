import { makeStyles } from '@mui/styles';

import { Theme } from '@libs/ui/themes/default';

const useStyles = makeStyles((theme: Theme) => ({
  paperRoot: {
    padding: '24px 16px',
  },
  commentsIcon: {
    marginRight: 11,
  },
  titleRoot: {
    height: theme.spacing(8),
    marginBottom: theme.spacing(4),
  },
  commentsIconRoot: {
    height: theme.spacing(5.5),
  },
  commentsWrapper: {
    'ul&': {
      height: theme.spacing(50),
      flexWrap: 'nowrap',
    },
  },
  commentWrapper: {
    '&:not(:last-of-type)': {
      marginBottom: theme.spacing(1),
    },
  },
  postCommentWrapper: {
    marginTop: theme.spacing(4),
  },
}));

export default useStyles;
