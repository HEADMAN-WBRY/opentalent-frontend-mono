import { makeStyles } from '@mui/styles';

import { Theme } from '@libs/ui/themes/default';

const useStyles = makeStyles((theme: Theme) => ({
  commentTitleWrapper: {
    marginBottom: theme.spacing(1.25),
  },
  avatarWrapper: {
    marginRight: theme.spacing(2),
  },
  fullNameWrapper: {
    marginRight: theme.spacing(2),
    marginLeft: theme.spacing(2),
  },
  avatarRoot: {
    width: theme.spacing(8),
    height: theme.spacing(8),
  },
  divider: {
    width: theme.spacing(1),
    height: theme.spacing(1),
    background: theme.palette.other.contrastText,
    borderRadius: theme.spacing(10),
  },
}));

export default useStyles;
