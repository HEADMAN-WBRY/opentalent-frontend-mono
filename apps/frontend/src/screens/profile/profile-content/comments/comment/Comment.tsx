import { Avatar, Grid } from '@mui/material';
import React from 'react';

import { ChatMessage } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import useStyles from './styles';

export interface CommentProps {
  data: ChatMessage;
}

const Comment = (props: CommentProps) => {
  const classes = useStyles(props);
  const { data } = props;
  const name = [data?.user?.first_name, data?.user?.last_name]
    .filter(Boolean)
    .join(', ');

  return (
    <Grid container>
      <Grid
        classes={{
          root: classes.commentTitleWrapper,
        }}
        container
        alignItems="center"
      >
        <Grid
          classes={{
            root: classes.avatarWrapper,
          }}
          item
        >
          <Avatar
            classes={{
              root: classes.avatarRoot,
            }}
            variant="circular"
            alt="Avatar"
            src=""
          />
        </Grid>
        <Grid item>
          <Typography variant="subtitle2">{name}</Typography>
        </Grid>
        <Grid
          classes={{
            root: classes.fullNameWrapper,
          }}
          item
        >
          <div className={classes.divider} />
        </Grid>
        <Grid item>
          <Typography variant="body2" color="textSecondary">
            {data?.created_at}
          </Typography>
        </Grid>
      </Grid>
      <Grid item>
        <Typography variant="body2" color="textSecondary">
          {data?.message}
        </Typography>
      </Grid>
    </Grid>
  );
};

export default Comment;
