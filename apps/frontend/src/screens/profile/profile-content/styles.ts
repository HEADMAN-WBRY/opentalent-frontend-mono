import { makeStyles } from '@mui/styles';

const useStyles = makeStyles(() => ({
  skills: {
    width: '100%',
  },
}));

export default useStyles;
