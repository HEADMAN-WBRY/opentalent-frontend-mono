import { ChatTypes } from 'components/chat';
import {
  generateChatName,
  getJobChannelId,
  mapTalentToChatUserData,
  UserToTalentChatMeta,
} from 'components/chat/client-to-talent';
import {
  CompanyUserChatData,
  CustomChatUserData,
  useStreamChatContext,
} from 'components/chat/common/provider';
import { DEFAULT_AVATAR } from 'consts/common';
import { useCallback, useEffect, useState } from 'react';
import { Channel, DefaultGenerics, StreamChat } from 'stream-chat';

import { Talent } from '@libs/graphql-types';

export const useCreateChat = () => {
  const { client, userData } = useStreamChatContext();

  const createChannel = useCallback(
    async (talent: Talent) => {
      if (!client || !userData) {
        return;
      }
      const talentData = mapTalentToChatUserData(talent);

      const chatId = getJobChannelId({
        talentId: talentData.chatId,
        companyUserId: userData.chatId,
      });
      const details: UserToTalentChatMeta = {
        type: ChatTypes.UserToTalent,
        companyUser: userData as CompanyUserChatData,
        talent: talentData,
      };

      const channel = await client.channel('messaging', chatId, {
        members: [talentData.chatId, userData.chatId],
        name: generateChatName(userData.name, talentData.name),
        image: DEFAULT_AVATAR,
        channelDetails: details,
      });

      return channel;
    },
    [client, userData],
  );

  return { createChannel };
};

export const createUserToTalent = async (
  client: StreamChat<DefaultGenerics>,
  {
    talentData,
    userData,
  }: {
    talentData: CustomChatUserData;
    userData: CompanyUserChatData;
  },
) => {
  if (!client || !userData || !talentData) {
    return;
  }

  const chatId = getJobChannelId({
    talentId: talentData.chatId,
    companyUserId: userData.chatId,
  });
  const details: UserToTalentChatMeta = {
    type: ChatTypes.UserToTalent,
    companyUser: userData,
    talent: talentData,
  };

  const channel = await client.channel('messaging', chatId, {
    members: [talentData.chatId, userData.chatId],
    name: generateChatName(userData.name, talentData.name),
    image: DEFAULT_AVATAR,
    channelDetails: details,
  });

  return channel;
};

interface CreateChatProps {
  talent: Talent;
}

export const useCreateChatWithTalent = ({ talent }: CreateChatProps) => {
  const [currentChannel, setCurrentChannel] =
    useState<Channel<DefaultGenerics>>();
  const { createChannel } = useCreateChat();

  const createChannelWithTalent = useCallback(async () => {
    const channel = await createChannel(talent);
    setCurrentChannel(channel);
  }, [createChannel, talent]);

  useEffect(() => {
    createChannelWithTalent();
  }, [createChannelWithTalent]);

  return {
    channel: currentChannel,
  };
};
