/* eslint-disable no-nested-ternary */
import { AvailableJobForTalent } from '@libs/graphql-types';
import { OptionType } from '@libs/ui/components/form/select';

export const mapJobsToOptions = (
  jobItem: AvailableJobForTalent,
): OptionType => {
  const label = jobItem?.is_applied
    ? 'already invited'
    : jobItem?.is_invited
    ? 'already applied'
    : '';
  const name = [jobItem?.job?.name || '', label].filter(Boolean).join(' - ');
  const value = jobItem?.job?.id || '';

  return {
    text: name,
    value,
    disabled: !!label,
  };
};
