import { ReactComponent as CheckIcon } from 'assets/icons/check.svg';
import { ReactComponent as EuroIcon } from 'assets/icons/euro.svg';
import { ReactComponent as MapPinIcon } from 'assets/icons/map-pin.svg';
import { DEFAULT_AVATAR } from 'consts/common';
import { useCurrentUser } from 'hooks/auth';
import { useHaveAccessToTalent } from 'hooks/talents';
import React, { useMemo } from 'react';
import { formatRate } from 'utils/job';
import { formatName } from 'utils/talent';

import { Avatar, Grid, Tooltip, Badge, Chip } from '@mui/material';
import Box from '@mui/material/Box';

import { Talent } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import useStyles from './styles';

export interface UserBlockProps {
  talent: Partial<Talent> | null;
  isXS: boolean;
}

const UserBlock = (props: UserBlockProps) => {
  const classes = useStyles(props);
  const { talent, isXS } = props;
  const rate = formatRate({ min: talent?.rate_min });

  const { isCompany } = useCurrentUser();
  const canSeeName = useHaveAccessToTalent(talent as Talent);
  const needHideFullName = isCompany && !canSeeName;

  const talentName = formatName({
    firstName: talent?.first_name,
    lastName: talent?.last_name,
    hideFullName: needHideFullName,
  });

  const isVerificationCheckShown = useMemo(
    () => talent?.is_invitation_accepted && !talent?.is_verification_required,
    [talent?.is_invitation_accepted, talent?.is_verification_required],
  );

  return (
    <Grid
      className={classes.root}
      direction={isXS ? 'column' : 'row'}
      spacing={6}
      container
      wrap="nowrap"
    >
      <Grid item justifyContent={isXS ? 'center' : undefined}>
        <Badge
          overlap="circular"
          classes={{ badge: classes.avatarBadge }}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          variant="standard"
          badgeContent={
            <Box>
              <Tooltip
                placement="right-end"
                open={talent?.available_now ? undefined : false}
                title={
                  talent?.hours_per_week
                    ? `Available now for ${talent.hours_per_week} hours per week`
                    : "Available now - let's get to work!"
                }
              >
                <Box width={22} height={22} />
              </Tooltip>
            </Box>
          }
        >
          <Avatar
            className={classes.avatar}
            src={talent?.avatar?.avatar || DEFAULT_AVATAR}
          />
        </Badge>
      </Grid>
      <Grid item>
        <Grid wrap="nowrap" direction="column" container>
          <Grid
            justifyContent={isXS ? 'center' : undefined}
            spacing={2}
            item
            container
            style={{ flexWrap: 'nowrap', alignItems: 'center' }}
          >
            <Grid item>
              <Typography className={classes.name} noWrap variant="h5">
                {talentName}
              </Typography>
            </Grid>
            {isVerificationCheckShown && (
              <Grid item>
                <Tooltip title="This person was verified by Opentalent through interviews, social media and/or legal document checks.">
                  <CheckIcon className={classes.checkIcon} />
                </Tooltip>
              </Grid>
            )}
            {talent?.is_verification_required && (
              <Typography
                variant="subtitle2"
                className={classes.pendingVerification}
              >
                pending verification
              </Typography>
            )}
            {talent?.is_recruiter && (
              <Grid item>
                <Chip
                  size="small"
                  label="Recruiter"
                  className={classes.recruiterChip}
                />
              </Grid>
            )}
          </Grid>
          <Grid item>
            <Typography
              align={isXS ? 'center' : undefined}
              variant="body2"
              color="textSecondary"
              className={classes.positionTitle}
            >
              {talent?.recent_position_title}
            </Typography>
          </Grid>
          <Grid className={classes.info} item>
            <Grid
              justifyContent={isXS ? 'center' : undefined}
              alignItems="stretch"
              spacing={2}
              container
            >
              {!!rate?.length && (
                <>
                  <Grid item>
                    <EuroIcon />
                  </Grid>
                  <Grid item>
                    <Typography variant="body2" color="textSecondary">
                      {rate}
                    </Typography>
                  </Grid>
                </>
              )}
              <Grid item>&nbsp;</Grid>
              {talent?.location && (
                <>
                  <Grid item>
                    <MapPinIcon color="disabled" />
                  </Grid>
                  <Grid item>
                    <Typography
                      className={classes.location}
                      variant="body2"
                      color="textSecondary"
                    >
                      {talent?.location}
                    </Typography>
                  </Grid>
                </>
              )}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default UserBlock;
