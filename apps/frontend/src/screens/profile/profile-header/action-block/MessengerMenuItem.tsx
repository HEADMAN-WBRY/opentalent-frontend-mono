import { useStreamChatContext } from 'components/chat/common/provider';
import { useHaveAccessToTalent } from 'hooks/talents';
import React, { useCallback, useEffect, useState } from 'react';
import { useOpenChatModal } from 'screens/profile/modals/chat-with-talent';

import SendIcon from '@mui/icons-material/Send';

import { Talent } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';

import { useOpenLimitedProfileAccessModal } from '../../LimitedProfileAccessModal';

interface MessengerMenuItemProps {
  closePopup: VoidFunction;
  talent: Talent;
}

const useMessengerStatus = (streamChatId: string) => {
  const [isEnabled, setIsEnabled] = useState(false);
  const { client } = useStreamChatContext();

  const queryTalent = useCallback(async () => {
    if (!streamChatId) {
      return;
    }

    const res = await client.queryUsers({
      id: {
        $eq: streamChatId,
      },
    });
    setIsEnabled(!!res.users.length);
  }, [client, streamChatId]);

  useEffect(() => {
    queryTalent();
  }, [queryTalent]);

  return isEnabled;
};

const MessengerMenuItem = ({ closePopup, talent }: MessengerMenuItemProps) => {
  const hasAccess = useHaveAccessToTalent(talent as Talent);
  const openStartChatting = useOpenChatModal();
  const isEnabled = useMessengerStatus(talent.stream_chat_id || '');
  const openModal = useOpenLimitedProfileAccessModal();

  return (
    <Button
      variant="contained"
      color="primary"
      style={{ minWidth: '150px' }}
      size="medium"
      fullWidth
      disabled={!isEnabled}
      endIcon={<SendIcon />}
      onClick={() => {
        if (hasAccess) {
          openStartChatting({ talent });
          closePopup();
        } else {
          openModal();
        }
      }}
    >
      Message
    </Button>
  );
};

export default MessengerMenuItem;
