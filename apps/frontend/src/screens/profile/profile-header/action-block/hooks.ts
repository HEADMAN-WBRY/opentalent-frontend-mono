import { useApolloClient } from '@apollo/client';
import { TalentModals } from 'components/custom/talent/types';
import { useCurrentUser } from 'hooks/auth';
import { usePushWithQuery } from 'hooks/routing';
import { useHaveAccessToTalent } from 'hooks/talents';
import { GET_JOBS_TOTAL } from 'screens/profile/queries';

import { Query, Talent } from '@libs/graphql-types';

export const useGetCompanyJobs = () => {
  const client = useApolloClient();
  const data = client.readQuery<Query>({
    query: GET_JOBS_TOTAL,
  });
  return data?.jobs?.paginatorInfo?.total || 0;
};

export const useOnInviteAction = (talentId: string) => {
  const pushWithQuery = usePushWithQuery();
  const total = useGetCompanyJobs();

  const modal = total
    ? TalentModals.InviteByCompany
    : TalentModals.CreateCompany;

  return () => pushWithQuery({ query: { modal, talentId } });
};

export const useNeedShowContacts = (talent: Partial<Talent>) => {
  const { data, isTalent, isCompany } = useCurrentUser();
  const hasAccess = useHaveAccessToTalent(talent as Talent);

  const isCurrentTalent = isTalent && data?.currentTalent?.id === talent?.id;
  const isInUsersCompany = isCompany && hasAccess;

  return isCurrentTalent || isInUsersCompany;
};
