import { TalentActions } from 'components/custom/talent/actions-menu';
import { usePopupState } from 'material-ui-popup-state/hooks';
import React from 'react';

import { Grid } from '@mui/material';

import { Talent } from '@libs/graphql-types';
import { ButtonProps } from '@libs/ui/components/button';

import MessengerMenuItem from './MessengerMenuItem';

interface ContactTalentMenuProps {
  btnProps?: ButtonProps;
  talent: Partial<Talent>;
  hideContacts?: boolean;
  refetchUser?: VoidFunction;
}

const ContactTalentMenu = ({
  talent,
  hideContacts,
  btnProps = {},
  refetchUser,
}: ContactTalentMenuProps) => {
  const popupState = usePopupState({
    variant: 'popper',
    popupId: 'demoPopper',
  });

  return (
    <Grid container spacing={4} direction="column" wrap="nowrap">
      {!hideContacts && (
        <Grid item>
          <MessengerMenuItem
            talent={talent as Talent}
            closePopup={popupState.close}
          />
        </Grid>
      )}
      <Grid style={{ display: 'grid' }} item>
        <TalentActions talent={talent as Talent} refetch={refetchUser} />
      </Grid>
    </Grid>
  );
};

export default ContactTalentMenu;
