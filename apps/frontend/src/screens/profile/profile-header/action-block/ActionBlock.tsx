import EditIcon from '@mui/icons-material/Edit';
import { Box, Button } from '@mui/material';
import React from 'react';
import { Link } from 'react-router-dom';
import { pathManager } from 'routes/consts';

import { Talent } from '@libs/graphql-types';

import ContactTalentMenu from './ContactTalentMenu';
import useStyles from './styles';

interface ActionBlockProps {
  isTalentFlow: boolean;
  talent: Partial<Talent>;
  refetchTalent: VoidFunction;
}

const ActionBlock = ({
  isTalentFlow,
  talent,
  refetchTalent,
}: ActionBlockProps) => {
  const classes = useStyles();

  return (
    <Box className={classes.container}>
      {isTalentFlow ? (
        <Link to={pathManager.talent.editProfile.generatePath()}>
          <Button
            endIcon={<EditIcon fontSize="inherit" />}
            variant="outlined"
            color="secondary"
            size="small"
          >
            Edit profile
          </Button>
        </Link>
      ) : (
        <ContactTalentMenu refetchUser={refetchTalent} talent={talent} />
      )}
    </Box>
  );
};

export default ActionBlock;
