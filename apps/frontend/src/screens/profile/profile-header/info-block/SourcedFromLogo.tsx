import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Tooltip from '@mui/material/Tooltip';
import { DEFAULT_AVATAR } from 'consts/common';
import React from 'react';

import useStyles from './styles';

interface SourcedFromLogoProps {
  sourceName?: string;
  sourceLogo?: string;
  invitedByName: string;
  talentName: string;
}

const getTooltipTitle = ({
  invitedByName,
  sourceName,
  talentName,
}: SourcedFromLogoProps) => {
  if (invitedByName === '') {
    return 'Team member of OpenTalent';
  }
  const sourceNamePrefix =
    sourceName && sourceName !== 'OpenTalent' ? `from ${sourceName}` : '';
  return `${invitedByName} ${sourceNamePrefix} invited ${talentName} to OpenTalent.`;
};

const SourcedFromLogo = (props: SourcedFromLogoProps) => {
  const { sourceLogo = DEFAULT_AVATAR } = props;
  const classes = useStyles();
  const title = getTooltipTitle(props);
  return (
    <Box position="relative">
      <Tooltip
        // open={invitedByName ? undefined : false}
        classes={{
          tooltip: classes.sourcedTooltip,
          tooltipPlacementLeft: classes.sourcedTooltipPlacementLeft,
          tooltipPlacementRight: classes.sourcedTooltipPlacementRight,
        }}
        placement="right-end"
        title={title}
      >
        <Avatar className={classes.sourcedAvatar} src={sourceLogo} />
      </Tooltip>
    </Box>
  );
};

export default SourcedFromLogo;
