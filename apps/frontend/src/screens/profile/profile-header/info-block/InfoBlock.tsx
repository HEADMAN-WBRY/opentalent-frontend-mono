import { Grid } from '@mui/material';
import React from 'react';
import { formatName } from 'utils/talent';

import {
  Talent, // TalentInvitationTypeEnum
} from '@libs/graphql-types';
// import InfoIcon from '@mui/icons-material/Info';
import Typography from '@libs/ui/components/typography';

import InvitedByLink from './InvitedByLink';
import SourcedFromLogo from './SourcedFromLogo';
import useStyles from './styles';

interface InfoBlockProps {
  isTalentFlow?: boolean;
  isSM?: boolean;
  isXS?: boolean;
  talent: Partial<Talent> | null;
}

const InfoBlock = ({ isTalentFlow, isXS, talent }: InfoBlockProps) => {
  const classes = useStyles();
  const sourceLogo = talent?.source?.logo;
  const sourceName = talent?.source?.name;
  const invitedBy = talent?.invited_by;
  // const invitedByCompany =
  //   invitedBy?.invitation_type ===
  //   TalentInvitationTypeEnum.InvitationTypeByCompany;

  const talentName = formatName({
    firstName: talent?.first_name,
    lastName: talent?.last_name,
  });

  const invitedByName = formatName({
    firstName: talent?.invited_by?.first_name,
    lastName: talent?.invited_by?.last_name,
  });

  return (
    <Grid
      className={classes.infoBlock}
      container
      justifyContent={isXS ? 'center' : undefined}
    >
      {!isTalentFlow && sourceLogo && (
        <Grid item className={classes.sourcedInfo}>
          <SourcedFromLogo
            sourceLogo={sourceLogo}
            invitedByName={invitedByName}
            sourceName={sourceName}
            talentName={talentName}
          />
        </Grid>
      )}
      <Grid item>
        {invitedBy && (
          <Grid
            direction="column"
            className={classes.invitedBlock}
            justifyContent="center"
            container
          >
            <Grid item>
              <Grid container>
                <Grid item>
                  <Typography
                    transform="uppercase"
                    variant="overline"
                    color="textSecondary"
                  >
                    Invited by &nbsp;
                  </Typography>
                </Grid>
                <Grid item>
                  {
                    // invitedByCompany && (
                    //   <Tooltip
                    //     TransitionComponent={Zoom}
                    //     title="This user has been invited by a company"
                    //   >
                    //     <InfoIcon color="disabled" />
                    //   </Tooltip>
                    // )
                  }
                </Grid>
              </Grid>
            </Grid>
            <Grid item>
              <InvitedByLink
                invitedBy={invitedBy}
                isTalentFlow={!!isTalentFlow}
              />
            </Grid>
          </Grid>
        )}
      </Grid>
    </Grid>
  );
};

export default InfoBlock;
