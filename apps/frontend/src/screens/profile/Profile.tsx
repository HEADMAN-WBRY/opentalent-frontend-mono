import {
  CreateCampaignModal,
  InviteByCompanyModal,
} from 'components/custom/talent/modals';
import { ConnectedPageLayout } from 'components/layout/page-layout';
import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';
import { useRouteMatch } from 'react-router-dom';
import { pathManager } from 'routes';

import Grid from '@mui/material/Grid';

import { Tag } from '@libs/graphql-types';

import { LimitedProfileAccessModal } from './LimitedProfileAccessModal';
import { useProfileInfo } from './hooks';
import { ChatWithTalentModal } from './modals/chat-with-talent';
import ProfileContent from './profile-content';
import ProfileHeader from './profile-header';
import ProfileSidebar from './profile-sidebar';
import useStyles from './styles';
import { ProfileScreenProps } from './types';

const Profile = (props: ProfileScreenProps) => {
  const talentId = props.match?.params?.id;
  const { isSM, isXS } = useMediaQueries();
  const classes = useStyles(props);
  const isTalentFlow = useRouteMatch({
    path: pathManager.talent.profile.generatePath(),
    exact: true,
  });
  const {
    loading,
    talent: talentData,
    fetchTalent,
    data,
  } = useProfileInfo(talentId);
  const companyTags = (data?.currentUserCompanyTags as Tag[]) || [];

  return (
    <ConnectedPageLayout
      isLoading={loading}
      headerProps={{ accountProps: {} }}
      documentTitle="Profile"
      drawerProps={{}}
      contentSpacing={isXS ? 0 : undefined}
    >
      {!!talentData && (
        <Grid direction="column" container className={classes.container}>
          <ProfileHeader
            talent={talentData as any}
            isTalentFlow={!!isTalentFlow}
            refetchTalent={fetchTalent}
          />
          <Grid item>
            <Grid
              container
              direction={isSM ? 'column-reverse' : 'row'}
              spacing={4}
            >
              <Grid md={4} item>
                <ProfileSidebar talent={talentData as any} />
              </Grid>
              <Grid md={8} item>
                <ProfileContent
                  talent={talentData as any}
                  companyTags={companyTags}
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      )}

      <InviteByCompanyModal />
      <CreateCampaignModal />
      <LimitedProfileAccessModal talentId={talentId || ''} />
      <ChatWithTalentModal />
    </ConnectedPageLayout>
  );
};

export default Profile;
