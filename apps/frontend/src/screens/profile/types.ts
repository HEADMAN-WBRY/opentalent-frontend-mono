import { RouteChildrenProps } from 'react-router-dom';

export type TalentAvatar = {
  avatar: string;
};

export type Talent = {
  firstName?: string;
  lastName?: string;
  address?: string;
  location?: string;
  recentPositionTitle?: string;
  about?: string;
  avatar?: TalentAvatar;
  talentWorkHistory?: [TalentWorkHistory];
  skills?: {
    data: [Skill];
  };
};

export type TalentWorkHistory = {
  id?: string;
  positionTitle?: string;
  companyName?: string;
  workedFrom?: string;
  workedTo?: string;
};

export type Skill = {
  id?: string;
  skillTypeId?: number;
  name?: string;
};

export type ProfileParams = {
  talentId: string;
};

export interface ProfileScreenProps
  extends RouteChildrenProps<{ id: string }> {}
