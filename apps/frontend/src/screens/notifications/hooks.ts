import { useQuery } from '@apollo/client';
import { GET_NOTIFICATIONS } from 'graphql/notifications';

import {
  PaginatorInfo,
  Query,
  QueryCurrentUserNotificationsArgs,
} from '@libs/graphql-types';

export const useNotifications = () => {
  const { data, loading, fetchMore, variables } = useQuery<
    Query,
    QueryCurrentUserNotificationsArgs
  >(GET_NOTIFICATIONS, {
    variables: {
      page: 1,
      first: 20,
    },
  });

  const notifications = data?.currentUserNotifications?.data || [];
  const hasMore =
    data?.currentUserNotifications?.paginatorInfo?.hasMorePages || false;
  const paginator =
    data?.currentUserNotifications?.paginatorInfo || ({} as PaginatorInfo);

  const loadMore = () => {
    fetchMore({
      variables: {
        ...variables,
        page: (paginator?.currentPage || 1) + 1,
      },
    });
  };

  return {
    notifications,
    isLoading: loading,
    loadMore,
    hasMore,
  };
};
