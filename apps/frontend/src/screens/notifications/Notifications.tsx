import { Box } from '@mui/material';
import { ConnectedPageLayout } from 'components/layout/page-layout';
import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';

import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import NotificationList from './NotificationList';
import { useNotifications } from './hooks';
import { ScreenProps } from './types';

const JobBoard = ({ match }: ScreenProps) => {
  const { isXS } = useMediaQueries();
  const { notifications, isLoading, loadMore, hasMore } = useNotifications();

  return (
    <ConnectedPageLayout
      documentTitle="Job board"
      drawerProps={{}}
      headerProps={{ accountProps: {} }}
      contentSpacing={isXS ? 0 : undefined}
      isLoading={isLoading}
    >
      <Box pt={4} pb={6}>
        <Typography variant="h5">Notifications</Typography>
      </Box>
      <NotificationList notifications={notifications} />
      <Box pt={6} pb={6} style={{ display: 'flex' }} justifyContent="center">
        <Button
          disabled={!hasMore}
          onClick={loadMore}
          variant="outlined"
          color="secondary"
        >
          view more
        </Button>
      </Box>
    </ConnectedPageLayout>
  );
};

export default JobBoard;
