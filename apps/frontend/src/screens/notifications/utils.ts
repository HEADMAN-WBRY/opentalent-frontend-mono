import { isToday, parseISO } from 'date-fns';

import { Notification } from '@libs/graphql-types';

export const sortNotifications = (notifications: Notification[]) =>
  notifications.reduce(
    (acc, item) => {
      if (isToday(parseISO(item.created_at))) {
        acc.today.push(item);
      } else {
        acc.earlier.push(item);
      }
      return acc;
    },
    { today: [] as Notification[], earlier: [] as Notification[] },
  );
