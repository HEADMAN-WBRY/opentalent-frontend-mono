import { Box, Grid } from '@mui/material';
import NotificationItem from 'components/layout/header/notifications/NotificationItem';
import {
  useChangeNotificationStatus,
  useMarkAllAsRead,
  useRemoveNotification,
} from 'components/layout/header/notifications/hooks';
import React from 'react';

import { Notification } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import { sortNotifications } from './utils';

interface NotificationListProps {
  notifications: Notification[];
}

const NotificationList = ({ notifications }: NotificationListProps) => {
  const { markAllAsRead, isMarkingInProgress } = useMarkAllAsRead();
  const sortedNotifications = sortNotifications(notifications);
  const { handleChangeStatus, isStatusLoading } = useChangeNotificationStatus();
  const { handleDelete, isDeleteInProgress } = useRemoveNotification();

  return <>
    <Grid justifyContent="space-between" alignItems="center" container>
      <Grid item>
        <Typography variant="h6" color="textSecondary">
          Today
        </Typography>
      </Grid>
      <Grid item>
        <Button
          onClick={markAllAsRead}
          disabled={isMarkingInProgress}
          color="tertiary"
        >
          Mark all as read
        </Button>
      </Grid>
    </Grid>
    <Box pt={8} pb={8}>
      {sortedNotifications.today.map((i) => (
        <NotificationItem
          changeStatus={handleChangeStatus}
          deleteNotification={handleDelete}
          isLoading={isStatusLoading || isDeleteInProgress}
          notification={i}
          key={i.id}
        />
      ))}
      {!sortedNotifications.today.length && (
        <Typography variant="body1">No notifications today</Typography>
      )}
    </Box>

    {!!sortedNotifications.earlier.length && (
      <>
        <Box pb={4} pt={8}>
          <Typography variant="h6" color="textSecondary">
            Earlier
          </Typography>
        </Box>

        <Box pt={8}>
          {sortedNotifications.earlier.map((i) => (
            <NotificationItem
              changeStatus={handleChangeStatus}
              deleteNotification={handleDelete}
              isLoading={isStatusLoading || isDeleteInProgress}
              notification={i}
              key={i.id}
            />
          ))}
        </Box>
      </>
    )}
  </>;
};

export default React.memo(NotificationList);
