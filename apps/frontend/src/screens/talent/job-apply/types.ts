import { RouteComponentProps } from 'react-router-dom';

export type ScreenProps = RouteComponentProps<{ id: string }>;
