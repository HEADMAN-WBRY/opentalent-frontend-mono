import DateRangeIcon from '@mui/icons-material/DateRange';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import MenuBookIcon from '@mui/icons-material/MenuBook';
import PermContactCalendarIcon from '@mui/icons-material/PermContactCalendar';
import TimelapseIcon from '@mui/icons-material/Timelapse';
import WatchLaterIcon from '@mui/icons-material/WatchLater';
import WorkIcon from '@mui/icons-material/Work';
import { ReactComponent as EuroIcon } from 'assets/icons/euro.svg';

export const ICONS_MAP = {
  book: MenuBookIcon,
  location: LocationOnIcon,
  clock: WatchLaterIcon,
  calendar: PermContactCalendarIcon,
  period: DateRangeIcon,
  client: WorkIcon,
  timelapse: TimelapseIcon,
  euro: EuroIcon,
};
