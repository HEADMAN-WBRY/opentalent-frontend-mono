import { Box } from '@mui/material';
import { MAX_JOB_RATE, MIN_JOB_RATE } from 'consts/jobs';
import { Form, Formik } from 'formik';
import React from 'react';
import * as yup from 'yup';

import { Job, JobApplication } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import Typography from '@libs/ui/components/typography';

import { useApplyForAction } from './hooks';

interface ApplyFormProps {
  job: Job;
  application?: JobApplication;
}

const validator = yup.object().shape({
  rate: yup.number().max(MAX_JOB_RATE).min(MIN_JOB_RATE),
  pitch: yup.string().trim().max(1000),
});

const ApplyForm = ({ job, application }: ApplyFormProps) => {
  const { onSubmit, isLoading } = useApplyForAction(job.id);
  return (
    <Box>
      <Box pb={8}>
        <Typography variant="h6">What is your rate?</Typography>
      </Box>
      <Box pb={4}>
        <Typography variant="body1" color="textSecondary">
          Please enter your hourly rate range or indicate a specific rate for
          this job
        </Typography>
      </Box>
      <Formik
        validationSchema={validator}
        onSubmit={onSubmit}
        initialValues={{
          pitch: application?.pitch || '',
          rate: application?.rate ? String(application?.rate) : '',
        }}
      >
        {() => (
          <Form>
            <Box pb={8} maxWidth="360px">
              <ConnectedTextField
                size="medium"
                name="rate"
                fullWidth
                variant="filled"
                label="Your rate for this job"
                InputProps={{ endAdornment: '€/h' }}
                disabled={!!application}
              />
            </Box>
            <Typography color="textSecondary" variant="body1">
              Describe why you are the perfect candidate for this job.
            </Typography>
            <Box pt={4} pb={8}>
              <ConnectedTextField
                size="medium"
                name="pitch"
                multiline
                rows={8}
                fullWidth
                variant="filled"
                label="Your motivational letter"
                helperText="Max. 1 000 characters"
                disabled={!!application}
              />
            </Box>
            <Box pb={8}>
              <Button
                type="submit"
                size="large"
                variant="contained"
                color="primary"
                disabled={!!application || isLoading}
              >
                apply
              </Button>
            </Box>
          </Form>
        )}
      </Formik>
    </Box>
  );
};

export default ApplyForm;
