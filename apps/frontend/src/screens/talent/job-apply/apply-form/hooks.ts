import { useMutation } from '@apollo/client';
import { FormikHelpers } from 'formik';
import useMixPanel from 'hooks/common/useMixPanel';
import { useSnackbar } from 'notistack';
import { useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { pathManager } from 'routes';
import { APPLY_FOR_JOB } from 'screens/talent/job-board-v2/queries';

import { Mutation, MutationApplyForJobArgs } from '@libs/graphql-types';

import { INITIAL_VALUES } from './consts';

export const useApplyForAction = (job_id: string) => {
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();
  const mixPanel = useMixPanel();
  const [applyForJobRequest, { loading }] = useMutation<
    Mutation,
    MutationApplyForJobArgs
  >(APPLY_FOR_JOB, {
    variables: { job_id },
    onCompleted: () => {
      enqueueSnackbar('You successfully applied to job', {
        variant: 'success',
      });
      mixPanel.track(
        'User sent an application for a job and got the success message',
      );
      history.push(pathManager.talent.jobBoard.generatePath());
    },
  });

  const onSubmit = useCallback(
    (
      values: typeof INITIAL_VALUES,
      helpers: FormikHelpers<typeof INITIAL_VALUES>,
    ) => {
      applyForJobRequest({
        variables: { job_id, rate: Number(values.rate), pitch: values.pitch },
      });
    },
    [applyForJobRequest, job_id],
  );

  return { onSubmit, isLoading: loading };
};
