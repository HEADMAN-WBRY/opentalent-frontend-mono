import { Box, Grid, SwipeableDrawer } from '@mui/material';
import { makeStyles } from '@mui/styles';
import FormikAutoSaving from 'components/form/formik/FormikAutoSave';
import { ConnectedPageLayout } from 'components/layout/page-layout';
import { Formik } from 'formik';
import useMediaQueries from 'hooks/common/useMediaQueries';
import useMixPanel from 'hooks/common/useMixPanel';
import React, { useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { useToggle } from 'react-use';
import { noop } from 'utils/common';

import Typography from '@libs/ui/components/typography';

import Content from './content';
import Filter from './filter';
import { useAutoSaveHandler, useInitialValues } from './hooks';

interface JobBoardProps extends RouteComponentProps {}

const useStyles = makeStyles((theme) => ({
  settingsDrawer: {
    position: 'static',
    borderLeft: 'none',

    [theme.breakpoints.down('lg')]: {
      position: 'fixed',
      width: '40%',
    },
    [theme.breakpoints.down('md')]: {
      position: 'fixed',
      width: '100%',
    },
  },
  title: {
    fontWeight: 500,
    fontSize: '24px',
    lineHeight: '32px',
  },
}));

const JobBoard = (props: JobBoardProps) => {
  const classes = useStyles();
  const saveToQuery = useAutoSaveHandler();
  const initialValues = useInitialValues();
  const { isMD } = useMediaQueries();
  const mixPanel = useMixPanel();

  const [isDrawerOpen, toggleIsDrawerOpen] = useToggle(false);
  const openDrawer = () => toggleIsDrawerOpen(true);

  useEffect(() => {
    mixPanel.track('User visited the job board');
  }, [mixPanel]);

  return (
    <ConnectedPageLayout
      documentTitle="Job board"
      drawerProps={{}}
      headerProps={{ accountProps: {} }}
      contentSpacing={4}
    >
      <Box pt={4} pb={isMD ? 2 : 6}>
        <Typography className={classes.title}>
          Open projects, gigs and jobs
        </Typography>
      </Box>
      <Formik initialValues={initialValues} onSubmit={noop}>
        <Grid
          container
          wrap="nowrap"
          direction={isMD ? 'column' : 'row'}
          spacing={4}
        >
          <FormikAutoSaving onValuesChange={saveToQuery} />
          <Grid style={{ minWidth: 320 }} item>
            <SwipeableDrawer
              classes={{
                paper: classes.settingsDrawer,
              }}
              anchor="right"
              variant={isMD ? 'temporary' : 'permanent'}
              open={isDrawerOpen}
              onClose={() => toggleIsDrawerOpen(false)}
              onOpen={openDrawer}
            >
              <Filter toggleDrawer={toggleIsDrawerOpen} />
            </SwipeableDrawer>
          </Grid>
          <Grid item style={{ flexGrow: 1 }}>
            <Content openDrawer={openDrawer} />
          </Grid>
        </Grid>
      </Formik>
    </ConnectedPageLayout>
  );
};

export default JobBoard;
