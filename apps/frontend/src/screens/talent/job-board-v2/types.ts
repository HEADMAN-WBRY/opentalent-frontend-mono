import { JobOrderByColumn } from '@libs/graphql-types';
import { OptionType } from '@libs/ui/components/form/select';

export interface FilterForm {
  categories: OptionType[];
  search: string;
  skills: OptionType[];
  rate_min: string;
  rate_max: string;
  order_by_column?: JobOrderByColumn;
  is_saved: boolean;
  page: string;
}
