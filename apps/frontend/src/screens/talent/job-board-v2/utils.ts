import {
  JobOrderByColumn,
  QueryCurrentTalentJobBoardSearchArgs,
  SortOrder,
} from '@libs/graphql-types';
import { OptionType } from '@libs/ui/components/form/select';

import { FilterForm } from './types';

const mapOptionsToQuery = (opts: OptionType[]) =>
  opts.map(({ value, text, skill_type }) => {
    const arr = [value, text];
    return skill_type ? arr.concat(skill_type) : arr;
  });

export const mapFormToQuery = (form: FilterForm) => {
  return {
    ...form,
    categories: mapOptionsToQuery(form.categories || []),
    skills: mapOptionsToQuery(form.skills || []),
  };
};

const mapOptionsFromQuery = (str: string) =>
  str?.split(';').map((s) => {
    const [value, text, skill_type] = s?.split(',');
    return { value, text, ...(skill_type && { skill_type }) };
  });

export const mapQueryToVariables = (
  query: Record<string, any>,
): QueryCurrentTalentJobBoardSearchArgs => {
  const values = mapQueryToInitialValues(query);

  return {
    search: values.search || undefined,
    categories: values.categories?.map((i) => `${i.value}`) || undefined,
    skills: values.skills?.map((i) => `${i.value}`) || undefined,
    rate_min: (values as any).rate_min || undefined,
    rate_max: (values as any).rate_max || undefined,
    is_saved: values.is_saved,
    page: Number(values.page) || 1,
    first: 20,
    ...(values?.order_by_column && {
      order_by: [
        {
          column: values.order_by_column as JobOrderByColumn,
          order: SortOrder.Desc,
        },
      ],
    }),
  };
};

export const mapQueryToInitialValues = (
  query: Record<string, any>,
): Partial<FilterForm> => {
  return {
    categories: mapOptionsFromQuery(query?.categories),
    skills: mapOptionsFromQuery(query?.skills),
    search: query.search,
    rate_max: query.rate_max,
    rate_min: query.rate_min,
    order_by_column: query.order_by_column
      ? query.order_by_column
      : JobOrderByColumn.CampaignStart,
    is_saved: query.is_saved,
    page: query.page,
  };
};
