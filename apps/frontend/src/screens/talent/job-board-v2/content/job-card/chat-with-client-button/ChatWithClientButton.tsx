import { ChatTypes } from 'components/chat';
import React from 'react';
import { pathManager } from 'routes';

import { Job } from '@libs/graphql-types';
import { RouterButton } from '@libs/ui/components/button';

interface ChatWithClientButtonProps {
  job: Job;
  disabled: boolean;
}

const ChatWithClientButton = ({ job, disabled }: ChatWithClientButtonProps) => {
  return (
    <RouterButton
      disabled={disabled}
      to={{
        pathname: pathManager.chat.generatePath(),
        state: {
          strategy: {
            type: ChatTypes.UserToTalent,
            data: job.campaign_owner!,
          },
        },
      }}
      size="small"
      variant="outlined"
    >
      Contact recruiter
    </RouterButton>
  );
};

export default ChatWithClientButton;
