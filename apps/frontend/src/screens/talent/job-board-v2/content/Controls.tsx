import LibraryBooksIcon from '@mui/icons-material/LibraryBooks';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import TuneIcon from '@mui/icons-material/Tune';
import { Box, Button, Grid, Hidden } from '@mui/material';
import { JOB_ORDER_BY_COLUMN_OPTIONS } from 'consts/common';
import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { pathManager } from 'routes';
import { JobMarkType } from 'types';

import { ConnectedSelect } from '@libs/ui/components/form/select';

interface ControlsProps {
  openDrawer: VoidFunction;
}

const Controls = ({ openDrawer }: ControlsProps) => {
  const location = useLocation();
  const { isMD, isSM } = useMediaQueries();

  return (
    <Box pb={4}>
      <Grid
        container
        wrap={isMD ? 'wrap' : 'nowrap'}
        alignItems="center"
        spacing={2}
        justifyContent="space-between"
      >
        <Grid
          style={{
            flexGrow: 1,
            width: isMD ? '100%' : 'auto',
            order: isMD ? 1 : 0,
          }}
          item
        >
          <ConnectedSelect
            name="order_by_column"
            margin="dense"
            variant="filled"
            label="Sort by"
            hideNoneValue
            // fullWidth
            options={JOB_ORDER_BY_COLUMN_OPTIONS}
          />
        </Grid>
        <Hidden lgUp>
          <Grid style={{ flexGrow: 1, width: isSM ? '100%' : 'inherit' }} item>
            <Button onClick={openDrawer} endIcon={<TuneIcon />}>
              Search Filter
            </Button>
          </Grid>
        </Hidden>
        <Grid item>
          <Link
            to={{
              state: { jobBoardSearch: location.search },
              pathname: pathManager.talent.markedJobs.generatePath({
                type: JobMarkType.Applied,
              }),
            }}
          >
            <Button endIcon={<MailOutlineIcon />}>My applications</Button>
          </Link>
        </Grid>
        <Grid item>
          <Link
            to={{
              state: { jobBoardSearch: location.search },
              pathname: pathManager.talent.markedJobs.generatePath({
                type: JobMarkType.Saved,
              }),
            }}
          >
            <Button endIcon={<LibraryBooksIcon />}>Saved jobs</Button>
          </Link>
        </Grid>
      </Grid>
    </Box>
  );
};

export default Controls;
