import { Pagination } from '@mui/lab';
import { Box } from '@mui/material';
import { useCurrentTime } from 'hooks/common/useCurrentTime';
import React from 'react';

import { Job } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import ApplyModal from '../apply-modal';
import { useJobList, useJobModals, usePaginationChange } from '../hooks';
import InviteModal from '../invite-modal';
import Controls from './Controls';
import JobCard from './job-card';

interface ContentProps {
  openDrawer: VoidFunction;
}

const Content = ({ openDrawer }: ContentProps) => {
  const currentTime = useCurrentTime();
  const onPaginationChange = usePaginationChange();
  const { jobs, loadingJobList, currentPage, lastPage, loadJobs } =
    useJobList();
  const { inviteJob, applyJob, handleClose, onJobApply, onInvite } =
    useJobModals();

  return (
    <div>
      <Controls openDrawer={openDrawer} />
      <div>
        {loadingJobList && (
          <Typography variant="body1">
            Loading results, please wait...
          </Typography>
        )}
        {!jobs.length && !loadingJobList && (
          <Typography variant="body1">
            No jobs matching your criteria. Try to change search options.
          </Typography>
        )}
        {jobs.map((job, index) => (
          <JobCard
            // eslint-disable-next-line react/no-array-index-key
            key={`${job?.posted_at}-${job?.name}-${index}`}
            onInvite={onInvite}
            onJobApply={onJobApply}
            job={job as Job}
            currentTime={currentTime}
            loadJobs={loadJobs}
          />
        ))}
      </div>
      {!loadingJobList && (
        <Box display="flex" justifyContent="center" mt={4}>
          <Pagination
            page={currentPage}
            showFirstButton
            showLastButton
            count={lastPage}
            variant="outlined"
            shape="rounded"
            onChange={onPaginationChange}
          />
        </Box>
      )}
      {inviteJob && <InviteModal handleClose={handleClose} job={inviteJob} />}
      {applyJob && <ApplyModal handleClose={handleClose} job={applyJob} />}
    </div>
  );
};

export default React.memo(Content);
