import { gql } from '@apollo/client';
import JOB_FRAGMENT from 'graphql/fragments/talent/jobBoardJobFragment';

export const GET_JOBS = gql`
  ${JOB_FRAGMENT}
  query GetJobs(
    $skills: [ID]
    $categories: [ID]
    $rate_min: Float
    $rate_max: Float
    $search: String
    $order_by: [QueryCurrentTalentJobBoardSearchOrderByOrderByClause!]
    $is_saved: Boolean
    $first: Int = 20
    $page: Int
  ) {
    currentTalentJobBoardSearch(
      skills: $skills
      categories: $categories
      rate_min: $rate_min
      rate_max: $rate_max
      search: $search
      order_by: $order_by
      is_saved: $is_saved
      first: $first
      page: $page
    ) {
      paginatorInfo {
        count
        currentPage
        firstItem
        hasMorePages
        lastItem
        lastPage
        perPage
        total
      }
      data {
        ...JobBoardJobFragment
      }
    }
  }
`;

export const SAVE_JOB = gql`
  mutation SaveJobToFavorites($job_id: ID!) {
    saveJobToFavorites(job_id: $job_id)
  }
`;

export const DELETE_JOB = gql`
  mutation DeleteJobFromFavorites($job_id: ID!) {
    deleteJobFromFavorites(job_id: $job_id)
  }
`;

export const APPLY_FOR_JOB = gql`
  mutation ApplyForJob($job_id: ID!, $rate: Float, $pitch: String) {
    applyForJob(job_id: $job_id, rate: $rate, pitch: $pitch)
  }
`;

export const WITHDRAW_JOB = gql`
  mutation CancelApplicationForJob($job_id: ID!, $reason: String) {
    cancelApplicationForJob(job_id: $job_id, reason: $reason)
  }
`;
