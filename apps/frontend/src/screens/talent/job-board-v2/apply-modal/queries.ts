import { gql } from '@apollo/client';

export const APPLY_FOR_JOB = gql`
  mutation ApplyForJob($job_id: ID!, $rate: Float, $pitch: String) {
    applyForJob(job_id: $job_id, rate: $rate, pitch: $pitch)
  }
`;
