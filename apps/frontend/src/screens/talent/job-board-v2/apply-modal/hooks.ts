import { useMutation } from '@apollo/client';
import { useCallback, useState } from 'react';

import { Mutation, MutationApplyForJobArgs } from '@libs/graphql-types';

import { APPLY_FOR_JOB } from './queries';

export const useApplyForAction = (job_id: string) => {
  const [isSubmitted, setIsSubmitted] = useState<boolean>(false);
  const [applyForJobRequest, { loading }] = useMutation<
    Mutation,
    MutationApplyForJobArgs
  >(APPLY_FOR_JOB, {
    variables: { job_id },
    onCompleted: (res) => {
      if (res.applyForJob) {
        setIsSubmitted(true);
      }
    },
  });

  const applyForJob = useCallback(() => {
    applyForJobRequest();
  }, [applyForJobRequest]);

  return { applyForJob, isApplying: loading, isSubmitted };
};
