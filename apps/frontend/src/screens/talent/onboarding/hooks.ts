import { LOCAL_STORAGE_KEYS } from 'consts/ls-keys';
import { useSearchParams } from 'hooks/routing';
import { useEffect, useState } from 'react';
import { mayBeAddDeviceId } from 'utils/common/mayBeAddDeviceId';

export const useTmpVarsFromQuery = () => {
  const { email, companyId, appliedJobId } = useSearchParams();

  const [finalEmail, setFinalEmail] = useState(
    localStorage.getItem(LOCAL_STORAGE_KEYS.talentOnboardingEmail) ||
      email ||
      '',
  );
  const [finalAppliedJobId, setAppliedJobId] = useState(
    localStorage.getItem(LOCAL_STORAGE_KEYS.talentOnboardingAppliedJobId) ||
      appliedJobId ||
      '',
  );

  useEffect(() => {
    if (companyId && typeof companyId === 'string') {
      localStorage.setItem(
        LOCAL_STORAGE_KEYS.talentOnboardingCompanyId,
        companyId,
      );
    }
    if (email && typeof email === 'string') {
      localStorage.setItem(LOCAL_STORAGE_KEYS.talentOnboardingEmail, email);
      setFinalEmail(email);
    }

    if (!!appliedJobId) {
      localStorage.setItem(
        LOCAL_STORAGE_KEYS.talentOnboardingAppliedJobId,
        appliedJobId as string,
      );
      setAppliedJobId(appliedJobId);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return {
    email: finalEmail,
    appliedJobId: finalAppliedJobId,
  };
};

export const useDeviceId = () => {
  useEffect(() => {
    mayBeAddDeviceId(LOCAL_STORAGE_KEYS.talentOnboardingDeviceId);
  }, []);
};
