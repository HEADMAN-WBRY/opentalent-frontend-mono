import { createContext } from 'react';

const TalentNotExistYetContext = createContext(false);

export default TalentNotExistYetContext;
