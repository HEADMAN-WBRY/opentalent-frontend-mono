import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';
import React from 'react';

import Typography from '@libs/ui/components/typography';

import { ReactComponent as EarnImg } from './assets/earn-step.svg';

const useStyles = makeStyles((theme) => ({
  title: {
    fontSize: '28px',
    lineHeight: '44px',
    fontStyle: 'italic',
    fontWeight: 600,
  },
  image: {
    margin: '0 auto',
    transform: 'translateX(-6%)',

    [theme.breakpoints.down('md')]: {
      width: '100%',
    },
  },
}));

const IntroStep4 = () => {
  const classes = useStyles();

  return (
    <Box mt={4}>
      <Box pb={4}>
        <Typography variant="h4" className={classes.title}>
          Invite talent and earn money.
        </Typography>
      </Box>
      <Box>
        <EarnImg className={classes.image} />
      </Box>
    </Box>
  );
};

export default IntroStep4;
