import cn from 'classnames';
import FixedFooter from 'components/custom/onboarding/fixed-footer';
import React from 'react';
import { Link, Redirect, RouteComponentProps } from 'react-router-dom';
import { pathManager } from 'routes/consts';

import { Box, Grid, Hidden } from '@mui/material';
import Grow from '@mui/material/Grow';
import { makeStyles } from '@mui/styles';

import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import IntroStep1 from './IntroStep1';
import IntroStep2 from './IntroStep2';
import IntroStep3 from './IntroStep3';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    maxWidth: 730,
    position: 'relative',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    textAlign: 'center',
  },
  button: {
    maxWidth: '330px',
    width: '100%',

    [theme.breakpoints.down('md')]: {
      maxWidth: '100%',
    },
  },
  stepper: {
    height: 32,
    margin: '16px 0 24px',
    width: '100%',

    '& i': {
      position: 'relative',
      display: 'block',
      border: `2px solid ${theme.palette.tertiary.main}`,
      width: 12,
      height: 12,
      borderRadius: '100%',

      '&::before': {
        position: 'absolute',
        content: "''",
        background: theme.palette.tertiary.main,
        top: '50%',
        left: '50%',
        width: 0,
        height: 0,
        display: 'block',
        transform: 'translate(-50%, -50%)',
        transition: 'all .3s',
        borderRadius: '100%',
      },

      '&$activeStep': {
        '&::before': {
          width: 18,
          height: 18,
        },
      },
    },
  },
  activeStep: {},
}));

const CONTENT_MAP = [
  {
    Content: IntroStep1,
    nextLink: pathManager.talent.onboarding.intro.generatePath({ step: 2 }),
    step: 1,
  },
  {
    Content: IntroStep2,
    nextLink: pathManager.talent.onboarding.intro.generatePath({ step: 3 }),
    step: 2,
  },
  {
    Content: IntroStep3,
    nextLink: pathManager.talent.onboarding.creating.generatePath({ step: 1 }),
    step: 3,
  },
];

const Intro = ({ match }: RouteComponentProps<{ step: string }>) => {
  const step = Number(match?.params?.step);
  const classes = useStyles({ step });
  const isExistedStep = step > 0 && step <= CONTENT_MAP.length;

  if (!isExistedStep) {
    return (
      <Redirect
        to={pathManager.talent.onboarding.intro.generatePath({ step: 1 })}
      />
    );
  }

  const { Content, nextLink } = CONTENT_MAP[step - 1];
  const linkButton = (
    <Link to={nextLink}>
      <Button variant="contained" color="primary" className={classes.button}>
        next
      </Button>
    </Link>
  );

  return (
    <>
      <Box className={classes.wrapper}>
        <Typography align="center" variant="overline">
          What to expect from OpenTalent 👇
        </Typography>
        <Grow timeout={500} key={step} in>
          <div>
            <Content />
          </div>
        </Grow>
        <Grid
          alignItems="center"
          justifyContent="center"
          spacing={4}
          className={classes.stepper}
          container
        >
          {CONTENT_MAP.map(({ step: itemStep }) => (
            <Grid key={itemStep} item>
              <Link
                to={pathManager.talent.onboarding.intro.generatePath({
                  step: itemStep,
                })}
              >
                <i
                  className={cn({ [classes.activeStep]: itemStep === step })}
                />
              </Link>
            </Grid>
          ))}
        </Grid>

        <Hidden mdDown>
          <Box pt={4}>{linkButton}</Box>
        </Hidden>
      </Box>
      <Hidden mdUp>
        <FixedFooter>
          <Box width="100%" px={4}>
            {linkButton}
          </Box>
        </FixedFooter>
      </Hidden>
    </>
  );
};

export default Intro;
