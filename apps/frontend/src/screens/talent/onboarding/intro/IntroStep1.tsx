import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';
import Lottie from 'lottie-react';
import React from 'react';

import Typography from '@libs/ui/components/typography';

import * as animationData from './assets/lottie-matching.json';

const useStyles = makeStyles((theme) => ({
  title: {
    fontSize: '28px',
    lineHeight: '44px',
    fontStyle: 'italic',
  },
  image: {
    width: 601,
    margin: '0 auto',

    [theme.breakpoints.down('md')]: {
      width: '100%',
    },
  },
}));

const IntroStep1 = () => {
  const classes = useStyles();

  return (
    <div>
      <Box mt={4}>
        <Box pb={6}>
          <Typography variant="h4" fontWeight={600} className={classes.title}>
            Only relevant roles, based on your skills.
          </Typography>
        </Box>
        <div className={classes.image}>
          <Lottie loop animationData={(animationData as any).default} />
        </div>
      </Box>
    </div>
  );
};

export default IntroStep1;
