import { useQuery } from '@apollo/client';
import { Box, CircularProgress, Grow } from '@mui/material';
import { makeStyles } from '@mui/styles';
import React from 'react';
import { RouteComponentProps } from 'react-router-dom';

import { Query } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import FormRow from './FormRow';
import { GET_SCREEN_DATA } from './queries';

interface FormProps extends Pick<RouteComponentProps, 'history' | 'location'> {}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    maxWidth: 864,
    textAlign: 'center',

    [theme.breakpoints.down('sm')]: {
      'body &': {
        maxWidth: 'calc(100% - 32px)',
        position: 'static',
        transform: 'none',
      },
    },
  },
  title: {
    lineHeight: '20px',
    color: theme.palette.grey[500],
    marginBottom: theme.spacing(12),
  },
}));

const MAX_LINKS_COUNT = 5;

const Form: React.FC<FormProps> = ({ location, history }) => {
  const classes = useStyles();
  const { data, loading } = useQuery<Query>(GET_SCREEN_DATA);
  const links = data?.currentTalentInvitationLinks || [];

  return (
    <Box className={classes.wrapper}>
      <Grow unmountOnExit in={loading}>
        <CircularProgress color="secondary" />
      </Grow>
      <Grow timeout={500} in={!loading}>
        <div>
          <Typography align="center" variant="h5">
            So what’s next?
          </Typography>
          <Box pt={4} pb={2}>
            <Typography className={classes.title} variant="subtitle2">
              You can now invite up to 5 people in your network to OpenTalent.
              <br />
              If they get hired, you’ll earn 50% of their commissions.
            </Typography>
          </Box>

          {links.map((invitationLink, index) => (
            <FormRow
              key={invitationLink?.id}
              invitationLink={invitationLink}
              index={index}
            />
          ))}
          {links.length < MAX_LINKS_COUNT && (
            <FormRow key="new" index={links.length} />
          )}
        </div>
      </Grow>
    </Box>
  );
};

export default Form;
