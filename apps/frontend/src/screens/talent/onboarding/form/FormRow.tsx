import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import PermIdentityIcon from '@mui/icons-material/PermIdentity';
import ShareIcon from '@mui/icons-material/Share';
import WhatsAppIcon from '@mui/icons-material/WhatsApp';
import { Box, Button, Grid, Grow, InputAdornment } from '@mui/material';
import { makeStyles } from '@mui/styles';
import cn from 'classnames';
import { Formik } from 'formik';
import useMediaQueries from 'hooks/common/useMediaQueries';
import qs from 'querystring';
import React, { useMemo } from 'react';
import { useHistory } from 'react-router-dom';
import * as yup from 'yup';

import { InvitationLink } from '@libs/graphql-types';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import Typography from '@libs/ui/components/typography';

import { useCopyToClipboardAction, useOnCreateHandler } from './hooks';

interface FormRowProps {
  index: number;
  invitationLink?: InvitationLink;
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: 868,
    position: 'relative',
    margin: '0 auto',
    padding: '0 56px',

    [theme.breakpoints.down('md')]: {
      'body &': {
        width: '100%',
        padding: 0,
      },
    },

    '& button span': {
      letterSpacing: 0.3,
    },
  },
  index: {
    position: 'absolute',
    left: 0,
    top: '50%',
    transform: 'translateY(-50%)',
    color: theme.palette.primary.main,

    [theme.breakpoints.down('md')]: {
      'body &': {
        top: 0,
        transform: 'translateY(8px)',
        position: 'relative',
      },
    },

    '& span': {
      width: 32,
      height: 32,
      lineHeight: '30px',
      border: `2px solid ${theme.palette.primary.main}`,
      display: 'block',
      borderRadius: '100%',
    },
  },
  userIcon: {
    color: theme.palette.text.secondary,
  },
  checkIcon: {
    color: 'transparent',
  },
  checkIconSuccess: {
    color: theme.palette.success.main,
  },
  input: {
    '& > div': {
      paddingLeft: theme.spacing(2),
      height: 46,
    },
  },
  buttons: {
    width: 364,
  },
  goBtn: {
    color: theme.palette.secondary.contrastText,
    borderColor: theme.palette.grey[500],

    '&:hover': {
      border: `1px solid ${theme.palette.secondary.contrastText}`,
    },
  },
}));

const validation = yup.object().shape({
  name: yup.string().trim().nullable().required(),
});

const FormRow = ({ index, invitationLink }: FormRowProps) => {
  const { onCreate, loading } = useOnCreateHandler();
  const classes = useStyles();
  const { isXS, isSM } = useMediaQueries();
  const copyLink = useCopyToClipboardAction();
  const history = useHistory();
  const correctLink = useMemo(() => {
    const serverLink = invitationLink?.url;

    if (!serverLink) {
      return '';
    }

    const link = new URL(invitationLink?.url || '');
    return `${window.location.origin}${link.pathname}`;
  }, [invitationLink?.url]);
  const sendLink = () =>
    history.push({
      search: qs.stringify({
        link: invitationLink?.id,
        name: invitationLink?.name,
      }),
    });

  return (
    <Formik
      validationSchema={validation}
      onSubmit={(values, helpers) => {
        onCreate(values);
        helpers.resetForm();
      }}
      initialValues={{ name: invitationLink?.name || '' }}
    >
      {({ values, handleSubmit }) => (
        <Grow timeout={(1000 * index) / 2} in unmountOnExit appear>
          <Grid
            spacing={6}
            className={classes.root}
            container
            wrap={isSM ? 'wrap' : 'nowrap'}
          >
            <Grid className={classes.index} item>
              <Typography component="span">{index + 1}</Typography>
            </Grid>
            <Grid component={Box} flexGrow={4} item>
              <ConnectedTextField
                size="medium"
                className={classes.input}
                variant="standard"
                fullWidth
                disabled={!!invitationLink || loading}
                name="name"
                label={null}
                placeholder="Enter First Name (e.g. John)"
                InputProps={{
                  startAdornment: (
                    <InputAdornment
                      className={classes.userIcon}
                      position="start"
                    >
                      <PermIdentityIcon color="inherit" />
                    </InputAdornment>
                  ),
                  endAdornment: (
                    <InputAdornment
                      className={cn(classes.checkIcon, {
                        [classes.checkIconSuccess]: !!invitationLink,
                      })}
                      position="start"
                    >
                      <CheckCircleOutlineIcon color="inherit" />
                    </InputAdornment>
                  ),
                }}
              />
            </Grid>
            <Grid component={Box} className={classes.buttons} flexGrow={1} item>
              {invitationLink ? (
                <Grid wrap={isXS ? 'wrap' : 'nowrap'} spacing={4} container>
                  <Grid component={Box} flexGrow={2} item>
                    <Button
                      size="large"
                      color="secondary"
                      variant="outlined"
                      fullWidth
                      onClick={() =>
                        copyLink({
                          name: values.name,
                          link: correctLink,
                        })
                      }
                      endIcon={<WhatsAppIcon />}
                      className={classes.goBtn}
                    >
                      COPY LINK
                    </Button>
                  </Grid>
                  <Grid component={Box} flexGrow={1} item>
                    <Button
                      size="large"
                      color="secondary"
                      variant="outlined"
                      fullWidth
                      endIcon={<MailOutlineIcon />}
                      onClick={sendLink}
                      className={classes.goBtn}
                    >
                      SEND BY EMAIL
                    </Button>
                  </Grid>
                </Grid>
              ) : (
                <Button
                  size="large"
                  color="primary"
                  variant="contained"
                  fullWidth
                  onClick={() => handleSubmit()}
                  disabled={loading}
                  endIcon={<ShareIcon />}
                >
                  generate PERSONAL Invite
                </Button>
              )}
            </Grid>
          </Grid>
        </Grow>
      )}
    </Formik>
  );
};

export default FormRow;
