import { Step, StepLabel, Stepper } from '@mui/material';
import { makeStyles } from '@mui/styles';
import cn from 'classnames';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { pathManager } from 'routes';

import { STEPS_DATA } from '../consts';

interface StepperProps {
  current: number;
}

export const useStyles = makeStyles((theme) => ({
  stepper: {
    background: 'none',
    padding: 0,
    marginBottom: theme.spacing(18),

    '& .MuiStepConnector-root': {
      [theme.breakpoints.down('sm')]: {
        visibility: 'hidden',
      },
    },
  },
  root: {
    [theme.breakpoints.down('sm')]: {
      padding: 0,
    },

    '&:last-child .MuiStepLabel-iconContainer': {
      padding: 0,
    },

    '& span > svg': {
      '& > circle': {
        color: theme.palette.grey[700],
      },
      '& > text': {
        color: theme.palette.primary.contrastText,
      },
    },
  },
  current: {
    '& span > svg': {
      fontSize: 48,
      '& > circle': {
        color: theme.palette.primary.main,
      },
    },
  },
  completed: {
    '& svg': {
      color: `${theme.palette.grey[500]} !important`,
    },
  },
  clickable: {
    cursor: 'pointer',
  },
}));

const TopStepper = ({ current }: StepperProps) => {
  const history = useHistory();
  const classes = useStyles();

  return (
    <Stepper activeStep={current - 1} className={classes.stepper}>
      {STEPS_DATA.map(({ step }) => {
        const isClickable = current > step;
        const isCurrent = step === current;

        return (
          <Step
            key={step}
            onClick={() => {
              if (!isClickable) {
                return;
              }
              history.push(
                pathManager.talent.onboarding.creating.generatePath({
                  step,
                }),
              );
            }}
            classes={{
              root: cn(classes.root, {
                [classes.current]: isCurrent,
                [classes.clickable]: isClickable,
                [classes.completed]: isClickable,
              }),
            }}
          >
            <StepLabel />
          </Step>
        );
      })}
    </Stepper>
  );
};

export default TopStepper;
