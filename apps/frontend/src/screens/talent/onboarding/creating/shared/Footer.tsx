import FixedFooter from 'components/custom/onboarding/fixed-footer';
import React from 'react';
import { Link } from 'react-router-dom';

import ArrowBackIcon from '@mui/icons-material/ArrowBack';

import Button from '@libs/ui/components/button';

import { useSaveAction } from '../saveStepHooks';
import NextButton from './NextButton';

interface FooterProps {
  currentStep: number;
  backLink?: string;
  nextLink: string;
  disabled: boolean;
}

const Footer = ({ currentStep, backLink, nextLink, disabled }: FooterProps) => {
  const { isSaving, save } = useSaveAction();

  return (
    <FixedFooter>
      {backLink && (
        <Link style={{ width: '100%', marginRight: 24 }} to={backLink}>
          <Button
            variant="outlined"
            color="primary"
            fullWidth
            startIcon={<ArrowBackIcon />}
          >
            Back
          </Button>
        </Link>
      )}
      <NextButton
        currentStep={currentStep}
        nextLink={nextLink}
        disabled={disabled || isSaving}
        onClick={save}
      />
    </FixedFooter>
  );
};

export default Footer;
