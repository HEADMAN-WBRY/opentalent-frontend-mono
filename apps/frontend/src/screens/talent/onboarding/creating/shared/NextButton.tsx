import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import SaveIcon from '@mui/icons-material/Save';
import { Button } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { useFormikContext } from 'formik';
import React from 'react';
import { useHistory } from 'react-router-dom';

import { CreatingFormState } from '../../../shared/profile/types';
import { STEPS_DATA } from '../consts';

interface IProps {
  currentStep: number;
  nextLink: string;
  disabled: boolean;
  onClick: () => Promise<any>;
}

const useStyles = makeStyles((theme) => ({
  disabled: {
    pointerEvents: 'none',
  },

  link: {
    width: '100%',
  },

  button: {
    width: '100%',
    background: `${theme.palette.primary.main} !important`,
  },
}));

const NextButton: React.FC<IProps> = ({
  currentStep,
  nextLink,
  disabled,
  onClick,
}) => {
  const classes = useStyles();
  const history = useHistory();
  const isLast = currentStep === STEPS_DATA.length;
  const { submitForm } = useFormikContext<CreatingFormState>();
  const goToNext = async () =>
    onClick().finally(() => {
      history.push(nextLink);
    });

  if (isLast) {
    return (
      <Button
        variant="contained"
        data-test-id="saveButton"
        color="primary"
        endIcon={<SaveIcon />}
        onClick={submitForm}
        className={classes.button}
        disabled={disabled}
        fullWidth
      >
        Save profile
      </Button>
    );
  }

  return (
    <Button
      data-test-id="nextButton"
      variant="contained"
      color="primary"
      endIcon={<ArrowForwardIcon />}
      className={classes.button}
      disabled={disabled}
      fullWidth
      onClick={goToNext}
    >
      Next
    </Button>
  );
};

export default NextButton;
