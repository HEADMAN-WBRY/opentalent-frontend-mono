import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme) => ({
  wrapper: {
    maxWidth: 928,
    width: '100%',
    margin: '56px auto 0 auto',
  },
  stepper: {
    background: 'none',
    padding: 0,
    marginBottom: theme.spacing(18),
  },
  title: {
    marginBottom: theme.spacing(2),
  },
  subTitle: {
    marginBottom: theme.spacing(8),
    color: theme.palette.grey[500],
  },
  bottom: {
    height: 74,
    backgroundColor: theme.palette.other.black,
    position: 'fixed',
    bottom: 0,
    left: 0,
    right: 0,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 2,
  },
  buttonBack: {
    width: 232,
    marginRight: theme.spacing(8),

    [theme.breakpoints.down('md')]: {
      width: 'auto',
    },
  },
}));
