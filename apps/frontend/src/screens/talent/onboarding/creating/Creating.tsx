import { Formik, getIn } from 'formik';
import React from 'react';
import { Redirect, useParams } from 'react-router-dom';
import { pathManager } from 'routes';

import { Box, CircularProgress, Grow } from '@mui/material';

import Typography from '@libs/ui/components/typography';

import { CreatingFormState } from '../../shared/profile/types';
import { STEPS_DATA } from './consts';
import { useInitialState, useSubmitAction } from './hooks';
import CheckStepErrors from './shared/CheckStepErrors';
import Footer from './shared/Footer';
import TopStepper from './shared/TopStepper';
import { useStyles } from './styles';
import { validator } from './validator';

interface CreatingProps {}

const Creating = (props: CreatingProps) => {
  const classes = useStyles();
  const { step } = useParams<{ step: string }>();
  const currentStep = step ? Number(step) : 1;
  const stepContent = STEPS_DATA[currentStep - 1];
  const initialState = useInitialState();
  const { loading, onSubmit } = useSubmitAction();

  if (!stepContent) {
    return (
      <Redirect
        to={pathManager.talent.onboarding.creating.generatePath({ step: 1 })}
      />
    );
  }

  if (!initialState) {
    return (
      <Box justifyContent="center" display="flex">
        <CircularProgress />
      </Box>
    );
  }

  const { title, subTitle, Content, nextLink, backLink, statePath } =
    stepContent;

  return (
    <Formik<CreatingFormState>
      initialValues={initialState}
      onSubmit={onSubmit}
      validationSchema={validator}
      validateOnChange
      validateOnMount
    >
      {({ errors, values }) => {
        const err = getIn(errors, statePath);
        return (
          <Box className={classes.wrapper} mt={14}>
            <CheckStepErrors errors={errors} currentStep={currentStep} />
            <Box data-test-id="creatingScreen">
              <Grow timeout={500} key={step} in>
                <div>
                  <Typography
                    variant="h5"
                    fontWeight={500}
                    className={classes.title}
                  >
                    {title}
                  </Typography>
                  <Typography variant="subtitle1" className={classes.subTitle}>
                    {subTitle}
                  </Typography>
                  <TopStepper current={currentStep} />

                  <Content />
                </div>
              </Grow>
              <Footer
                currentStep={currentStep}
                nextLink={nextLink}
                backLink={backLink}
                disabled={err || loading}
              />
            </Box>
          </Box>
        );
      }}
    </Formik>
  );
};

export default Creating;
