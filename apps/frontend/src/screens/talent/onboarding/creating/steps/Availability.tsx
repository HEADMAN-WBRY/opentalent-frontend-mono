import { MAX_DATE } from 'consts/date';
import { useFormikContext } from 'formik';
import React from 'react';
import { AvailabilityType } from 'screens/talent/edit-profile/types';

import { Box, Grid } from '@mui/material';
import Collapse from '@mui/material/Collapse';
import { makeStyles } from '@mui/styles';

import { modelPath } from '@libs/helpers/form';
import { ConnectedDatePicker } from '@libs/ui/components/form/datepicker';
import { ConnectedRadioGroup } from '@libs/ui/components/form/radio/Radio';
import { OptionType } from '@libs/ui/components/form/select';
import Typography from '@libs/ui/components/typography';

import { CreatingFormState } from '../../../shared/profile/types';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    maxWidth: 250,
    margin: '0 auto',
  },
  root: {
    '& > label > span': {
      color: `${theme.palette.secondary.contrastText} !important`,
    },
  },
}));

const AVAILABILITY_OPTIONS = [
  { value: AvailabilityType.Now, text: 'Now' },
  { value: AvailabilityType.Later, text: 'Later' },
];

const HOURS_OPTIONS: OptionType[] = Array.from({ length: 4 }).map((_, i) => {
  const value = String(50 - (i + 1) * 10);
  return {
    value,
    text: `${value} hrs/week`,
  };
});

const Availability: React.FC = () => {
  const { values } = useFormikContext<CreatingFormState>();
  const classes = useStyles();
  const availableNow = values.availability?.availableNow;

  return (
    <Box className={classes.wrapper}>
      <Typography variant="body2">When will you be available?</Typography>
      <Box pt={4}>
        <Grid spacing={4} container direction="column" wrap="nowrap">
          <Grid item>
            <ConnectedRadioGroup
              data-test-id="available_now"
              options={AVAILABILITY_OPTIONS}
              classes={{ root: classes.root }}
              name={modelPath<CreatingFormState>(
                (m) => m.availability.availableNow,
              )}
            />
          </Grid>
          <Collapse in={availableNow === AvailabilityType.Later}>
            <Box padding={2}>
              <ConnectedDatePicker
                inputFormat="dd/MM/yyyy"
                name={modelPath<CreatingFormState>(
                  (m) => m.availability.availableDate,
                )}
                minDate={new Date()}
                maxDate={MAX_DATE}
                TextFieldProps={{
                  variant: 'filled',
                  size: 'small',
                  fullWidth: true,
                  label: 'Available date',
                }}
              />
            </Box>
          </Collapse>
          <Collapse in={availableNow !== undefined}>
            <Box padding={2}>
              <ConnectedRadioGroup
                name={modelPath<CreatingFormState>(
                  (m) => m.availability.hoursPerWeek,
                )}
                options={HOURS_OPTIONS}
                classes={{ root: classes.root }}
              />
            </Box>
          </Collapse>
        </Grid>
      </Box>
    </Box>
  );
};

export default Availability;
