import { INPUT_SUGGESTION_LENGTH } from 'consts/skills';
import React from 'react';
import { useAddSkill } from 'screens/talent/edit-profile/history-section/companies/hooks';
import { MAX_COMPANIES_COUNT } from 'screens/talent/shared/profile/consts';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { modelPath } from '@libs/helpers/form';
import { ConnectedMultipleSelect } from '@libs/ui/components/form/multiple-select';

import { CreatingFormState } from '../../../shared/profile/types';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    maxWidth: 500,
    margin: '0 auto',
  },
}));

const Companies: React.FC = () => {
  const classes = useStyles();
  const path = modelPath<CreatingFormState>((m) => m.companies.companies);
  const {
    onInputChange,
    skillsSuggest,
    onSelectSkill,
    inputValue = '',
  } = useAddSkill(path);
  const showNoOptionsText =
    inputValue.length > INPUT_SUGGESTION_LENGTH && !skillsSuggest?.length;

  return (
    <Box className={classes.wrapper}>
      <Grid item>
        <ConnectedMultipleSelect
          options={skillsSuggest}
          name={path}
          chipProps={{
            color: 'tertiary',
            size: 'small',
          }}
          noOptionsText={
            showNoOptionsText
              ? `press enter to create ${inputValue}`
              : 'e.g. ABN, Microsoft, Google'
          }
          autoCompleteProps={{
            onChange: onSelectSkill,
            popupIcon: null,
          }}
          inputProps={{
            variant: 'filled',
            label: `Select up to ${MAX_COMPANIES_COUNT} companies`,
            onChange: onInputChange,
            value: inputValue,
          }}
        />
      </Grid>
    </Box>
  );
};

export default Companies;
