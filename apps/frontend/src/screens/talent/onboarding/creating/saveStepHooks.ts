import { useMutation } from '@apollo/client';
import { useFormikContext } from 'formik';
import { UPDATE_TALENT_PROFILE } from 'graphql/talents';
import { SET_TMP_DATA } from 'graphql/tmp-data';
import { useCurrentUser } from 'hooks/auth';
import { useCallback } from 'react';
import { mapFormToTalentUpdate } from 'screens/talent/shared/profile/mappers';
import { CreatingFormState } from 'screens/talent/shared/profile/types';

import {
  Mutation,
  MutationSetTempDataItemArgs,
  MutationUpdateTalentProfileArgs,
} from '@libs/graphql-types';

import { getTmpDataArgs } from '../utils';

const useSaveUnknownTalent = () => {
  const { values } = useFormikContext<CreatingFormState>();
  const [saveUnknownTalent, { loading }] = useMutation<
    Mutation,
    MutationSetTempDataItemArgs
  >(SET_TMP_DATA);

  const save = useCallback(async () => {
    return saveUnknownTalent({
      variables: {
        ...getTmpDataArgs(),
        data: JSON.stringify(values),
      },
    });
  }, [saveUnknownTalent, values]);

  return { save, isSaving: loading };
};

const useSaveTalentData = () => {
  const { values } = useFormikContext<CreatingFormState>();
  const [submitFrom, { loading }] = useMutation<
    Mutation,
    MutationUpdateTalentProfileArgs
  >(UPDATE_TALENT_PROFILE);
  const save = useCallback(async () => {
    return submitFrom({ variables: mapFormToTalentUpdate(values) });
  }, [submitFrom, values]);

  return { save, isSaving: loading };
};

export const useSaveAction = () => {
  const { isAuthorized } = useCurrentUser();
  const currentSubmitHook = isAuthorized
    ? useSaveTalentData
    : useSaveUnknownTalent;
  const { isSaving, save } = currentSubmitHook();

  return { isSaving, save };
};
