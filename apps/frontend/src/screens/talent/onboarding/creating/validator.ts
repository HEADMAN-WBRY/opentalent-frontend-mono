import { MAX_DATE } from 'consts/date';
import { SKILLS_LIMITS, SOLUTIONS_WITH_HARD } from 'consts/skills';
import errors from 'consts/validationErrors';
import { AvailabilityType } from 'screens/talent/edit-profile/types';
import {
  MAX_COMPANIES_COUNT,
  MAX_DESCRIBE_CHARS,
} from 'screens/talent/shared/profile/consts';
import * as yup from 'yup';

import { SkillTypeEnum } from '@libs/graphql-types';
import {
  maxStringValidator,
  string64Validator,
  varCharStringValidator,
} from '@libs/helpers/yup';

export const validator = yup.object().shape({
  profile: yup.object().shape({
    firstName: varCharStringValidator.required(),
    lastName: varCharStringValidator.required(),
    position: maxStringValidator.required(),
    location: varCharStringValidator.required(),
    category: yup.string().required(),
    linkedLink: maxStringValidator.customUrl(),
    subcategories: yup.array().max(3).min(1),
    phone: string64Validator.phone(),
  }),
  cv: yup.object().shape({
    documents: yup.array().of(yup.object()),
  }),
  availability: yup.object().shape({
    availableNow: yup.string().required(),
    availableDate: yup
      .date()
      .nullable()
      .when('availableNow', {
        is: (availableNow: AvailabilityType) =>
          availableNow === AvailabilityType.Later,
        then: yup
          .date()
          .typeError(errors.invalidDate)
          .nullable()
          .min(new Date())
          .required()
          .max(MAX_DATE),
        otherwise: yup.date().nullable(),
      }),
    hoursPerWeek: yup.number().required(),
  }),
  rate: yup.object().shape({
    min: yup.string().required(),
  }),
  companies: yup.object().shape({
    companies: yup
      .array()
      .max(MAX_COMPANIES_COUNT, `Maximum ${MAX_COMPANIES_COUNT} companies`),
  }),
  describe: yup.object().shape({
    about: yup.string().trim().max(MAX_DESCRIBE_CHARS),
  }),
  skills: yup.object().shape({
    [SOLUTIONS_WITH_HARD]: yup
      .array()
      .max(
        SKILLS_LIMITS[SOLUTIONS_WITH_HARD].max,
        errors.maxOptionsLength(SKILLS_LIMITS[SOLUTIONS_WITH_HARD].max),
      )
      .min(
        SKILLS_LIMITS[SOLUTIONS_WITH_HARD].min,
        errors.minOptionsLength(SKILLS_LIMITS[SOLUTIONS_WITH_HARD].min),
      ),
    [SkillTypeEnum.SoftSkills]: yup
      .array()
      .max(
        SKILLS_LIMITS[SkillTypeEnum.SoftSkills].max,
        errors.maxOptionsLength(SKILLS_LIMITS[SkillTypeEnum.SoftSkills].max),
      )
      .min(
        SKILLS_LIMITS[SkillTypeEnum.SoftSkills].min,
        errors.minOptionsLength(SKILLS_LIMITS[SkillTypeEnum.SoftSkills].min),
      ),
  }),
});
