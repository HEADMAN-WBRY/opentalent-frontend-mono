import React from 'react';

const InviteByTalent = React.lazy(() => import('./InviteByTalent'));

export default InviteByTalent;
