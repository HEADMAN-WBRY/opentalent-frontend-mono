import { ConnectedPageLayout } from 'components/layout/page-layout';
import { usePushWithQuery, useSearchParams } from 'hooks/routing';
import React from 'react';
import { RouteComponentProps } from 'react-router-dom';

import { TabContext, TabPanel } from '@mui/lab';
import { Box } from '@mui/material';

import { Company } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import InvitationTabs from './InvitationTabs';
import InviteClients from './invite-clients';
import InviteTalents from './invite-talents/InviteTalents';
import { useRequestInviteData } from './invite-talents/hooks';
import { InviteTabs } from './types';

interface ProfileProps extends RouteComponentProps {}

const getTab = (tab: string) =>
  new Set([InviteTabs.Talent, InviteTabs.Company]).has(tab as InviteTabs)
    ? tab
    : InviteTabs.Talent;

const InviteByTalent = (props: ProfileProps) => {
  const { tab } = useSearchParams();
  const currentTab = getTab(tab as string);
  const pushWithQuery = usePushWithQuery();
  const onChange = (tab: string) => pushWithQuery({ query: { tab } });
  const { data, id, isLoading, getInvitationRequest } = useRequestInviteData();

  return (
    <ConnectedPageLayout
      headerProps={{ accountProps: {} }}
      documentTitle="InviteByTalent"
      drawerProps={{}}
      isLoading={isLoading}
    >
      <TabContext value={currentTab}>
        <Box pt={6} mb={8}>
          <Typography textAlign="center" variant="h4">
            Recommend OpenTalent and get paid
          </Typography>
        </Box>
        <InvitationTabs onChange={onChange} />

        <TabPanel value={InviteTabs.Talent}>
          <InviteTalents
            talentInvitationsInfo={data?.talentInvitationsInfo}
            getInvitationRequest={getInvitationRequest}
            id={id}
          />
        </TabPanel>
        <TabPanel value={InviteTabs.Company}>
          <InviteClients
            companies={
              data?.talentInvitationsInfo?.referred_companies as Company[]
            }
          />
        </TabPanel>
      </TabContext>
    </ConnectedPageLayout>
  );
};

export default InviteByTalent;
