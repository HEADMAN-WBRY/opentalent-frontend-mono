import { useMutation } from '@apollo/client';
import { CREATE_INVITATION_LINK } from 'graphql/talents';
import useMixPanel from 'hooks/common/useMixPanel';
import { useSnackbar } from 'notistack';
import { useCallback } from 'react';
import { useCopyToClipboard } from 'react-use';

import {
  Mutation,
  MutationCreateInvitationLinkArgs,
  MutationCreateTalentByTalentInvitationArgs,
} from '@libs/graphql-types';
import { FormikSubmit } from '@libs/helpers/form';

import { INVITE_TALENT } from '../../queries';
import { INITIAL_VALUES } from './consts';

interface HookParams {
  id: string;
  getInvitationRequest: VoidFunction;
}

export const useSubmitEmailForm = ({
  id,
  getInvitationRequest,
}: HookParams) => {
  const { enqueueSnackbar } = useSnackbar();
  const mixPanel = useMixPanel();
  const [submitInvite, { loading, error }] = useMutation<
    Mutation,
    MutationCreateTalentByTalentInvitationArgs
  >(INVITE_TALENT);
  const onSubmit = useCallback(
    async (values: typeof INITIAL_VALUES) => {
      try {
        await submitInvite({
          variables: { ...values, inviting_talent_id: id },
        });
        enqueueSnackbar('Your invite was sent', {
          variant: 'success',
        });
        mixPanel.track('User was invited through the invite page');
        await getInvitationRequest();
      } catch (e) {
        if (
          (e as any).graphQLErrors[0].extensions?.validation?.email?.includes(
            'The email has already been taken.',
          )
        ) {
          enqueueSnackbar('This person already has OpenTalent account', {
            variant: 'warning',
          });
        } else {
          enqueueSnackbar('Something went wrong', {
            variant: 'error',
          });
        }
      }
    },
    [enqueueSnackbar, getInvitationRequest, id, mixPanel, submitInvite],
  );

  return { onSubmit, loading, error };
};

export const useSubmitLinkForm = ({ id, getInvitationRequest }: HookParams) => {
  const { enqueueSnackbar } = useSnackbar();
  const [, copyToClipboard] = useCopyToClipboard();
  const [getInviteLink, { loading }] = useMutation<
    Mutation,
    MutationCreateInvitationLinkArgs
  >(CREATE_INVITATION_LINK, {
    onCompleted: (values) => {
      const link = values.createInvitationLink?.url;

      if (link) {
        copyToClipboard(link);
        getInvitationRequest();
        enqueueSnackbar('Link copied successfully!', { variant: 'success' });
      }
    },
    // eslint-disable-next-line no-console
    onError: console.error,
  });

  const onSubmit: FormikSubmit<{ name: string }> = useCallback(
    async ({ name }) => {
      getInviteLink({ variables: { inviting_talent_id: id, name } });
    },
    [getInviteLink, id],
  );

  return { loading, onSubmit };
};
