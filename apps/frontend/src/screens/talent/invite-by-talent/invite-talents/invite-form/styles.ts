import { makeStyles } from '@mui/styles';

import { Theme } from '@libs/ui/themes/default';

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    paddingTop: theme.spacing(10),
    paddingBottom: theme.spacing(14),
    background: theme.palette.grey[200],
  },
  title: {
    maxWidth: 670,
    padding: `0 ${theme.spacing(2)}`,
    margin: `0 auto ${theme.spacing(4)}`,

    [theme.breakpoints.down('sm')]: {
      fontSize: 24,
    },
  },
  form: {
    marginTop: theme.spacing(6),
    width: 450,
    margin: '0 auto',
    padding: theme.spacing(10),

    [theme.breakpoints.down('sm')]: {
      width: 'auto',
      margin: `0 ${theme.spacing(4)}`,
      padding: theme.spacing(6),
    },
  },
}));

export default useStyles;
