export const INITIAL_VALUES = {
  email: '',
  name: '',
};

export const FORM_TYPES = {
  link: 'link',
  email: 'email',
};
