import errors from 'consts/validationErrors';
import { Form, Formik } from 'formik';
import React from 'react';
import EmailControl from 'screens/company-user/create-profile/email-control';
import * as yup from 'yup';

import MailOutlineIcon from '@mui/icons-material/MailOutline';
import PermIdentityIcon from '@mui/icons-material/PermIdentity';
import { Grid } from '@mui/material';

import { maxStringValidator } from '@libs/helpers/yup';
import Button from '@libs/ui/components/button';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import Typography, { RouterLink } from '@libs/ui/components/typography';

import { FORM_TYPES, INITIAL_VALUES } from './consts';
import { useSubmitEmailForm } from './hooks';

interface InviteEmailFormProps {
  disabled: boolean;
  id: string;
  getInvitationRequest: VoidFunction;
}

const validator = yup.object().shape({
  email: maxStringValidator
    .email(errors.invalidEmail)
    .required(errors.required),
  name: maxStringValidator.required(errors.required),
});

const InviteEmailForm = ({
  getInvitationRequest,
  disabled,
  id,
}: InviteEmailFormProps) => {
  const { onSubmit, loading } = useSubmitEmailForm({
    id,
    getInvitationRequest,
  });

  return (
    <Formik
      validationSchema={validator}
      initialValues={INITIAL_VALUES}
      onSubmit={onSubmit}
    >
      {() => (
        <Form>
          <Grid spacing={8} container direction="column">
            <Grid item>
              <Typography variant="h6" align="center">
                Who do you want to invite?
              </Typography>
            </Grid>
            <Grid item>
              <Typography variant="body2" align="center">
                send invite by email
              </Typography>
            </Grid>
            <Grid item>
              <ConnectedTextField
                label="First Name"
                fullWidth
                name="name"
                variant="outlined"
                InputProps={{
                  startAdornment: <PermIdentityIcon color="disabled" />,
                }}
              />
            </Grid>
            <Grid item>
              <EmailControl
                label="Email"
                name="email"
                fullWidth
                variant="outlined"
                InputProps={{
                  startAdornment: <MailOutlineIcon color="disabled" />,
                }}
              />
            </Grid>
            <Grid item>
              <Button
                type="submit"
                variant="contained"
                color="primary"
                fullWidth
                disabled={disabled || !!loading}
              >
                Send invite
              </Button>
            </Grid>

            <Grid item>
              <Typography
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                }}
                variant="body2"
              >
                or&nbsp;
                <RouterLink to={{ search: `?form=${FORM_TYPES.link}` }}>
                  share your personal invite link
                </RouterLink>
              </Typography>
            </Grid>
          </Grid>
        </Form>
      )}
    </Formik>
  );
};

export default InviteEmailForm;
