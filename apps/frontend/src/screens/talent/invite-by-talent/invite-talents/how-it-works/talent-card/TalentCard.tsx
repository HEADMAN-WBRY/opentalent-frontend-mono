import { Avatar, Grid } from '@mui/material';
import { ReactComponent as CheckIcon } from 'assets/icons/check.svg';
import { ReactComponent as LetterIcon } from 'assets/icons/letter.svg';
import { DEFAULT_AVATAR } from 'consts/common';
import React from 'react';
import { formatName } from 'utils/talent';

import { Talent } from '@libs/graphql-types';
import Chip from '@libs/ui/components/chip/Chip';
import Typography from '@libs/ui/components/typography';

import useStyles from './styles';

interface TalentCardProps {
  count?: {
    max: number;
    current: number;
  };
  talent?: Talent;
}

const TalentCard = (props: TalentCardProps) => {
  const { count, talent } = props;
  const classes = useStyles(props);
  const avatar = talent?.avatar?.avatar || DEFAULT_AVATAR;
  const name = formatName({
    firstName: talent?.first_name,
    lastName: talent?.last_name,
  });
  const email = talent?.email;
  const isAccepted = talent?.is_invitation_accepted;

  return (
    <Grid className={classes.container} container spacing={5}>
      <Grid item>
        {count ? (
          <LetterIcon />
        ) : (
          <Avatar className={classes.avatar} src={avatar} />
        )}
      </Grid>
      <Grid item>
        <Grid>
          <Typography variant="h6">
            {count ? `Spread the love ❤️` : name}
          </Typography>
        </Grid>
        <Grid>
          <Typography variant="body2" color="textSecondary">
            {count
              ? `(${count?.max - count?.current}/${count?.max} invites left)`
              : email}
          </Typography>
        </Grid>
        {!count && (
          <Grid className={classes.status}>
            {isAccepted ? (
              <CheckIcon className={classes.check} />
            ) : (
              <Chip label="pending activation" color="grey" size="small" />
            )}
          </Grid>
        )}
      </Grid>
    </Grid>
  );
};

export default TalentCard;
