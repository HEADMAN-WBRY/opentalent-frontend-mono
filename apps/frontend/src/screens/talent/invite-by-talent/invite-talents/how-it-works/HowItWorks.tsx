import { Box, Grid } from '@mui/material';
import React from 'react';

import { Talent } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import useStyles from './styles';
import TalentCard from './talent-card';

interface HowItWorksProps {
  max: number;
  left: number;
  invites: Talent[];
  openModal: VoidFunction;
}

const HowItWorks = (props: HowItWorksProps) => {
  const { max, invites, left } = props;
  const classes = useStyles(props);

  return (
    <Grid
      className={classes.container}
      spacing={4}
      container
      direction="column"
    >
      <Grid item>
        <Typography variant="h6" align="center">
          How it works?
        </Typography>
      </Grid>
      <Grid item>
        <Box className={classes.paragraph}>
          <Typography variant="body2" align="center">
            We believe that talented individuals like yourself can be mobilised
            to act as the best talent spotters. So we built a system that
            enables you to source talent from your personal network and get paid
            for doing so. For every person you invite, and who gets hired
            through OpenTalent, we will pay you €4,50 per hour for the first 6
            months. That’s €4.320 for a simple intro! You‘ll start with 5
            invites only, so spend them wisely.
          </Typography>
        </Box>
      </Grid>
      <Grid item>
        <Typography
          className={classes.talentsTitle}
          variant="h6"
          align="center"
        >
          People you invited
        </Typography>
      </Grid>
      <Grid item>
        <Grid container spacing={6} direction="column">
          {invites.map((talent) => (
            <Grid key={talent?.id} item>
              <TalentCard talent={talent} />
            </Grid>
          ))}
          {left && (
            <Grid item>
              <TalentCard count={{ max, current: max - left }} />
            </Grid>
          )}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default HowItWorks;
