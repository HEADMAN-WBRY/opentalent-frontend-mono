import { useMutation } from '@apollo/client';
import { useSnackbar } from 'notistack';
import { useCallback } from 'react';

import {
  Mutation,
  MutationCreateChiefsProgramApplicationArgs,
} from '@libs/graphql-types';

import { CREATE_CHIEFS_PROGRAM } from './queries';

export const useJoinProgrammeModalSubmit = ({
  handleClose,
  id,
  onSuccess,
}: {
  handleClose: VoidFunction;
  id: string;
  onSuccess: VoidFunction;
}) => {
  const { enqueueSnackbar } = useSnackbar();
  const [request, { loading }] = useMutation<
    Mutation,
    MutationCreateChiefsProgramApplicationArgs
  >(CREATE_CHIEFS_PROGRAM, { variables: { talent_id: id } });

  const onSubmit = useCallback(async () => {
    try {
      await request();
      handleClose();
      onSuccess();
    } catch (e) {
      enqueueSnackbar('Something went wrong', {
        variant: 'error',
      });
    }
  }, [enqueueSnackbar, handleClose, onSuccess, request]);

  return { onSubmit, loading };
};
