import React from 'react';

import { Box } from '@mui/material';

import { Company } from '@libs/graphql-types';
import Typography, { OuterLink } from '@libs/ui/components/typography';

import ClientsList from './clients-list';
import RefLinkBlock from './ref-link-block';

interface InviteClientsProps {
  companies?: Company[];
}

const InviteClients = ({ companies }: InviteClientsProps) => {
  return (
    <Box pt={6}>
      <RefLinkBlock />

      <Box mt={16} mb={10}>
        <Typography paragraph fontWeight={500} variant="h6" textAlign="center">
          How it works?
        </Typography>

        <Typography textAlign="center">
          Earn 10% of the Year 1 revenues of each client you refer.{' '}
          <OuterLink
            target="_blank"
            href="https://opentalent.notion.site/Get-Paid-830a2d2bd1c34f51a06a0c2637564c18"
          >
            Learn more here.
          </OuterLink>
        </Typography>
      </Box>

      <ClientsList companies={companies} />
    </Box>
  );
};

export default InviteClients;
