import React from 'react';

import { Box } from '@mui/material';

import { Company } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import CompanyCard from './company-card';

interface ClientsListProps {
  companies?: Company[];
}

const ClientsList = ({ companies }: ClientsListProps) => {
  return (
    <>
      <Box mb={8}>
        <Typography paragraph fontWeight={500} variant="h6" textAlign="center">
          Clients you invited
        </Typography>
      </Box>

      {companies?.map((company) => (
        <Box mb={4}>
          <CompanyCard
            key={company.id}
            name={company.name}
            logo={company.logo?.path}
          />
        </Box>
      ))}
    </>
  );
};

export default ClientsList;
