import { Grid } from '@mui/material';
import React from 'react';

import Typography from '@libs/ui/components/typography';

interface SettingItemProps
  extends React.PropsWithChildren<{ title: string; subtitle: string }> {}

const SettingItem = ({ title, subtitle, children }: SettingItemProps) => {
  return (
    <Grid justifyContent="space-between" wrap="nowrap" spacing={2} container>
      <Grid item>
        <Typography variant="subtitle1">{title}</Typography>
        <Typography variant="body2" color="textSecondary">
          {subtitle}
        </Typography>
      </Grid>
      <Grid item>{children}</Grid>
    </Grid>
  );
};

export default SettingItem;
