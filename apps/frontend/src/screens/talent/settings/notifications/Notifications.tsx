import { Formik } from 'formik';
import React from 'react';

import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import {
  InstantMatchesNotificationsScoreEnum,
  TalentAccountSettingsInput,
} from '@libs/graphql-types';
import { EXTERNAL_LINKS } from '@libs/helpers/consts';
import { modelPath } from '@libs/helpers/form';
import Button from '@libs/ui/components/button';
import {
  ConnectedSelect,
  makeOptionsFromObject,
} from '@libs/ui/components/form/select';
import { ConnectedSwitch } from '@libs/ui/components/form/switch';
import Typography, { OuterLink } from '@libs/ui/components/typography';
import InfoLinkContainer from '@libs/ui/components/typography/InfoLinkContainer';

import SettingItem from './SettingItem';
import { useInitialValues, useUpdateUser } from './hooks';

interface NotificationsProps {
  submitText?: string;
  onSuccess?: VoidFunction;
  isCreate?: boolean;
}

const MATCH_OPTIONS = makeOptionsFromObject({
  [InstantMatchesNotificationsScoreEnum.Score_50]: '+50% Matching Score',
  [InstantMatchesNotificationsScoreEnum.Score_70]: '+70% Matching Score',
  [InstantMatchesNotificationsScoreEnum.Score_90]: '+90% Matching Score',
});

const useStyles = makeStyles((theme) => ({
  switch: {
    margin: 0,

    '& .Mui-checked': {
      color: theme.palette.success.dark,

      '& + .MuiSwitch-track': {
        backgroundColor: theme.palette.success.dark,
      },
    },
  },
  legalTabIcon: {
    background: theme.palette.primary.main,
    borderRadius: '100%',
  },
}));

const Notifications = ({
  onSuccess,
  submitText = 'Submit',
}: NotificationsProps) => {
  const initialValues = useInitialValues();
  const classes = useStyles();
  const { loading, updateRequest } = useUpdateUser({ onSuccess });

  return (
    <>
      <Box mb={8}>
        <Typography variant="h6">
          What Notifications do you want to receive?
        </Typography>
        <Typography color="textSecondary" paragraph>
          Only get notified about what’s important to you
        </Typography>
      </Box>
      <Box maxWidth="600px">
        <Formik<TalentAccountSettingsInput>
          onSubmit={updateRequest}
          initialValues={initialValues}
        >
          {({ submitForm, dirty }) => (
            <Grid spacing={4} direction="column" container wrap="nowrap">
              <Grid item>
                <SettingItem
                  title="Company and product updates"
                  subtitle="We'll keep you in the loop about what's going at OpenTalent."
                >
                  <ConnectedSwitch
                    label=""
                    color="secondary"
                    formControlProps={{
                      label: '',
                      classes: { root: classes.switch },
                    }}
                    name={modelPath<TalentAccountSettingsInput>(
                      (m) => m.receive_company_and_product_updates,
                    )}
                  />
                </SettingItem>
              </Grid>
              <Grid item>
                <SettingItem
                  title="Messages (chat)"
                  subtitle="Receive direct messages from companies / hiring managers."
                >
                  <ConnectedSwitch
                    label=""
                    formControlProps={{
                      label: '',
                      classes: { root: classes.switch },
                    }}
                    color="secondary"
                    name={modelPath<TalentAccountSettingsInput>(
                      (m) => m.receive_direct_messages,
                    )}
                  />
                </SettingItem>
              </Grid>
              <Grid item>
                <Box mb={2}>
                  <SettingItem
                    title="Instant Matches"
                    subtitle="Get notified of jobs that match your skills and preferences."
                  >
                    {' '}
                  </SettingItem>
                </Box>
                <Box mb={4}>
                  <ConnectedSelect
                    variant="outlined"
                    margin="dense"
                    options={MATCH_OPTIONS}
                    hideNoneValue
                    name={modelPath<TalentAccountSettingsInput>(
                      (m) => m.instant_matches_notifications_min_score,
                    )}
                  />
                </Box>

                <InfoLinkContainer>
                  <OuterLink target="_blank" href={EXTERNAL_LINKS.matching}>
                    What is meant by Matching Score
                    <OpenInNewIcon
                      style={{ marginBottom: -6, marginLeft: 6 }}
                    />
                  </OuterLink>
                </InfoLinkContainer>
              </Grid>
              <Grid item>
                <Box pt={4}>
                  <Button
                    fullWidth
                    onClick={submitForm}
                    variant="contained"
                    color="primary"
                    disabled={loading || !dirty}
                  >
                    {submitText}
                  </Button>
                </Box>
              </Grid>
            </Grid>
          )}
        </Formik>
      </Box>
    </>
  );
};

export default Notifications;
