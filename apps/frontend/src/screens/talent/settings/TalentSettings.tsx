import { TabContext, TabPanel } from '@mui/lab';
import { Box, Tab, Tabs } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { ConnectedPageLayout } from 'components/layout/page-layout';
import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';

import Typography from '@libs/ui/components/typography';

import General from './general';
import { useSettingsTabs } from './hooks';
import Notifications from './notifications';
import { SettingsTabs } from './types';

interface TalentSettingsProps {}

const useStyles = makeStyles((theme) => ({
  tabPanel: {
    padding: `${theme.spacing(6)} 0`,
  },
  legalTabIcon: {
    background: theme.palette.primary.main,
    borderRadius: '100%',
  },
}));

const TalentSettings = (props: TalentSettingsProps) => {
  const { isXS } = useMediaQueries();
  const classes = useStyles();
  const { tab, onTabChange } = useSettingsTabs();

  return (
    <ConnectedPageLayout
      drawerProps={{}}
      documentTitle="Settings"
      contentSpacing={isXS ? 4 : 8}
    >
      <Box mb={4}>
        <Typography variant="h5" paragraph>
          Account Settings
        </Typography>
      </Box>

      <TabContext value={tab as string}>
        <Tabs
          value={tab as string}
          selectionFollowsFocus
          onChange={onTabChange}
        >
          <Tab value={SettingsTabs.General} label="General" />
          <Tab value={SettingsTabs.Notifications} label="Notifications" />
        </Tabs>
        <TabPanel
          classes={{ root: classes.tabPanel }}
          value={SettingsTabs.General}
        >
          <General />
        </TabPanel>
        <TabPanel
          classes={{ root: classes.tabPanel }}
          value={SettingsTabs.Notifications}
        >
          <Notifications />
        </TabPanel>
      </TabContext>
    </ConnectedPageLayout>
  );
};

export default TalentSettings;
