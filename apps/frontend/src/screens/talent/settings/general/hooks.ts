import { useCurrentUser } from 'hooks/auth';
import { useMemo } from 'react';

import { Talent } from '@libs/graphql-types';

export const useUserData = () => {
  const { getData } = useCurrentUser();
  const talentData = useMemo(() => getData().user as Talent, [getData]);

  return [
    { text: 'Name', value: talentData?.first_name || '' },
    { text: 'Last name', value: talentData?.last_name || '' },
    { text: 'Email', value: talentData?.email || '' },
    { text: 'Phone', value: talentData?.talent_data?.phone || '' },
    {
      text: 'LinkedIn link',
      value: talentData?.talent_data?.linkedin_profile_link || '',
    },
  ].filter((i) => Boolean(i.value));
};
