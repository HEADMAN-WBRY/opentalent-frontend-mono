import * as yup from 'yup';

import errors from 'consts/validationErrors';

export default yup.object().shape({
  first_name: yup.string().required(errors.required),
  last_name: yup.string().required(errors.required),
  position: yup.string().required(errors.required),
  email: yup.string().email(errors.invalidEmail).required(errors.required),
});
