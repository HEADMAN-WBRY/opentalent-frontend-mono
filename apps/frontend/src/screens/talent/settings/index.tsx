import React from 'react';

const CompanySettings = React.lazy(() => import('./TalentSettings'));

export default CompanySettings;
