import { useCurrentUser } from 'hooks/auth';
import { useSearchParams, usePushWithQuery } from 'hooks/routing';

import { SettingsTabs } from './types';

export const useSettingsTabs = () => {
  const { tab = SettingsTabs.Notifications } = useSearchParams();
  const push = usePushWithQuery();
  const onTabChange = (e: React.ChangeEvent<unknown>, tab: string) =>
    push({ query: { tab } });

  return { tab, onTabChange };
};

export const useClientContract = () => {
  const { data } = useCurrentUser();
  const contract = data?.currentCompanyUser?.company?.contract;

  return contract;
};
