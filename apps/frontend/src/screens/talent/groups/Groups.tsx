import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { ConnectedPageLayout } from 'components/layout/page-layout';
import React from 'react';
import { Link } from 'react-router-dom';

import Typography from '@libs/ui/components/typography';

import GroupsTable from './GroupsTable';

const useStyles = makeStyles(() => ({
  contentWrapper: {
    overflow: 'hidden',
  },
}));

const Groups = () => {
  const classes = useStyles();
  return (
    <ConnectedPageLayout
      classes={{ contentWrapper: classes.contentWrapper }}
      documentTitle="Groups"
      drawerProps={{}}
      headerProps={{
        accountProps: {},
      }}
    >
      <Box pt={2} pb={6}>
        <Typography variant="h5">Groups</Typography>
      </Box>

      <Box pb={10}>
        <Typography variant="body1">
          These companies have added you to their private on-demand workforce.
        </Typography>
      </Box>

      <Box pb={10}>
        <GroupsTable />
      </Box>

      <Typography variant="body1">
        Apply for new jobs{' '}
        <Link to="/alo">
          <Typography component="span" color="tertiary" variant="body1">
            here
          </Typography>
        </Link>{' '}
        to get employed by more companies.
      </Typography>
    </ConnectedPageLayout>
  );
};

export default Groups;
