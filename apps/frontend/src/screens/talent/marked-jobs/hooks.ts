import { useQuery } from '@apollo/client';
import { useSearchParams } from 'hooks/routing';
import { useEffect } from 'react';

import {
  QueryCurrentTalentJobBoardSearchOrderByOrderByClause,
  Query,
  SortOrder,
  JobOrderByColumn,
  QueryCurrentTalentJobBoardSearchArgs,
} from '@libs/graphql-types';

import { getJobsQuery } from './queries';

const getOrder = (
  column: any = JobOrderByColumn.CampaignStart,
): QueryCurrentTalentJobBoardSearchOrderByOrderByClause => ({
  column,
  order: SortOrder.Desc,
});

export const useScreenData = ({ queryPatch }: { queryPatch: string }) => {
  const { order_by, page } = useSearchParams();
  const { data, loading, fetchMore, refetch } = useQuery<
    Query,
    QueryCurrentTalentJobBoardSearchArgs
  >(getJobsQuery(queryPatch), {
    variables: {
      order_by: [getOrder(order_by)],
      page: page as any,
    },
  });

  useEffect(() => {
    fetchMore({ variables: { page, order_by: [getOrder(order_by)] } });
  }, [order_by, page, fetchMore]);

  return { data, isLoading: loading, refetch };
};
