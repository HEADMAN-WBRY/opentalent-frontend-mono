import { useCurrentTime } from 'hooks/common/useCurrentTime';
import React from 'react';

import { Job } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import JobCard from '../../job-board-v2/content/job-card';
import { useJobModals } from '../../job-board-v2/hooks';
import InviteModal from '../../job-board-v2/invite-modal';

interface JobListProps {
  jobs: Job[];
  refetch: any;
}

const JobList = ({ jobs, refetch }: JobListProps) => {
  const currentTime = useCurrentTime();
  const { inviteJob, handleClose, onJobApply, onInvite } = useJobModals();

  return (
    <div>
      <div>
        {!jobs.length && (
          <Typography variant="body1">
            No jobs matching your criteria. Try to change search options.
          </Typography>
        )}
        {jobs.map((job, index) => (
          <JobCard
            key={job.id}
            onInvite={onInvite}
            onJobApply={onJobApply}
            job={job as Job}
            currentTime={currentTime}
            onJobSave={refetch}
          />
        ))}
      </div>
      {inviteJob && <InviteModal handleClose={handleClose} job={inviteJob} />}
    </div>
  );
};

export default React.memo(JobList);
