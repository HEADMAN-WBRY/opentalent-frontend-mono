import LibraryBooksIcon from '@mui/icons-material/LibraryBooks';
import { Box, Grid, SelectChangeEvent } from '@mui/material';
import { JOB_ORDER_BY_COLUMN_OPTIONS } from 'consts/common';
import { usePushWithQuery, useSearchParams } from 'hooks/routing';
import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { pathManager } from 'routes';

import Button from '@libs/ui/components/button';
import Select from '@libs/ui/components/form/select';

interface FilterProps {}

const Filter = (props: FilterProps) => {
  const location = useLocation();
  const push = usePushWithQuery();
  const { order_by } = useSearchParams();
  const onOrderChange = (e: SelectChangeEvent<unknown>) => {
    push({ query: { order_by: e.target.value } });
  };

  return (
    <Grid
      container
      justifyContent="space-between"
      wrap="nowrap"
      alignItems="center"
    >
      <Grid item>
        <Box style={{ width: 320 }} flexGrow={1} mb={2}>
          <Select
            name="order_by_column"
            margin="dense"
            variant="filled"
            label="Sort by"
            fullWidth
            onChange={onOrderChange}
            defaultValue={JOB_ORDER_BY_COLUMN_OPTIONS[0].value}
            value={order_by}
            hideNoneValue
            options={JOB_ORDER_BY_COLUMN_OPTIONS}
          />
        </Box>
      </Grid>
      <Grid item>
        <Link
          to={{
            pathname: pathManager.talent.jobBoard.generatePath(),
            search: (location.state as any)?.jobBoardSearch || '',
          }}
        >
          <Button endIcon={<LibraryBooksIcon />}>back to all jobs</Button>
        </Link>
      </Grid>
    </Grid>
  );
};

export default Filter;
