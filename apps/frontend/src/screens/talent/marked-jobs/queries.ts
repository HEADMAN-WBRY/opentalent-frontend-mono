import { gql } from '@apollo/client';
import FULL_JOB_FRAGMENT from 'graphql/fragments/talent/talentJobFragment';

export const getJobsQuery = (queryPatch = '') => {
  return gql`
    ${FULL_JOB_FRAGMENT}
    query GetAppliedJobs(
      $order_by: [QueryCurrentTalentJobBoardSearchOrderByOrderByClause!]
      $first: Int
      $page: Int
    ) {
      currentTalentJobBoardSearch(
        order_by: $order_by
        first: $first
        page: $page
        ${queryPatch}
      ) {
        paginatorInfo {
          count
          currentPage
          firstItem
          hasMorePages
          lastItem
          lastPage
          perPage
          total
        }
        data {
          ...FullJob
        }
      }
    }
  `;
};

export const GET_APPLIED_JOBS = gql`
  ${FULL_JOB_FRAGMENT}
  query GetAppliedJobs(
    $order_by: [QueryCurrentTalentJobBoardSearchOrderByOrderByClause!]
    $first: Int
    $page: Int
  ) {
    currentTalentJobBoardSearch(
      order_by: $order_by
      first: $first
      page: $page
      is_applied: true
    ) {
      paginatorInfo {
        count
        currentPage
        firstItem
        hasMorePages
        lastItem
        lastPage
        perPage
        total
      }
      data {
        ...FullJob
      }
    }
  }
`;

export const SAVE_JOB = gql`
  mutation SaveJobToFavorites($job_id: ID!) {
    saveJobToFavorites(job_id: $job_id)
  }
`;

export const DELETE_JOB = gql`
  mutation DeleteJobFromFavorites($job_id: ID!) {
    deleteJobFromFavorites(job_id: $job_id)
  }
`;
