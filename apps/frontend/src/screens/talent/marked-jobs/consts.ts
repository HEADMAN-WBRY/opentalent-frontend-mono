import { JobMarkType } from 'types';

export const PAGE_DATA = {
  [JobMarkType.Applied]: {
    title: 'My applications',
    queryPatch: 'is_applied: true',
  },
  [JobMarkType.Saved]: {
    title: 'Saved jobs',
    queryPatch: 'is_saved: true',
  },
};
