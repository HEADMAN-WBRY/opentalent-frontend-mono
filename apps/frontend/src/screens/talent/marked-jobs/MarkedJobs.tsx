import { Box } from '@mui/material';
import { ConnectedPageLayout } from 'components/layout/page-layout';
import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';
import { JobMarkType } from 'types';

import { Job } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import { PAGE_DATA } from './consts';
import Filter from './filter';
import { useScreenData } from './hooks';
import JobList from './job-list';
import JobsPagination from './pagination';
import { PageProps } from './types';

const MarkedJobs = ({ match }: PageProps) => {
  const pageData = PAGE_DATA[match.params.type as JobMarkType];
  const { isXS } = useMediaQueries();
  const { data, isLoading, refetch } = useScreenData({
    queryPatch: pageData.queryPatch,
  });
  const jobs = (data?.currentTalentJobBoardSearch?.data || []) as Job[];
  const { currentPage = 1, lastPage = 1 } =
    data?.currentTalentJobBoardSearch?.paginatorInfo || {};

  return (
    <ConnectedPageLayout
      documentTitle="Job board"
      drawerProps={{}}
      headerProps={{ accountProps: {} }}
      contentSpacing={isXS ? 0 : undefined}
      isLoading={isLoading}
    >
      <Box maxWidth="768px" pt={4}>
        <Typography variant="h5">{pageData.title}</Typography>
      </Box>
      <Box pt={8}>
        <Filter />
      </Box>
      <Box pt={8}>
        <JobList refetch={refetch} jobs={jobs} />
      </Box>
      <JobsPagination currentPage={currentPage} lastPage={lastPage} />
    </ConnectedPageLayout>
  );
};

export default MarkedJobs;
