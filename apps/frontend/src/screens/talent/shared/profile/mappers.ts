import { MIN_JOB_RATE } from 'consts/jobs';
import { LOCAL_STORAGE_KEYS } from 'consts/ls-keys';
import { format, parseISO } from 'date-fns';
import { AvailabilityType } from 'screens/talent/edit-profile/types';

import {
  MutationCreateUntrustedTalentProfileArgs,
  MutationUpdateTalentProfileArgs,
  Talent,
  TalentDocument,
  TalentGeneralInfoInput,
  TalentWorkHistory,
  WorkHistoryInput,
} from '@libs/graphql-types';
import { parseNumber } from '@libs/helpers/common';
import { OptionType } from '@libs/ui/components/form/select';

import {
  AvailabilityFormState,
  CompaniesFormState,
  CreatingFormState,
  CustomWorkHistory,
  CVFormState,
  DescribeFormState,
  PictureFormState,
  ProfileFormState,
  RateFormState,
  SkillsFormState,
} from './types';
import { getDefaultSkills, getDefaultWorkHistory } from './utils';

const mapToOptions = (i: any) => ({ ...i, text: i.name, value: i.id });
const getValuesFromOptions = (opt: OptionType[]) => opt.map((i) => i.value);

export const mapTalentToProfileForm = (talent?: Talent): ProfileFormState => ({
  firstName: talent?.first_name || '',
  lastName: talent?.last_name || '',
  phone: talent?.talent_data?.phone || '',
  location: talent?.location || '',
  category: talent?.category?.id || '',
  position: talent?.recent_position_title || '',
  linkedLink: talent?.talent_data?.linkedin_profile_link || '',
  vat: talent?.talent_data?.vat_number,
  subcategories: talent?.subcategories?.map(mapToOptions) || [],
});

export const mapTalentPicture = (talent?: Talent): PictureFormState => ({
  avatar: talent?.avatar?.avatar || '',
  hash: talent?.avatar?.hash || '',
});

export const mapTalentAvailability = (
  talent?: Talent,
): AvailabilityFormState => ({
  availableDate: talent?.available_date || '',
  availableNow: talent?.available_now
    ? AvailabilityType.Now
    : AvailabilityType.Later,
  hoursPerWeek:
    (talent?.hours_per_week && String(talent?.hours_per_week)) || '',
});

export const mapTalentRate = (talent?: Talent): RateFormState => ({
  min: talent?.rate_min || MIN_JOB_RATE,
});

export const mapTalentSkills = (talent?: Talent): SkillsFormState => {
  const skills = talent?.skills?.data || [];
  const result = skills.reduce((acc, skill) => {
    acc[skill.skill_type].push(mapToOptions(skill));
    return acc;
  }, getDefaultSkills());

  result.SOLUTIONS_WITH_HARD = result.SOLUTIONS.concat(result.HARD_SKILLS);

  return result;
};

export const mapTalentToCompanies = (talent?: Talent): CompaniesFormState => ({
  companies: talent?.companies?.map(mapToOptions) || [],
});

export const mapTalentDescribe = (talent?: Talent): DescribeFormState => ({
  about: talent?.about || '',
});

export const mapTalentDocuments = (talent?: Talent): CVFormState => ({
  documents: (talent?.documents || []) as TalentDocument[],
});

export const mapWorkHistoryToClient = (
  talent?: Talent,
): CustomWorkHistory[] => {
  const workHistory = (talent?.talent_work_history ||
    []) as TalentWorkHistory[];

  return workHistory.length
    ? workHistory.map(
        ({ worked_from, worked_to, company_name, position_title, id }) => ({
          worked: [
            parseISO(worked_from),
            worked_to ? parseISO(worked_to) : null,
          ],
          companyName: company_name,
          positionTitle: position_title,
          workNow: !worked_to,
          id,
        }),
      )
    : getDefaultWorkHistory();
};

export const mapTalentToClient = (talent?: Talent): CreatingFormState => {
  return {
    profile: mapTalentToProfileForm(talent),
    picture: mapTalentPicture(talent),
    availability: mapTalentAvailability(talent),
    rate: mapTalentRate(talent),
    skills: mapTalentSkills(talent),
    companies: mapTalentToCompanies(talent),
    cv: mapTalentDocuments(talent),
    describe: mapTalentDescribe(talent),
    workHistory: mapWorkHistoryToClient(talent),
  };
};

// To server
const formatDate = (date: Date) => (date ? format(date, 'yyyy-M-dd') : '');
const mapWorkHistoryToServer = (form: CreatingFormState): WorkHistoryInput[] =>
  form.workHistory
    .filter((item) => !!item.positionTitle || !!item.companyName)
    .map((item) => ({
      position_title: item.positionTitle,
      company_name: item.companyName,
      worked_from: item.worked[0]
        ? formatDate(item.worked[0] as Date)
        : undefined,
      worked_to:
        item.worked[1] && !item.workNow
          ? formatDate(item.worked[1] as Date)
          : undefined,
    }));

const mapFormToGeneralInfo = (
  form: CreatingFormState,
): TalentGeneralInfoInput => ({
  first_name: form.profile.firstName,
  last_name: form.profile.lastName,
  recent_position_title: form.profile.position,
  location: form.profile.location,
  rate_min: Number(form.rate.min),
  talent_category_id: form.profile.category,
  talent_subcategory_ids: form.profile.subcategories.map((i) =>
    String(i.value),
  ),
});

export const mapFormToTalentCreation = (
  form: CreatingFormState,
): MutationCreateUntrustedTalentProfileArgs => ({
  email: localStorage.getItem(LOCAL_STORAGE_KEYS.talentOnboardingEmail) || '',
  source_company_id:
    localStorage.getItem(LOCAL_STORAGE_KEYS.talentOnboardingCompanyId) ||
    undefined,
  general_info: mapFormToGeneralInfo(form),
  apply_to_job_id:
    localStorage.getItem(LOCAL_STORAGE_KEYS.talentOnboardingAppliedJobId) ||
    undefined,
});

export const mapFormToTalentUpdate = (
  form: CreatingFormState,
): MutationUpdateTalentProfileArgs => {
  const talentId = form.talentData?.id;

  if (!talentId) {
    throw new Error('No talent id!');
  }

  const availHours = form.availability.hoursPerWeek;

  return {
    talent_id: talentId,
    avatar: form.picture.hash,
    about: form.describe.about,
    general_info: mapFormToGeneralInfo(form),
    account_info: {
      linkedin_profile_link: form.profile.linkedLink,
      phone: form.profile.phone,
      vat_number: form.profile.vat,
    },
    documents: form.cv.documents.map((doc) => doc.hash),
    skills: getValuesFromOptions(Object.values(form.skills).flat()) as string[],
    companies: getValuesFromOptions(form.companies.companies) as string[],
    available_now: form.availability.availableNow === AvailabilityType.Now,
    available_date: form.availability.availableDate || undefined,
    hours_per_week: parseNumber(availHours),
    work_history: mapWorkHistoryToServer(form),
  };
};
