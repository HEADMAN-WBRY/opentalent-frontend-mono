import React from 'react';

const Invitation = React.lazy(() => import('./Invitation'));

export default Invitation;
