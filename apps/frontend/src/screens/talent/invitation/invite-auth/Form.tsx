// import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import PermIdentityIcon from '@mui/icons-material/PermIdentity';
import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';
import React from 'react';

import Button from '@libs/ui/components/button';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';

interface FormProps {
  onSubmit: (e?: React.FormEvent<HTMLFormElement> | undefined) => void;
  loading: boolean;
}

const useStyles = makeStyles((theme) => ({
  input: {
    '& > div': {
      paddingLeft: theme.spacing(2),
      height: 46,
    },
  },
  adornment: {
    paddingTop: theme.spacing(1),
    paddingRight: theme.spacing(2),
    color: theme.palette.text.secondary,
  },
}));

const Form = ({ onSubmit, loading }: FormProps) => {
  const classes = useStyles();

  return (
    <Box>
      <Box pb={4}>
        <ConnectedTextField
          className={classes.input}
          variant="standard"
          fullWidth
          placeholder="Enter your email"
          name="email"
          InputProps={{
            startAdornment: (
              <Box className={classes.adornment}>
                <PermIdentityIcon />
              </Box>
            ),
          }}
        />
      </Box>
      {/* <Box pb={4}>
        <ConnectedTextField
          className={classes.input}
          variant="standard"
          fullWidth
          placeholder="Enter your password"
          type="password"
          InputProps={{
            startAdornment: (
              <Box className={classes.adornment}>
                <LockOutlinedIcon />
              </Box>
            ),
          }}
          name="password"
        />
      </Box>
      <Box pb={4}>
        <ConnectedTextField
          className={classes.input}
          variant="standard"
          fullWidth
          placeholder="Confirm password"
          name="password_repeat"
          type="password"
          InputProps={{
            startAdornment: (
              <Box className={classes.adornment}>
                <LockOutlinedIcon />
              </Box>
            ),
          }}
        />
      </Box> */}
      <Button
        onClick={onSubmit}
        fullWidth
        variant="contained"
        color="primary"
        disabled={loading}
      >
        Activate account
      </Button>
    </Box>
  );
};

export default Form;
