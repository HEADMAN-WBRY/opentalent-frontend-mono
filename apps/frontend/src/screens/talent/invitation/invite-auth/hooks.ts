import { useMutation } from '@apollo/client';
import { useSnackbar } from 'notistack';
import { useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import * as yup from 'yup';

import {
  Mutation,
  MutationCreateTalentAccountFromInvitationLinkArgs,
} from '@libs/graphql-types';

import { INVITE_FORM_SUBMIT } from './queries';
import { InviteFormModel } from './types';

export const useInviteFormSubmit = ({ key }: { key: string }) => {
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();
  const [submit, { loading }] = useMutation<
    Mutation,
    MutationCreateTalentAccountFromInvitationLinkArgs
  >(INVITE_FORM_SUBMIT, {
    onCompleted: () => {
      enqueueSnackbar('Success!', { variant: 'success' });
      history.push('/invitation/success');
    },
    // eslint-disable-next-line no-console
    onError: console.error,
  });
  const onSubmit = useCallback(
    (variables: InviteFormModel) => {
      submit({ variables: { email: variables.email, key } });
    },
    [key, submit],
  );

  return { onSubmit, loading };
};

export const useValidator = () => {
  // const client = useApolloClient();
  return yup.object().shape({
    // password_repeat: yup
    //   .string()
    //   .test('match', "Passwords don't match", function test(value) {
    //     return value === this.parent.password;
    //   })
    //   .required(),
    // password: yup.string().required(),
    email: yup.string().email().required(),
    // .test(
    //   'email-exist-check',
    //   'A user account with this email already exists',
    //   async function test(email) {
    //     const isValid = await yup.string().email().required().validate(email);

    //     if (isValid) {
    //       const res = await client.query({
    //         query: CHECK_TALENT_EMAIL,
    //         variables: { talent_email: email },
    //       });
    //       return !res.data.checkTalentExistsByEmail;
    //     }

    //     return this.schema.validate(email);
    //   },
    // ),
  });
};
