export interface InviteFormModel {
  password_repeat: string;
  password: string;
  email: string;
}
