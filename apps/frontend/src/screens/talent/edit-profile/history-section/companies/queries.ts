import { gql } from '@apollo/client';

export const SEARCH_COMPANIES = gql`
  query SearchCompanies($search: String!, $first: Int, $page: Int) {
    companies(search: $search, first: $first, page: $page) {
      data {
        id
        name
      }
    }
  }
`;

export const CREATE_COMPANY = gql`
  mutation CreateCompany($name: String!) {
    createCompany(name: $name) {
      id
      name
    }
  }
`;
