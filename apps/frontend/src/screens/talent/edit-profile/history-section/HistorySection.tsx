import { useFormikContext } from 'formik';
import { useFormikListActions } from 'hooks/form';
import React, { useCallback } from 'react';
import { CustomWorkHistory } from 'screens/talent/shared/profile/types';
import { moveToIndex } from 'utils/common';

import AddIcon from '@mui/icons-material/Add';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import DeleteIcon from '@mui/icons-material/Delete';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';

import { modelPath } from '@libs/helpers/form';
import Button from '@libs/ui/components/button';
import { ConnectedCheckbox } from '@libs/ui/components/form/checkbox';
import { ConnectedDatePicker } from '@libs/ui/components/form/datepicker';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import StepSection from '@libs/ui/components/step-section';

import { ProfileFormState } from '../types';
import Companies from './companies';

interface HistorySectionProps {}

const HistorySection = (props: HistorySectionProps) => {
  const { setFieldValue } = useFormikContext<ProfileFormState>();
  const { list, onAddItem, onDeleteItem, replaceList } = useFormikListActions<
    ProfileFormState,
    CustomWorkHistory
  >({
    pathToList: modelPath<ProfileFormState>((m) => m.workHistory),
    getDefaultItem: () => ({
      id: Date.now(),
      positionTitle: '',
      companyName: '',
      worked: [null, null],
      workNow: false,
    }),
  });

  const changeWorkNow = (index: number) => (e: any, val: boolean) => {
    if (val) {
      const value = [...list[index].worked];
      value[1] = null;
      setFieldValue(
        modelPath<ProfileFormState>((m) => m.workHistory[index].worked),
        value,
      );
    }
  };

  const changeWorkPosition = useCallback(
    ({ index, toStart }: { index: number; toStart: boolean }) => {
      const result = moveToIndex(list, index, toStart ? index + 1 : index - 1);
      replaceList(result);
    },
    [list, replaceList],
  );

  return (
    <StepSection index={3} title="Work Experience">
      {list.map((history, index, arr) => (
        <div key={history?.id}>
          <Grid spacing={4} direction="column" container>
            <Grid alignItems="center" wrap="nowrap" spacing={2} item container>
              <Grid component={Box} flexGrow={1} item>
                <ConnectedTextField
                  // name={`work_history.${index}.position_title`}
                  name={modelPath<ProfileFormState>(
                    (m) => m.workHistory[index].positionTitle,
                  )}
                  fullWidth
                  variant="filled"
                  label="Position title"
                  size="small"
                />
              </Grid>
              <Grid item>
                <IconButton
                  onClick={() => changeWorkPosition({ toStart: true, index })}
                  disabled={index <= 0}
                  size="small"
                >
                  <ArrowUpwardIcon />
                </IconButton>
              </Grid>
              <Grid item>
                <IconButton
                  onClick={() => changeWorkPosition({ toStart: false, index })}
                  disabled={list.length === index + 1}
                  size="small"
                >
                  <ArrowDownwardIcon />
                </IconButton>
              </Grid>
              <Grid item>
                <IconButton
                  size="small"
                  disabled={arr.length <= 1}
                  onClick={() => onDeleteItem(index)}
                  aria-label="delete"
                >
                  <DeleteIcon />
                </IconButton>
              </Grid>
            </Grid>
            <Grid spacing={4} container item>
              <Grid xs={12} item>
                <ConnectedTextField
                  name={modelPath<ProfileFormState>(
                    (m) => m.workHistory[index].companyName,
                  )}
                  fullWidth
                  variant="filled"
                  label="Company name"
                  size="small"
                />
              </Grid>
            </Grid>
            <Grid spacing={4} item>
              <Grid spacing={4} container>
                <Grid xs={12} sm={6} item>
                  <ConnectedDatePicker
                    TextFieldProps={{
                      variant: 'filled',
                      size: 'small',
                      label: 'Start period',
                    }}
                    maxDate={new Date()}
                    inputFormat="dd/MM/yyyy"
                    helperText=""
                    name={modelPath<ProfileFormState>(
                      (m) => m.workHistory[index].worked[0],
                    )}
                  />
                </Grid>
                {!list[index]?.workNow && (
                  <Grid xs={12} sm={6} item>
                    <ConnectedDatePicker
                      TextFieldProps={{
                        variant: 'filled',
                        size: 'small',
                        label: 'End period',
                      }}
                      maxDate={new Date()}
                      inputFormat="dd/MM/yyyy"
                      helperText=""
                      name={modelPath<ProfileFormState>(
                        (m) => m.workHistory[index].worked[1],
                      )}
                    />
                  </Grid>
                )}
              </Grid>
            </Grid>
            <Grid spacing={4} container item>
              <Grid xs={12} item>
                <ConnectedCheckbox
                  name={modelPath<ProfileFormState>(
                    (m) => m.workHistory[index].workNow,
                  )}
                  label="I’m currently working here"
                  size="small"
                  onChange={changeWorkNow(index)}
                />
              </Grid>
            </Grid>
          </Grid>
          {index + 1 < arr.length && (
            <Box padding="20px 0">
              <Divider />
            </Box>
          )}
        </div>
      ))}
      <Box paddingTop="14px">
        <Button onClick={() => onAddItem()} startIcon={<AddIcon />}>
          Add another position
        </Button>
      </Box>
      <Box pt="14px">
        <Companies />
      </Box>
    </StepSection>
  );
};

export default HistorySection;
