import { useCallback } from 'react';
import useLocalStorage from 'react-use/lib/useLocalStorage';

export const useWelcomeModalProps = (talentId: string) => {
  const [value, setValue] = useLocalStorage(
    `welcome_modal_shown.talent_${talentId}`,
    false,
  );
  const handleClose = useCallback(() => {
    setValue(true);
  }, [setValue]);

  return { open: !value, handleClose };
};
