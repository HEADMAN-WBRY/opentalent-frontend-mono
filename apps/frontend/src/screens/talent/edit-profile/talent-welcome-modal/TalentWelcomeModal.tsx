import React from 'react';

import { DefaultModal } from '@libs/ui/components/modals';
import Typography from '@libs/ui/components/typography';

import { useWelcomeModalProps } from './hooks';

interface TalentWelcomeModalProps {
  talentId: string;
  isInvitationAccepted: boolean;
}

const TalentWelcomeModal = ({
  talentId,
  isInvitationAccepted,
}: TalentWelcomeModalProps) => {
  const { open, handleClose } = useWelcomeModalProps(talentId);
  return (
    <DefaultModal
      handleClose={handleClose}
      open={open && !isInvitationAccepted}
      title={
        <>
          Hi Awesome You <span role="img">🥳</span>
        </>
      }
      textAlign="start"
    >
      <br />
      <Typography paragraph>
        We&apos;re super excited to have you here!
      </Typography>
      <Typography paragraph>
        We wanted to let you know that you&apos;re one of the first people to
        join OpenTalent.
      </Typography>
      <Typography paragraph>
        As we&apos;re adding new people gradually, to make sure nothing breaks,
        we appreciate any feedback, tips or ideas you might have about improving
        the experience of our network of top freelancers and agencies across
        Europe.
      </Typography>
      <Typography paragraph>
        Go ahead by completing your profile so we can start the matchmaking
        process with awesome gigs as well as help you build your freelance
        business.
      </Typography>
      <Typography paragraph>
        <span role="img">🙏</span> The OpenTalent Team
      </Typography>
    </DefaultModal>
  );
};

export default TalentWelcomeModal;
