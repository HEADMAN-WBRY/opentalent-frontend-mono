import { ConnectedDropzone } from 'components/form/dropzone';
import React from 'react';
import { useDocumentsChange } from 'screens/talent/shared/profile/hooks';

import { Grid } from '@mui/material';

import { modelPath } from '@libs/helpers/form';
// import { ReactComponent as CheckIcon } from 'assets/icons/check.svg';
import ConnectedPhoneField from '@libs/ui/components/form/phone-field/ConnectedPhoneField';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import StepSection from '@libs/ui/components/step-section';
import Typography from '@libs/ui/components/typography';

import { ProfileFormState, RoutePropsType } from '../types';
import ServerFiles from './ServerFiles';

interface VerificationSectionProps extends RoutePropsType {
  talentId: string;
}

const VerificationSection = ({ talentId }: VerificationSectionProps) => {
  const { onDocumentsChange } = useDocumentsChange({
    name: modelPath<ProfileFormState>((m) => m.cv.documents),
    talentId,
  });

  return (
    <StepSection index={5} title="More information - we're almost there :)">
      <Grid spacing={4} direction="column" container>
        <Grid item>
          <Typography variant="body2">
            Enter your social media profiles and contact details
          </Typography>
        </Grid>
        <Grid item spacing={4} container>
          <Grid sm={6} xs={12} item>
            <ConnectedTextField
              // name="account_info.linkedin_profile_link"
              name={modelPath<ProfileFormState>((m) => m.profile.linkedLink)}
              size="small"
              fullWidth
              variant="filled"
              label="LinkedIn profile link"
              // InputProps={{
              //   endAdornment: (
              //     <CheckIcon color={theme.palette.text.secondary} />
              //   ),
              // }}
            />
          </Grid>
          <Grid xs={12} sm={6} item>
            <ConnectedPhoneField
              name={modelPath<ProfileFormState>((m) => m.profile.phone)}
              size="small"
              fullWidth
              defaultCountry="nl"
              variant="filled"
              label="Phone"
              // InputProps={{
              //   endAdornment: <CheckIcon color={theme.palette.success.main} />,
              // }}
            />
          </Grid>
        </Grid>
        <Grid item>
          <Typography variant="body2">
            Upload documents (a CV, Cover Letter, or a work portfolio can make
            your profile shine!)
          </Typography>
        </Grid>
        <Grid item>
          <ConnectedDropzone
            name={modelPath<ProfileFormState>((m) => m.cv.documents)}
            withoutList
            onChange={onDocumentsChange}
            dropzoneOptions={{
              accept: 'application/pdf',
            }}
          />
          <ServerFiles
            name={modelPath<ProfileFormState>((m) => m.cv.documents)}
          />
        </Grid>
        <Grid item>
          <Typography variant="body2">Value Adding Tax (VAT) number</Typography>
        </Grid>
        <Grid item>
          <ConnectedTextField
            fullWidth
            name={modelPath<ProfileFormState>((m) => m.profile.vat)}
            size="small"
            variant="filled"
            label="Enter your European Union VAT number"
          />
        </Grid>
      </Grid>
    </StepSection>
  );
};

export default VerificationSection;
