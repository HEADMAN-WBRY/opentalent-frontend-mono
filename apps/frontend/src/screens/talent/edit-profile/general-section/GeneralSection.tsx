import { SubCategorySelector } from 'components/custom/subcategory-selector';
import ConnectedImageUpload from 'components/form/image-upload/ConnectedImageUpload';
import { TALENT_CATEGORIES } from 'graphql/talents';
import React, { useState } from 'react';
import { useAvatarChange } from 'screens/talent/shared/profile/hooks';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { Query, TalentCategory } from '@libs/graphql-types';
import { COUNTRY_OPTIONS } from '@libs/helpers/consts/countries';
import { modelPath } from '@libs/helpers/form';
import {
  ConnectedGraphSelect,
  ConnectedSelect,
} from '@libs/ui/components/form/select';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import StepSection from '@libs/ui/components/step-section';
import Typography from '@libs/ui/components/typography';

import { ABOUT_LENGTH_LIMIT } from '../consts';
import { ProfileFormState } from '../types';

interface GeneralSectionProps {
  values: ProfileFormState;
}

const useStyles = makeStyles((theme) => ({
  subcategory: {
    '&:empty': {
      display: 'none',
    },
  },
}));

const GeneralSection: React.FC<GeneralSectionProps> = ({ values }) => {
  const classes = useStyles();
  const [categories, setCategories] = useState<TalentCategory[]>([]);
  const talentId = values.talentData?.id;
  const hashPath = modelPath<ProfileFormState>((m) => m.picture.hash);
  const chosenFile = values.picture.files?.[0];
  const aboutLength = values.describe.about.length;
  const initialAvatar =
    (chosenFile && URL.createObjectURL(chosenFile)) || values.picture.avatar;
  const { onAvatarChange } = useAvatarChange({
    name: hashPath,
    avatarPath: '',
    talentId,
  });

  return (
    <StepSection index={1} title="Name and general info">
      <Grid spacing={4} direction="column" container>
        <Grid item>
          <Box paddingBottom="26px">
            <ConnectedImageUpload
              name={modelPath<ProfileFormState>((m) => m.picture.files)}
              initialImage={initialAvatar}
              onChange={onAvatarChange}
              label="Upload avatar"
            />
          </Box>
        </Grid>
        <Grid item>
          <Grid spacing={4} container>
            <Grid xs={12} sm={6} item>
              <ConnectedTextField
                size="small"
                name={modelPath<ProfileFormState>((m) => m.profile.firstName)}
                fullWidth
                variant="filled"
                label="First name"
              />
            </Grid>
            <Grid xs={12} sm={6} item>
              <ConnectedTextField
                name={modelPath<ProfileFormState>((m) => m.profile.lastName)}
                size="small"
                fullWidth
                variant="filled"
                label="Last name"
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <ConnectedSelect
            name={modelPath<ProfileFormState>((m) => m.profile.location)}
            fullWidth
            variant="filled"
            label="Country"
            options={COUNTRY_OPTIONS}
          />
        </Grid>
        <Grid item>
          <ConnectedTextField
            name={modelPath<ProfileFormState>((m) => m.profile.position)}
            fullWidth
            size="small"
            variant="filled"
            label="Personal Tagline"
            helperText="Describe yourself in max. 40 characters"
          />
        </Grid>
        <Grid item>
          <ConnectedTextField
            name={modelPath<ProfileFormState>((m) => m.describe.about)}
            fullWidth
            size="small"
            variant="filled"
            label="About"
            multiline
            rows={4}
          />
          <Box textAlign="right">
            <Typography variant="caption" color="textSecondary">
              {`${aboutLength} of ${ABOUT_LENGTH_LIMIT}`}
            </Typography>
          </Box>
        </Grid>
        <Grid item>
          <Grid spacing={4} container>
            <Grid xs={12} sm={6} item>
              <ConnectedTextField
                name={modelPath<ProfileFormState>((m) => m.rate.min)}
                fullWidth
                size="small"
                variant="filled"
                label="Min rate, €/h"
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <ConnectedGraphSelect
            query={TALENT_CATEGORIES}
            queryOptions={{
              fetchPolicy: 'network-only',
              onCompleted: (query: Query) =>
                setCategories((query?.talentCategories as any[]) || []),
            }}
            dataPath="talentCategories"
            dataMap={{ text: 'name', value: 'id' }}
            formControlProps={{ size: 'small' }}
            fullWidth
            name={modelPath<ProfileFormState>((m) => m.profile.category)}
            variant="filled"
            label="Category"
          />
        </Grid>
        <Grid className={classes.subcategory} item>
          <SubCategorySelector
            categoryPath={modelPath<ProfileFormState>(
              (m) => m.profile.category,
            )}
            name={modelPath<ProfileFormState>((m) => m.profile.subcategories)}
            categories={categories}
            chipProps={{
              color: 'tertiary',
              size: 'small',
            }}
            autoCompleteProps={{
              filterSelectedOptions: true,
              popupIcon: null,
            }}
            inputProps={{
              variant: 'filled',
              label: 'Select sub-category',
            }}
          />
        </Grid>
        {/* <Grid item>
          <ConnectedGraphSelect
            query={TALENT_SOURCES}
            dataMap={{ text: 'name', value: 'id' }}
            dataPath="talentSources"
            formControlProps={{ size: 'small' }}
            fullWidth
            name="general_info.talent_source_id"
            variant="filled"
            label="Source"
            disabled
          />
        </Grid> */}
      </Grid>
    </StepSection>
  );
};

export default GeneralSection;
