import { MAX_DATE } from 'consts/date';
import React from 'react';

import { Box, Grid } from '@mui/material';
import Collapse from '@mui/material/Collapse';

import { modelPath } from '@libs/helpers/form';
import { ConnectedDatePicker } from '@libs/ui/components/form/datepicker';
import { ConnectedRadioGroup } from '@libs/ui/components/form/radio/Radio';
import { OptionType } from '@libs/ui/components/form/select';
import StepSection from '@libs/ui/components/step-section';
import Typography from '@libs/ui/components/typography';

import { ProfileFormState, AvailabilityType } from '../types';

interface AvailabilitySectionProps {
  availableNow?: AvailabilityType;
}

const AVAILABILITY_OPTIONS = [
  { value: AvailabilityType.Now, text: 'Now' },
  { value: AvailabilityType.Later, text: 'Later' },
];

const HOURS_OPTIONS: OptionType[] = Array.from({ length: 4 }).map((_, i) => {
  const value = String(50 - (i + 1) * 10);
  return {
    value,
    text: `${value} hrs/week`,
  };
});

const AvailabilitySection = ({ availableNow }: AvailabilitySectionProps) => {
  return (
    <StepSection index={2} title="Availability">
      <Typography variant="body2">When will you be available?</Typography>
      <Box pt={4}>
        <Grid spacing={4} container direction="column" wrap="nowrap">
          <Grid item>
            <ConnectedRadioGroup
              name={modelPath<ProfileFormState>(
                (m) => m.availability.availableNow,
              )}
              options={AVAILABILITY_OPTIONS as any}
            />
          </Grid>
          <Collapse in={availableNow === AvailabilityType.Later}>
            <Box padding={2}>
              <ConnectedDatePicker
                inputFormat="dd/MM/yyyy"
                name={modelPath<ProfileFormState>(
                  (m) => m.availability.availableDate,
                )}
                minDate={new Date()}
                maxDate={MAX_DATE}
                TextFieldProps={{
                  variant: 'filled',
                  size: 'small',
                  fullWidth: true,
                  label: 'Available date',
                }}
              />
            </Box>
          </Collapse>
          <Collapse in={availableNow !== undefined}>
            <Box padding={2}>
              <ConnectedRadioGroup
                options={HOURS_OPTIONS}
                name={modelPath<ProfileFormState>(
                  (m) => m.availability.hoursPerWeek,
                )}
              />
            </Box>
          </Collapse>
        </Grid>
      </Box>
    </StepSection>
  );
};

export default AvailabilitySection;
