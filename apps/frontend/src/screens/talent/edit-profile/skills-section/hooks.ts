import { useLazyQuery, useMutation } from '@apollo/client';
import { debounce } from '@mui/material';
import { INPUT_SUGGESTION_LENGTH, SOLUTIONS_WITH_HARD } from 'consts/skills';
import { useFormikContext } from 'formik';
import {
  SUGGEST_SKILL,
  SEARCH_SKILL,
  CHECK_SKILL_EXISTS,
} from 'graphql/skills';
import { useSnackbar } from 'notistack';
import { useCallback, useMemo, useState } from 'react';

import {
  Mutation,
  MutationSuggestSkillArgs,
  Query,
  QuerySkillSearchArgs,
  Skill,
  SkillTypeEnum,
} from '@libs/graphql-types';
import { OptionType } from '@libs/ui/components/form/select';

export type AllSkillsEnum = typeof SOLUTIONS_WITH_HARD | SkillTypeEnum;

const useHandleFieldChange = () => {
  const { setFieldValue, getFieldProps } = useFormikContext();
  return useCallback(
    ({ skillType, skill }: { skillType: AllSkillsEnum; skill: Skill }) => {
      const fieldPath = `skills.${skillType}`;
      const { value = [] } = getFieldProps<OptionType[]>(fieldPath);
      const isDuplicate = value.some((opt) => opt.value === skill.id);

      if (isDuplicate) {
        return;
      }

      setFieldValue(
        fieldPath,
        (value as any).concat({
          text: skill?.name,
          value: skill?.id,
          skill_type: skill?.skill_type,
        }),
      );
    },
    [getFieldProps, setFieldValue],
  );
};

const useSkillsSuggestion = ({
  activeSkillType,
}: {
  activeSkillType: AllSkillsEnum | null | undefined;
}) => {
  const [getSkills, { loading, data, variables: suggestVariables }] =
    useLazyQuery<Query, QuerySkillSearchArgs>(SEARCH_SKILL, {
      fetchPolicy: 'network-only',
    });
  const getSkillsSuggest = useMemo(() => debounce(getSkills, 300), [getSkills]);
  const skillsSuggest: OptionType[] = useMemo(() => {
    const currentSuggest = (data?.skillSearch ?? [])?.map(
      ({ id, name, skill_type }) => ({
        value: id,
        text: name,
        skill_type,
      }),
    );

    if (!activeSkillType) {
      return [];
    }

    if (activeSkillType === SOLUTIONS_WITH_HARD) {
      return suggestVariables?.skill_type?.some((i) =>
        [SkillTypeEnum.HardSkills, SkillTypeEnum.Solutions].includes(i as any),
      )
        ? currentSuggest
        : [];
    }

    return suggestVariables?.skill_type?.includes(activeSkillType)
      ? currentSuggest
      : [];
  }, [data, suggestVariables, activeSkillType]);

  return { getSkillsSuggest, skillsSuggest, skillsLoading: loading };
};

const useSuggestSkillAction = () => {
  const [suggestSkill] = useMutation<Mutation, MutationSuggestSkillArgs>(
    SUGGEST_SKILL,
  );

  return useCallback(
    async (name: string, skillType: SkillTypeEnum, reason?: string) => {
      const data = await suggestSkill({
        variables: { name, skill_type: skillType, reason },
      });
      return data?.data?.suggestSkill;
    },
    [suggestSkill],
  );
};

interface IInitialogGialog {
  name: string;
  skill_type: SkillTypeEnum;
  reason?: string;
}

const getMergedSkillType = (type: SkillTypeEnum): AllSkillsEnum => {
  if (
    [SkillTypeEnum.HardSkills, SkillTypeEnum.Solutions].includes(type as any)
  ) {
    return SOLUTIONS_WITH_HARD;
  }
  return type;
};

const useCreateSkillDialog = (
  setInputValue: (value: string) => void,
  inputValue?: string,
  activeSkillType?: AllSkillsEnum | null,
) => {
  const suggestSkill = useSuggestSkillAction();
  const changeField = useHandleFieldChange();
  const { enqueueSnackbar } = useSnackbar();
  const [dialogOpened, setDialogOpened] = useState(false);
  const openCreateSkillDialog = useCallback(() => {
    setDialogOpened(true);
    setInitialSkillDialogValues({
      skill_type: (activeSkillType === 'SOLUTIONS_WITH_HARD'
        ? SkillTypeEnum.Solutions
        : activeSkillType) as never,
      name: inputValue || '',
      reason: '',
    });
  }, [activeSkillType, inputValue]);

  const closeCreateSkillDialog = useCallback(() => {
    setDialogOpened(false);
  }, []);

  const [initialSkillDialogValues, setInitialSkillDialogValues] =
    useState<IInitialogGialog>({
      skill_type: SkillTypeEnum.Solutions,
      name: '',
      reason: '',
    });

  const [checkSkillExists] = useLazyQuery<Query>(CHECK_SKILL_EXISTS, {
    variables: { skill_type: activeSkillType, name: '' },
    onCompleted: async (data: any) => {
      const skill = data?.skillExists;
      const finalSkillType = getMergedSkillType(
        initialSkillDialogValues.skill_type,
      );

      if (!skill) {
        const newSkill = await suggestSkill(
          initialSkillDialogValues.name || '',
          initialSkillDialogValues.skill_type as SkillTypeEnum,
          initialSkillDialogValues.reason,
        );
        setDialogOpened(false);
        enqueueSnackbar(
          `Skill "${initialSkillDialogValues.name}" created successfully!`,
          { variant: 'success' },
        );
        setInputValue('');
        changeField({
          skill: newSkill as Skill,
          skillType: finalSkillType as AllSkillsEnum,
        });
      } else {
        changeField({
          skillType: finalSkillType as AllSkillsEnum,
          skill,
        });
      }
    },
  });

  const onSubmit = useCallback(
    (values: { name: string; skill_type: SkillTypeEnum; reason?: string }) => {
      setInitialSkillDialogValues(values);
      console.log('🚀 ~ file: hooks.ts ~ line 189 ~ values', values);
      checkSkillExists({
        variables: { skill_type: values.skill_type, name: values.name },
      });
    },
    [checkSkillExists],
  );

  return {
    dialogOpened,
    openCreateSkillDialog,
    closeCreateSkillDialog,
    initialSkillDialogValues,
    onSubmitSkillDialog: onSubmit,
  };
};

export const useAddSkill = () => {
  const [inputValue, setInputValue] = useState<string>();
  const [activeSkillType, setActiveTypeSkill] = useState<
    AllSkillsEnum | null | undefined
  >();

  const { getSkillsSuggest, skillsSuggest, skillsLoading } =
    useSkillsSuggestion({
      activeSkillType,
    });

  const onInputBlur = useCallback(() => {
    setActiveTypeSkill(null);
    setInputValue(undefined);
  }, []);
  const onInputFocus = (type: AllSkillsEnum) => () => {
    setActiveTypeSkill(type);
  };
  const onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(e?.target?.value);
    if (e?.target?.value.length > INPUT_SUGGESTION_LENGTH) {
      // допилить
      if (activeSkillType !== SOLUTIONS_WITH_HARD) {
        getSkillsSuggest({
          variables: {
            search: e.target.value,
            skill_type: [activeSkillType] as SkillTypeEnum[],
          },
        });
      } else {
        getSkillsSuggest({
          variables: {
            search: e.target.value,
            skill_type: [SkillTypeEnum.Solutions, SkillTypeEnum.HardSkills],
          },
        });
      }
    }
  };

  const onSelectSkill = () => {
    setInputValue('');
  };

  const {
    dialogOpened,
    openCreateSkillDialog,
    closeCreateSkillDialog,
    initialSkillDialogValues,
    onSubmitSkillDialog,
  } = useCreateSkillDialog(setInputValue, inputValue, activeSkillType);

  return {
    onInputBlur,
    onInputFocus,
    onInputChange,
    skillsSuggest,
    skillsLoading,
    onSelectSkill,
    inputValue,
    openCreateSkillDialog,
    closeCreateSkillDialog,
    initialSkillDialogValues,
    dialogOpened,
    onSubmitSkillDialog,
  };
};
