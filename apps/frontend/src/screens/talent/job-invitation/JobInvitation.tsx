import { Box, Grid } from '@mui/material';
import { ConnectedPageLayout } from 'components/layout/page-layout';
import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';
import { Link } from 'react-router-dom';
import { pathManager } from 'routes';

import { Job } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import JobInfo from '../job-apply/job-info';
import DeclineModal from './DeclineModal';
import ClientMessage from './client-message';
import { useScreenData } from './hooks';
import { ScreenProps } from './types';

const JobBoard: React.FC<ScreenProps> = ({ match }) => {
  const { isXS } = useMediaQueries();
  const { jobInvitation, isLoading } = useScreenData({ id: match.params.id });
  const job = jobInvitation?.job;

  return (
    <ConnectedPageLayout
      documentTitle="Job board"
      drawerProps={{}}
      headerProps={{ accountProps: {} }}
      contentSpacing={isXS ? 0 : undefined}
      isLoading={isLoading}
    >
      <Box maxWidth="768px" pt={4}>
        <Typography variant="h5">
          Invitation to apply for a job: {job?.name}
        </Typography>

        <Box pb={2}>
          <JobInfo job={job as Required<Job>} />
        </Box>

        <Box pb={6}>
          <ClientMessage message={jobInvitation?.message} />
        </Box>

        <Grid spacing={6} container>
          <Grid item>
            <Link
              to={
                job
                  ? pathManager.talent.jobApply.generatePath({
                      id: job?.id || '',
                    })
                  : ''
              }
            >
              <Button data-test-id="accept" color="primary" variant="contained">
                ACCEPT INVITATION
              </Button>
            </Link>
          </Grid>
          <Grid item>
            <DeclineModal matchId={match.params.id} />
          </Grid>
        </Grid>
      </Box>
    </ConnectedPageLayout>
  );
};

export default JobBoard;
