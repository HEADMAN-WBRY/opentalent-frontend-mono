import React from 'react';

const JobInvitation = React.lazy(() => import('./JobInvitation'));

export default JobInvitation;
