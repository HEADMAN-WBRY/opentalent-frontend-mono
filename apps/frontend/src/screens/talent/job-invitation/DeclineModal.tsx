import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Formik } from 'formik';
import React, { useState } from 'react';

import Button from '@libs/ui/components/button';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import { DefaultModal } from '@libs/ui/components/modals';
import Typography from '@libs/ui/components/typography';

import { useHandleDecline } from './hooks';

const useStyles = makeStyles((theme) => ({
  dialogContent: {
    padding: 0,
    width: '100%',
  },
  confirmBtn: {
    backgroundColor: theme.palette.warning.main,

    '&:hover': {
      backgroundColor: theme.palette.warning.light,
    },
  },
  dialogDesc: {
    marginBottom: theme.spacing(4),
  },
}));

const useDecline = (id: string) => {
  const [isDialogOpened, setIsDalogOpened] = useState(false);

  const closeDialog = () => {
    setIsDalogOpened(false);
  };

  const openDialog = () => {
    setIsDalogOpened(true);
  };

  const { declineInvitation, declineInProgress } = useHandleDecline({
    id,
  });

  const decline = ({ reason }: { reason: string }) => {
    declineInvitation({ variables: { job_invitation_id: id, reason } });
  };

  return {
    isDialogOpened,
    closeDialog,
    openDialog,
    declineInvitation: decline,
    declineInProgress,
  };
};

const DeclineModal: React.FC<{ matchId: string }> = ({ matchId }) => {
  const classes = useStyles();

  const {
    isDialogOpened,
    closeDialog,
    openDialog,
    declineInvitation,
    declineInProgress,
  } = useDecline(matchId);

  return (
    <>
      <Button
        data-test-id="decline"
        onClick={openDialog}
        disabled={declineInProgress}
        variant="outlined"
        color="secondary"
      >
        DECLINE INVITATION
      </Button>
      <Formik onSubmit={declineInvitation} initialValues={{ reason: '' }}>
        {({ isValid, handleSubmit }) => {
          return (
            <DefaultModal
              actions={
                <Button
                  className={classes.confirmBtn}
                  fullWidth
                  color="primary"
                  variant="contained"
                  disabled={!isValid}
                  onClick={handleSubmit}
                >
                  Decline
                </Button>
              }
              title="You are going to decline an invitation"
              open={isDialogOpened}
              handleClose={closeDialog}
            >
              <Box className={classes.dialogContent}>
                <Typography className={classes.dialogDesc}>
                  Please confirm and write your reason
                </Typography>
                <ConnectedTextField
                  name="reason"
                  label="Why are you declining the invitation?"
                  variant="filled"
                  multiline
                  rows={5}
                  fullWidth
                  size="small"
                />
              </Box>
            </DefaultModal>
          );
        }}
      </Formik>
    </>
  );
};

export default DeclineModal;
