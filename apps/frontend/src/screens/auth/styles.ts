import { makeStyles } from '@mui/styles';

import { Theme } from '@libs/ui/themes/default';

const useStyles = makeStyles((theme: Theme) => ({
  title: {
    [theme.breakpoints.down('xl')]: {
      fontSize: 42,
      lineHeight: '50px',
    },

    [theme.breakpoints.down('sm')]: {
      fontSize: 36,
      lineHeight: '42px',
    },
  },
  subtitle: {
    paddingTop: 32,

    [theme.breakpoints.down('sm')]: {
      paddingTop: 24,
    },
  },
  infoColumn: {
    display: 'flex',
    flexDirection: 'column',
  },
  formColumn: {
    [theme.breakpoints.down('sm')]: {
      marginTop: theme.spacing(8),
    },
  },
  illustration: {
    width: '100%',
    maxWidth: 550,
  },
  illustrationBlock: {
    marginTop: '6vw',

    [theme.breakpoints.down('sm')]: {
      order: -1,
      marginTop: 0,
      marginBottom: theme.spacing(6),
    },
  },
  container: {
    flex: 1,
    display: 'flex',
  },
  gridContainer: {
    flex: 1,

    [theme.breakpoints.down('sm')]: {
      alignItems: 'flex-start',
      alignContent: 'flex-start',
    },
  },
  form: {
    padding: '40px 24px',
  },
  formTitle: {
    marginBottom: theme.spacing(4),
  },
  formButton: {},
}));

export default useStyles;
