import { gql } from '@apollo/client';

export const GET_CURRENT_TALENT_INVITATION_STATUS = gql`
  query GetCurrentTalent {
    currentTalent {
      is_invitation_accepted
    }
  }
`;
