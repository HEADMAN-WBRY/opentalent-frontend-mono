import React, { useEffect } from 'react';

import { useAuth0 } from 'hooks/auth/useAuth0';

import { AppLoadingScreen } from './helpers';

const Logout = () => {
  const { logout } = useAuth0();

  useEffect(() => {
    logout({ federated: true, localOnly: false });
  }, [logout]);

  return <AppLoadingScreen />;
};

export default Logout;
