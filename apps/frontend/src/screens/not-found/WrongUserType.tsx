import { Box } from '@mui/material';
import { ConnectedPageLayout } from 'components/layout/page-layout';
import React from 'react';

import Typography from '@libs/ui/components/typography';

export const WrongUserType = () => {
  return (
    <ConnectedPageLayout
      documentTitle="Forbidden"
      headerProps={{ accountProps: {} }}
      drawerProps={{}}
    >
      <Box pt="60px">
        <Typography variant="h1">403.</Typography>
        <Typography variant="h3">Forbidden</Typography>
      </Box>
    </ConnectedPageLayout>
  );
};
