import React from 'react';

import predicates from 'auth0/predicates';
import PrivateRoute from 'routes/PrivateRoute';
import { pathManager } from 'routes/consts';
import LazyProfile from 'screens/profile';
import EditProfile from 'screens/talent/edit-profile';
import Groups from 'screens/talent/groups';
import InviteByTalent from 'screens/talent/invite-by-talent';
import JobApply from 'screens/talent/job-apply';
import TalentJobBoardV2 from 'screens/talent/job-board-v2';
import TalentJobInvitation from 'screens/talent/job-invitation';
import MarkedJobs from 'screens/talent/marked-jobs';
import TalentSettings from 'screens/talent/settings';

export const TALENT_ROUTES = [
  <PrivateRoute
    predicate={predicates.isTalent}
    exact
    component={TalentJobBoardV2}
    path={pathManager.talent.jobBoard.getRoute()}
    key={pathManager.talent.jobBoard.getRoute()}
  />,
  <PrivateRoute
    component={JobApply}
    predicate={predicates.isTalent}
    path={pathManager.talent.jobApply.getRoute()}
    key={pathManager.talent.jobApply.getRoute()}
  />,
  <PrivateRoute
    component={MarkedJobs}
    predicate={predicates.isTalent}
    path={[pathManager.talent.markedJobs.getRoute()]}
    key={pathManager.talent.markedJobs.getRoute()}
  />,
  <PrivateRoute
    component={TalentJobInvitation}
    predicate={predicates.isTalent}
    path={pathManager.talent.jobInvitation.getRoute()}
    key={pathManager.talent.jobInvitation.getRoute()}
  />,
  <PrivateRoute
    exact
    predicate={predicates.isTalent}
    path={pathManager.talent.editProfile.getRoute()}
    key={pathManager.talent.editProfile.getRoute()}
    component={EditProfile}
  />,
  <PrivateRoute
    component={LazyProfile}
    predicate={predicates.isTalent}
    exact
    path={pathManager.talent.profile.getRoute()}
    key={pathManager.talent.profile.getRoute()}
  />,
  <PrivateRoute
    component={InviteByTalent}
    predicate={predicates.isTalent}
    exact
    path={pathManager.talent.invite.getRoute()}
    key={pathManager.talent.invite.getRoute()}
  />,
  <PrivateRoute
    component={Groups}
    predicate={predicates.isTalent}
    path={pathManager.talent.groups.getRoute()}
    key={pathManager.talent.groups.getRoute()}
  />,
  <PrivateRoute
    component={TalentSettings}
    predicate={predicates.isTalent}
    path={pathManager.talent.settings.main.getRoute()}
    key={pathManager.talent.settings.main.getRoute()}
  />,
];
