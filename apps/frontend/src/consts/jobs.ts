import { JobLocationTypeEnum } from '@libs/graphql-types';
import { OptionType } from '@libs/ui/components/form/select';

export const MAX_JOB_RATE = 300;
export const MIN_JOB_RATE = 25;

export const JOB_CAPACITY_OPTIONS: OptionType[] = [10, 20, 30, 40].map(
  (value) => ({
    text: `${value}  hrs/week`,
    value,
  }),
);

export const LOCATION_TYPE_OPTIONS: OptionType[] = [
  { value: JobLocationTypeEnum.Remote, text: 'Remote (in Europe)' },
  { value: JobLocationTypeEnum.Hybrid, text: 'Hybrid' },
  { value: JobLocationTypeEnum.OnSite, text: 'On-site' },
];

export const JOB_FINDERS_FEE = {
  min: 1000,
  max: 15000,
};
