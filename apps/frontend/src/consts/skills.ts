import { SkillTypeEnum } from '@libs/graphql-types';

export const SOLUTIONS_WITH_HARD = 'SOLUTIONS_WITH_HARD' as const;

export const SKILLS_LIMITS = {
  [SkillTypeEnum.SoftSkills]: {
    min: 2,
    max: 6,
  },
  [SOLUTIONS_WITH_HARD]: {
    max: 15,
    min: 5,
  },
};

export const INPUT_SUGGESTION_LENGTH = 1;
