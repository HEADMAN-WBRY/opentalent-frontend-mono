import { addYears } from 'date-fns';

export const DEFAULT_DATE_SCHEME = 'dd/MM/yyyy';
export const MAX_DATE = addYears(new Date(), 30);
