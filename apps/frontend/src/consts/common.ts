import defaultAvatarPath from 'assets/images/default-avatar.svg';

import {
  JobOrderByColumn,
  JobLocationTypeEnum,
  JobTypeEnum,
  JobMatchDeclinationReasonsEnum,
  SkillTypeEnum,
} from '@libs/graphql-types';
import { OptionType } from '@libs/ui/components/form/select';

export const DEFAULT_AVATAR = defaultAvatarPath;

export const SKILL_TYPES_MAP = {
  [SkillTypeEnum.HardSkills]: 'Hard Skills',
  [SkillTypeEnum.Solutions]: 'Solutions',
  [SkillTypeEnum.SoftSkills]: 'Soft Skills',
};

export const JOB_TYPE_MAP = {
  [JobLocationTypeEnum.OnSite]: 'On-site',
  [JobLocationTypeEnum.Remote]: 'Remote',
  [JobLocationTypeEnum.Hybrid]: 'Hybrid',
};

export const JOB_ORDER_BY_COLUMN_MAP = {
  [JobOrderByColumn.CampaignStart]: 'Latest campaign',
  [JobOrderByColumn.RateMax]: 'Highest rate',
};
export const JOB_ORDER_BY_COLUMN_OPTIONS: OptionType[] = Object.entries(
  JOB_ORDER_BY_COLUMN_MAP,
).map(([value, text]) => ({ value, text }));

export const JOB_TYPES_LABELS_MAP = {
  [JobTypeEnum.Freelance]: 'Freelance job',
  [JobTypeEnum.Permanent]: 'Permanent role',
  [JobTypeEnum.Project]: 'Project',
};

export const JOB_MATCH_DECLINE_REASONS = {
  [JobMatchDeclinationReasonsEnum.None]: 'None',
  [JobMatchDeclinationReasonsEnum.NotEnoughSkills]: 'Not enough skills',
};

export const INFINITY_SIGN = '∞';
