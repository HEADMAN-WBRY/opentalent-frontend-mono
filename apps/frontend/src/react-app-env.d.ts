/// <reference types="react-scripts" />
declare namespace NodeJS {
  interface ProcessEnv {
    REACT_APP_GQL_PATH: string;
    REACT_APP_BACKEND_DOMAIN: string;
    REACT_APP_STREAM_CHAT_KEY: string;
    REACT_APP_SENTRY_DSN?: string;
  }
}
