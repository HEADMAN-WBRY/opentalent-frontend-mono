import { JobMatchTypeEnum } from '@libs/graphql-types';

export const JOB_MATCH_TYPES_LABELS: Record<JobMatchTypeEnum, string> = {
  [JobMatchTypeEnum.TalentApplication]: 'Applied',
  [JobMatchTypeEnum.InstantMatch]: 'Instant match',
  [JobMatchTypeEnum.OpentalentSuggestion]: 'Opentalent suggestion',
  [JobMatchTypeEnum.Invited]: 'Invited',
  [JobMatchTypeEnum.Intake]: 'Intake',
  [JobMatchTypeEnum.Hired]: 'Hired',
  [JobMatchTypeEnum.Rejected]: 'Rejected',
  [JobMatchTypeEnum.Withdrawn]: 'Withdrawn',
};
