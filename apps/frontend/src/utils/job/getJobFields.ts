import { JOB_TYPES_LABELS_MAP, JOB_TYPE_MAP } from 'consts/common';

import { Job, JobLocationTypeEnum, JobTypeEnum } from '@libs/graphql-types';

import { formatDate, formatRate, getJobCapacity } from './common';

interface JobFieldOption {
  value: string;
  id: string;
  label?: string;
  hint?: string;
}

export enum JobFieldType {
  Rate = 'rate',
  StartDate = 'start_date',
  EndDate = 'end_date',
  Capacity = 'capacity',
  Type = 'type',
  Client = 'client',
  Category = 'category',
  Location = 'location',
  LocationType = 'location_type',
  Fee = 'fee',
  Salary = 'salary',
}

const getRateFromJob = (job: Job): JobFieldOption => ({
  id: JobFieldType.Rate,
  value:
    formatRate({
      min: job?.rate_min,
      max: job?.rate_max,
      isNegotiable: job.is_rate_negotiable,
    }) || '',
  label: 'Rate:',
});
const getJobStartDate = (job: Job): JobFieldOption => ({
  id: 'start_date' as const,
  value: formatDate(job.start_date) || '',
  label: 'Starting date:',
});
const getJobEndDate = (job: Job): JobFieldOption => ({
  id: JobFieldType.EndDate,
  value: formatDate(job.end_date) || '',
  label: 'End date:',
});
const getJobCapacityField = (job: Job): JobFieldOption => ({
  label: 'Capacity:',
  value: getJobCapacity(job) || '',
  id: JobFieldType.Capacity,
});
const getJobType = (job: Job): JobFieldOption => ({
  label: 'Type:',
  value: JOB_TYPES_LABELS_MAP[job?.type || JobTypeEnum.Freelance],
  id: JobFieldType.Type,
});
const getJobClient = (job: Job): JobFieldOption => ({
  label: 'Client:',
  value: job.client || '',
  id: JobFieldType.Client,
});
const getJobCategory = (job: Job): JobFieldOption => ({
  label: 'Category:',
  id: JobFieldType.Category,
  value: job.category?.name || '',
});
const getJobLocation = (job: Job): JobFieldOption => {
  const countryAndCity = job.country;
  const locationType = job.location_type!;
  const locationTypeText = JOB_TYPE_MAP[locationType];
  const isRemote = locationType === JobLocationTypeEnum.Remote;

  const finalString = isRemote
    ? locationTypeText
    : [locationTypeText, countryAndCity].filter(Boolean).join(' in ');

  return {
    label: 'Location:',
    id: JobFieldType.Location,
    value: finalString,
  };
};
// const getJobLocationType = (job: Job): JobFieldOption => ({
//   label: 'Location type:',
//   id: JobFieldType.LocationType,
//   value: job.location_type ? JOB_TYPE_MAP[job.location_type] : '',
// });
const getJobFindersFee = (job: Job): JobFieldOption => ({
  label: 'Finder’s fee:',
  id: JobFieldType.Fee,
  value:
    formatRate({
      min: job?.finders_fee,
      period: job.type === JobTypeEnum.Permanent ? 'month' : 'hour',
    }) || '',
  hint: 'This means that this job includes a reward for recommendation of suitable candidates',
});
const getJobSalary = (job: Job): JobFieldOption => ({
  label: 'Salary:',
  id: JobFieldType.Salary,
  value:
    formatRate({
      min: job?.salary_min,
      max: job?.salary_max,
      isNegotiable: job.is_salary_negotiable,
      period: job.type === JobTypeEnum.Permanent ? 'month' : 'hour',
    }) || '',
});

const getAllJobFields = (job: Job): Record<string, JobFieldOption> => {
  return {
    [JobFieldType.Rate]: getRateFromJob(job),
    [JobFieldType.Client]: getJobClient(job),
    [JobFieldType.Type]: getJobType(job),
    [JobFieldType.Capacity]: getJobCapacityField(job),
    [JobFieldType.StartDate]: getJobStartDate(job),
    [JobFieldType.EndDate]: getJobEndDate(job),
    [JobFieldType.Category]: getJobCategory(job),
    // [JobFieldType.LocationType]: getJobLocationType(job),
    [JobFieldType.Location]: getJobLocation(job),
    [JobFieldType.Fee]: getJobFindersFee(job),
    [JobFieldType.Salary]: getJobSalary(job),
  };
};

const DEFAULT_TALENT_FIELDS = [
  JobFieldType.Rate,
  JobFieldType.StartDate,
  JobFieldType.EndDate,
  JobFieldType.Capacity,
  JobFieldType.Location,
  JobFieldType.Client,
];
const TALENT_FIELDS = {
  [JobTypeEnum.Permanent]: [
    JobFieldType.Salary,
    JobFieldType.StartDate,
    JobFieldType.EndDate,
    JobFieldType.Capacity,
    JobFieldType.Location,
    JobFieldType.Client,
  ],
  [JobTypeEnum.Freelance]: DEFAULT_TALENT_FIELDS,
  [JobTypeEnum.Project]: DEFAULT_TALENT_FIELDS,
};

const DEFAULT_COMPANY_FIELDS = [
  JobFieldType.Rate,
  JobFieldType.StartDate,
  JobFieldType.EndDate,
  JobFieldType.Capacity,
  JobFieldType.Type,
  JobFieldType.Client,
  // JobFieldType.Category,
  JobFieldType.Location,
];
const COMPANY_FIELDS = {
  [JobTypeEnum.Permanent]: [
    JobFieldType.Type,
    JobFieldType.Category,
    JobFieldType.Salary,
    JobFieldType.StartDate,
    JobFieldType.EndDate,
    JobFieldType.Capacity,
    JobFieldType.Location,
    JobFieldType.Client,
    JobFieldType.Fee,
  ],
  [JobTypeEnum.Freelance]: DEFAULT_COMPANY_FIELDS,
  [JobTypeEnum.Project]: DEFAULT_COMPANY_FIELDS,
};

export const getJobFieldsForTalent = (job: Job) => {
  const allFields = getAllJobFields(job);
  const fieldTypesArr = TALENT_FIELDS[job?.type || JobTypeEnum.Freelance];
  return fieldTypesArr
    .map((field) => allFields[field])
    .filter((i) => !!i.value);
};
export const getJobFieldsForCompany = (job: Job) => {
  const allFields = getAllJobFields(job);
  const fieldTypesArr = COMPANY_FIELDS[job?.type || JobTypeEnum.Freelance];
  return fieldTypesArr
    .map((field) => allFields[field])
    .filter((i) => !!i.value);
};
