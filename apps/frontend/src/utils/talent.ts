interface FormatNameArgs {
  firstName?: string;
  lastName?: string;
  hideFullName?: boolean;
}

export const formatName = ({
  firstName = '',
  lastName = '',
  hideFullName = false,
}: FormatNameArgs) => {
  const lastNameFirstLetter = lastName?.[0] || '';
  const finalLastName = hideFullName ? `${lastNameFirstLetter}...` : lastName;
  const nameArr = [firstName, finalLastName];

  return nameArr.some(Boolean) ? nameArr.filter(Boolean).join(' ') : '';
};
