/* eslint-disable @typescript-eslint/no-explicit-any */

export const moveToIndex = (
  arr: any[],
  prevIndex: number,
  nextIndex: number,
) => {
  const res = arr.slice();

  const movedItem = res.splice(prevIndex, 1);
  res.splice(nextIndex, 0, ...movedItem);

  return res;
};

export const getNextIndex = (currentIndex: number, length: number) =>
  (currentIndex + 1) % length;
export const getPreviousIndex = (currentIndex: number, length: number) =>
  (currentIndex - 1 + length) % length;
