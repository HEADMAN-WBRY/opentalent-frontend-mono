This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Start project

1. Clone env file:

```bash
cp /apps/frontend/.env.local.example /apps/frontend/.env.local
```

2. Start project from root of mono:

```bash
npx nx serve frontend
```
