import { TransitionsOptions } from '@mui/material';

export const TRANSITION_THEME_OPTIONS: TransitionsOptions = {
  duration: {
    short: 0.1,
    standard: 0.3,
  },
};
