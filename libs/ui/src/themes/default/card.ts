export const CARD_THEME_OPTIONS = {
  MuiCardContent: {
    root: {
      '&:last-child': {
        paddingBottom: 16,
      },
    },
  },
};
