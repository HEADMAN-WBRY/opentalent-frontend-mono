import { PALETTE } from './palette';

export const CHECKBOX_THEME_OPTIONS = {
  MuiCheckbox: {
    colorPrimary: {
      color: `${PALETTE.tertiary.main} !important`,
    },
  },
};
