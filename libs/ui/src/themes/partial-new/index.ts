import { createTheme, adaptV4Theme } from '@mui/material/styles';
import { deepmerge } from '@mui/utils';

import { themeParams } from '../default';

const theme = createTheme(
  adaptV4Theme(
    deepmerge(
      // FIXME:
      themeParams as any,
      {
        palette: {
          primary: {
            main: '#F2FF88',
            light: '#FFFF8A',
            dark: '#D1D100',
            contrastText: 'rgba(0, 0, 0, 0.87)',
          },
          secondary: {
            main: '#FFF',
            dark: '#FFF',
            light: '#FFF',
            contrastText: 'rgba(0, 0, 0, 0.87)',
          },
          text: {
            primary: 'rgba(0, 0, 0, 0.87)',
            secondary: 'rgba(0, 0, 0, 0.54)',
            disabled: 'rgba(0, 0, 0, 0.38)',
            hint: 'rgba(0, 0, 0, 0.54)',
          },
        },
        overrides: {
          MuiListItemText: {
            secondary: {
              // color: 'inherit',
            },
            primary: {
              // color: 'inherit',
            },
          },
          MuiDrawer: {
            paperAnchorDockedLeft: {
              borderRight: 'none',
            },
          },
        },
      } as any,
    ),
  ),
);

export default theme;
export type Theme = typeof theme;
