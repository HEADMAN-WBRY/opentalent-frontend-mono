import cn from 'classnames';
import React from 'react';

import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@mui/material';
import { makeStyles } from '@mui/styles';
import { CSSProperties } from '@mui/styles/withStyles';

import Button from '@libs/ui/components/button';

export type DefaultModalProps = React.PropsWithChildren<{
  open: boolean;
  handleClose: VoidFunction;
  title: React.ReactNode;
  actions?: React.ReactNode;
  textAlign?: CSSProperties['textAlign'];
  className?: string;
}>;

const useStyles = makeStyles((theme) => ({
  modal: {
    padding: theme.spacing(10),
    textAlign: ({ textAlign }: DefaultModalProps) => textAlign || 'center',
  },
  root: {},
  title: {
    padding: 0,
  },
  actions: {
    paddingTop: theme.spacing(6),
  },
  noHorizontalPadding: {
    paddingLeft: 0,
    paddingRight: 0,
    overflowY: 'visible',
  },
}));

const DefaultModal = (props: DefaultModalProps) => {
  const { open, handleClose, children, actions, title, className } = props;
  const classes = useStyles(props);

  return (
    <Dialog
      classes={{ root: classes.root, paper: cn(classes.modal, className) }}
      open={open}
      onClose={handleClose}
    >
      <DialogTitle className={classes.title}>{title}</DialogTitle>
      <DialogContent className={classes.noHorizontalPadding}>
        {children}
      </DialogContent>
      <DialogActions
        className={cn(classes.actions, classes.noHorizontalPadding)}
      >
        {actions || (
          <Button
            fullWidth
            variant="contained"
            color="primary"
            autoFocus
            size="large"
            onClick={handleClose}
          >
            ok
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
};

export default DefaultModal;
