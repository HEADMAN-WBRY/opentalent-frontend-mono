import { makeStyles } from '@mui/styles';

const INDEX_SIZE = 48;
const MOBILE_INDEX_SIZE = 32;

const useStyles = makeStyles((theme) => ({
  stepContainer: {
    position: 'relative',
  },
  index: {
    ...theme.typography.body1,
    fontStyle: 'normal',
    width: INDEX_SIZE,
    height: INDEX_SIZE,
    display: 'inline-flex',
    justifyContent: 'center',
    alignItems: 'center',
    background: theme.palette.tertiary.main,
    borderRadius: '100%',
    marginRight: theme.spacing(4),

    [theme.breakpoints.down('sm')]: {
      width: MOBILE_INDEX_SIZE,
      height: MOBILE_INDEX_SIZE,
    },
  },
  title: {},
  content: {
    margin: `16px 0 16px ${INDEX_SIZE / 2}px`,
    padding: '16px 44px',
    borderLeft: `1px solid ${theme.palette.other.light}`,
    background: 'transparent',

    [theme.breakpoints.down('sm')]: {
      margin: `8px ${MOBILE_INDEX_SIZE / 2}px 8px ${MOBILE_INDEX_SIZE / 2}px`,
      padding: `16px 0 16px 0`,
      border: 'none',
    },
  },
}));

export default useStyles;
