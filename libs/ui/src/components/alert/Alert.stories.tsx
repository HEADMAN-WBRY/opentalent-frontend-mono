import { Box } from '@mui/material';
import { useSnackbar } from 'notistack';
import React from 'react';

import SnackbarProvider from '@libs/ui/components/snackbar/SnackbarProvider';

import Button from '../button';
import Alert from './index';

export default {
  title: 'Components/Alert',
  component: Alert,
};

export const Default = () => {
  return (
    <>
      <Box mb={4} width={200}>
        <Alert variant="filled" severity="success" onClose={() => ''}>
          Success
        </Alert>
      </Box>
      <Box mb={4} width={200}>
        <Alert variant="filled" severity="error" onClose={() => ''}>
          Error
        </Alert>
      </Box>
      <Box mb={4} width={200}>
        <Alert icon={false} variant="filled" severity="info" onClose={() => ''}>
          Info
        </Alert>
      </Box>
      <Box mb={4} width={200}>
        <Alert
          icon={false}
          variant="filled"
          severity="info"
          action={<Button color="primary">Undo</Button>}
        >
          Notification
        </Alert>
      </Box>
    </>
  );
};

const SomeSnackbar = () => {
  const { enqueueSnackbar } = useSnackbar();
  const openSnackbar = () => {
    enqueueSnackbar('Error', { persist: true, variant: 'error' });
    enqueueSnackbar('Warning', { persist: true, variant: 'warning' });
    enqueueSnackbar('Info', { persist: true, variant: 'info' });
    enqueueSnackbar('Success', { persist: true, variant: 'success' });
  };
  return (
    <Button variant="contained" onClick={openSnackbar}>
      Open alerts
    </Button>
  );
};

export const SnackbarLogic = () => (
  <SnackbarProvider>
    <SomeSnackbar />
  </SnackbarProvider>
);
