import React from 'react';
import { Link, LinkProps } from 'react-router-dom';

import Typography, { TypographyProps } from './Typography';

interface RouterLinkProps extends TypographyProps {
  to: LinkProps['to'];
  target?: string;
  rel?: string;
}

export const RouterLink = (props: RouterLinkProps) => {
  return (
    <Typography pointer color="tertiary" component={Link as any} {...props} />
  );
};
