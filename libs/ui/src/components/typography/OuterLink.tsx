import { makeStyles } from '@mui/styles';
import cn from 'classnames';
import React from 'react';

import Typography, { TypographyProps } from './Typography';

interface OuterLinkProps extends TypographyProps {
  href: string;
  target?: string;
  rel?: string;
}

const useStyles = makeStyles((theme) => ({
  link: {
    cursor: 'pointer',
  },
}));

export const OuterLink = ({
  href,
  rel,
  target,
  children,
  className,
  ...rest
}: OuterLinkProps) => {
  const classes = useStyles();

  return (
    <Typography
      component="a"
      {...{
        target,
        rel,
        href,
      }}
      color="tertiary"
      className={cn(className, classes.link)}
      {...rest}
    >
      {children}
    </Typography>
  );
};
