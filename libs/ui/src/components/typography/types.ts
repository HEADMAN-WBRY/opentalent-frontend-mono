export type CustomVariants = 'buttonLarge' | 'buttonSmall' | 'captionSmall';
export type CustomColors = 'tertiary' | 'success';
