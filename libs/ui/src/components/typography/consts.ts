import { CustomVariants, CustomColors } from './types';

export const CUSTOM_VARIANTS: CustomVariants[] = [
  'buttonLarge',
  'buttonSmall',
  'captionSmall',
];
export const CUSTOM_COLORS: CustomColors[] = ['tertiary', 'success'];
