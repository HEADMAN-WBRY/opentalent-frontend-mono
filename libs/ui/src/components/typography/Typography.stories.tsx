import Box from '@mui/material/Box';
import React from 'react';

import Typography, { TypographyProps } from './Typography';

export default {
  title: 'Components/Typography',
  component: Typography,
};

const renderBatch = (count: number, prefix: string) =>
  Array(count)
    .fill('')
    .map((_, index) => {
      const i = index + 1;
      return (
        <Box key={i}>
          <Typography variant={`${prefix}${i}` as TypographyProps['variant']}>
            {`${prefix}${i}`}
          </Typography>
        </Box>
      );
    });

export const Titles = () => renderBatch(6, 'h');

export const Subtitles = () => renderBatch(2, 'subtitle');

export const Body = () => renderBatch(2, 'body');

export const Button = () => (
  <>
    <Box>
      <Typography variant="buttonLarge">Button large</Typography>
    </Box>
    <Box>
      <Typography variant="button">Button</Typography>
    </Box>
    <Box>
      <Typography variant="buttonSmall">Button small</Typography>
    </Box>
  </>
);

export const Caption = () => (
  <>
    <Box>
      <Typography variant="caption">Caption</Typography>
    </Box>
    <Box>
      <Typography variant="captionSmall">Caption small</Typography>
    </Box>
  </>
);

export const Overline = () => (
  <Box>
    <Typography variant="overline">Overline</Typography>
  </Box>
);
