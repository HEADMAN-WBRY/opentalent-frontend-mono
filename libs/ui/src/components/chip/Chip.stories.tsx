import { Box, Grid } from '@mui/material';
import { ReactComponent as SubtractIcon } from 'assets/icons/subtract.svg';
import React from 'react';

import Chip, { ChipProps } from './Chip';

export default {
  title: 'Components/Chip',
  component: Chip,
};

const renderChips = (
  color: ChipProps['color'],
  variant: ChipProps['variant'],
  icon: boolean,
) => {
  return (
    <Grid container spacing={3}>
      {['small', 'medium'].map((size) => (
        <Grid key={size} item>
          <Chip
            size={size as ChipProps['size']}
            variant={variant}
            color={color}
            label={size}
            icon={icon ? <SubtractIcon /> : undefined}
          />
        </Grid>
      ))}
      <Grid item>
        <Chip
          disabled
          variant={variant}
          color={color}
          label={`Disabled ${color}`}
        />
      </Grid>
    </Grid>
  );
};

export const FilledChips = () => {
  return (
    <>
      <Box mb={4}>
        {renderChips('default', 'default', true)}
        <br />
        {renderChips('primary', 'default', true)}
        <br />
        {renderChips('secondary', 'default', true)}
      </Box>
    </>
  );
};

export const OutlinedChips = () => {
  return (
    <>
      <Box mb={4}>
        {renderChips('default', 'outlined', false)}
        <br />
        {renderChips('primary', 'outlined', false)}
        <br />
        {renderChips('secondary', 'outlined', false)}
      </Box>
    </>
  );
};
