import { Box } from '@mui/material';
import React, { useState } from 'react';

import { OptionType } from '../select';
import CircleSelect from './CircleSelect';

export default {
  title: 'Form/CircleSelect',
  component: CircleSelect,
};

const mockSelectData = [
  {
    value: 10,
    text: 'Ten',
  },
  {
    value: 20,
    text: 'Twelve',
  },
  {
    value: 30,
    text: 'Thirty',
  },
];

export const MediumSelects = () => {
  const [value, setValue] = useState<OptionType>();
  return (
    <Box mb={4}>
      <CircleSelect
        value={value}
        onChange={setValue}
        options={mockSelectData}
      />
    </Box>
  );
};
