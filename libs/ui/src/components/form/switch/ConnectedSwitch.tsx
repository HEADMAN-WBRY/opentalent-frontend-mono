import { useField } from 'formik';
import React, { useCallback } from 'react';

import Switch, { SwitchProps } from './Switch';

interface ConnectedSwitchProps extends SwitchProps {
  name: string;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

const ConnectedSwitch = (props: ConnectedSwitchProps) => {
  const { name, onChange } = props;
  const [field, meta] = useField(name);

  const error = meta.touched && meta.error;
  const onChangeHandler = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      field.onChange(e);
      onChange?.(e);
    },
    [field, onChange],
  );

  return (
    <Switch
      {...props}
      {...field}
      checked={field.value}
      onChange={onChangeHandler}
      error={error}
    />
  );
};

export default ConnectedSwitch;
