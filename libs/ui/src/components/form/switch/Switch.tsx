import {
  FormControlLabel,
  FormControlLabelProps,
  Switch as MuiSwitch,
  SwitchProps as MuiSwitchProps,
} from '@mui/material';
import React from 'react';

export interface SwitchProps extends MuiSwitchProps {
  formControlProps?: Omit<FormControlLabelProps, 'control'>;
  label: string;
  name?: string;
  error?: string | false;
}

const Switch = (props: SwitchProps) => {
  const { formControlProps, label, ...rest } = props;
  return (
    <FormControlLabel
      control={<MuiSwitch color="primary" {...rest} />}
      label={label}
      {...formControlProps}
    />
  );
};

export default Switch;
