import { makeStyles } from '@mui/styles';
import cn from 'classnames';
import PhoneInput, { PhoneFieldInputProps } from 'material-ui-phone-number';
import React, { useCallback, useState } from 'react';

export const useStyles = makeStyles((theme) => ({
  root: {
    '& svg': {
      maxWidth: '20px',
    },
  },
}));

const PhoneField = ({ onChange, ...rest }: PhoneFieldInputProps) => {
  const [value, setValue] = useState<string>('');
  const classes = useStyles();
  const handleChange = useCallback(
    (phoneValue) => {
      setValue(phoneValue);
      if (onChange) onChange(phoneValue);
    },
    [onChange],
  );

  return (
    <PhoneInput
      className={cn('MuiInput-root', classes.root)}
      defaultCountry="nl"
      value={value}
      onChange={handleChange}
      {...rest}
    />
  );
};

export default PhoneField;
