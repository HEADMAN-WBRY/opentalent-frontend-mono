import { useField } from 'formik';
import { PhoneFieldInputProps } from 'material-ui-phone-number';
import React from 'react';

import PhoneField from './PhoneField';

type ConnectedMaskedTextFieldProps = {
  name: string;
} & PhoneFieldInputProps;

export const ConnectedPhoneField = (props: ConnectedMaskedTextFieldProps) => {
  const { name, helperText, ...rest } = props;
  const [field, meta, fieldHelpers] = useField(name);

  const error = meta.touched && !!meta.error;
  const finalHelperText = meta.touched && meta.error ? meta.error : helperText;

  return (
    <PhoneField
      {...rest}
      {...field}
      onChange={(value: string) => fieldHelpers.setValue(value)}
      error={error}
      helperText={finalHelperText}
    />
  );
};

export default ConnectedPhoneField;
