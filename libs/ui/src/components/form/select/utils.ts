import { OptionsGroup, OptionType } from './types';

export const makeOptionsFromObject = (obj: Record<string, any>): OptionType[] =>
  Object.entries(obj).map(([value, text]) => ({ text, value }));

export const isItGroup = (
  item: OptionType | OptionsGroup,
): item is OptionsGroup => item.options !== undefined;
