import { colors } from '@mui/material';
import { makeStyles } from '@mui/styles';

const CAPTION_PADDING_LEFT = 80;

const useStyles = makeStyles((theme) => ({
  root: {},
  circles: {
    width: 88,
    height: 88,
    position: 'relative',
  },
  circle: {
    position: 'absolute',
    borderRadius: '100%',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    cursor: 'pointer',

    '&::after': {
      content: 'attr(data-title)',
      position: 'absolute',
      top: 0,
      left: '100%',
      paddingLeft: 20,
      borderBottom: '1px solid transparent',
      fontSize: 14,
      lineHeight: '17px',
      color: colors.grey[500],
      whiteSpace: 'nowrap',
      display: 'block',
      fontWeight: 500,
    },
    '&::before': {
      right: -2,
      top: '50%',
      content: '""',
      height: 4,
      width: 4,
      borderRadius: '50%',
      position: 'absolute',
      background: 'black',
      display: 'none',
    },

    '&:nth-child(1)': {
      width: 58,
      height: 58,
      background: 'white',
      zIndex: 3,
      paddingLeft: 53,

      '&::after': {
        top: 9,
        paddingBottom: 4,
        paddingLeft: 22,
      },
      '&::before': {},
    },

    '&:nth-child(2)': {
      width: '100%',
      height: '100%',
      background: theme.palette.primary.main,
      zIndex: 2,

      '&::after': {
        top: 45,
        paddingLeft: 7,
        paddingTop: 4,
        borderTop: '1px solid transparent',
        borderBottom: 'none',
      },
    },

    '&:nth-child(3)': {
      width: '100%',
      height: '100%',
      background: colors.grey[300],
      zIndex: 1,

      '&::after': {
        top: '71%',
        marginLeft: 28,
        paddingLeft: CAPTION_PADDING_LEFT - 28,
      },
    },
  },
  circleActive: {
    'div$circle&': {
      '&:nth-child(1)': {
        background: 'white',
      },
      '&:nth-child(2)': {
        background: '#c4c4c4',
      },
    },

    '&::after': {
      borderColor: `${colors.grey[700]} !important`,
      color: colors.grey[800],
    },
    '&::before': {
      display: 'block',
    },
  },
  labels: {},
}));

export default useStyles;
