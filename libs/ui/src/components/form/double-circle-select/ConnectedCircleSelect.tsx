import { useField } from 'formik';
import React, { useCallback } from 'react';

import { OptionType } from '../select';
import CircleSelect, { CircleSelectProps } from './CircleSelect';

interface ConnectedCircleSelectProps extends CircleSelectProps {
  name: string;
}

const ConnectedCircleSelect = ({
  name,
  onChange,
  ...props
}: ConnectedCircleSelectProps) => {
  const [field, , helpers] = useField(name);
  const handleChange = useCallback(
    (opt: OptionType) => {
      helpers.setValue(opt);
      if (onChange) {
        onChange(opt);
      }
    },
    [helpers, onChange],
  );

  return <CircleSelect {...props} {...field} onChange={handleChange} />;
};

export default ConnectedCircleSelect;
