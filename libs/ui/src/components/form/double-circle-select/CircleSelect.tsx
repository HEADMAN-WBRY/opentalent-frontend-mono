/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events */
import cn from 'classnames';
import React from 'react';

import { OptionType } from '../select';
import useStyles from './styles';

export interface CircleSelectProps {
  options: OptionType[];
  value?: OptionType;
  onChange?: (value: OptionType) => void;
}

const CircleSelect = (props: CircleSelectProps) => {
  const classes = useStyles(props);
  // eslint-disable-next-line no-console
  const { options, value, onChange = console.log } = props;

  return (
    <div className={cn(classes.circles, 'circles')}>
      {options.map((option) => {
        if (!option) {
          return null;
        }
        const isActive = value && value.value === option.value;
        const onClick = (e: React.SyntheticEvent) => {
          e.stopPropagation();
          e.preventDefault();
          onChange(option);
        };
        return (
          <div
            key={option?.value}
            className={cn(classes.circle, {
              [classes.circleActive]: isActive,
              active: isActive,
            })}
            title={option.text}
            data-title={option.text}
            onClick={onClick}
          />
        );
      })}
    </div>
  );
};

export default CircleSelect;
