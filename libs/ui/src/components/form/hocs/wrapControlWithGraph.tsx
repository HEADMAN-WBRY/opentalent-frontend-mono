import { DocumentNode, QueryHookOptions, useLazyQuery } from '@apollo/client';
import { getIn } from 'formik';
import React, { useEffect, useMemo } from 'react';

import { OptionType } from '@libs/ui/components/form/select';

type MapFunction = (item: any) => OptionType;
type DataMapType =
  | {
      text: string;
      value: string;
    }
  | MapFunction;

interface HocProps {
  dataMap?: DataMapType;
  query: DocumentNode;
  queryOptions?: QueryHookOptions;
  dataPath: string;
  value?: any;
}

type DefaultProps = {
  options: OptionType[];
};

const mapOptions = (data: any[], dataMap: DataMapType) => {
  if (dataMap instanceof Function) {
    return data?.map(dataMap);
  }
  return data?.map((item: any) => ({
    value: item?.[dataMap.value],
    text: item?.[dataMap.text],
  }));
};

function wrapControlWithGraph<ComponentProps = DefaultProps>(
  Component: React.FC<any>,
) {
  return ({
    query,
    queryOptions,
    dataPath,
    dataMap = { text: 'name', value: 'id' },
    value,
    ...props
  }: ComponentProps & HocProps) => {
    const [request, { data, loading }] = useLazyQuery(query, queryOptions);
    const options = useMemo(
      () => mapOptions(getIn(data, dataPath, []), dataMap),
      [data, dataPath, dataMap],
    );

    useEffect(() => {
      request();
    }, [request]);

    return (
      <Component
        options={options}
        disabled={loading}
        value={value}
        {...props}
      />
    );
  };
}
export default wrapControlWithGraph;
