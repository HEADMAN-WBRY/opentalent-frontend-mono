import { Box, Grid } from '@mui/material';
import { CheckboxProps } from '@mui/material/Checkbox';
import React from 'react';

import Checkbox from './Checkbox';

export default {
  title: 'Components/Checkbox',
  component: Checkbox,
};

const renderCheckboxes = (
  color: CheckboxProps['color'],
  checked: CheckboxProps['checked'],
  onChange: CheckboxProps['onChange'],
  label?: string,
) => {
  return (
    <Grid container spacing={3}>
      <Grid item>
        <Checkbox {...{ checked, onChange, color }} label={label} />
      </Grid>
      <Grid item>
        <Checkbox
          {...{ checked, onChange, color, disabled: true }}
          label={label}
        />
      </Grid>
      <Grid item>
        <Checkbox
          {...{ checked, onChange, color, indeterminate: true }}
          label={label}
        />
      </Grid>
      <Grid item>
        <Checkbox
          {...{
            checked,
            onChange,
            color,
            indeterminate: true,
            disabled: true,
          }}
          label={label}
        />
      </Grid>
    </Grid>
  );
};

export const CheckboxWOLabel = () => {
  const [checked, setChecked] = React.useState(true);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setChecked(event.target.checked);
  };

  return (
    <>
      <Box mb={4}>{renderCheckboxes('default', checked, handleChange)}</Box>
      <Box mb={4}>{renderCheckboxes('primary', checked, handleChange)}</Box>
      <Box mb={4}>{renderCheckboxes('secondary', checked, handleChange)}</Box>
    </>
  );
};

export const CheckboxWLabel = () => {
  const [checked, setChecked] = React.useState(true);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setChecked(event.target.checked);
  };

  return (
    <>
      <Box mb={4}>
        {renderCheckboxes('default', checked, handleChange, 'Label')}
      </Box>
      <Box mb={4}>
        {renderCheckboxes('primary', checked, handleChange, 'Label')}
      </Box>
      <Box mb={4}>
        {renderCheckboxes('secondary', checked, handleChange, 'Label')}
      </Box>
    </>
  );
};
