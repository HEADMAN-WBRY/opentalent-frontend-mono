import React from 'react';
import InputMask, { Props as MaskedInputProps } from 'react-input-mask';

import TextField, { TextFieldProps } from '@libs/ui/components/form/text-field';

export type MaskedTextFieldProps = TextFieldProps & {
  maskedProps: MaskedInputProps;
};

const MaskedTextField = (props: MaskedTextFieldProps) => {
  const { maskedProps, ...textFieldProps } = props;
  return (
    <InputMask {...maskedProps}>
      <TextField {...textFieldProps} />
    </InputMask>
  );
};

export default MaskedTextField;
