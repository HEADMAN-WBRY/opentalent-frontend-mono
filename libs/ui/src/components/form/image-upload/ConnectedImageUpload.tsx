import { useField } from 'formik';
import React, { useCallback } from 'react';

import ImageUpload, { ImageUploadProps } from './ImageUpload';

interface ConnectedImageUploadProps extends ImageUploadProps {
  name: string;
}

const ConnectedImageUpload = (props: ConnectedImageUploadProps) => {
  const { name, helperText } = props;
  const [, meta, helperProps] = useField<HTMLInputElement>(name);
  const onChange = useCallback(
    (files) => {
      helperProps.setValue(files);
      props?.onChange?.(files);
    },
    [helperProps, props],
  );
  const error = meta.touched && !!meta.error;
  const finalHelperText = meta.touched && meta.error ? meta.error : helperText;

  return (
    <ImageUpload
      helperText={finalHelperText}
      error={error}
      {...props}
      onChange={onChange}
    />
  );
};

export default ConnectedImageUpload;
