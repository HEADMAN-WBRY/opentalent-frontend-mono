import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  container: {},
  delimer: {
    padding: `0 ${theme.spacing(2)}`,
  },
}));

export default useStyles;
