import { DateRange } from '@mui/lab';
import { Box, Grid } from '@mui/material';
import validationErrors from 'consts/validationErrors';
import { Formik } from 'formik';
import React from 'react';
import { noop } from 'utils/common';
import * as yup from 'yup';

import Button from '@libs/ui/components/button';

import ConnectedDatePicker from './ConnectedDatePicker';
import ConnectedDateRangePicker from './ConnectedDateRangePicker';
import DatePicker from './DatePicker';
import DateRangePicker from './DateRangePicker';

export default {
  title: 'Form/Datepicker',
  component: DateRangePicker,
};

export const SimpleDatePicker = () => {
  const [value, setValue] = React.useState<Date | null>(null);

  return (
    <Box padding="20px" bgcolor="white" mb={2}>
      <DatePicker
        onChange={((newValue: Date) => setValue(newValue)) as any}
        value={value}
        TextFieldProps={{ helperText: null }}
      />
    </Box>
  );
};

export const RangePicker = () => {
  const [value, setValue] = React.useState<DateRange<Date>>([null, null]);

  return (
    <Box padding="20px" bgcolor="white" mb={2}>
      <DateRangePicker
        onChange={(newValue) => setValue(newValue as DateRange<Date>)}
        value={value}
      />
    </Box>
  );
};

const validation = yup.object().shape({
  date: yup.date().nullable().required(validationErrors.required),
});

export const BasicConnectedDatePicker = () => {
  return (
    <Formik
      onSubmit={noop}
      validationSchema={validation}
      initialValues={{ date: null }}
    >
      {({ handleSubmit }) => {
        return (
          <Grid direction="column" container>
            <Grid item>
              <ConnectedDatePicker
                label="Connected"
                name="date"
                helperText="Date"
              />
            </Grid>
            <br />
            <Grid item>
              <Button variant="contained" onClick={handleSubmit}>
                Submit
              </Button>
            </Grid>
          </Grid>
        );
      }}
    </Formik>
  );
};

const validationDateRange = yup.object().shape({
  date: yup
    .array()
    .of(yup.date().nullable().required(validationErrors.required)),
});

export const BasicConnectedDateRangePicker = () => {
  return (
    <Formik
      onSubmit={noop}
      validationSchema={validationDateRange}
      initialValues={{ date: [null, null] }}
    >
      {({ handleSubmit }) => {
        return (
          <Grid direction="column" container>
            <Grid item>
              <ConnectedDateRangePicker
                label="Connected"
                name="date"
                FirstTextFieldProps={{ helperText: 'Date1' }}
                SecondTextFieldProps={{ helperText: 'Date2' }}
              />
            </Grid>
            <br />
            <Grid item>
              <Button variant="contained" onClick={handleSubmit}>
                Submit
              </Button>
            </Grid>
          </Grid>
        );
      }}
    </Formik>
  );
};
