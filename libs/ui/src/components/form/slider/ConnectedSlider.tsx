import { useField } from 'formik';
import React, { useCallback } from 'react';

import Typography from '@libs/ui/components/typography/Typography';

import Slider, { SliderProps } from './Slider';

interface ConnectedDatePickerProps
  extends Omit<SliderProps, 'value' | 'onChange'> {
  name: string;
  helperText?: string;
  visibleMin?: number;
}

const ConnectedSlider = (props: ConnectedDatePickerProps) => {
  const { name, helperText, visibleMin, ...rest } = props;
  const [field, , helpers] = useField({ name });

  const changed = useCallback(
    (event: any, newValue: number | number[]) => {
      if (Array.isArray(field.value)) {
        helpers.setValue(newValue, true);
      } else {
        if (!visibleMin || (visibleMin && newValue >= visibleMin)) {
          helpers.setValue(String(newValue), true);
        }
      }
    },
    [field.value, helpers, visibleMin],
  );

  return (
    <>
      <Slider
        value={field.value}
        valueLabelDisplay="on"
        onChange={changed}
        {...rest}
      />
      {props.helperText && (
        <Typography variant="subtitle1">{props.helperText}</Typography>
      )}
    </>
  );
};

export default ConnectedSlider;
