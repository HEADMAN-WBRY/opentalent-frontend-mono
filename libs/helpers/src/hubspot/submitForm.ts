interface FormValue {
  name: string;
  value: string | number;
}

export const submitHubspotForm = async ({
  fields,
  formInfo,
}: {
  fields: FormValue[];
  formInfo: {
    portalId: string;
    formId: string;
  };
}) => {
  const mappedFields = fields.map((params) => ({
    ...params,
    objectTypeId: '0-1',
  }));

  const response = await fetch(
    `https://api.hsforms.com/submissions/v3/integration/submit/${formInfo.portalId}/${formInfo.formId}`,
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        fields: mappedFields,
      }),
    },
  );
  return await response.json();
};
