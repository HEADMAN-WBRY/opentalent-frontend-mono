export const formatCurrency = (num: number) =>
  new Intl.NumberFormat('de-DE', {
    style: 'currency',
    currency: 'EUR',
    maximumSignificantDigits: 10,
  }).format(num);
