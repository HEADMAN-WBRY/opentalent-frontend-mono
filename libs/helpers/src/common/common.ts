export const noop = (...params: any) => undefined;

export const sleep = (seconds: number) =>
  new Promise((res) => setTimeout(res, seconds));

export const stopEvent = (e: React.UIEvent) => {
  e.stopPropagation();
  e.preventDefault();
};

export const uniq = (arr: any[]) => [...new Set(arr)];

export const isNumber = (num: any): num is number =>
  num !== '' && !Number.isNaN(+num) && Number.isFinite(+num) && !isNil(num);

export const parseNumber = (num: any): number | undefined =>
  isNumber(num) ? +num : undefined;

const nilTypes = [undefined, null];
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const isNil = (arg: any): arg is typeof nilTypes[number] =>
  nilTypes.includes(arg);
